INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_04_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Composition</property>
<property key="it">Composizione</property>
</properties>

','cittametro_internalpagegallery',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('notfound','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Page not found</property>
<property key="it">Pagina non trovata</property>
</properties>','single_frame_page',1,NULL,'2017-02-17 16:37:10');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('errorpage','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Error page</property>
<property key="it">Pagina di errore</property>
</properties>','single_frame_page',1,NULL,'2017-02-17 21:11:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('documenti','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Documents and data</property>
<property key="it">Documenti e dati</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-10 15:37:08');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_07','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Municipalities</property>
<property key="it">Comuni</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-19 11:15:25');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('search_result','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Search Result</property>
<property key="it">Risultati della Ricerca</property>
</properties>','1-column',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <mimeType>text/html</mimeType>
</config>','2020-06-08 08:43:13');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('homepage','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Homepage</property>
<property key="it">Homepage</property>
</properties>

','cittametro_homepage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-26 08:30:16');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_04_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Convocations</property>
<property key="it">Convocazioni</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Administration areas</property>
<property key="it">Aree amministrative</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:51');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('novita','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">News</property>
<property key="it">Novita</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('nov_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Press releases</property>
<property key="it">Comunicati stampa</property>
</properties>

','cittametro_internalpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_06','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Agreements between publics and privates</property>
<property key="it">Accordi tra enti e privati</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Municipal notice board</property>
<property key="it">Documenti albo pretorio</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Legislation</property>
<property key="it">Atti normativi</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_07_dett','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Municipality</property>
<property key="it">Comune</property>
</properties>

','cittametro_internalpagedetail',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <mimeType>text/html</mimeType>
  <useextradescriptions>false</useextradescriptions>
  <descriptions>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </descriptions>
  <keywords>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </keywords>
  <friendlycode>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </friendlycode>
  <complexParameters />
</config>

','2021-11-19 15:10:24');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_05','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Administrative staff</property>
<property key="it">Personale amministrativo</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:51');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('nov_04','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Events</property>
<property key="it">Eventi</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('nov_04_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cultural events</property>
<property key="it">Eventi culturali</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('nov_04_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Social events</property>
<property key="it">Eventi sociali</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('nov_04_05','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Sports events</property>
<property key="it">Eventi sportivi</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ser_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Environment</property>
<property key="it">Ambiente</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ser_04','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Education and formation</property>
<property key="it">Educazione e formazione</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ser_05','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Mobility and transportation</property>
<property key="it">Mobilita e trasporti</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ser_05_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Issuance of permissions and authorizations for on-road sports competitions</property>
<property key="it">Rilascio nullaosta e autorizzazioni per competizioni sportive su strada</property>
</properties>

','cittametro_internalpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('focus','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Focus</property>
<property key="it">Focus</property>
</properties>

','cittametro_focuspage',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <extragroups>
    <group name="free" />
  </extragroups>
  <charset>utf-8</charset>
  <mimeType>text/html</mimeType>
  <useextradescriptions>false</useextradescriptions>
  <descriptions>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </descriptions>
  <keywords>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </keywords>
  <friendlycode>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </friendlycode>
  <complexParameters>
    <lang code="en" />
    <lang code="it" />
  </complexParameters>
</config>

','2021-11-26 11:09:58');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ser_05_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Issuance of permissions and authorizations for special transportations</property>
<property key="it">Rilascio nullaosta e autorizzazioni per trasporti eccezionali</property>
</properties>

','cittametro_internalpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_5','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Technical and support documents</property>
<property key="it">Documenti tecnici e di supporto</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_02_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Statute</property>
<property key="it">Statuto</property>
</properties>

','cittametro_internalpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('login','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Login page</property>
<property key="it">Pagina di login</property>
</properties>

','cittametro_internalpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('cerca','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Search results</property>
<property key="it">Risultato della ricerca</property>
</properties>

','cittametro_internalpage',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:55');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_07_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Territory</property>
<property key="it">Territorio</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-19 11:11:38');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_03','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Notices</property>
<property key="it">Bandi</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_07','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Decrees and ordinances</property>
<property key="it">Decreti e ordinanze</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_02_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Regulations</property>
<property key="it">Regolamenti</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('service','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Service pages</property>
<property key="it">Pagine di servizio</property>
</properties>

','service',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('note_legali','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Legal notes</property>
<property key="it">Note legali</property>
</properties>

','cittametro_internalpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('privacy','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Privacy policy</property>
<property key="it">Privacy policy</property>
</properties>

','cittametro_internalpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:54');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ap','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Personal area</property>
<property key="it">Area personale</property>
</properties>

','cittametro_reservedpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:55');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_08','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Planning and accountability documents</property>
<property key="it">Documenti di programmazione e rendicontazione</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <extragroups>
    <group name="free" />
  </extragroups>
  <charset>utf-8</charset>
  <mimeType>text/html</mimeType>
  <useextradescriptions>false</useextradescriptions>
  <descriptions>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </descriptions>
  <keywords>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </keywords>
  <friendlycode>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </friendlycode>
  <complexParameters>
    <lang code="en" />
    <lang code="it" />
  </complexParameters>
</config>

','2021-11-10 16:05:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('evento','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Event</property>
<property key="it">Evento</property>
</properties>

','cittametro_internalpagedetail',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-08 08:41:17');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('arg_01_dett','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Argument</property>
<property key="it">Argomento</property>
</properties>

','cittametro_argumentpage',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-26 11:25:51');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amministrazione','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Administration</property>
<property key="it">Amministrazione</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-11 09:42:09');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('documento','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Document</property>
<property key="it">Documento</property>
</properties>

','cittametro_internalpagedetail',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <extragroups>
    <group name="free" />
  </extragroups>
  <charset>utf-8</charset>
  <mimeType>text/html</mimeType>
  <useextradescriptions>false</useextradescriptions>
  <descriptions>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </descriptions>
  <keywords>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </keywords>
  <friendlycode>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </friendlycode>
  <complexParameters>
    <lang code="en" />
    <lang code="it" />
  </complexParameters>
</config>

','2021-11-09 09:15:10');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('doc_04','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Forms</property>
<property key="it">Modulistica</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-11 09:11:48');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_04','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Politicians</property>
<property key="it">Politici</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-11 09:48:44');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('servizio','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Service</property>
<property key="it">Servizio</property>
</properties>

','cittametro_internalpagedetail',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <extragroups>
    <group name="free" />
  </extragroups>
  <charset>utf-8</charset>
  <mimeType>text/html</mimeType>
  <useextradescriptions>false</useextradescriptions>
  <descriptions>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </descriptions>
  <keywords>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </keywords>
  <friendlycode>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </friendlycode>
  <complexParameters>
    <lang code="en" />
    <lang code="it" />
  </complexParameters>
</config>

','2021-11-09 09:16:01');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('altresezioni','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Other sections</property>
<property key="it">Altre sezioni</property>
</properties>

','cittametro_sectionpage',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <mimeType>text/html</mimeType>
  <useextradescriptions>false</useextradescriptions>
  <descriptions>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </descriptions>
  <keywords>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </keywords>
  <friendlycode>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </friendlycode>
  <complexParameters />
</config>

','2021-11-05 11:18:36');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('organizzazione','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Organization detail</property>
<property key="it">Dettaglio organizzazione</property>
</properties>

','cittametro_internalpagedetail',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-09 15:11:06');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ilpatto_sviluppo','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Patto per lo sviluppo</property>
<property key="it">Patto per lo sviluppo</property>
</properties>

','ilpatto_template',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <extragroups>
    <group name="free" />
  </extragroups>
  <charset>utf-8</charset>
  <mimeType>text/html</mimeType>
  <useextradescriptions>false</useextradescriptions>
  <descriptions>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </descriptions>
  <keywords>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </keywords>
  <friendlycode>
    <property key="en" useDefaultLang="false" />
    <property key="it" useDefaultLang="false" />
  </friendlycode>
  <complexParameters>
    <lang code="en" />
    <lang code="it" />
  </complexParameters>
</config>

','2021-11-11 11:12:16');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ilpatto_gestisci_aree','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Area Management</property>
<property key="it">Gestione Aree</property>
</properties>

','ilpatto_template',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
</config>

','2021-11-11 11:12:59');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ilpatto_gestisci_settori','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Sector Management</property>
<property key="it">Gestione Settori</property>
</properties>

','ilpatto_template',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
</config>

','2021-11-11 11:13:06');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ilpatto_gestisci_progetti','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Project management</property>
<property key="it">Gestione Progetti</property>
</properties>

','ilpatto_template',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
</config>

','2021-11-11 11:13:15');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('persona','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Person detail</property>
<property key="it">Dettaglio persona</property>
</properties>

','cittametro_internalpagedetail',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-09 15:11:19');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('servizi','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Services</property>
<property key="it">Servizi</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-10 15:04:05');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ser_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Licences and concessions</property>
<property key="it">Autorizzazioni e concessioni</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('nov_03','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Notices</property>
<property key="it">Avvisi</property>
</properties>

','cittametro_internalpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('notizia','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Update</property>
<property key="it">Notizia</property>
</properties>

','cittametro_internalpagedetail',0,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('nov_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Information</property>
<property key="it">Notizie</property>
</properties>

','cittametro_internalpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('nov_04_04','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Business and commercial events</property>
<property key="it">Eventi di affari o commerciali</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('nov_04_03','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Political events</property>
<property key="it">Eventi politici</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:52');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ser_03','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Culture and free time</property>
<property key="it">Cultura e tempo libero</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:53');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('argomenti','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Arguments</property>
<property key="it">Argomenti</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:55');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_03','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Offices</property>
<property key="it">Uffici</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-11 15:27:10');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_07_09','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Monuments</property>
<property key="it">Monumenti</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-10-27 15:02:44');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Metropolitan mayor</property>
<property key="it">Sindaco metropolitano</property>
</properties>

','cittametro_internalpagegallery',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:49');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Metropolitan deputy mayor</property>
<property key="it">Vicesindaco metropolitano</property>
</properties>

','cittametro_internalpagegallery',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:49');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_03','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Metropolitan council</property>
<property key="it">Consiglio metropolitano</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:49');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_03_06','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Documents and proposals</property>
<property key="it">Atti e proposte</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_03_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Composition</property>
<property key="it">Composizione</property>
</properties>

','cittametro_internalpagegallery',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_03_05','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Memoranda</property>
<property key="it">Verbali</property>
</properties>

','cittametro_internalpagesection2',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_04_04','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Documents and proposals</property>
<property key="it">Atti e proposte</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Governing bodies</property>
<property key="it">Organi di governo</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:49');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_04','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Metropolitan conference</property>
<property key="it">Conferenza metropolitana</property>
</properties>

','cittametro_sectionpage',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_03_03','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Commissions</property>
<property key="it">Commissioni</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_03_04','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Convocations</property>
<property key="it">Convocazioni</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_03_02','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Groups</property>
<property key="it">Gruppi</property>
</properties>

','cittametro_internalpagesection',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('amm_01_04_03','free','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Memoranda</property>
<property key="it">Verbali</property>
</properties>

','cittametro_internalpagesection2',1,'<?xml version="1.0" encoding="UTF-8"?>
<config>
  <useextratitles>false</useextratitles>
  <charset>utf-8</charset>
  <useextradescriptions>false</useextradescriptions>
</config>

','2021-11-05 10:06:50');
