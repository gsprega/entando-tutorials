INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',3,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">100002</property>
<property key="contentId">NVT1316</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',5,'cagliari_widget_calendario_homepage','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">210006</property>
<property key="maxElemForItem">10</property>
<property key="filters">(attributeFilter=false;key=created;order=DESC)</property>
<property key="title_it">Eventi</property>
<property key="title_en">Eventi</property>
<property key="contentType">EVN</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',6,'cittametro_widget_argomenti_homepage','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=400003, contentId=ARG3563}, {modelId=400003, contentId=ARG3030}, {modelId=400003, contentId=ARG3566}, {modelId=400001, contentId=ARG2892}, {modelId=400001, contentId=ARG3563}, {modelId=400001, contentId=ARG3566}, {modelId=400001, contentId=ARG3032}]</property>
<property key="title_it">Argomenti in evidenza</property>
<property key="title_en">Argomenti in evidenza</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',7,'cagliari_widget_lista_multicontent','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="maxElemForItem">3</property>
<property key="layout">3</property>
<property key="maxElements">3</property>
<property key="title_it">Focus</property>
<property key="title_en">Focus</property>
<property key="contentTypes">[{modelId=500002, modelId2=500002, contentType=FCS}]</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',13,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('search_result',0,'logo',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('search_result',1,'navigation-menu','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(my_homepage) + code(my_homepage).children</property>
</properties>');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('search_result',2,'search_form',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('search_result',3,'keycloak-login',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('search_result',4,'search_result',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('search_result',8,'navigation-menu','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(sitemap)</property>
</properties>');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',8,'cittametro_widget_inevidenza','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=170001, contentId=STM3069}, {modelId=170001, contentId=STM3071}]</property>
<property key="title_it">Focus</property>
<property key="title_en">Focus</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',4,'cittametro_widget_inevidenza_homepage','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=100009, contentId=NVT2978}, {modelId=100009, contentId=NVT3010}, {modelId=100009, contentId=NVT3607}]</property>
<property key="maxElemForItem">3</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('homepage',9,'cittametro_widget_cerca_servizi_home',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_08',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_08',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_08',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN3863</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_08',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_08',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_08',5,'cittametro_widget_inevidenza','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=200009, contentId=TGN3882}]</property>
<property key="title_it">In evidenza</property>
<property key="title_en">In evidenza</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_08',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_01',2,'cittametro_mappa_territorio',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documento',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documento',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documento',2,'content_viewer',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizio',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizio',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizio',2,'content_viewer',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_01',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN2954</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amministrazione',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amministrazione',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amministrazione',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN2924</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amministrazione',6,'cittametro_widget_inevidenza','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=200009, contentId=TGN2954}, {modelId=200009, contentId=TGN2949}, {modelId=200009, contentId=TGN2956}, {modelId=200009, contentId=TGN2952}]</property>
<property key="title_it">Tutta l''Amministrazione</property>
<property key="title_en">Tutta l''Amministrazione</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amministrazione',10,'cagliari_widget_argomenti_consigliati','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">400001</property>
<property key="maxElements">7</property>
<property key="title_it">Argomenti</property>
<property key="title_en">Argomenti</property>
<property key="contentType">ARG</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amministrazione',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amministrazione',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amministrazione',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amministrazione',5,'cittametro_widget_inevidenza','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=120002, contentId=LGO2910}, {modelId=220015, contentId=ORG2939}, {modelId=230004, contentId=PRS3914}]</property>
<property key="title_it">In evidenza</property>
<property key="title_en">In evidenza</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('focus',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('focus',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('focus',10,'content_viewer',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_dett',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_dett',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_dett',2,'content_viewer',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_dett',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_04',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_04',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_04',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_04',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_04',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_04',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN3855</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_04',5,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="userFilters">(attributeFilter=true;key=titolo)+(attributeFilter=true;key=argomenti)+(attributeFilter=true;key=data_pubb)+(attributeFilter=true;key=area)</property>
<property key="modelId">700014</property>
<property key="maxElemForItem">15</property>
<property key="filters">(attributeFilter=true;key=titolo;order=ASC)</property>
<property key="layout">1</property>
<property key="title_it">Elenco della modulistica</property>
<property key="title_en">Elenco della modulistica</property>
<property key="contentType">DOC</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('organizzazione',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('organizzazione',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('organizzazione',2,'content_viewer',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('organizzazione',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_02',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_06',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_06',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_06',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_01',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_01',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_05',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_05',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_05',8,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_04',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_04',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_04',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_02',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_01',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_01',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizi',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizi',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizi',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizi',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizi',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('altresezioni',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('altresezioni',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('altresezioni',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('altresezioni',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_04',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_02',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_03',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_03',8,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04_03',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',11,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',10,'content_viewer',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',6,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="userFilters">(attributeFilter=true;key=data)+(attributeFilter=true;key=tipologia)</property>
<property key="modelId">100008</property>
<property key="maxElemForItem">4</property>
<property key="filters">(attributeFilter=false;key=created;order=DESC)</property>
<property key="layout">4</property>
<property key="modelId2">100007</property>
<property key="title_it">Novità</property>
<property key="title_en">Novità</property>
<property key="contentType">NVT</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_05',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_05',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_05',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_05',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_05',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_01',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_02',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_05',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_05',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_05',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('altresezioni',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('persona',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('persona',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('persona',2,'content_viewer',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('persona',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_02',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_02',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_02',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_04',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN3928</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_04',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_04',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_04',5,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="userFilters">(attributeFilter=true;key=titolo)+(attributeFilter=true;key=sezione)</property>
<property key="modelId">230013</property>
<property key="maxElemForItem">9</property>
<property key="filters">(attributeFilter=true;value=PP;key=tipologia)+(attributeFilter=true;key=cognome;order=ASC)+(attributeFilter=true;key=nome;order=ASC)+(attributeFilter=true;nullValue=true;key=data_fine)</property>
<property key="layout">1</property>
<property key="title_it">Elenco dei Politici</property>
<property key="title_en">Elenco dei Politici</property>
<property key="contentType">PRS</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('novita',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN2933</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('novita',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('novita',5,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">100014</property>
<property key="maxElemForItem">4</property>
<property key="layout">4</property>
<property key="modelId2">100007</property>
<property key="maxElements">4</property>
<property key="title_it">In evidenza</property>
<property key="title_en">In evidenza</property>
<property key="categories">evd_nvt</property>
<property key="contentType">NVT</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('novita',6,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">100006</property>
<property key="maxElemForItem">3</property>
<property key="filters">(attributeFilter=false;key=created)</property>
<property key="layout">2</property>
<property key="linkDescr_it">Vedi tutte</property>
<property key="linkDescr_en">Vedi tutte</property>
<property key="title_it">Notizie</property>
<property key="title_en">Notizie</property>
<property key="contentType">NVT</property>
<property key="pageLink">nov_02</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('novita',10,'cagliari_widget_argomenti_consigliati','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">400001</property>
<property key="maxElements">7</property>
<property key="title_it">Argomenti</property>
<property key="title_en">Argomenti</property>
<property key="contentType">ARG</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('novita',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('novita',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('novita',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('novita',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_02',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_06',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_06',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_06',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_06',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizi',5,'cittametro_widget_inevidenza','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=800007, contentId=SRV3699}, {modelId=800007, contentId=SRV3805}, {modelId=800007, contentId=SRV3807}, {modelId=800007, contentId=SRV3809}, {modelId=800007, contentId=SRV3811}, {modelId=800007, contentId=SRV3813}, {modelId=800007, contentId=SRV3815}, {modelId=800007, contentId=SRV3818}, {modelId=800007, contentId=SRV3820}]</property>
<property key="title_it">Servizi digitalizzati</property>
<property key="title_en">Servizi digitalizzati</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizi',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN2927</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',7,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="userFilters">(attributeFilter=true;key=tipologia)</property>
<property key="modelId">700005</property>
<property key="maxElemForItem">3</property>
<property key="filters">(attributeFilter=true;key=data_pubb;order=DESC)</property>
<property key="layout">3</property>
<property key="title_it">Documenti</property>
<property key="title_en">Documenti</property>
<property key="contentType">DOC</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',2,'cagliari_widget_argomento_dettaglio',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',3,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">100003</property>
<property key="maxElemForItem">5</property>
<property key="filters">(attributeFilter=false;key=created;order=DESC)</property>
<property key="layout">5</property>
<property key="modelId2">100007</property>
<property key="title_it">In evidenza</property>
<property key="title_en">In evidenza</property>
<property key="categories">arg_argomenti</property>
<property key="contentType">NVT</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',5,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="userFilters">(attributeFilter=true;key=tipologia)</property>
<property key="modelId">800004</property>
<property key="maxElemForItem">3</property>
<property key="filters">(attributeFilter=true;key=titolo;order=ASC)</property>
<property key="layout">3</property>
<property key="title_it">Servizi</property>
<property key="title_en">Servizi</property>
<property key="contentType">SRV</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_01',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_01',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_01',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_04',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_04',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('arg_01_dett',4,'cagliari_widget_lista_multicontent','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="maxElemForItem">3</property>
<property key="filters">(attributeFilter=false;key=jacms:title;order=ASC)</property>
<property key="layout">3</property>
<property key="title_it">Amministrazione</property>
<property key="title_en">Amministrazione</property>
<property key="contentTypes">[{userFilters=(attributeFilter=false;key=contentType), modelId=120013, modelId2=120013, contentType=LGO}, {userFilters=(attributeFilter=true;value=amm_03;key=tipologia)+(attributeFilter=true;value=amm_02;key=tipologia), modelId=220008, modelId2=220008, contentType=ORG}]</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizi',6,'cittametro_widget_inevidenza','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=200009, contentId=TGN3698}, {modelId=200009, contentId=TGN3832}, {modelId=200009, contentId=TGN3834}, {modelId=200009, contentId=TGN3836}, {modelId=200009, contentId=TGN3838}]</property>
<property key="title_it">Tutti i servizi</property>
<property key="title_en">Tutti i servizi</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('servizi',10,'cagliari_widget_argomenti_consigliati','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">400001</property>
<property key="filters">(attributeFilter=false;key=created)</property>
<property key="title_it">Argomenti</property>
<property key="title_en">Argomenti</property>
<property key="categories">arg_argomenti</property>
<property key="contentType">ARG</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documenti',5,'cittametro_widget_inevidenza','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=200009, contentId=TGN3845}, {modelId=200009, contentId=TGN3847}, {modelId=200009, contentId=TGN3849}, {modelId=200009, contentId=TGN3855}, {modelId=200009, contentId=TGN3857}, {modelId=200009, contentId=TGN3863}]</property>
<property key="title_it">Tutti i documenti</property>
<property key="title_en">Tutti i documenti</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documenti',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_04',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_04',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_04',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_04',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_04',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05_01',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05_02',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_05_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_5',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_5',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_5',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_5',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_5',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02_01',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('login',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documenti',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documenti',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documenti',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documenti',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('documenti',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN2930</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_06',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_01',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_01',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_01',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_03',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_03',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_03',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_03',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_03',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_07',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_07',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('evento',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('evento',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('evento',2,'content_viewer',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('evento',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_07',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_07',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_07',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02_02',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02_02',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('doc_02_02',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('note_legali',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('note_legali',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('note_legali',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('privacy',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('privacy',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('privacy',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ap',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ap',10,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('login',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('login',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('cerca',2,'cagliari_widget_advsearch_results','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contentTypesFilter">ARG,DOC,EVN,LGO,NVT,ORG,PRS,SRV,TGN,CNV</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('cerca',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(homepage) + code(novita) + code(servizi) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('cerca',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('cerca',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_settori',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_settori',6,'ilpatto_gestione_settori',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_settori',2,'cittametro_pulsanti_gestione','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(ilpatto_sviluppo) + code(ilpatto_gestisci_aree) + code(ilpatto_gestisci_settori) + code(ilpatto_gestisci_progetti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_settori',9,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_progetti',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('argomenti',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_progetti',2,'cittametro_pulsanti_gestione','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(ilpatto_sviluppo) + code(ilpatto_gestisci_aree) + code(ilpatto_gestisci_settori) + code(ilpatto_gestisci_progetti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_progetti',9,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_progetti',6,'ilpatto_gestione_progetti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_03',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_03',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_03',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_sviluppo',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_sviluppo',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_sviluppo',2,'cittametro_pulsanti_gestione','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(ilpatto_sviluppo) + code(ilpatto_gestisci_aree) + code(ilpatto_gestisci_settori) + code(ilpatto_gestisci_progetti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_sviluppo',3,'ilpatto_stats_stato',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_sviluppo',4,'ilpatto_stats_aree',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ap',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_sviluppo',5,'ilpatto_mappa_comuni',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_sviluppo',9,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_aree',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_aree',2,'cittametro_pulsanti_gestione','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(ilpatto_sviluppo) + code(ilpatto_gestisci_aree) + code(ilpatto_gestisci_settori) + code(ilpatto_gestisci_progetti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_aree',6,'ilpatto_gestione_aree',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ilpatto_gestisci_aree',9,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_03',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_03',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_03',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('notizia',2,'content_viewer',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('notizia',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('notizia',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('notizia',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_01',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_04',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_04',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_04',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_03',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_03',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('nov_04_03',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_02',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_02',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_02',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_03',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_03',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_03',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_03',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('ser_03',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('argomenti',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('argomenti',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('argomenti',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('argomenti',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN3020</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('argomenti',5,'cagliari_widget_argomenti_inevidenza_img',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('argomenti',6,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">400005</property>
<property key="maxElemForItem">9</property>
<property key="layout">2</property>
<property key="title_it">Tutti gli Argomenti</property>
<property key="title_en">Tutti gli Argomenti</property>
<property key="contentType">ARG</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('argomenti',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_03',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_03',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_03',2,'content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">200002</property>
<property key="contentId">TGN2956</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_03',5,'cittametro_widget_inevidenza','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="contents">[{modelId=220005, contentId=ORG3700}]</property>
<property key="maxElemForItem">6</property>
<property key="title_it">In evidenza</property>
<property key="title_en">In evidenza</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_03',6,'cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="modelId">220021</property>
<property key="maxElemForItem">15</property>
<property key="filters">(attributeFilter=true;value=amm_03;key=tipologia)+(attributeFilter=true;key=titolo)</property>
<property key="layout">2</property>
<property key="title_it">Tutti gli uffici</property>
<property key="title_en">Tutti gli uffici</property>
<property key="contentType">ORG</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_09',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_09',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_07_09',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04',3,'cittametro_cerca_contenuti',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti) + code(ilpatto_sviluppo)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04',4,'cittametro_navigation_child',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_04',12,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_03',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_03',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_03',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_04',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_03_04',6,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_01',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_01',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_01',5,'cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1) + code(novita).subtree(1) + code(documenti).subtree(1)</property>
</properties>

');
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_02',1,'cittametro_widget_menu_link_utili',NULL);
INSERT INTO widgetconfig (pagecode,framepos,widgetcode,config) VALUES ('amm_01_02',0,'cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="navSpec">code(amministrazione) + code(servizi) + code(novita) + code(documenti)</property>
</properties>

');
