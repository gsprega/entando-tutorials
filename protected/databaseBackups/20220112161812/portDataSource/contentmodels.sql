INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400009,'ARG','In evidenza foto h144 (img+titolo)','
<article class="scheda-app h144">
	<div class="scheda-arg-img">
    #if ($content.immagine.getImagePath(''0'') != "")
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">
      	<div class="bg-full">
          <img src="$content.immagine.getImagePath(''4'')" alt="$content.immagine.text" />
      	</div>
    		<div class="velo"></div>
    	</a>
    #end
    <h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">$content.titolo.text</a></h4>
  </div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400001,'ARG','Sezioni - badge (titolo)','
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text $i18n.getLabel("CITTAMETRO_PORTAL_FROMARGUMENTSEVIDENCE")" href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" class="badge badge-pill badge-argomenti">$content.titolo.text</a>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400011,'ARG','Lista ricerca (ico + cat + titolo + abs + link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL("argomenti")" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg> <span>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400010,'ARG','Link lista (titolo)','
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<a href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400005,'ARG','Lista argomenti (ico+titolo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<article class="scheda-brick scheda-argomento-lista scheda-round">
	<div class="scheda-argomento-lista-testo">
    <div class="scheda-icona">
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
    </div>
    <h4>
      <a href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
        $content.titolo.text
       </a>
    </h4>
  </div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400006,'ARG','In evidenza foto h488 (img+titolo)','
<article class="scheda-app h488">
	<div class="scheda-arg-img">
    #if ($content.immagine.getImagePath(''0'') != "")
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">
      	<div class="bg-full img-fit-cover">
          <img src="$content.immagine.getImagePath(''5'')" alt="$content.immagine.text" />
      	</div>
    		<div class="velo"></div>
    	</a>
    #end
    <h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">$content.titolo.text</a></h4>
  </div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400007,'ARG','In evidenza foto h264 (img+titolo)','
<article class="scheda-app h264">
	<div class="scheda-arg-img">
    #if ($content.immagine.getImagePath(''0'') != "")
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">
      	<div class="bg-full img-fit-cover">
          <img src="$content.immagine.getImagePath(''4'')" alt="$content.immagine.text" />
      	</div>
    		<div class="velo"></div>
    	</a>
    #end
    <h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">$content.titolo.text</a></h4>
  </div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400008,'ARG','In evidenza foto h184 (img+titolo)','
<article class="scheda-app h184">
	<div class="scheda-arg-img">
    #if ($content.immagine.getImagePath(''0'') != "")
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">
      	<div class="bg-full img-fit-cover">
          <img src="$content.immagine.getImagePath(''4'')" alt="$content.immagine.text" />
      	</div>
    		<div class="velo"></div>
    	</a>
    #end
    <h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">$content.titolo.text</a></h4>
  </div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400004,'ARG','Dettaglio completo','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<div class="bg-argomento img-fit-cover">
	#if ($content.immagine.getImagePath("0") != "")
		<img src="$content.immagine.getImagePath(''0'')" alt="$content.immagine.text" />
	#end
</div>
<div class="container box-argomento">
	<div class="row">
		<div class="col-sm-12" id="briciole">
			<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Home" class=""><strong>Home</strong></a><span class="separator">/</span></li>
					<li class="breadcrumb-item"><a href="$content.getPageURL("argomenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")"><strong>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</strong></a><span class="separator">/</span></li>
					<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6">
			<div class="icona-sezione">
				<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
			</div>
			<div class="titolo-sezione">
				<h2>$content.titolo.text</h2>
				<p>$content.descr.text</p>
			</div>
		</div>
		<div class="offset-lg-1 col-lg-5 col-md-6">
			#if($content.gestione.size() > 0)
				<aside id="menu-area" data-ng-cloak data-ng-controller="FiltriController">
					<p>$i18n.getLabel("CITTAMETRO_CONTENT_MANAGE_ARG")</p>
					#foreach ($gest in $content.gestione)
						#set($linkGest = $gest.destination.replaceAll("#|C;|!",""))
						#if ($linkGest.contains("ORG"))
							<div data-ng-init="getContent(''$linkGest'',''220005'')" data-ng-bind-html="renderContent[''$linkGest''][''220005'']"></div>
						#end
					#end
				</aside>
			#end
		</div>
	</div>
</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400012,'ARG','Titolo','
$content.titolo.text
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400013,'ARG','Descrizione','
$content.descr.text
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400002,'ARG','Sezioni - In evidenza (ico+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<article class="scheda scheda-round">
	<div class="scheda-icona">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
  </div>
  <div class="scheda-testo">
  	<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
  	<p>$content.descr.text</p>
  </div>
  <div class="scheda-footer">
  	<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_EXPLORE_ARGUMENT") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.titolo.text" href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_EXPLORE_ARGUMENT") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
  </div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (400003,'ARG','Homepage - In evidenza (ico+titolo+abs+links)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<article class="scheda scheda-round" data-ng-cloak data-ng-controller="FiltriController">
	<div class="scheda-icona">
	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
  </div>
  <div class="scheda-testo">
	<h4><a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text $i18n.getLabel("CITTAMETRO_PORTAL_FROMARGUMENTSEVIDENCE")" href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
	<p>$content.descr.text</p>
	<div data-ng-init="setParameters(''5'', ''5'', ''NVT,EVN'', '''', ''$content.argomento.mapKey'', '''', '''', ''(order=DESC;key=jpattributeextended:datapubblicazione)'',''multi'')">
		<ul class="scheda-lista" data-ng-if="contents.length > 0" >
			<li data-ng-repeat="elem in contents">
				<span data-ng-if="elem.indexOf(''NVT'') != -1" data-ng-init="getContent(elem,''100010'')" data-ng-bind-html="renderContent[elem][''100010'']"></span>
				<span data-ng-if="elem.indexOf(''EVN'') != -1" data-ng-init="getContent(elem,''210004'')" data-ng-bind-html="renderContent[elem][''210004'']"></span>
			</li>
		</ul>
	</div>
  </div>
  <div class="scheda-footer">
	<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_EXPLORE_ARGUMENT") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.titolo.text" href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_EXPLORE_ARGUMENT") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
  </div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (180001,'BAN','In evidenza (sfondo+titolo+abs)','
<article class="scheda-app">
	#if ($content.immagine.getImagePath(''0'') != "")
		<div class="bg-full">
			<img src="$content.immagine.getImagePath(''3'')" alt="$content.immagine.text" />
		</div>
  #end
	<p>
  	#if ($content.link.destination != "")
		<a href="$content.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.titolo.text">
			<strong>$content.titolo.text</strong><br />
			$content.descr.text
		</a>
    #else
    <a>
    <strong>$content.titolo.text</strong><br />
			$content.descr.text
    </a>
    #end
	</p>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (180002,'BAN','Banner largo (sfondo+abs+titolo)	','
	#if ($content.immagine.getImagePath(''0'') != "")
		<div class="bg-centroinfo">
			<img src="$content.immagine.getImagePath(''0'')" alt="$content.immagine.text" />
		</div>
  #end
	<div class="container">
		<div class="row row-centroinfo">
			<div class="col-md-8">
				<div class="testo-centroinfo text-center">
					<h4>$content.descr.text</h4>
				</div>
			</div>
      #if ($content.num_verde.text != "")
       <div class="col-md-4">
         <div class="box-numeroverde">
         		#if ($content.link.destination != "")
						<a href="$content.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.titolo.text">
            #end
           		$content.num_verde.text
            #if ($content.link.destination != "")
						</a>
            #end
         </div>
       </div>
      #end
		</div>
	</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (180003,'BAN','Banner largo immagine','
#if ($content.immagine.getImagePath(''0'') != "")
<div class="container">
	<div class="img-banner-largo img-fit-cover">
		<img src="$content.immagine.getImagePath(''0'')" alt="$content.immagine.text" class="img-fluid" />
	</div>
</div>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (180004,'BAN','Homepage - Banner speciale','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set ($linktitolo = "#")
#if ($content.link_arg.mapKey != "")
	#set ($linktitolo = $content.getPageURL($info.getConfigParameter("pagina_argomento")) + "?categoryCode=" + $content.link_arg.mapKey)
#else
	#if ($content.link.destination != "")
  	#set ($linktitolo = $content.link.destination)
  #end
#end
<div class="allerta-meteo allerta3 $content.sfondo.mapKey">
	<article>
		#if ($content.immagine.getImagePath(''0'') != "")
			<div class="bg-allerta">
				<div class="bg-allerta-img">
					<img src="$content.immagine.getImagePath(''0'')" alt="$content.immagine.text" />
				</div>
		  </div>
		#end
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-9">
					<h2><a href="$linktitolo" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h2>
				</div>
			</div>
			#if ($content.link_puls.size() > 0)
			<div class="row">
				<div class="col-12 mt8 mb8">
					#foreach ($pulsante in $content.link_puls)
						<a class="btn btn-gen bg-giallo" href="$pulsante.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $pulsante.text" #if($pulsante.symbolicLink.destType == 1)target="_blank" #end>$pulsante.text</a>
					#end
				</div>
			</div>
			#end
		</div>
	</article>
</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (110002,'CNV','Dettaglio completo volante','
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.sezione.mapKey != "")
							#set($codicePaginaContenuto = $content.sezione.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end												
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.desc.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>

<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.testo.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-descrizion" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_CNV_descrizion")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_descrizion")</a>
						#end
						#if ($content.tipologia.mapValue != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-organo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_organo")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_organo")</a>
						#end
						#if ($content.data.fullDate != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-numerodata" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_numerodata")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_numerodata")</a>
						#end
						#if ($content.luogo.sede_link.destination != "" || $content.luogo.sede_desc.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-luogo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_CNV_luogo")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_luogo")</a>
						#end
						#if ($content.odg && $content.odg.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-odg" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_argtrattati")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_argtrattati")</a>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_documenti")</a>
						#end
						#if ($content.altriLink && $content.altriLink.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-linkcorrelati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_linkcorrelati")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_linkcorrelati")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi" data-ng-cloak data-ng-controller="FiltriController">
					#if ($content.testo.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-descrizion"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_descrizion")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.testo.text
							</div>
						</div>
					#end
					#if ($content.tipologia.mapValue != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-organo"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_organo")</h4>
							</div>
						</div>
						<div class="row listaluoghi">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									<div class="col-lg-6">
										<article class="scheda-ufficio-contatti scheda-round">
											<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
											<div class="scheda-ufficio-testo">    
												<strong>$content.tipologia.mapValue</strong>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					#end
					#if ($content.data.fullDate != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-numerodata"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_numerodata")</h4>
							</div>
						</div>
						<div class="row stepper mb0">
							<div class="offset-md-1 col-md-11">
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data.getFormattedDate("MMM")/$content.data.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
										    
											<p>#if ($content.data.getFormattedDate(''HH:mm'') != "00:00") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_hours") $content.data.getFormattedDate(''HH:mm'') - #end $i18n.getLabel("CITTAMETRO_CONTENT_CNV_convocazione") n. $content.numero.text</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					#end
					#if ($content.luogo.sede_link.destination != "" || $content.luogo.sede_desc.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-luogo"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_luogo")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 listaluoghi">
								<div class="row">
									#if ($content.luogo.sede_link.destination != "" && $content.luogo.sede_link.destination.contains("LGO"))
										#set($linkScheda = $content.luogo.sede_link.destination.replaceAll("#|C;|!",""))
										<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''120004'')" data-ng-bind-html="renderContent[''$linkScheda''][''120004'']"></div>
									#else
										<div class="col-md-9">
											<article class="scheda-ufficio-contatti scheda-round">
												<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-place"></use></svg>
												<div class="scheda-ufficio-testo">
													<h4 class="mb24"><a>$content.luogo.sede_desc.text</a></h4>
												</div>
											</article>
										</div>
									#end
								</div>
							</div>
						</div>
					#end
					#set($videoscaduto = false)
					#if ($content.data.fullDate != "")
						#set($videoscaduto = $info.getExpiredDate($content.data.getFormattedDate("dd/MM/yyyy")))
					#end
					#if (($content.linkstr.destination != "") && (!$videoscaduto))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="videoplayer">
									#set($linkVideo = $content.linkstr.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
								</div>
							</div>
						</div>
					#elseif (($content.video.destination != "") && ($videoscaduto))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="videoplayer">
									#set($linkVideo = $content.video.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.odg && $content.odg.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-odg"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_argtrattati")</h4>
							</div>
						</div>
						#set ($contaordgg = 0)
						#foreach ($argomento in $content.odg)
							#if ($argomento.tipologia.text == ''Ordine del giorno'')
								#if ($contaordgg==0)
									<div class="row steppertime">
										<div class="offset-md-1 col-md-11">
											<div class="articolo-titoletto mt0 mb24">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_ordinedelgiorno")</div>
								#end
								#set ($contaordgg = $contaordgg + 1)
											<div class="step">
												<div class="testo-step ml0">
													<div class="scheda-gestione">
														<p>$argomento.paragrafo.text
														 #if ($argomento.proponente.text != '''') <br/>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_prop"): $argomento.proponente.text #end
														</p>
								#if ($argomento.linkAtto.destination != ''''	|| $argomento.allegato.attachPath != '''')
														<p class="link-step">
															#if ($argomento.linkAtto.destination != '''')
																<a href="$argomento.linkAtto.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $argomento.linkAtto.text" #if($argomento.linkAtto.symbolicLink.destType == 1)target="_blank" #end>$argomento.linkAtto.text</a>
															#end
															#if ($argomento.allegato.attachPath != '''')
																#set ($fileNameSplit = $argomento.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$argomento.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $argomento.allegato.resource.instance.fileLength"
																aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $argomento.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $argomento.allegato.resource.instance.fileLength)" target="_blank">
																$argomento.allegato.text
																</a>
															#end
														</p>
								#end
													</div>
												</div>
											</div>
							#end
						#end
						#if ($contaordgg >=1)
										</div>
									</div>
						#end
						
						#set ($containter = 0)
						#foreach ($argomento in $content.odg)
							#if ($argomento.tipologia.text == ''Interrogazioni'')
								#if ($containter==0)
									<div class="row steppertime">
										<div class="offset-md-1 col-md-11">
											<div class="articolo-titoletto mb24">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_interrogazioni")</div>
								#end
								#set ($containter = $containter + 1)
											<div class="step">
												<div class="testo-step ml0">
													<div class="scheda-gestione">
														<p>$argomento.paragrafo.text
														 #if ($argomento.proponente.text != '''') <br/>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_prop"): $argomento.proponente.text #end
														</p>
								#if ($argomento.linkAtto.destination != ''''	|| $argomento.allegato.attachPath != '''')
														<p class="link-step">
															#if ($argomento.linkAtto.destination != '''')
																<a href="$argomento.linkAtto.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $argomento.linkAtto.text" #if($argomento.linkAtto.symbolicLink.destType == 1)target="_blank" #end>$argomento.linkAtto.text</a>
															#end
															#if ($argomento.allegato.attachPath != '''')
																#set ($fileNameSplit = $argomento.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$argomento.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $argomento.allegato.resource.instance.fileLength"
																aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $argomento.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $argomento.allegato.resource.instance.fileLength)" target="_blank">
																$argomento.allegato.text
																</a>
															#end
														</p>
								#end
													</div>
												</div>
											</div>
							#end
						#end
						#if ($containter >=1)
										</div>
									</div>
						#end
						
						#set ($contaordggprec = 0)
						#foreach ($argomento in $content.odg)
							#if ($argomento.tipologia.text == ''Ordini del giorno precedenti'')
								#if ($contaordggprec==0)
									<div class="row steppertime">
										<div class="offset-md-1 col-md-11">
											<div class="articolo-titoletto mb24">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_ordinegiornoprec")</div>
								#end
								#set ($contaordggprec = $contaordggprec + 1)
											<div class="step">
												<div class="testo-step ml0">
													<div class="scheda-gestione">
														<p>$argomento.paragrafo.text
														 #if ($argomento.proponente.text != '''') <br/>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_prop"): $argomento.proponente.text #end
														</p>
								#if ($argomento.linkAtto.destination != ''''	|| $argomento.allegato.attachPath != '''')
														<p class="link-step">
															#if ($argomento.linkAtto.destination != '''')
																<a href="$argomento.linkAtto.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $argomento.linkAtto.text" #if($argomento.linkAtto.symbolicLink.destType == 1)target="_blank" #end >$argomento.linkAtto.text</a>
															#end
															#if ($argomento.allegato.attachPath != '''')
																#set ($fileNameSplit = $argomento.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$argomento.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $argomento.allegato.resource.instance.fileLength"
																aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $argomento.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $argomento.allegato.resource.instance.fileLength)" target="_blank">
																$argomento.allegato.text
																</a>
															#end
														</p>
								#end
													</div>
												</div>
											</div>
							#end
						#end
						#if ($contaordggprec >=1)
										</div>
									</div>
						#end
						
						#set ($contaaltro = 0)
						#foreach ($argomento in $content.odg)
							#if ($argomento.tipologia.text == ''Altro'')
								#if ($contaaltro==0)
									<div class="row steppertime">
										<div class="offset-md-1 col-md-11">
											<div class="articolo-titoletto mb24">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_altro")</div>
								#end
								#set ($contaaltro = $contaaltro + 1)
											<div class="step">
												<div class="testo-step ml0">
													<div class="scheda-gestione">
														<p>$argomento.paragrafo.text
														 #if ($argomento.proponente.text != '''') <br/>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_prop"): $argomento.proponente.text #end
														</p>
								#if ($argomento.linkAtto.destination != ''''	|| $argomento.allegato.attachPath != '''')
														<p class="link-step">
															#if ($argomento.linkAtto.destination != '''')
																<a href="$argomento.linkAtto.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $argomento.linkAtto.text" #if($argomento.linkAtto.symbolicLink.destType == 1)target="_blank" #end>$argomento.linkAtto.text</a>
															#end
															#if ($argomento.allegato.attachPath != '''')
																#set ($fileNameSplit = $argomento.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$argomento.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $argomento.allegato.resource.instance.fileLength"
																aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $argomento.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $argomento.allegato.resource.instance.fileLength)" target="_blank">
																$argomento.allegato.text
																</a>
															#end
														</p>
								#end
													</div>
												</div>
											</div>
							#end
						#end
						#if ($contaaltro >=1)
											</ul>
										</div>
									</div>
						#end
					#end
					
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.attachPath != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#set ($fileNameSplit = $allegato.resource.instance.fileName.split("\."))
														#set ($last = $fileNameSplit.size() - 1)
														#set ($fileExt = $fileNameSplit[$last])	
														<a href="$allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.resource.instance.fileLength"
															aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.resource.instance.fileLength)" target="_blank">
															$allegato.text
														</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.altriLink && $content.altriLink.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-linkcorrelati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_linkcorrelati")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								#set ($contalink = 1)
								#foreach ($altrilink in $content.altriLink)
									#if ($altrilink.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$altrilink.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $altrilink.text" #if($altrilink.symbolicLink.destType == 1)target="_blank" #end><strong>$altrilink.text</strong></a>
											</article>
										</div>
										#set ($contalink = $contalink + 1)
									#end
									#if ($contalink % 2 != 0 || $foreach.count==$content.altriLink.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (110004,'CNV','Argomento (ico+cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.sezione.mapValue
	</div>
	<div class="scheda-testo scheda-testo-large">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
 		<p>$content.desc.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (110003,'CNV','Lista ricerca (ico+cat+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.sezione.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> <span>$content.sezione.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.desc.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (500001,'FCS','Dettaglio completo','#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<div class="bg-argomento img-fit-cover">
	#if ($content.immagine.getImagePath("0") != "")
		<img src="$content.immagine.getImagePath(''0'')" alt="$content.immagine.text" />
	#end
</div>
#*
<div class="container box-argomento">
	<div class="row">
		<div class="col-sm-12" id="briciole">
			<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Home" class=""><strong>Home</strong></a><span class="separator">/</span></li>
					<li class="breadcrumb-item"><a href="$content.getPageURL("argomenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")"><strong>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</strong></a><span class="separator">/</span></li>
					<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6">
			<div class="icona-sezione">
				<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
			</div>
			<div class="titolo-sezione">
				<h2>$content.titolo.text</h2>
				<p>$content.descr.text</p>
			</div>
		</div>
		<div class="offset-lg-1 col-lg-5 col-md-6">
			#if($content.gestione.size() > 0)
				<aside id="menu-area" data-ng-cloak data-ng-controller="FiltriController">
					<p>$i18n.getLabel("CITTAMETRO_CONTENT_MANAGE_ARG")</p>
					#foreach ($gest in $content.gestione)
						#set($linkGest = $gest.destination.replaceAll("#|C;|!",""))
						#if ($linkGest.contains("ORG"))
							<div data-ng-init="getContent(''$linkGest'',''220005'')" data-ng-bind-html="renderContent[''$linkGest''][''220005'']"></div>
						#end
					#end
				</aside>
			#end
		</div>
	</div>
</div>
*#',NULL);
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (500002,'FCS','Focus - Homepage (img + ico + titolo + abs + link)','<article class="scheda scheda-focus scheda-round">
	<div class="scheda-testo mt16">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
</div>
<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
</div>
</article>',NULL);
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (110001,'CNV','Lista tabellare (org+num+data+ora+luogo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<th scope="row" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_ORGANO'')">
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-piu" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEEALL''): $content.titolo.text" onclick="espandiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add"></use></svg>
	</button>
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-meno" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEELESS''): $content.titolo.text" onclick="chiudiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-remove"></use></svg>
	</button>
	<a href="$content.getContentLink()" title="$i18n.getLabel(''CITTAMETRO_PORTAL_GOTOPAGE''): $content.titolo.text">$content.tipologia.mapValue</a>
</th>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_NUMEROANNO'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_NUMEROANNO'')">$content.numero.text/$content.data.getFormattedDate(''yyyy'')</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_DATA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_DATA'')">$content.data.getFormattedDate(''dd/MM/yyyy'')</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_ORA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_ORA'')">#if ($content.data.getFormattedDate(''HH:mm'') != "00:00") $content.data.getFormattedDate(''HH:mm'')#end </td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_LUOGO'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_LUOGO'')">
	#if ($content.luogo.sede_link.destination != '''')
		$content.luogo.sede_link.text
	#else
		$content.luogo.sede_desc.text
  #end
</td>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700011,'DOC','Dettaglio completo','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($scaduto = false)
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
					#if ($content.data_scad.fullDate != "")
						#set($scaduto = $info.getExpiredDate($content.data_scad.getFormattedDate("dd/MM/yyyy")))
						#if ($scaduto && $content.sottotip.mapKey == ''doc_04_02'')
							<div class="alert alert-warning mt16" role="alert">
								<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-error_outline"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_DOC_regolamento_scaduto")
							</div>
						#end
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto" class="img-fit-cover">
	<figure>
		<img src="$content.immagine.getImagePath("5")" alt="$content.immagine.text" class="img-fluid">
		<figcaption>$content.immagine.text</figcaption>
	</figure>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.oggetto.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-oggetto" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")</a>
						#end
						#if ($content.descr_est.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-descrizion" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_DOC_descrizion")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_descrizion")</a>
						#end
						#if ($content.canal_link.destination != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-istanza" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_DOC_istanza")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_istanza")</a>
						#end
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-datapub" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")</a>
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_documento")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_documenti")</a>
						#end
						#if ($content.fasi && $content.fasi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-scadenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")</a>
						#end
						#if ($content.allegati && $content.allegati.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-allegati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")</a>
						#end
						#if ($content.dataset && $content.dataset.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-dataset" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")</a>
						#end
						#if ($content.servizi && $content.servizi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")</a>
						#end
						#if ($content.area.destination != "")
							#if ($content.ufficio.destination != "")
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")</a>
							#else 
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")</a>
							#end
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.oggetto.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-oggetto"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p>$content.oggetto.text</p>
							</div>
						</div>
					#end
					#if ($content.descr_est.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-descrizion"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_descrizion")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.descr_est.text
							</div>
						</div>
					#end
					#if ($content.formati && $content.formati.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-8 argomenti mb0">
								<p class="argomenti-sezione-elenco">
									<span class="articolo-titoletto mt0">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_formati")</span>:
									#foreach ($formato in $content.formati)
										<a class="badge badge-pill badge-argomenti mb0">$formato.text</a>
									#end
								</p>
							</div>
						</div>
					#end
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 paragrafo">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.canal_link.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-istanza"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_istanza")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p class="mt12">
									<a class="btn btn-default btn-iscriviti" href="$content.canal_link.destination" target="_blank">$content.canal_link.text</a>
								</p>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-datapub"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")</h4>
						</div>
					</div>
					<div class="row stepper mb0">
						<div class="offset-md-1 col-md-11">
							<div class="step">
								<div class="date-step">
									<span class="date-step-giorno">$content.data_pubb.getFormattedDate("dd")</span><br />
									<span class="date-step-mese">$content.data_pubb.getFormattedDate("MMM")/$content.data_pubb.getFormattedDate("yy")</span>
									<span class="pallino"></span>
								</div>
								<div class="testo-step">
									<div class="scheda-gestione">
										<p>$i18n.getLabel("CITTAMETRO_CONTENT_PUBDATE")</p>
									</div>
								</div>
							</div>
							#if ($content.data_scad.fullDate != "")
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data_scad.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data_scad.getFormattedDate("MMM")/$content.data_scad.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
											<p>$i18n.getLabel("CITTAMETRO_CONTENT_DATESCAD")</p>
										</div>
									</div>
								</div>
							#end
							#if ($content.data_esito.fullDate != "")
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data_esito.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data_esito.getFormattedDate("MMM")/$content.data_esito.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
											<p>$i18n.getLabel("CITTAMETRO_CONTENT_DATEESITO")</p>
										</div>
									</div>
								</div>
							#end
						</div>
					</div>
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.attachPath != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#set ($fileNameSplit = $allegato.resource.instance.fileName.split("\."))
														#set ($last = $fileNameSplit.size() - 1)
														#set ($fileExt = $fileNameSplit[$last])	
														<a href="$allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.resource.instance.fileLength"
															aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.resource.instance.fileLength)" target="_blank">
															$allegato.text
														</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.fasi && $content.fasi.size() > 0)
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-scadenze"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")</h4>
						</div>
					</div>
					<div class="row steppertime">
						<div class="offset-md-1 col-md-11">
							#foreach ($fase in $content.fasi)
								#if (($foreach.count > 1) && ((!$fase.fs_accorpa.booleanValue) || ($fase.fs_accorpa.booleanValue && $fase.fs_testo.text != "")))
												</p>
											</div>
										</div>
									</div>
								#end
								#if ($fase.fs_accorpa.booleanValue)
									#if ($fase.fs_testo.text != "")
									<div class="step">
										<div class="date-step">
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$fase.fs_testo.text</p>
												#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
													<p class="link-step">
													#set($filescaduto = false)
													#if ($fase.fs_datasc.fullDate != "")
														#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
													#end
													#if ($filescaduto)
														#if ($fase.fs_alleg.attachPath != "")
															#set($filescadutotxt = $fase.fs_alleg.text)
														#end
														#if ($fase.fs_link.destination != "")
															#set($filescadutotxt = $fase.fs_link.text)
														#end
														<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
													#else
														#if ($fase.fs_alleg.attachPath != "")
															#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])	
															<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
																$fase.fs_alleg.text
															</a>
														#end
														#if ($fase.fs_link.destination != "")
															#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])	
															<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
																$fase.fs_link.text
															</a>
														#end
													#end
												#end
									#else
										#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
											<br />
											#set($filescaduto = false)
											#if ($fase.fs_datasc.fullDate != "")
												#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
											#end
											#if ($filescaduto)
												#if ($fase.fs_alleg.attachPath != "")
													#set($filescadutotxt = $fase.fs_alleg.text)
												#end
												#if ($fase.fs_link.destination != "")
													#set($filescadutotxt = $fase.fs_link.text)
												#end
												<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
											#else
												#if ($fase.fs_alleg.attachPath != "")
													#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
													#set ($last = $fileNameSplit.size() - 1)
													#set ($fileExt = $fileNameSplit[$last])
													<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
														$fase.fs_alleg.text
													</a>
												#end
												#if ($fase.fs_link.destination != "")
													#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
													#set ($last = $fileNameSplit.size() - 1)
													#set ($fileExt = $fileNameSplit[$last])
													<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
														$fase.fs_link.text
													</a>
												#end
											#end
										#end
									#end
								#else
									<div class="step">
										<div class="date-step">
											#if ($fase.fs_data.fullDate != "")
												<span class="date-step-giorno">$fase.fs_data.getFormattedDate("dd")</span>
												<span class="date-step-mese">$fase.fs_data.getFormattedDate("MMM")/$fase.fs_data.getFormattedDate("yy")</span>
											#end
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$fase.fs_testo.text</p>
												#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
													<p class="link-step">
													#set($filescaduto = false)
													#if ($fase.fs_datasc.fullDate != "")
														#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
													#end
													#if ($filescaduto)
														#if ($fase.fs_alleg.attachPath != "")
															#set($filescadutotxt = $fase.fs_alleg.text)
														#end
														#if ($fase.fs_link.destination != "")
															#set($filescadutotxt = $fase.fs_link.text)
														#end
														<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
													#else
														#if ($fase.fs_alleg.attachPath != "")
															#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
																$fase.fs_alleg.text
															</a>
														#end
														#if ($fase.fs_link.destination != "")
															#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])		
															<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
																$fase.fs_link.text
															</a>
														#end
													#end
												#end
											#end
										#end
									</div>
								</div>
							</div>
						</div>
					</div>
					#end
					#if ($content.allegati && $content.allegati.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-allegati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($alleg in $content.allegati)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($alleg.allegato.attachPath != '''' || $alleg.link.destination != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($alleg.link.destination != '''')
														<a href="$alleg.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $alleg.link.text">
															$alleg.link.text
														</a>
														#else 
															#if ($alleg.allegato.attachPath != '''')
																#set ($fileNameSplit = $alleg.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])		
																<a href="$alleg.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $alleg.allegato.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $alleg.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $alleg.allegato.resource.instance.fileLength)" target="_blank">
																	$alleg.allegato.text
																</a>
															#end
														#end
														
														<br />
														<span>$alleg.descr.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.allegati.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.dataset && $content.dataset.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-allegati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($datas in $content.dataset)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($datas.destination != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<h4>
														<a href="$datas.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $datas.text">$datas.text</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.dataset.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.servizi && $content.servizi.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#foreach ($servizio in $content.servizi)
									#if ($foreach.count % 2 != 0)
										<div class="row row-eq-height allegati-riga">
									#end
									<div class="col-lg-6">
										#set($linkServizio = $servizio.link.destination.replaceAll("#|C;|!",""))
										#if ($linkServizio.contains("SRV"))
											<div data-ng-init="getContent(''$linkServizio'',''800001'')" data-ng-bind-html="renderContent[''$linkServizio''][''800001'']"></div>
										#else
											#set($linkServizio = $servizio.link.destination.replaceAll("#|U;|!",""))
											<article class="scheda scheda-round">
												<div class="scheda-icona-small">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
												</div>
												<div class="scheda-testo-small">
													<h4><a href="$linkServizio" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $servizio.link.text">$servizio.link.text</a></h4>
													<p>$servizio.descr.text</p>
												</div>
											</article>
										#end
									</div>
									#if ($foreach.count % 2 == 0 || $foreach.count == $content.servizi.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.area.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-area"> </a>
								#if ($content.ufficio.destination != "")
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")</h4>
								#else 
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")</h4>
								#end
							</div>
						</div>
						<div class="row listaluoghi">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									#if ($content.ufficio.destination != "")
										#set($linkScheda = $content.ufficio.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("ORG"))
											<div class="col-lg-6">
												<div data-ng-init="getContent(''$linkScheda'',''220004'')" data-ng-bind-html="renderContent[''$linkScheda''][''220004'']"></div>
											</div>
										#end
									#end
									<div class="col-lg-6">
										#set($linkStruttura = $content.area.destination.replaceAll("#|C;|!",""))
										#if ($linkStruttura.contains("ORG"))
											<div data-ng-init="getContent(''$linkStruttura'',''220005'')" data-ng-bind-html="renderContent[''$linkStruttura''][''220005'']"></div>
										#end
									</div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end					
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-11">
							#if ($content.rif_norma && $content.rif_norma.size() > 0)
								<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_rif_norma"):</span>
								#set ($contadoc = 1)
								#foreach ($norma in $content.rif_norma)
									#if ($norma.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$norma.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $norma.text" #if($norma.symbolicLink.destType == 1)target="_blank" #end><strong>$norma.text</strong></a>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
										#if ($contadoc % 2 != 0 || $foreach.count==$content.rif_norma.size())
											</div>
										#end
									#end
								#end
								</p>
							#end
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
						</div>
					</div>
					#if ($content.autori.text != "" || $content.licenza.text != "" || $content.protoc.text != "" || $content.protoc_dt.fullDate != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-11">
								#if ($content.autori.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_autori"):</strong> $content.autori.text</p>
								#end
								#if ($content.licenza.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_licenza"):</strong> $content.licenza.text</p>
								#end
								#if ($content.protoc.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_protoc"):</strong>
									$content.protoc.text
									#if ($content.protoc_dt.fullDate != "")
										$i18n.getLabel("CITTAMETRO_CONTENT_DOC_protoc_data") $content.protoc_dt.getFormattedDate("dd/MM/yyyy")
									#end
									</p>
								#end
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong><br />
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700010,'DOC','Lista contenuti (cat+titolo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $content.titolo.text">$content.titolo.text</a></h4>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700001,'DOC','Sezioni - In evidenza (cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTODOC"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700014,'DOC','Sezioni - In evidenza (cat+titolo+abs-area)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTODOC"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
    <p>
      	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area"):</strong> <a href="$content.area.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.area.text">$content.area.text</a>
    </p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700005,'DOC','Argomento (ico+cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
	</div>
	<div class="scheda-testo scheda-testo-large">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700002,'DOC','Homepage - In evidenza (cat+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
		<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
		</a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLDOCUMENTI")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700004,'DOC','Pagine interne - In evidenza (cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTODOC"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700007,'DOC','Dettaglio completo volante','
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if (($content.tipologia.mapKey != "") || ($content.sottotip.mapKey != ""))
							#if ($content.sottotip.mapKey != "")
								#set($codicePaginaContenuto = $content.sottotip.mapKey)
							#else 	
								#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#end
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($scaduto = false)
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
					#if ($content.data_scad.fullDate != "")
						#set($scaduto = $info.getExpiredDate($content.data_scad.getFormattedDate("dd/MM/yyyy")))
						#if ($scaduto && $content.sottotip.mapKey == ''doc_04_02'')
							<div class="alert alert-warning mt16" role="alert">
								<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-error_outline"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_DOC_regolamento_scaduto")
							</div>
						#end
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto" class="img-fit-cover">
	<figure>
		<img src="$content.immagine.getImagePath("5")" alt="$content.immagine.text" class="img-fluid">
		<figcaption>$content.immagine.text</figcaption>
	</figure>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.oggetto.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-oggetto" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")</a>
						#end
						#if ($content.descr_est.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-descrizion" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_DOC_descrizion")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_descrizion")</a>
						#end
						#if ($content.canal_link.destination != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-istanza" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_DOC_istanza")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_istanza")</a>
						#end
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-datapub" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")</a>
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_documento")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_documenti")</a>
						#end
						#if ($content.fasi && $content.fasi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-scadenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")</a>
						#end
						#if ($content.allegati && $content.allegati.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-allegati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")</a>
						#end
						#if ($content.dataset && $content.dataset.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-dataset" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")</a>
						#end
						#if ($content.servizi && $content.servizi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")</a>
						#end
						#if ($content.area.destination != "")
							#if ($content.ufficio.destination != "")
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")</a>
							#else 
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")</a>
							#end
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.oggetto.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-oggetto"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p>$content.oggetto.text</p>
							</div>
						</div>
					#end
					#if ($content.descr_est.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-descrizion"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_descrizion")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.descr_est.text
							</div>
						</div>
					#end
					#if ($content.formati && $content.formati.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-8 argomenti mb0">
								<p class="argomenti-sezione-elenco">
									<span class="articolo-titoletto mt0">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_formati")</span>:
									#foreach ($formato in $content.formati)
										<a class="badge badge-pill badge-argomenti mb0">$formato.text</a>
									#end
								</p>
							</div>
						</div>
					#end
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 paragrafo">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.canal_link.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-istanza"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_istanza")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p class="mt12">
									<a class="btn btn-default btn-iscriviti" href="$content.canal_link.destination" target="_blank">$content.canal_link.text</a>
								</p>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-datapub"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")</h4>
						</div>
					</div>
					<div class="row stepper mb0">
						<div class="offset-md-1 col-md-11">
							<div class="step">
								<div class="date-step">
									<span class="date-step-giorno">$content.data_pubb.getFormattedDate("dd")</span><br />
									<span class="date-step-mese">$content.data_pubb.getFormattedDate("MMM")/$content.data_pubb.getFormattedDate("yy")</span>
									<span class="pallino"></span>
								</div>
								<div class="testo-step">
									<div class="scheda-gestione">
										<p>$i18n.getLabel("CITTAMETRO_CONTENT_PUBDATE")</p>
									</div>
								</div>
							</div>
							#if ($content.data_scad.fullDate != "")
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data_scad.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data_scad.getFormattedDate("MMM")/$content.data_scad.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
											<p>$i18n.getLabel("CITTAMETRO_CONTENT_DATESCAD")</p>
										</div>
									</div>
								</div>
							#end
							#if ($content.data_esito.fullDate != "")
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data_esito.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data_esito.getFormattedDate("MMM")/$content.data_esito.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
											<p>$i18n.getLabel("CITTAMETRO_CONTENT_DATEESITO")</p>
										</div>
									</div>
								</div>
							#end
						</div>
					</div>
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.attachPath != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#set ($fileNameSplit = $allegato.resource.instance.fileName.split("\."))
														#set ($last = $fileNameSplit.size() - 1)
														#set ($fileExt = $fileNameSplit[$last])	
														<a href="$allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.resource.instance.fileLength"
															aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.resource.instance.fileLength)" target="_blank">
															$allegato.text
														</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.fasi && $content.fasi.size() > 0)
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-scadenze"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")</h4>
						</div>
					</div>
					<div class="row steppertime">
						<div class="offset-md-1 col-md-11">
							#foreach ($fase in $content.fasi)
								#if (($foreach.count > 1) && ((!$fase.fs_accorpa.booleanValue) || ($fase.fs_accorpa.booleanValue && $fase.fs_testo.text != "")))
												</p>
											</div>
										</div>
									</div>
								#end
								#if ($fase.fs_accorpa.booleanValue)
									#if ($fase.fs_testo.text != "")
									<div class="step">
										<div class="date-step">
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$fase.fs_testo.text</p>
												#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
													<p class="link-step">
													#set($filescaduto = false)
													#if ($fase.fs_datasc.fullDate != "")
														#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
													#end
													#if ($filescaduto)
														#if ($fase.fs_alleg.attachPath != "")
															#set($filescadutotxt = $fase.fs_alleg.text)
														#end
														#if ($fase.fs_link.destination != "")
															#set($filescadutotxt = $fase.fs_link.text)
														#end
														<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
													#else
														#if ($fase.fs_alleg.attachPath != "")
															#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])	
															<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
																$fase.fs_alleg.text
															</a>
														#end
														#if ($fase.fs_link.destination != "")
															#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])	
															<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
																$fase.fs_link.text
															</a>
														#end
													#end
												#end
									#else
										#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
											<br />
											#set($filescaduto = false)
											#if ($fase.fs_datasc.fullDate != "")
												#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
											#end
											#if ($filescaduto)
												#if ($fase.fs_alleg.attachPath != "")
													#set($filescadutotxt = $fase.fs_alleg.text)
												#end
												#if ($fase.fs_link.destination != "")
													#set($filescadutotxt = $fase.fs_link.text)
												#end
												<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
											#else
												#if ($fase.fs_alleg.attachPath != "")
													#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
													#set ($last = $fileNameSplit.size() - 1)
													#set ($fileExt = $fileNameSplit[$last])
													<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
														$fase.fs_alleg.text
													</a>
												#end
												#if ($fase.fs_link.destination != "")
													#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
													#set ($last = $fileNameSplit.size() - 1)
													#set ($fileExt = $fileNameSplit[$last])
													<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
														$fase.fs_link.text
													</a>
												#end
											#end
										#end
									#end
								#else
									<div class="step">
										<div class="date-step">
											#if ($fase.fs_data.fullDate != "")
												<span class="date-step-giorno">$fase.fs_data.getFormattedDate("dd")</span>
												<span class="date-step-mese">$fase.fs_data.getFormattedDate("MMM")/$fase.fs_data.getFormattedDate("yy")</span>
											#end
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$fase.fs_testo.text</p>
												#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
													<p class="link-step">
													#set($filescaduto = false)
													#if ($fase.fs_datasc.fullDate != "")
														#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
													#end
													#if ($filescaduto)
														#if ($fase.fs_alleg.attachPath != "")
															#set($filescadutotxt = $fase.fs_alleg.text)
														#end
														#if ($fase.fs_link.destination != "")
															#set($filescadutotxt = $fase.fs_link.text)
														#end
														<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
													#else
														#if ($fase.fs_alleg.attachPath != "")
															#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
																$fase.fs_alleg.text
															</a>
														#end
														#if ($fase.fs_link.destination != "")
															#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])		
															<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
																$fase.fs_link.text
															</a>
														#end
													#end
												#end
											#end
										#end
									</div>
								</div>
							</div>
						</div>
					</div>
					#end
					#if ($content.allegati && $content.allegati.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-allegati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($alleg in $content.allegati)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($alleg.allegato.attachPath != '''' || $alleg.link.destination != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($alleg.link.destination != '''')
														<a href="$alleg.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $alleg.link.text">
															$alleg.link.text
														</a>
														#else 
															#if ($alleg.allegato.attachPath != '''')
																#set ($fileNameSplit = $alleg.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])		
																<a href="$alleg.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $alleg.allegato.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $alleg.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $alleg.allegato.resource.instance.fileLength)" target="_blank">
																	$alleg.allegato.text
																</a>
															#end
														#end
														
														<br />
														<span>$alleg.descr.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.allegati.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.dataset && $content.dataset.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-allegati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($datas in $content.dataset)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($datas.destination != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<h4>
														<a href="$datas.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $datas.text">$datas.text</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.dataset.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.servizi && $content.servizi.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#foreach ($servizio in $content.servizi)
									#if ($foreach.count % 2 != 0)
										<div class="row row-eq-height allegati-riga">
									#end
									<div class="col-lg-6">
										#set($linkServizio = $servizio.link.destination.replaceAll("#|C;|!",""))
										#if ($linkServizio.contains("SRV"))
											<div data-ng-init="getContent(''$linkServizio'',''800001'')" data-ng-bind-html="renderContent[''$linkServizio''][''800001'']"></div>
										#else
											#set($linkServizio = $servizio.link.destination.replaceAll("#|U;|!",""))
											<article class="scheda scheda-round">
												<div class="scheda-icona-small">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
												</div>
												<div class="scheda-testo-small">
													<h4><a href="$linkServizio" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $servizio.link.text">$servizio.link.text</a></h4>
													<p>$servizio.descr.text</p>
												</div>
											</article>
										#end
									</div>
									#if ($foreach.count % 2 == 0 || $foreach.count == $content.servizi.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.area.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-area"> </a>
								#if ($content.ufficio.destination != "")
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")</h4>
								#else 
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")</h4>
								#end
							</div>
						</div>
						<div class="row listaluoghi">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									#if ($content.ufficio.destination != "")
										#set($linkScheda = $content.ufficio.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("ORG"))
											<div class="col-lg-6">
												<div data-ng-init="getContent(''$linkScheda'',''220004'')" data-ng-bind-html="renderContent[''$linkScheda''][''220004'']"></div>
											</div>
										#end
									#end
									<div class="col-lg-6">
										#set($linkStruttura = $content.area.destination.replaceAll("#|C;|!",""))
										#if ($linkStruttura.contains("ORG"))
											<div data-ng-init="getContent(''$linkStruttura'',''220005'')" data-ng-bind-html="renderContent[''$linkStruttura''][''220005'']"></div>
										#end
									</div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end					
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-11">
							#if ($content.rif_norma && $content.rif_norma.size() > 0)
								<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_rif_norma"):</span>
								#set ($contadoc = 1)
								#foreach ($norma in $content.rif_norma)
									#if ($norma.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$norma.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $norma.text" #if($norma.symbolicLink.destType == 1)target="_blank" #end><strong>$norma.text</strong></a>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
										#if ($contadoc % 2 != 0 || $foreach.count == $content.rif_norma.size())
											</div>
										#end
									#end
								#end
								</p>
							#end
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
						</div>
					</div>
					#if ($content.autori.text != "" || $content.licenza.text != "" || $content.protoc.text != "" || $content.protoc_dt.fullDate != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-11">
								#if ($content.autori.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_autori"):</strong> $content.autori.text</p>
								#end
								#if ($content.licenza.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_licenza"):</strong> $content.licenza.text</p>
								#end
								#if ($content.protoc.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_protoc"):</strong>
									$content.protoc.text
									#if ($content.protoc_dt.fullDate != "")
										$i18n.getLabel("CITTAMETRO_CONTENT_DOC_protoc_data") $content.protoc_dt.getFormattedDate("dd/MM/yyyy")
									#end
									</p>
								#end
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong><br />
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700009,'DOC','Sezioni - in evidenza (titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
						<h4>
            		<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
            </h4>
						<p>$content.descr.text</p>
					</div>
					<div class="scheda-footer">
            		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
					</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700006,'DOC','Sezioni - in evidenza (cat+dt+titolo+tip+dtscad)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue - <span class="datagrigia">$content.fasi_iniz.getFormattedDate("dd MMM yyyy")</span>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>
      	<br />
      	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_TIPOLOGY"):</strong> $content.tipologia.mapValue
        #if ($content.data_scad.fullDate != '')
        <br /><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DATESCAD"):</strong>$content.data_scad.getFormattedDate("dd MMMM yyyy")
        #end
      </p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700012,'DOC','Lista interna (cat+titolo+abs+pubb+scad)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTODOC"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
    <p>
      	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_data_pubb"):</strong> $content.data_pubb.getFormattedDate("dd MMMM yyyy")
        #if ($content.data_scad.fullDate != '')
        <br /><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_data_scad"):</strong> $content.data_scad.getFormattedDate("dd MMMM yyyy")
        #end
      </p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700013,'DOC','Sezioni - in evidenza (ico+cat+titoletto+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700003,'DOC','Sezioni - in evidenza (ico+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_SEE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (700008,'DOC','Lista ricerca (ico+cat+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.tipologia.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210005,'EVN','Sito tematico (titolo+abs)','
<h3>$content.titolo.text</h3>
<p>$content.descr.text</p>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210004,'EVN','Homepage - Argomenti (cat+titolo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg>
		$content.titolo.text
</a>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210002,'EVN','Evento padre (ico+cat+titolo+img+data)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
	<article class="scheda scheda-evento scheda-round">
		<div class="scheda-icona-small mr130">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $content.tipologia.mapValue<br />
      #if ($content.luogo.sch_luogo.destination != "")
      	<span>$content.luogo.sch_luogo.text</span>
      #else
      	<span>$content.luogo.nome.text</span>
      #end
		</div>
		<div class="scheda-testo">
			<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $content.titolo.text">$content.titolo.text</a></h4>
		</div>
    #if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("4")" alt="$content.immagine.text" />
 			</figure>
      <div class="scheda-data">
				<strong>$content.data_iniz.getFormattedDate("dd")</strong>
				$content.data_iniz.getFormattedDate("MMM")
			</div>
		</div>
    #end
	</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210006,'EVN','Homepage - Calendario (cat+titolo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
$content.tipologia.mapValue - 
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOAPPOINTMENT"): $content.titolo.text">
	$content.titolo.text
</a>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210003,'EVN','Lista ricerca (ico + cat + titolo + abs + link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.tipologia.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210009,'EVN','Sezioni - In evidenza (cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($sezionePage = ''novita'')
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg>
    #if ($content.tipologia.mapKey != "")
    	$content.tipologia.mapValue
    #else 
    	$i18n.getLabel("CITTAMETRO_CONTENT_EVENTS")
    #end
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210010,'EVN','Sezioni - In evidenza (titolo+abs)','
<article class="scheda scheda-round scheda-news">	
	<div class="scheda-testo mt32 no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210007,'EVN','Homepage - In evidenza (titolo+abs+arg+img)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($sezionePage = ''novita'')
<article>
	#if ($content.immagine.getImagePath("0") != "")
	<div class="novita-foto img-fit-cover"> 
		<img src="$content.immagine.getImagePath("4")" alt="$content.immagine.text" />
	</div>
  #end
	<div class="novita-testo">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">$content.titolo.text</a></h2>
					<p>$content.descr.text</p>
					<div class="argomenti">
						#set ($argomenti = $content.argomenti.values)
						#foreach ($Category in $content.getCategories())
							#if($argomenti.contains($Category.code))
								<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
							#end
						#end
					</div>
				<a href="$content.getPageURL($sezionePage)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_ALLNEWS")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLNEWS") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
				</div>
			</div>
		</div>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210001,'EVN','Dettaglio completo','
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						#if (!$content.genitore.booleanValue)
							#if ($content.parte_di.destination != "")
							<li class="breadcrumb-item"><a href="$content.parte_di.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.parte_di.text"><strong>$content.parte_di.text</strong></a><span class="separator">/</span></li>
							#end
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#else
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#end
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>

#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					#if($content.sottotit.text != "")
					<h4>$content.sottotit.text</h4>
					#end
					<p>$content.descr.text</p>
					#if($content.stato.text == "Disattivo")
					<div class="alert alert-warning mt16" role="alert">
						<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-error_outline"></use></svg> $content.stato_mot.text
					</div>
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
						#if ($content.vedi_cal.booleanValue)
						<div class="mt32">
							<a class="btn btn-default btn-celeste" title="$i18n.getLabel("CITTAMETRO_CONTENT_EVN_gotocal")" href="#">
								<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-calendar_today"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVN_gotocal")
							</a>
						</div>
						#end
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if (($content.dim_img.booleanValue) && ($content.immagine.getImagePath("0") != ""))
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
	<figure>
		<img src="$content.immagine.getImagePath("5")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
		<figcaption>$content.immagine.text</figcaption>
	</figure>
  </div>
</section>
#end
<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.intro.text != "" || $content.desc_dest.text != "" || $content.galleria.destination != "" || $content.video.destination != "" || $content.persone.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-intro" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_intro")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_intro")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-luogo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_luogo")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_luogo")</a>
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-date_orari" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_date_orari")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_date_orari")</a>
						#if ($content.prezzo.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-prezzo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_prezzo")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_prezzo")</a>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-contatti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_contatti")</a>
						#if ($content.figli && $content.figli.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-figli" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_figli")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_figli")</a>
						#end
						#if ($content.parte_di.destination != "" && ! $content.genitore.booleanValue)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-parte_di" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_parte_di")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_parte_di")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_MIGHT_INTERESTED")">$i18n.getLabel("CITTAMETRO_MIGHT_INTERESTED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					<div data-ng-cloak data-ng-controller="FiltriController">
						#if ($content.intro.text != "" || $content.desc_dest.text != "" || $content.galleria.destination != "" || $content.video.destination != "" || $content.persone.size() > 0)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-intro"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_intro")</h4>
								</div>
							</div>
							#if ($content.intro.text != "")
								<div class="row">
									<div class="offset-md-1 col-md-8 testolungo">
										$content.intro.text
									</div>
								</div>
							#end
							#if (! $content.dim_img.booleanValue)
								<div class="row">
									<div class="offset-lg-1 col-lg-11 col-md-12 articolo-foto-interna img-fit-cover">
										<figure>
											<img src="$content.immagine.getImagePath("5")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
										</figure>
									</div>
								</div>
							#end
							#if ($content.galleria.destination != "")
								<div class="row">
									<div class="col-md-12 mt32">
										<div class="galleriasfondo"></div>
									</div>
									<div class="offset-md-1 col-md-11 mt32">
										<div class="galleriaslide">
											<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
											#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
										</div>
									</div>
								</div>
							#end
							#if ($content.video.destination != "")
								<div class="row">
									<div class="offset-md-1 col-md-11 paragrafo">
										<div class="videoplayer">
											#set($linkVideo = $content.video.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
										</div>
									</div>
								</div>
							#end
							#if ($content.desc_dest.text != "")
								<div class="row">
									<div class="offset-md-1 col-md-8 testolungo">
										<span class="mt24 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_desc_dest")</span>
										$content.desc_dest.text
									</div>
								</div>
							#end
							#if ($content.persone && $content.persone.size() > 0)
								<div class="row">
									<div class="offset-md-1 col-md-8 testolungo">
										<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_persone")</p>
									</div>
								</div>
								<div class="row articolo-ulterioriinfo">
									<div class="offset-md-1 col-md-11">
										<div class="argomenti persone">
											#foreach ($persona in $content.persone)
												#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
												<span data-ng-init="getContent(''$linkDipendente'',''230007'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230007'']"></span>
											#end
										</div>
									</div>
								</div>
							#end	
						#end
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-luogo"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_luogo")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 listaluoghi">
								<div class="row">
									#if ($content.luogo.sch_luogo.destination != "")
										#set($linkScheda = $content.luogo.sch_luogo.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("LGO"))
											<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''120004'')" data-ng-bind-html="renderContent[''$linkScheda''][''120004'']"></div>
										#end
									#else
										<div class="col-md-9">
											<article class="scheda-ufficio-contatti scheda-round">
												<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-place"></use></svg>
												<div class="scheda-ufficio-testo">
													#if ($content.luogo.nome.text != "")
														<h4 class="mb24"><a>$content.luogo.nome.text</a>
															#if ($content.luogo.indirizzo.text != "" || $content.luogo.cap.text != "")
																<br /><span>
																#if ($content.luogo.indirizzo.text != "")
																	$content.luogo.indirizzo.text
																#end
																#if ($content.luogo.cap.text != "")
																	#if ($content.luogo.indirizzo.text != "")
																		-
																	#end
																	$content.luogo.cap.text
																#end
																</span>
															#end
														</h4>
													#end
													<p>
														#if ($content.luogo.quartiere.text != "")
															$content.luogo.quartiere.text
														#end
														#if ($content.luogo.circoscr.text != "")
															#if ($content.luogo.quartiere.text != "")
																<br />
															#end
															$content.luogo.circoscr.text
														#end
													</p>
												</div>
											</article>
										</div>
									#end
								</div>
							</div>
						</div>
					</div>
					#if ($content.luogo.gps_lat.text != "" && $content.luogo.gps_lon.text != "")
						<div id="schedamappa" class="mt32" style="height:368px"></div>
						<div id="schedamappah" class="mt32"></div>
						<script>
							var latIn =  $content.luogo.gps_lat.text;
							var lngIn =  $content.luogo.gps_lon.text;
							var fscala =  "16";
							var mapCa = L.map(''schedamappa'', {
							  layers:[OSM]
							});
							var baseMaps = {
								"CartoDB": CartoDB,
								"Open Street Map": OSM,
								"Google Sat": Google,
								"Esri Sat": Esri_sat
							};
							var icona = L.icon({
								iconUrl: ''$imgURL/marker-icon.png'',
								iconSize: [25, 34],
								iconAnchor: [13, 34],
								popupAnchor: [-3, -76],
								shadowUrl: ''$imgURL/marker-shadow.png'',
								shadowSize: [41, 34],
								shadowAnchor: [13, 34]
							});
							mapCa.setView([latIn, lngIn], fscala);
							L.marker([latIn, lngIn], {icon: icona}).addTo(mapCa);
						</script>
					#end
					<div data-ng-cloak data-ng-controller="FiltriController">
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-date_orari"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_date_orari")</h4>
							</div>
						</div>
						<div class="row stepper mt12">
							<div class="offset-md-1 col-md-11">
								#if ($content.data_iniz.getFullDate() != $content.data_fine.getFullDate())
									<div class="step">
										<div class="date-step">
											<span class="date-step-giorno">$content.data_iniz.getFormattedDate("dd")</span><br><span class="date-step-mese">$content.data_iniz.getFormattedDate("MMM")/$content.data_iniz.getFormattedDate("yy")</span>
											<span class="pallino"></span>
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_inizio_evento")</p>
											</div>
										</div>
									</div>
									<div class="step">
										<div class="date-step">
											<span class="date-step-giorno">$content.data_fine.getFormattedDate("dd")</span><br><span class="date-step-mese">$content.data_fine.getFormattedDate("MMM")/$content.data_fine.getFormattedDate("yy")</span>
											<span class="pallino"></span>
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_fine_evento")</p>
											</div>
										</div>
									</div>
								#else 
									<div class="step">
										<div class="date-step">
											<span class="date-step-giorno">$content.data_iniz.getFormattedDate("dd")</span><br><span class="date-step-mese">$content.data_iniz.getFormattedDate("MMM")/$content.data_iniz.getFormattedDate("yy")</span>
											<span class="pallino"></span>
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_inizio_fine_evento")</p>
											</div>
										</div>
									</div>
								#end
							</div>
						</div>
						#if ($content.ricorrenza.ric_lun.booleanValue || $content.ricorrenza.ric_mar.booleanValue || $content.ricorrenza.ric_mer.booleanValue || $content.ricorrenza.ric_gio.booleanValue || $content.ricorrenza.ric_ven.booleanValue || $content.ricorrenza.ric_sab.booleanValue || $content.ricorrenza.ric_dom.booleanValue)
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<span class="mt24 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_giorni")</span>
								<p>
								#set($contag = 0)
								#if ($content.ricorrenza.ric_lun.booleanValue)
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_lun")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_mar.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_mar")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_mer.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_mer")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_gio.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_gio")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_ven.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_ven")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_sab.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_sab")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_dom.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_dom")
								#end
								</p>
							</div>
						</div>
						#end
						#if ($content.orari.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<span class="mt24 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_orari")</span>
								$content.orari.text
								
								#if ($content.agg_cal.booleanValue)
									<script type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js" async defer></script>
									<div title="$i18n.getLabel("CITTAMETRO_CONTENT_EVN_agg_cal")" class="addeventatc btn btn-default btn-celeste" data-styling="none" role="button">
										<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add_circle"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVN_agg_cal")
										<span class="start">$content.data_iniz.getFormattedDate("yyyy/MM/dd") $content.data_iniz.getFormattedDate("HH:mm")</span>
										<span class="end">$content.data_fine.getFormattedDate("yyyy/MM/dd") $content.data_fine.getFormattedDate("HH:mm")</span>
										<span class="timezone">Europe/Rome</span>
										<span class="title">$content.titolo.text</span>
										<span class="description">$content.descr.text</span>
									</div>
								#end
							</div>
						</div>
						#end	
						#if ($content.prezzo.size() > 0)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-prezzo"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_prezzo")</h4>
								</div>
							</div>
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									#set ($giagratuito = 0)
									#foreach ($biglietto in $content.prezzo)
										#if (($biglietto.tipo.text != "Gratuito") || ($giagratuito == 0))
											<div class="callout sp callout-highlight">
												#if ($biglietto.tipo.text == "Gratuito")
													<div class="callout-title"><strong>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_gratis")</strong></div>
													#set ($giagratuito = 1)
												#else
													#set ($costoappo = $biglietto.costo.text)
													#set ($numerovirgola = $costoappo.length() - 3)
													#set ($decimali = $costoappo.substring($numerovirgola,$costoappo.length()))
													#set ($numerofinale = $decimali)
													#set ($numerocar = $numerovirgola - 1)
													#set ($cicli = 0)
													#foreach ($i in [$numerocar..0])
														#if ($cicli == 3)
															#set ($numerofinale = "." + $numerofinale)
															#set ($cicli = 0)
														#end
														#set ($numerofinale = $costoappo.charAt($i) + $numerofinale)
														#set ($cicli = $cicli + 1)
													#end
													<div class="callout-title"><span>$biglietto.tipo.text</span><br /><strong>$numerofinale</strong> euro</div>
												#end
												#if ($biglietto.descr.text != '''')
													$biglietto.descr.text
												#end
											</div>
										#end
									#end
								</div>
							</div>
						#end
						#if ($content.info_bigl.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									$content.info_bigl.text
								</div>
							</div>
						#end
						#if ($content.link_bigl.destination != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									<p class="mt16">
										<a class="btn btn-default btn-iscriviti" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.link_bigl.text" href="$content.link_bigl.destination">
											$content.link_bigl.text
										</a>
									</p>
								</div>
							</div>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-documenti"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_documenti")</h4>
								</div>
							</div>
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									#set ($contadoc = 1)
									#foreach ($allegato in $content.documenti)
										#if ($contadoc % 2 != 0)
											<div class="row allegati-riga">
										#end
											#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
												<div class="col-md-6">
													<article class="allegato">
														<div class="scheda-allegato">
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#if ($allegato.documento.attachPath != '''')
																	#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
																	#set ($last = $fileNameSplit.size() - 1)
																	#set ($fileExt = $fileNameSplit[$last])
																	<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																		$allegato.documento.text
																	</a>
																#end
																#if ($allegato.link.destination != '''')
																<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.link.text">
																	$allegato.link.text
																</a>
																#end
																<br />
																<span>$allegato.descr.text</span>
															</h4>
														</div>
													</article>
												</div>
												#set ($contadoc = $contadoc + 1)
											#end
										#if ($contadoc % 2 != 0 || $foreach.count == $content.documenti.size())
											</div>
										#end
									#end
								</div>
							</div>
						#end
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-contatti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_contatti")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">
								<div class="row">
									#if ($content.organizz.link_uff.destination != "")
										#set($linkScheda = $content.organizz.link_uff.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("ORG"))
											<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''220003'')" data-ng-bind-html="renderContent[''$linkScheda''][''220003'']"></div>
										#end
									#else
										<div class="col-md-9">
											<article class="scheda-ufficio-contatti scheda-round">
												<div class="scheda-ufficio-testo">
													#if ($content.organizz.soggetto.text != "")
														<h4 class="mb24"><a>$content.organizz.soggetto.text</a></h4>
													#end
													#if (($content.organizz.persona.text != "") || ($content.organizz.telefono.text != "") || ($content.organizz.reperib.text != "") || ($content.organizz.email.text != "") || ($content.organizz.sito_web.destination != ""))
														<p>
															#if ($content.organizz.persona.text != "")
																$i18n.getLabel("CITTAMETRO_CONTENT_EVN_organizz_referente"): $content.organizz.persona.text<br />
															#end
															#if ($content.organizz.telefono.text != "")
																$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.organizz.telefono.text<br />
															#end
															#if ($content.organizz.reperib.text != "")
																$i18n.getLabel("CITTAMETRO_CONTENT_EVN_organizz_reperib"): $content.organizz.reperib.text<br />
															#end
															#if ($content.organizz.email.text != "")
																$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.organizz.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.organizz.email.text</a><br />
															#end
															#if ($content.organizz.sito_web.destination != "")
																$i18n.getLabel("CITTAMETRO_PORTAL_WEBSITE"): <a href="$content.organizz.sito_web.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.organizz.sito_web.text">$content.organizz.sito_web.text</a>
															#end
														</p>
													#end
												</div>
											</article>
										</div>
									#end
								</div>
							</div>
						</div>
						#if ($content.supporto.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_supporto")</p>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">
								<div class="row">
									#set($linkScheda = $content.supporto.destination.replaceAll("#|C;|!",""))
									#if ($linkScheda.contains("ORG"))
										<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''220003'')" data-ng-bind-html="renderContent[''$linkScheda''][''220003'']"></div>
									#end
								</div>
							</div>
						</div>
						#end
						#if ($content.figli && $content.figli.size() > 0)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-figli"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_figli")</h4>
								</div>
							</div>
							<div class="row">
								<div class="offset-md-1 col-md-11 lista-eventi-figli">
									<div class="row">
									#foreach ($figlio in $content.figli)
										#set($linkEvento = $figlio.destination.replaceAll("#|C;|!",""))
										#if ($linkEvento.contains("EVN"))
											<div class="col-lg-6 col-md-12 mb16" data-ng-init="getContent(''$linkEvento'',''210002'')" data-ng-bind-html="renderContent[''$linkEvento''][''210002'']"></div>
										#end
									#end
									</div>
								</div>
							</div>
						#end
						#if ($content.parte_di.destination != "" && ! $content.genitore.booleanValue)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-parte_di"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_parte_di")</h4>
								</div>
							</div>
							<div class="row">
								<div class="offset-md-1 col-md-11 lista-eventi-figli">
									<div class="row">
										#set($linkEvento = $content.parte_di.destination.replaceAll("#|C;|!",""))
										#if ($linkEvento.contains("EVN"))
											<div class="col-lg-6 col-md-12 mb16" data-ng-init="getContent(''$linkEvento'',''210002'')" data-ng-bind-html="renderContent[''$linkEvento''][''210002'']"></div>
										#end
									</div>
								</div>
							</div>
						#end
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-info"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
							</div>
						</div>
						#if ($content.ult_info.text != "")
							<div class="row articolo-ulterioriinfo">
								<div class="offset-md-1 col-md-8">
									$content.ult_info.text
								</div>
							</div>
						#end
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-11">
								#if ($content.patro_den.text != "" || $content.patro_link.destination != "")
									<p>
										<strong>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_patro"):</strong><br />
										#if ($content.patro_link.destination != "")
											<a href="$content.patro_link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.patro_link.text" class="scheda-logo">
										#end
										#if ($content.patro_logo.getImagePath("2") != "")
											<img src="$content.patro_logo.getImagePath("2")" alt="$content.patro_logo.text" />
										#else
											#if ($content.patro_den.text != "")
												$content.patro_den.text
											#else
												$content.patro_link.text
											#end
										#end
										#if ($content.patro_link.destination != "")
											</a>
										#end
									</p>
								#end
								#if ($content.sponsor && $content.sponsor.size() > 0)
									<p class="mt32">
										<strong>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_sponsor"):</strong><br />
										#foreach ($spons in $content.sponsor)
											#if ($spons.sitoweb.destination != "")
												<a href="$spons.sitoweb.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $spons.sitoweb.text" class="scheda-logo-piccolo">
											#end
											#if ($spons.logo.getImagePath("2") != "")
												<img src="$spons.logo.getImagePath("2")" alt="$spons.logo.text" />
											#else
												#if ($spons.denominaz.text != "")
													$spons.denominaz.text
												#else
													$spons.sitoweb.text
												#end
											#end
											#if ($spons.sitoweb.destination != "")
												</a>
											#end
										#end
									</p>
								#end
								#if ($content.str_polit && $content.str_polit.size() > 0)
									<p class="mt32 mb0"><strong>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_str_polit")</strong></p>
									<div class="listaluoghi">
										<div class="row">
											#foreach ($pol in $content.str_polit)
												<div class="col-lg-6">
													<article class="scheda-ufficio-contatti scheda-round">
														<div class="scheda-ufficio-testo">
															#if ($pol.pol_scheda.destination != "")
																<h4><a href="$pol.pol_scheda.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $pol.pol_scheda.text">$pol.pol_scheda.text</a></h4>
															#else
																<h4><a>$pol.pol_nome.text</a></h4>
															#end
														</div>
													</article>
												</div>
											#end
										</div>
									</div>
								#end
								
								#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
									<div class="callout important sp">
										<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
										#foreach ($aiuto in $content.box_aiuto)
											#if ($aiuto.testo.text != "")
												$aiuto.testo.text
											#end
											#if ($aiuto.link.destination != '''')
												<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
											#end
										#end
									</div>
								#end
								<div class="row">
									<div class="col-md-12 mt16">
										<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
										<p class="data-articolo">
											<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_MIGHT_INTERESTED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210014,'EVN','Homepage - In evidenza (cat+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $content.tipologia.mapValue
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLNOVITA")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210008,'EVN','Sezioni - In evidenza (img+cat+titolo+abs+arg)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
			</figure>
			#if ($content.data_iniz != "")
				<div class="scheda-data">
					<strong>$content.data_iniz.getFormattedDate("d")</strong>
					$content.data_iniz.getFormattedDate("MMM")
				</div>
			#end
		</div>
	#end
	<div class="scheda-icona-small">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVENTS")
	</div>
	<div class="scheda-testo">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	#if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
		</div>
	#end
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210011,'EVN','Sezioni - In evidenza (img+titolo+abs+arg)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($sezionePage = ''novita'')
<article class="scheda scheda-round scheda-news">
	#if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
				<figure>
					<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
				</figure>
				#if ($content.data_iniz != "")
					<div class="scheda-data">
						<strong>$content.data_iniz.getFormattedDate("d")</strong>
						$content.data_iniz.getFormattedDate("MMM")
					</div>
				#end
		</div>
	#end
	<div class="scheda-testo mt32">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>	
	<div class="scheda-argomenti">
		#set ($argomenti = $content.argomenti.values)
		#foreach ($Category in $content.getCategories())
			#if($argomenti.contains($Category.code))
				<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
			#end
		#end
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210013,'EVN','Sezioni - In evidenza (img+cat+dt+titolo+abs+arg)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
			</figure>
			#if ($content.data_iniz != "")
				<div class="scheda-data">
					<strong>$content.data_iniz.getFormattedDate("d")</strong>
					$content.data_iniz.getFormattedDate("MMM")
				</div>
			#end
		</div>
	#end
	<div class="scheda-icona-small #if ($content.foto.getImagePath("0") != "") mr114 #end">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVENTS")
	</div>
	<div class="scheda-testo #if($content.immagine.getImagePath("0") == "")mr-0#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
		<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
		</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (210012,'EVN','Sezioni - In evidenza (cat+data+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVENTS")
  	- $content.data_iniz.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (300001,'GAL','Novità (slide)','
#if ($content.immagini.size() > 0)
	<div id="owl-galleria" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
		#foreach ($elemento in $content.immagini)
			<figure>
				<div class="galleria-foto">
					<a href="$elemento.immagine.getImagePath("0")" class="venobox" data-gall="gallery-$content.getId()" data-title="$elemento.immagine.text">
						<img src="$elemento.immagine.getImagePath("3")" alt="$elemento.testo.text" class="img-fluid">
					</a>
				</div>
				<figcaption>$elemento.immagine.text</figcaption>
			</figure>
		#end
	</div>
#end

<script>
$(document).ready(function () {
	var owl2 = $(''#owl-galleria'');
	owl2.owlCarousel({
		nav:false,
		startPosition: 0,
		autoPlay:false,
		responsiveClass:true,
		responsive:{
				0:{
					items:1,
				},
				576: {
					items:2,
				},
				768: {
					items:2,
				},
				991: {
					items:3,
				},
			}
	});
	
	$(''.venobox'').venobox({
		titleattr: ''data-title''
	});
});
</script>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120004,'LGO','Servizio e Evento (ico+tit+ind)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
	<article class="scheda-ufficio-contatti scheda-round">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">
			<h4>
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
				<br /><span>$content.indirizzo.text</span>
			</h4>
    </div>
	</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120009,'LGO','Lista (titolo+indirizzo+img)','
<div class="scheda-gestione">
	<p>
		<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
			<strong>$content.titolo.text</strong>
		</a><br />
		<span>
    #if ($content.indirizzo.text != "")
       $content.indirizzo.text
    #end
    </span>
	</p>
  #if ($content.immagine.getImagePath("0") != "")
  	<div class="thumbs-round img-fit-cover"> 
			<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
    </div>
  #end
</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120013,'LGO','Argomento (ico+cat+titolo+indirizzo+img)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small mr130">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
	</div>
	<div class="scheda-testo">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
 		#if ($content.indirizzo.text != "")
       $content.indirizzo.text
    #end
	</div>
	<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120003,'LGO','Lista ricerca (ico + cat + titolo + abs + link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.tipologia.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120010,'LGO','Luogo padre (ico+cat+indirizzo+titolo+img)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<div class="col-lg-6 col-md-12 mb16">
	<article class="scheda scheda-evento scheda-round">
		<div class="scheda-icona-small mr130">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue<br />
		</div>
		<div class="scheda-testo">
			<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $content.titolo.text">$content.titolo.text</a></h4>
		</div>
    #if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("4")" alt="$content.immagine.text" />
 			</figure>
		</div>
    #end
	</article>
</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120005,'LGO','Indirizzo','
$content.indirizzo.text
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120007,'LGO','Organizzazione (ico+tit+ind+contatti)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
			<h4 class="mb24">
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
        <br />
        <span>
        	$content.indirizzo.text
        	#if ($content.cap.text != "")
      		- $content.cap.text
        	#end
        </span>
			</h4>
      #if ($content.orario.text != "")
      $content.orario.text
      #end
      <p>
      	#if ($content.telefono.text != "")
      	    $i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
        #end
        #if ($content.email != "")
            $i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAILTO") $content.email.text">$content.email.text</a>
        #end
      </p>

','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120006,'LGO','Contatti (tel+email)','
#if ($content.telefono.text != "")
  $i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
#end
#if ($content.email != "")
    $i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAILTO") $content.email.text">$content.email.text</a>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120001,'LGO','Sezioni - In evidenza (cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120011,'LGO','Luogo figlio','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
  <article class="scheda-ufficio-contatti scheda-round mt8">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">
      <h4>
      	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
    </div>
  </article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120012,'LGO','Titolo','
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120002,'LGO','Sezioni - In evidenza (ico+cat+titoletto+abs+lnk)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
  <div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100001,'NVT','Dettaglio completo','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<section id="intro" data-ng-controller="FiltriController">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
						#if ($content.persone && $content.persone.size() > 0)
							<div class="mt16"></div>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PEOPLE")</h4>
							<div class="argomenti-sezione-elenco persone" data-ng-cloak>
								#foreach ($persona in $content.persone)
									#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
									<span data-ng-init="getContent(''$linkDipendente'',''230007'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230007'']"></span>
								#end
							</div>
						#end
					</div>
				</aside>
			</div>
		</div>
		<div class="row mt40">
			<div class="offset-xl-1 col-xl-2 offset-lg-1 col-lg-3 col-md-3">
				<p class="data-articolo"><span>$i18n.getLabel("CITTAMETRO_CONTENT_DATE"):</span><br /><strong>$content.data.getFormattedDate("dd MMMM yyyy")</strong></p>
			</div>
			<div class="offset-xl-1 col-xl-2 offset-lg-1 col-lg-3 offset-md-1 col-md-3">
				
			</div>
		</div>
	</div>
</section>
#if ($content.dim_foto.booleanValue)
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
	<figure>
		<img src="$content.foto.getImagePath("0")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
		<figcaption>$content.foto.text</figcaption>
	</figure>
  </div>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_NVT_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_NVT_documenti")</a>
						#end
						#if ($content.luoghi && $content.luoghi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-luoghi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_NVT_luoghi")">$i18n.getLabel("CITTAMETRO_CONTENT_NVT_luoghi")</a>
						#end
						#if ($content.link_est && $content.link_est.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-linkesterni" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_NVT_link_est")">$i18n.getLabel("CITTAMETRO_CONTENT_NVT_link_est")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8 mt24">
				<div class="articolo-paragrafi">
					#if ($content.testo.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.testo.text
							</div>
						</div>
					#end
					#if ($content.video.destination == "")
						#if (! $content.dim_foto.booleanValue)
							<div class="row">
								<div class="offset-lg-1 col-lg-11 col-md-12 articolo-foto-interna">
									<figure>
										<img src="$content.foto.getImagePath("5")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
										<figcaption>$content.foto.text</figcaption>
									</figure>
								</div>
							</div>
						#end
					#end
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 mt32">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 mt32">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.video.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="videoplayer">
									#set($linkVideo = $content.video.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_NVT_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
										<div class="col-md-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($allegato.documento.attachPath != '''')
															#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																$allegato.documento.text
															</a>
														#end
														#if ($allegato.link.destination != '''')
														<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.link.text">
															$allegato.link.text
														</a>
														#end
														<br />
														<span>$allegato.descr.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.luoghi && $content.luoghi.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-luoghi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_NVT_luoghi")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 lista-eventi-figli">
								<div class="row">
								#foreach ($luogo in $content.luoghi)
									#set($linkLuogo = $luogo.destination.replaceAll("#|C;|!",""))
									#if ($linkLuogo.contains("LGO"))
										<div class="col-md-6">
											<div data-ng-init="getContent(''$linkLuogo'',''120009'')" data-ng-bind-html="renderContent[''$linkLuogo''][''120009'']"></div>
										</div>
									#end
								#end
								</div>
							</div>
						</div>
					#end
					#if ($content.link_est && $content.link_est.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-linkesterni"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_NVT_link_est")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-11">
								#set ($contalink = 1)
								#foreach ($altrilink in $content.link_est)
									#if ($altrilink.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$altrilink.destination" #if($altrilink.symbolicLink.destType == 1)target="_blank"  title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $altrilink.text" #else title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $altrilink.text"  #end >
                        <strong>$altrilink.text</strong></a>
											</article>
										</div>
										#set ($contalink = $contalink + 1)
									#end
									#if ($contalink % 2 != 0 || $foreach.count==$content.link_est.size())
										</div>
									#end
								#end
								</p>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" #if($aiuto.link.symbolicLink.destType == 1)target="_blank" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $aiuto.link.text" #else title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $aiuto.link.text" #end > 
                      <strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Servizi">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Novità">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Documenti">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (120008,'LGO','Dettaglio completo','<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						#if ($content.parte_di.destination != "")
							<li class="breadcrumb-item"><a href="$content.parte_di.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.parte_di.text"><strong>$content.parte_di.text</strong></a><span class="separator">/</span></li>
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#else
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#end
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>

#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					#if($content.sottotit.text != "" && $content.tipologia.mapKey != "lgo_t22")
					<h4>$content.sottotit.text</h4>
					#end
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>											
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
	<figure>
		<img src="$content.immagine.getImagePath("0")" alt="$content.immagine.text" class="img-fluid" />
		<figcaption>$content.immagine.text</figcaption>
	</figure>
  </div>
</section>
#end
<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.descrizion.text != "" || $content.interesse.size() > 0 || $content.galleria.destination != "" || $content.video.destination != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-descrizione" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_LGO_descrizione")">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_descrizione")</a>
						#end
						#if ($content.servizi && $content.servizi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_LGO_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_servizi")</a>
						#end
						#if ($content.accesso.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-accesso" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_LGO_accesso")">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_accesso")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-dove" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_LGO_dove")">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_dove")</a>
						#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != ""))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-orari" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_LGO_orari")">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_orari")</a>
						#end
						#if ($content.s_str_link.destination != "" || $content.s_str_nome.text != "" || $content.s_str_tel.text != "" || $content.s_str_mail.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-contatti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_LGO_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_contatti")</a>
						#end
						#if ($content.prezzo.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-prezzo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_LGO_prezzo")">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_prezzo")</a>
						#end
						#if ($content.parte_di.destination != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-parte_di" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_LGO_parte_di")">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_parte_di")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.descrizion.text != "" || $content.interesse.size() > 0 || $content.galleria.destination != "" || $content.video.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-descrizione"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_LGO_descrizione")</h4>
							</div>
						</div>
						#if ($content.descrizion.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									$content.descrizion.text
								</div>
							</div>
						#end
						#if ($content.interesse.size() > 0)
							<div class="row">
								<div class="offset-md-1 col-md-11 listaluoghi" data-ng-cloak data-ng-controller="FiltriController">
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_interesse")</p>
									<div class="row">
										#foreach ($item in $content.interesse)
											#if ($item.sch_luogo.destination != "")
												#set($linkLgo = $item.sch_luogo.destination.replaceAll("#|C;|!",""))
												<div class="col-md-6" data-ng-init="getContent(''$linkLgo'',''120011'')" data-ng-bind-html="renderContent[''$linkLgo''][''120011'']"></div>
											#else 
												<div class="col-md-6">
													<article class="scheda-ufficio-contatti scheda-round mt8">
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
														<div class="scheda-ufficio-testo">
															<h4><a>$item.titolo.text</a></h4>
														</div>
													</article>
												</div>
											#end
										#end
									</div>
								</div>
							</div>
						#end
						#if ($content.galleria.destination != "")
							<div class="row">
								<div class="col-md-12 mt32">
									<div class="galleriasfondo"></div>
								</div>
								<div class="offset-md-1 col-md-11 mt32">
									<div class="galleriaslide" data-ng-cloak data-ng-controller="FiltriController">
										<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
											#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
									</div>
								</div>
							</div>
						#end
						#if ($content.video.destination != "")
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo" data-ng-cloak data-ng-controller="FiltriController">
									<div class="videoplayer">
										#set($linkVideo = $content.video.destination.replaceAll("#|C;|!",""))
										<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
									</div>
								</div>
							</div>
						#end
					#end
					#if ($content.servizi && $content.servizi.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_LGO_servizi")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#foreach ($servizio in $content.servizi)
									#if ($foreach.count % 2 != 0)
										<div class="row row-eq-height">
									#end
									<div class="col-md-6" data-ng-cloak data-ng-controller="FiltriController">
										#set($linkServizio = $servizio.srv_link.destination.replaceAll("#|C;|!",""))
										#if ($linkServizio.contains("SRV"))
											<div data-ng-init="getContent(''$linkServizio'',''800001'')" data-ng-bind-html="renderContent[''$linkServizio''][''800001'']"></div>
										#else
											#set($linkServizio = $servizio.srv_link.destination.replaceAll("#|U;|!",""))
											<article class="scheda scheda-round">
												<div class="scheda-icona-small">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
												</div>
												<div class="scheda-testo-small">
													<h4><a href="$linkServizio" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $servizio.srv_link.text">$servizio.srv_link.text</a></h4>
													<p>$servizio.srv_descr.text</p>
												</div>
											</article>
										#end
									</div>
									#if ($foreach.count % 2 == 0 || $foreach.count == $content.servizi.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.accesso.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-accesso"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_LGO_accesso")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.accesso.text
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-dove"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_LGO_dove")</h4>
						</div>
					</div>
					<div class="row">
						<div class="offset-md-1 col-md-8 listaluoghi" data-ng-cloak data-ng-controller="FiltriController">
							<div class="row">
								<div class="col-md-12">
									<article class="scheda-ufficio-contatti scheda-round">
										<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-place"></use></svg>
										<div class="scheda-ufficio-testo">
											<h4 class="#if (($content.rif_tel.text != "") || ($content.rif_email.text != "")) mb24 #end">
												#if ($content.parte_di.destination != "")
													#set($linkPadre = $content.parte_di.destination.replaceAll("#|C;|!",""))
													#if ($linkPadre.contains("LGO"))
														<div data-ng-init="getContent(''$linkPadre'',''120012'')" data-ng-bind-html="renderContent[''$linkPadre''][''120012'']"></div>
													#end
												#else 
													<a>$content.titolo.text</a>
												#end
												<br /><span>
												#if ($content.indirizzo.text != "")
													$content.indirizzo.text
												#end 
												#if ($content.cap.text != "")
													#if ($content.indirizzo.text != "")-#end $content.cap.text
												#end
												</span>
											</h4>
											#if (($content.rif_tel.text != "") || ($content.rif_email.text != ""))
											<p>	
												#if ($content.rif_tel.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.rif_tel.text
												#end
												#if ($content.rif_email.text != "")
													#if ($content.rif_tel.text != "")<br />#end
													$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.rif_email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.rif_email.text</a>
												#end
												#if ($content.rif_web.destination != "")
													#if ($content.rif_tel.text != "" || $content.rif_email.text != "")<br />#end
													$i18n.getLabel("CITTAMETRO_PORTAL_WEBSITE"): <a href="$content.rif_web.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.rif_web.text">$content.rif_web.text</a>
												#end
											</p>
											#end
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
					#if ($content.gps_lat.text != "" && $content.gps_lat.text != "-")
						<div id="schedamappa" class="mt32" style="height:368px"></div>
						<div id="schedamappah" class="mt32"></div>
						<script nonce="$content.nonce">
							var latIn =  $content.gps_lat.text;
							var lngIn =  $content.gps_lon.text;
							var fscala =  "15";
							var mapCa = L.map(''schedamappa'', {
							  layers:[OSM]
							});
							var baseMaps = {
								"CartoDB": CartoDB,
								"Open Street Map": OSM,
								"Google Sat": Google,
								"Esri Sat": Esri_sat
							};
							var icona = L.icon({
								iconUrl: ''$imgURL/marker-icon.png'',
								iconSize: [25, 34],
								iconAnchor: [13, 34],
								popupAnchor: [-3, -76],
								shadowUrl: ''$imgURL/marker-shadow.png'',
								shadowSize: [41, 34],
								shadowAnchor: [13, 34]
							});
							mapCa.setView([latIn, lngIn], fscala);
							L.marker([latIn, lngIn], {icon: icona}).addTo(mapCa);
						</script>
					#end
					
					#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != ""))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-orari"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_LGO_orari")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p>
									#set($prec = false)
									#if ($info.getConfigParameter("orario_invernale") == ''true'')
										#if ($content.orario.inv_matt.text != "")
											#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
											$orarioinvmatt
											#set($prec = true)
										#end
										#if ($content.orario.inv_pom.text != "")
											#if ($prec)<br />#end
											#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
											$orarioinvpom
											#set($prec = true)
										#end
									#else
										#if ($content.orario.est_matt.text != "")
											#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
											$orarioestmatt
											#set($prec = true)
										#else 
											#if ($content.orario.inv_matt.text != "")
												#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
												$orarioinvmatt														
												#set($prec = true)
											#end
										#end
										#if ($content.orario.est_pom.text != "")
											#if ($prec)<br />#end														
											#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
											$orarioestpom
											#set($prec = true)
										#else
											#if ($content.orario.inv_pom.text != "")
												#if ($prec)<br />#end
												#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
												$orarioinvpom
												#set($prec = true)
											#end
										#end
									#end
									#if ($content.orario.continuo.text != "")
										#if ($prec)<br />#end
										#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
										$orariocontinuo													
									#end
								</p>
							</div>
						</div>
					#end
					#if ($content.s_str_link.destination != "" || $content.s_str_nome.text != "" || $content.s_str_tel.text != "" || $content.s_str_mail.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-contatti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_LGO_contatti")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo" data-ng-cloak data-ng-controller="FiltriController">
								#if ($content.s_str_link.destination != "")
									<div class="row">
										<div class="col-md-12">
											<article class="scheda-ufficio-contatti scheda-round mt12 mb0">
												<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
												<div class="scheda-ufficio-testo">
													#set($linkOrg = $content.s_str_link.destination.replaceAll("#|C;|!",""))
													#if ($linkOrg.contains("ORG"))
														<div data-ng-init="getContent(''$linkOrg'',''220016'')" data-ng-bind-html="renderContent[''$linkOrg''][''220016'']"></div>
													#end
												</div>
											</article>
										</div>
									</div>
								#else
									<div class="row">
										<div class="col-md-12">
											<article class="scheda-ufficio-contatti scheda-round mt12 mb0">
												<div class="scheda-ufficio-testo">
													<h4>
														<a>$content.s_str_nome.text</a>
													</h4>
													#if (($content.s_str_tel.text != "") || ($content.s_str_mail.text != ""))
														<p>	
															#if ($content.s_str_tel.text != "")
																$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.s_str_tel.text<br />
															#end
															#if ($content.s_str_mail.text != "")
																$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.s_str_mail.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.s_str_mail.text</a>
															#end	
														</p>
													#end
												</div>
											</article>
										</div>
									</div>
								#end
							</div>
						</div>
					#end
					#if ($content.prezzo.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-prezzo"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_LGO_prezzo")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								#set ($giagratuito = 0)
								#foreach ($biglietto in $content.prezzo)
									#if (($biglietto.tipo.text != "Gratuito") || ($giagratuito == 0))
										<div class="callout sp callout-highlight">
											#if ($biglietto.tipo.text == "Gratuito")
												<div class="callout-title"><strong>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_gratis")</strong></div>
												#set ($giagratuito = 1)
											#else
												#set ($costoappo = $biglietto.costo.text)
												#set ($numerovirgola = $costoappo.length() - 3)
												#set ($decimali = $costoappo.substring($numerovirgola,$costoappo.length()))
												#set ($numerofinale = $decimali)
												#set ($numerocar = $numerovirgola - 1)
												#set ($cicli = 0)
												#foreach ($i in [$numerocar..0])
													#if ($cicli == 3)
														#set ($numerofinale = "." + $numerofinale)
														#set ($cicli = 0)
													#end
													#set ($numerofinale = $costoappo.charAt($i) + $numerofinale)
													#set ($cicli = $cicli + 1)
												#end
												<div class="callout-title"><span>$biglietto.tipo.text</span><br /><strong>$numerofinale</strong> euro</div>
											#end
											#if ($biglietto.descr.text != '''')
												$biglietto.descr.text
											#end
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.info_bigl.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.info_bigl.text
							</div>
						</div>
					#end
					#if ($content.link_bigl.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p class="mt16">
									<a class="btn btn-default btn-iscriviti" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.link_bigl.text" href="$content.link_bigl.destination">
										$content.link_bigl.text
									</a>
								</p>
							</div>
						</div>
					#end
					#if ($content.parte_di.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-parte_di"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_LGO_parte_di")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 lista-eventi-figli" data-ng-cloak data-ng-controller="FiltriController">
								#set($linkPadre = $content.parte_di.destination.replaceAll("#|C;|!",""))
								#if ($linkPadre.contains("LGO"))
									<div class="row" data-ng-init="getContent(''$linkPadre'',''120010'')" data-ng-bind-html="renderContent[''$linkPadre''][''120010'']"></div>
								#end
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.sede_di.destination != "")
						<div class="row articolo-ulterioriinfo" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-8">
								<p class="mb0">$i18n.getLabel("CITTAMETRO_CONTENT_LGO_sede_di"):</p>
								#set($linkScheda = $content.sede_di.destination.replaceAll("#|C;|!",""))
								#if ($linkScheda.contains("ORG"))
									<div class="row" data-ng-init="getContent(''$linkScheda'',''220014'')" data-ng-bind-html="renderContent[''$linkScheda''][''220014'']"></div>
								#end
							</div>
						</div>
					#end
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end						
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Servizi">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Novità">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Documenti">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
',NULL);
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100013,'NVT','Lista ricerca (ico + cat + titolo + abs + link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.tipologia.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    <svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
		#elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
		</svg>
    <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100005,'NVT','Sezioni - In evidenza (titolo+abs)','
<article class="scheda scheda-round scheda-news">	
	<div class="scheda-testo mt32 no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100010,'NVT','Homepage - Argomenti (cat+titolo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">
		<svg class="icon">
  	#if ($content.tipologia.mapKey != "nov01")
    	<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
    #elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
		#end
	$content.titolo.text
</a>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100008,'NVT','Sezioni - In evidenza (img+cat+dt+titolo+abs+arg)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.foto.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
			</figure>
		</div>
	#end
	<div class="scheda-icona-small #if ($content.foto.getImagePath("0") != "") mr114 #end">
		<svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
		#elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
		</svg>
		$content.tipologia.mapValue - $content.data.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo #if($content.foto.getImagePath("0") == "")mr-0#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100006,'NVT','Sezioni - In evidenza (img+titolo+abs+arg)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.foto.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
			</figure>
		</div>
	#end
	<div class="scheda-testo mt32 #if ($content.foto.getImagePath("0") == "")no-mr-xs no-mr-sm#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>	
	<div class="scheda-argomenti">
		#set ($argomenti = $content.argomenti.values)
		#foreach ($Category in $content.getCategories())
			#if($argomenti.contains($Category.code))
				<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
			#end
		#end
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100004,'NVT','Sezioni - In evidenza (cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small  no-mr-sm no-mr-xs">
		<svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
    	<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
    #elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
    </svg> $content.tipologia.mapValue
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100009,'NVT','Homepage - In evidenza (cat+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
      <svg class="icon">
      #if ($content.tipologia.mapKey != "nov01")
        <use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
      #elseif ($content.tipologia.mapKey != "nov02")
        <use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
      #end
      </svg>
      $content.tipologia.mapValue
      </a>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLNOVITA")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100007,'NVT','Sezioni - In evidenza (cat+data+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
		#if ($content.tipologia.mapKey != "nov01")
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
    #elseif ($content.tipologia.mapKey != "nov02")
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
		#end
    $content.tipologia.mapValue - $content.data.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100003,'NVT','Sezioni - In evidenza (img+cat+titolo+abs+arg)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.foto.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
			</figure>
		</div>
	#end
	<div class="scheda-icona-small">
		<svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
		#elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
		</svg> $content.tipologia.mapValue
	</div>

	<div class="scheda-testo #if ($content.foto.getImagePath("0") == "")no-mr-xs no-mr-sm#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
		<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
		</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (134567,'NVT','Template per email','
#if($content.titolo && $content.titolo.text != "")
		<p>
			$content.titolo.text
		</p>
    #if($content.testo.text)
    <div>
			$content.testo.text
    </div>
    #end
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100012,'NVT','Homepage - Allerta meteo','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<div class="allerta-meteo allerta$content.allerta.mapKey">
	<article>
  	<div class="container">
			<div class="row">
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
        	<img src="$imgURL/protezione_civile.png" alt="$i18n.getLabel("CITTAMETRO_PORTAL_PROTEZIONE_CIVILE")" class="img-fluid" />
        </div>
        <div class="col-lg-11 col-md-10 col-sm-10 col-xs-9">
        	<h2><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h2>
 					<p>$content.descr.text</p>
        </div>
       </div>
      </div>
  </article>
</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100002,'NVT','Homepage - Notizia principale (titolo+abs+arg+img)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	#if ($content.foto.getImagePath("0") != "")
	<div class="novita-foto img-fit-cover"> 
		<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" />
	</div>
  #end
	<div class="novita-testo">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">$content.titolo.text</a></h2>
					<p>$content.descr.text</p>
					<div class="argomenti">
						#set ($argomenti = $content.argomenti.values)
						#foreach ($Category in $content.getCategories())
							#if($argomenti.contains($Category.code))
								#set($argPage = $info.getConfigParameter("pagina_argomento"))
								<a href="$content.getPageURL($argPage)?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
							#end
						#end
					</div>
				<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_ALLNEWS")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLNEWS") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
				</div>
			</div>
		</div>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (100014,'NVT','*Sezioni - In evidenza (img+ico+dt+titolo+abs+arg)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.foto.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
			</figure>
		</div>
	#end
	<div class="scheda-icona-small #if ($content.foto.getImagePath("0") != "") mr114 #end">
		<svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
		#elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
		</svg>
		$content.data.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo #if($content.foto.getImagePath("0") == "")mr-0#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220014,'ORG','Luogo (ico+titolo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<div class="col-md-9">
	<article class="scheda-ufficio-contatti scheda-round mt8">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">
			<h4>
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
			</h4>
    </div>
	</article>
</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220005,'ORG','Gestione (ico+titolo+indirizzo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda-ufficio-contatti scheda-round barretta-verde">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">    
     <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
       <strong>$content.titolo.text</strong>
     </a><br />
     <span>
       #if ($content.indirizzo.text != "")
         $content.indirizzo.text
       #end
       #if ($content.cap.text != "" && $content.cap.text != "-")
         - $content.cap.text
       #end
       #if ($content.ind_altro.text != "")
         <br />$content.ind_altro.text
       #end
     </span>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220013,'ORG','Intro organizzazione (titolo)','
<article class="scheda scheda-brick scheda-round">
	<div class="scheda-testo mt16">
		<h4>
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
    		$content.titolo.text
      </a>
    </h4>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220017,'ORG','Titolo (titolo+link)','
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $content.titolo.text">$content.titolo.text</a>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220011,'ORG','Dettaglio completo - card + badge','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.competenze.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-competenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</a>
						#end
						#if (($content.legami && $content.legami.size() > 0) || ($content.resp.destination != "") || ($content.assessor.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</a>
						#end
						#if ($content.persone && $content.persone.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-persone" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</a>
						#end
						#if ($content.tipologia.mapKey.equals("amm_02") || ($content.tipologia.mapKey.equals("amm_03")))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-sedi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</a>
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.competenze.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-competenze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.competenze.text
							</div>
						</div>
					#end
					#if (($content.legami && $content.legami.size() > 0) || ($content.resp.destination != "") || ($content.assessor.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</h4>
							</div>
						</div>						
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								#if ($content.resp.destination != "")
									<div class="row">
										<div class="col-md-6 scheda-persona-singola scheda-staff mt12">
											#set($linkP = $content.resp.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkP'',''230009'')" data-ng-bind-html="renderContent[''$linkP''][''230009'']"></div>
										</div>
									</div>
								#end
								#if ($content.legami && $content.legami.size() > 0)
									<div class="row">
										<div class="col-md-12">
											<article class="scheda-staff">
												<p class="ampio">
													#if ($content.tipologia.mapValue.contains("Aree"))
														$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_uffici")<br />
													#else 
														#if ($content.tipologia.mapValue.contains("Uffici"))
															$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_dipende")<br />
														#else
															$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_altre")<br />
														#end
													#end
													#foreach ($legame in $content.legami)
														#set($linkCont = $legame.destination.replaceAll("#|C;|!",""))
														<span data-ng-init="getContent(''$linkCont'',''220017'')" data-ng-bind-html="renderContent[''$linkCont''][''220017'']"></span>
														<br />
													#end
												</p>
											</article>
										</div>
									</div>
								#end
								#if ($content.assessor.size() > 0)
									<div class="row">
										#if ($content.assessor.size() > 1)
											<div class="col-sm-12 lista-persone">
												<span class="mb8 mt0 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessori")</span>
												<div class="row">
													#foreach ($ass in $content.assessor)
														<div class="col-lg-6 col-md-12 mb16">
															#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
														</div>
													#end
												</div>
											</div>
										#else
											<div class="col-md-6 scheda-persona-singola">
												<span class="mb8 mt12 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessore")</span>
												#foreach ($ass in $content.assessor)
													#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
													<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
												#end
											</div>
										#end
									</div>
								#end
							</div>
						</div>
					#end
					#if ($content.persone && $content.persone.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-persone"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								<div class="argomenti persone mt12">
									#foreach ($persona in $content.persone)
										#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
										<span data-ng-init="getContent(''$linkDipendente'',''230007'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230007'']"></span>
									#end
								</div>
							</div>
						</div>
					#end
					#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo #if ($content.persone && $content.persone.size() > 0)mt20#end">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</h4>
							</div>
						</div>
						#if ($content.tipologia.mapKey.equals("amm_02"))
							#set ($fieldsearch = ''area'')
						#else
							#set ($fieldsearch = ''uffici'')
						#end
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''SRV'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''800002'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''800002'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''800002'')" data-ng-bind-html="renderContent[elem.$][''800002'']"></div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo #if ($content.servizi && $content.servizi.size() > 0)mt8#end">
							<a id="articolo-par-sedi"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</h4>
						</div>
					</div>
					<div class="row">
						<div class="offset-md-1 col-md-11 listaluoghi">	
							<div class="row">
								<div class="col-lg-9 col-md-12">
									<article class="scheda-ufficio-contatti scheda-round">
										#if ($content.sede.destination != "")
											<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
										#end
										<div class="scheda-ufficio-testo">
											<h4 class="mb24">
												#if ($content.sede.destination != "")
													<a href="$content.sede.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.sede.text">$content.sede.text</a><br />
												#end
												#if ($content.indirizzo.text != "" || $content.cap.text != "")
													<span>
													#if ($content.indirizzo.text != "")
														$content.indirizzo.text
													#end
													#if ($content.ind_altro.text != "")
														- $content.ind_altro.text
													#end
													#if ($content.cap.text != "" && $content.cap.text != "-")
														 - $content.cap.text
													#end
													</span>
												#end
											</h4>
											#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
											#end
											#if (($content.telefono.text != "") || ($content.email.text != "") || ($content.pec.text != ""))
											<p>	
												#if ($content.telefono.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
												#end
												#if ($content.email.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
												#end
												#if ($content.pec.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
												#end	
											</p>
											#end
											#if ($content.sch_persone.size() > 0)
											<p>
												#foreach ($gente in $content.sch_persone)
													#if ($gente.destination != "")
														$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="$gente.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $gente.text"><strong>$gente.text</strong></a>
													#end
												#end
											</p>
											#end
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
					
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
										#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#if ($allegato.link.destination != '''')
															<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $allegato.link.text">
																$allegato.link.text
															</a>
															#else
																#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																	$allegato.documento.text
																</a>
															#end
															<br />
															<span>$allegato.descr.text</span>
														</h4>
													</div>
												</article>
											</div>
											#set ($contadoc = $contadoc + 1)
										#end
									#if ($contadoc % 2 != 0 || $foreach.count == $content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end

					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220010,'ORG','Dettaglio completo - card','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.competenze.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-competenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</a>
						#end
						#if (($content.legami && $content.legami.size() > 0) || ($content.resp.destination != "") || ($content.assessor.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</a>
						#end
						#if ($content.persone && $content.persone.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-persone" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</a>
						#end
						#if ($content.tipologia.mapKey.equals("amm_02") || ($content.tipologia.mapKey.equals("amm_03")))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-sedi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</a>
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.competenze.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-competenze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.competenze.text
							</div>
						</div>
					#end
					#if (($content.legami && $content.legami.size() > 0) || ($content.resp.destination != "") || ($content.assessor.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</h4>
							</div>
						</div>						
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								#if ($content.resp.destination != "")
									<div class="row">
										<div class="col-md-6 scheda-persona-singola scheda-staff mt12">
											#set($linkP = $content.resp.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkP'',''230009'')" data-ng-bind-html="renderContent[''$linkP''][''230009'']"></div>
										</div>
									</div>
								#end
								#if ($content.legami && $content.legami.size() > 0)
									<div class="row">
										<div class="col-md-12">
											<article class="scheda-staff">
												<p class="ampio">
													#if ($content.tipologia.mapValue.contains("Aree"))
														$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_uffici")<br />
													#else 
														#if ($content.tipologia.mapValue.contains("Uffici"))
															$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_dipende")<br />
														#else
															$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_altre")<br />
														#end
													#end
													#foreach ($legame in $content.legami)
														#set($linkCont = $legame.destination.replaceAll("#|C;|!",""))
														<span data-ng-init="getContent(''$linkCont'',''220017'')" data-ng-bind-html="renderContent[''$linkCont''][''220017'']"></span>
														<br />
													#end
												</p>
											</article>
										</div>
									</div>
								#end
								#if ($content.assessor.size() > 0)
									<div class="row">
										#if ($content.assessor.size() > 1)
											<div class="col-sm-12 lista-persone">
												<span class="mb8 mt0 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessori")</span>
												<div class="row">
													#foreach ($ass in $content.assessor)
														<div class="col-lg-6 col-md-12 mb16">
															#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
														</div>
													#end
												</div>
											</div>
										#else
											<div class="col-md-6 scheda-persona-singola">
												<span class="mb8 mt12 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessore")</span>
												#foreach ($ass in $content.assessor)
													#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
													<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
												#end
											</div>
										#end
									</div>
								#end
							</div>
						</div>
					#end
					#if ($content.persone && $content.persone.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-persone"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</h4>
							</div>
						</div>
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11 lista-persone">
								<div class="row">
									#foreach ($persona in $content.persone)
										<div class="col-lg-6 col-md-12 mb16">
											#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkDipendente'',''230005'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230005'']"></div>
										</div>
									#end
								</div>
							</div>
						</div>
					#end
					#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo #if ($content.persone && $content.persone.size() > 0)mt20#end">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</h4>
							</div>
						</div>
						#if ($content.tipologia.mapKey.equals("amm_02"))
							#set ($fieldsearch = ''area'')
						#else
							#set ($fieldsearch = ''uffici'')
						#end
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''SRV'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''800002'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''800002'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''800002'')" data-ng-bind-html="renderContent[elem.$][''800002'']"></div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo #if ($content.servizi && $content.servizi.size() > 0)mt8#end">
							<a id="articolo-par-sedi"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</h4>
						</div>
					</div>
					<div class="row">
						<div class="offset-md-1 col-md-11 listaluoghi">	
							<div class="row">
								<div class="col-lg-9 col-md-12">
									<article class="scheda-ufficio-contatti scheda-round">
										#if ($content.sede.destination != "")
											<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
										#end
										<div class="scheda-ufficio-testo">
											<h4 class="mb24">
												#if ($content.sede.destination != "")
													<a href="$content.sede.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.sede.text">$content.sede.text</a><br />
												#end
												#if ($content.indirizzo.text != "" || $content.cap.text != "")
													<span>
													#if ($content.indirizzo.text != "")
														$content.indirizzo.text 
													#end
													#if ($content.ind_altro.text != "")
														- $content.ind_altro.text
													#end
													#if ($content.cap.text != "" && $content.cap.text != "-")
														- $content.cap.text
													#end													
													</span>
												#end
											</h4>
											#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
											#end
											#if (($content.telefono.text != "") || ($content.email.text != "") || ($content.pec.text != ""))
											<p>	
												#if ($content.telefono.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
												#end
												#if ($content.email.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
												#end
												#if ($content.pec.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
												#end	
											</p>
											#end
											#if ($content.sch_persone.size() > 0)
											<p>
												#foreach ($gente in $content.sch_persone)
													#if ($gente.destination != "")
														$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="$gente.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $gente.text"><strong>$gente.text</strong></a>
													#end
												#end
											</p>
											#end
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
					
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
										#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
											<div class="col-md-6">
												<article class="allegato">
													<div class="scheda-allegato">
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#if ($allegato.link.destination != '''')
															<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $allegato.link.text">
																$allegato.link.text
															</a>
															#else
																#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																	$allegato.documento.text
																</a>
															#end
															<br />
															<span>$allegato.descr.text</span>
														</h4>
													</div>
												</article>
											</div>
											#set ($contadoc = $contadoc + 1)
										#end
									#if ($contadoc % 2 != 0 || $foreach.count == $content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end

					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
                    <p><a href="$aiuto.link.destination"   #if($aiuto.link.symbolicLink.destType == 1)target="_blank" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $aiuto.link.text"                     #else title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $aiuto.link.text" #end > 
                    <strong>$aiuto.link.text</strong></a></p>
										#end     
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Servizi">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Novità">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Documenti">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220016,'ORG','Titolo h4 (titolo+link)','
<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $content.titolo.text">$content.titolo.text</a></h4>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220018,'ORG','Servizio (ico+titolo+indirizzo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
	<article class="scheda-ufficio-contatti scheda-round">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">
			<h4>
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
				<br />
        #if ($content.indirizzo.text != "" || $content.cap.text != "")
        <span>
         #if ($content.indirizzo.text != "")
         	$content.indirizzo.text 
         #end
         #if ($content.cap.text != "" && $content.cap.text != "-")
        		- $content.cap.text
         #end
         #if ($content.ind_altro.text != "")
         	<br />$content.ind_altro.text
         #end
        </span>
        #end
			</h4>
    </div>
	</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220022,'ORG','Widget Help (orario)','
<p>
												#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != ""))
												#end
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
      								

','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220012,'ORG','Servizio (titolo+indirizzo+contatti) - col-6','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
			<h4 class="mb24">
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
				<br />
        #if ($content.indirizzo.text != "" || $content.cap.text != "")
        <span>
         #if ($content.indirizzo.text != "")
         	$content.indirizzo.text
         #end
         #if ($content.cap.text != "" && $content.cap.text != "-")
         	- $content.cap.text
         #end
        
         #if ($content.ind_altro.text != "")
         	<br />$content.ind_altro.text
         #end
        </span>
        #end
			</h4>
      <p>
      #if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
     	 $i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
      #end
      #set($prec = false)
      	#if ($info.getConfigParameter("orario_invernale") == ''true'')
      	#if ($content.orario.inv_matt.text != "")
      		#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
      		$orarioinvmatt
      		#set($prec = true)
      #end
      #if ($content.orario.inv_pom.text != "")
      	#if ($prec)<br />#end
      		#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
      		$orarioinvpom
      		#set($prec = true)
      	#end
      #else
      	#if ($content.orario.est_matt.text != "")
      		#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
      		$orarioestmatt
      		#set($prec = true)
      	#else 
      	#if ($content.orario.inv_matt.text != "")
      		#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
      		$orarioinvmatt														
      		#set($prec = true)
      	#end
      #end
      #if ($content.orario.est_pom.text != "")
      	#if ($prec)<br />#end														
      	#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
      	$orarioestpom
      	#set($prec = true)
      #else
      #if ($content.orario.inv_pom.text != "")
      	#if ($prec)<br />#end
      		#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
      		$orarioinvpom
      		#set($prec = true)
      	#end
      #end
     #end
     #if ($content.orario.continuo.text != "")
      #if ($prec)<br />#end
      #set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
      $orariocontinuo													
      #end
      </p>
      <p>	
		#if ($content.telefono.text != "")
			$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
		#end
		#if ($content.email.text != "")
			$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
		#end
		#if ($pec.text != "")
			$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
		#end	
	</p>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220019,'ORG','Persona (ico+cat+titolo) - col-6','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
	<article class="scheda scheda-round">
  	<div class="scheda-icona-small">
    	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
  			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
      </a>
    </div>
    <div class="scheda-testo-small">
			<h4>
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
			</h4>
    </div>
	</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220009,'ORG','Intro sezioni (titolo+testo+gestione)','
		<div class="row">
			<div class="offset-lg-1 col-lg-5 col-md-7">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-4 offset-lg-1 offset-md-1 col-md-4">
			</div>
		</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220002,'ORG','Sezioni - in evidenza (ico+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220021,'ORG','Lista (titolo + indirizzo - email + area)','
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
	<p>
		<br/>
    #if ($content.indirizzo.text != "" || $content.cap.text != "")
    	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_STR_indirizzo"):</strong>
         #if ($content.indirizzo.text != "")
         	$content.indirizzo.text 
         #end
         #if ($content.cap.text != "" && $content.cap.text != "-")
        		- $content.cap.text
         #end
      <br />
    #end
    #if ($content.email.text != "")
    	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_STR_email"):</strong> $content.email.text <br />
    #end
		#if (($content.tipologia.mapValue.contains("Uffici")) && ($content.legami.size() > 0))
			<strong>$i18n.getLabel("CITTAMETRO_CONTENT_STR_area"):</strong>								
			#foreach ($legame in $content.legami)
      	#set($linkStruttura = $legame.destination.replaceAll("#|C;|!",""))
      	#if ($linkStruttura.contains("ORG"))
        	<a href="$legame.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $legame.text">$legame.text</a>
				#end
      	</div>
      #end
		#end
	</p>
</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220001,'ORG','Dettaglio completo - badge - pagina volante','
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>

#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#if ($content.tipologia.mapKey.equals("amm_02"))
	#set ($fieldsearch = ''area'')
#else
	#set ($fieldsearch = ''uffici'')
#end
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.competenze.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-competenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</a>
						#end
						#if (($content.tipologia.mapKey.equals("amm_02")) && ($content.legami.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura_uffici" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_ufficio")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_ufficio")</a>
						#end
						#if (($content.tipologia.mapKey.equals("amm_03")) && ($content.legami.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura_area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_area")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_area")</a>
						#end
						#if (($content.resp.destination != "") || ($content.assessor.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</a>
						#end
						#if (($content.tipologia.mapKey.equals("amm_01_02_02")) || ($content.tipologia.mapKey.equals("amm_01_02_03")) || ($content.tipologia.mapKey.equals("amm_01_03")) || ($content.tipologia.mapKey.equals("amm_01_03_02")))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-persone" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-sedi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</a>
						#if ($content.tipologia.mapKey.equals("amm_02") || ($content.tipologia.mapKey.equals("amm_03")))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</a>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</a>
						#end
						#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-luoghi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_luoghi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_luoghi")</a>
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc-bandi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_bandi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_bandi")</a>
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc-ordinanze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_ordinanze")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_ordinanze")</a>
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc-modulistica" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_modulistica")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_modulistica")</a>
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc-regolamenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_regolamenti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_regolamenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.competenze.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-competenze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.competenze.text
							</div>
						</div>
					#end
					#if (($content.tipologia.mapKey.equals("amm_02")) && ($content.legami.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura_uffici"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_uffici")</h4>
							</div>
						</div>
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11 listaluoghi">	
								<div class="row">
									#foreach ($legame in $content.legami)
										<div class="col-lg-6">
											<article class="scheda-ufficio-contatti scheda-round">
												<div class="scheda-ufficio-testo">
													#set($linkCont = $legame.destination.replaceAll("#|C;|!",""))
													<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
												</div>
											</article>
										</div>
									#end
								</div>
							</div>
						</div>
					#end 
					#if (($content.tipologia.mapKey.equals("amm_03")) && ($content.legami.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura_area"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_area")</h4>
							</div>
						</div>
						<div class="row listaluoghi" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									#foreach ($legame in $content.legami)
										<div class="col-lg-9 col-md-12">
											#set($linkStruttura = $legame.destination.replaceAll("#|C;|!",""))
											#if ($linkStruttura.contains("ORG"))
												<div data-ng-init="getContent(''$linkStruttura'',''220005'')" data-ng-bind-html="renderContent[''$linkStruttura''][''220005'']"></div>
											#end
										</div>
									#end
								</div>
							</div>
						</div>
					#end
					#if (($content.resp.destination != "") || ($content.assessor.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</h4>
							</div>
						</div>						
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								#if ($content.assessor.size() > 0)
									<div class="row">
										#if ($content.assessor.size() > 1)
											<div class="col-sm-12 lista-persone">
												<span class="mb8 mt0 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessori")</span>
												<div class="row">
													#foreach ($ass in $content.assessor)
														<div class="col-lg-6 col-md-12 mb16">
															#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
														</div>
													#end
												</div>
											</div>
										#else
											<div class="col-lg-6 scheda-persona-singola">
												<span class="mb8 mt12 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessore")</span>
												#foreach ($ass in $content.assessor)
													#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
													<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
												#end
											</div>
										#end
									</div>
								#end
								#if ($content.resp.destination != "")
									<div class="row #if ($content.assessor.size() > 0 && $content.assessor.size() < 2)mt12#end">
										<div class="col-md-12">
											<article>
												#set($linkP = $content.resp.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkP'',''230010'')" data-ng-bind-html="renderContent[''$linkP''][''230010'']"></div>
											</article>
										</div>
									</div>
								#end
							</div>
						</div>
					#end
					#if ($content.persone && $content.persone.size() > 0)
						#if (($content.tipologia.mapKey.equals("amm_01_02_02")) || ($content.tipologia.mapKey.equals("amm_01_02_03")) || ($content.tipologia.mapKey.equals("amm_01_03")) || ($content.tipologia.mapKey.equals("amm_01_03_02")))
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-persone"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</h4>
								</div>
							</div>
						#else
							<div class="row mt12">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone_funzionari")</p>
								</div>
							</div>
						#end
						<div class="row articolo-ulterioriinfo" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								<div class="argomenti persone">
									#foreach ($persona in $content.persone)
										#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
										<span data-ng-init="getContent(''$linkDipendente'',''230007'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230007'']"></span>
									#end
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-sedi"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</h4>
						</div>
					</div>
					<div class="row">
						<div class="offset-md-1 col-md-11 listaluoghi">	
							<div class="row">
								<div class="col-lg-9 col-md-12">
									<article class="scheda-ufficio-contatti scheda-round">
										#if ($content.sede.destination != "")
											<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
										#end
										<div class="scheda-ufficio-testo">
											<h4 class="mb24">
												#if ($content.sede.destination != "")
													<a href="$content.sede.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.sede.text">$content.sede.text</a><br />
												#end
												#if ($content.indirizzo.text != "" || $content.cap.text != "")
													<span>
													#if ($content.indirizzo.text != "")
														$content.indirizzo.text 
													#end
													#if ($content.ind_altro.text != "")
														- $content.ind_altro.text
													#end
													#if ($content.cap.text != "" && $content.cap.text != "-")
														- $content.cap.text
													#end
													</span>
												#end
											</h4>
											
											#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
											#end
											#if (($content.telefono.text != "") || ($content.email.text != "") || ($content.pec.text != ""))
											<p>	
												#if ($content.telefono.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
												#end
												#if ($content.email.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
												#end
												#if ($content.pec.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
												#end	
											</p>
											#end
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
					#if ($content.sch_persone.size() > 0)
						<div class="row mt12">
							<div class="offset-md-1 col-md-11">
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone_contattare")</p>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								<div class="lista-persone-ruoli">
									#foreach ($gente in $content.sch_persone)
										#if ($gente.destination != "")
											<div class="persona-ruolo">
												#set($linkDipendente = $gente.destination.replaceAll("#|C;|!",""))
												<span data-ng-init="getContent(''$linkDipendente'',''230012'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230012'']"></span> - $gente.text
											</div>
										#end
									#end
								</div>
							</div>
						</div>
					#end

					#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</h4>
							</div>
						</div>
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''SRV'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=micro;value=false)+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''800002'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''800002'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''800002'')" data-ng-bind-html="renderContent[elem.$][''800002'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi_empty")</p>
							</div>
						</div>
					#end
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
										#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#if ($allegato.link.destination != '''')
															<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $allegato.link.text">
																$allegato.link.text
															</a>
															#else
																#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																	$allegato.documento.text
																</a>
															#end
															<br />
															<span>$allegato.descr.text</span>
														</h4>
													</div>
												</article>
											</div>
											#set ($contadoc = $contadoc + 1)
										#end
									#if ($contadoc % 2 != 0 || $foreach.count == $content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-luoghi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_luoghi")</h4>
							</div>
						</div>
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''LGO'', '''', '''', '' '', '''', ''(attributeFilter=true;key=s_str_link;value=$content.getId())+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height riga-mono">
									<div class="col-lg-6 col-md-6" data-ng-repeat="elem in contents" data-ng-init="getContent(elem.$,''120009'')" data-ng-bind-html="renderContent[elem.$][''120009'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_luoghi_empty")</p>
							</div>
						</div>

						#if ($content.tipologia.mapKey.equals("amm_03"))
							#set ($fieldsearch = ''ufficio'')
						#end
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc-bandi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_bandi")</h4>
							</div>
						</div>
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''DOC'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=tipologia;value=doc_09)+(attributeFilter=true;start=today;key=data_scad)+(order=DESC;attributeFilter=true;key=data_pubb)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''700010'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''700010'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''700010'')" data-ng-bind-html="renderContent[elem.$][''700010'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documento_empty")</p>
							</div>
						</div>
						
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc-ordinanze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_ordinanze")</h4>
							</div>
						</div>
						
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''DOC'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=tipologia;value=doc_10)+(order=DESC;attributeFilter=true;key=data_pubb)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''700010'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''700010'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''700010'')" data-ng-bind-html="renderContent[elem.$][''700010'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documento_empty")</p>
							</div>
						</div>
						
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc-modulistica"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_modulistica")</h4>
							</div>
						</div>
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''DOC'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=tipologia;value=doc_02)+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''700010'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''700010'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''700010'')" data-ng-bind-html="renderContent[elem.$][''700010'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documento_empty")</p>
							</div>
						</div>
						
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc-regolamenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_regolamenti")</h4>
							</div>
						</div>
						
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''DOC'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=sottotip;value=doc_04_02)+(attributeFilter=true;key=data_scad;nullValue=true)+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''700010'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''700010'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''700010'')" data-ng-bind-html="renderContent[elem.$][''700010'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documento_empty")</p>
							</div>
						</div>
					#end
					
					
					
					
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")<br/>$aiuto.testo.text #end 
                    
										#if ($aiuto.link.destination != '''')
                                        <p><a href="$aiuto.link.destination"   #if($aiuto.link.symbolicLink.destType == 1)target="_blank" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $aiuto.link.text" #else title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $aiuto.link.text" #end > 
                                         <strong>$aiuto.link.text</strong></a></p>
										#end                    
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Servizi">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Novità">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Documenti">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220003,'ORG','Servizio (ico+titolo+indirizzo+contatti)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
	<article class="scheda-ufficio-contatti scheda-round">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">
			<h4 class="mb24">
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
				<br />
        #if ($content.indirizzo.text != "" || $content.cap.text != "")
        <span>
         #if ($content.indirizzo.text != "")
         	$content.indirizzo.text 
         #end
         #if ($content.cap.text != "" && $content.cap.text != "-")
        		- $content.cap.text
         #end
         #if ($content.ind_altro.text != "")
         	<br />$content.ind_altro.text
         #end
        </span>
        #end
			</h4>
      <p>
												#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
													$i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
												#end
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
      								<p>	
												#if ($content.telefono.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
												#end
												#if ($content.email.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
												#end
												#if ($pec.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
												#end	
			</p>
    </div>
	</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220006,'ORG','Intro sezioni (titolo+testo+argomenti)','
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-7">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 offset-lg-1 offset-md-1 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220004,'ORG','Uffici (ico+titolo+indirizzo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda-ufficio-contatti scheda-round">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">    
     <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
       <strong>$content.titolo.text</strong>
     </a><br />
     <span>
       #if ($content.indirizzo.text != "")
         $content.indirizzo.text
       #end
       #if ($content.cap.text != "" && $content.cap.text != "-")
         - $content.cap.text
       #end
       #if ($content.ind_altro.text != "")
         <br />$content.ind_altro.text
       #end
     </span>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220020,'ORG','Lista ricerca (ico + cat + titolo + abs + link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.tipologia.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220007,'ORG','Homepage - In evidenza (cat+titolo+abs+link) ','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLAMMINISTRAZIONE")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220008,'ORG','Argomento (ico+cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
	</div>
	<div class="scheda-testo scheda-testo-large">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
 		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (220015,'ORG','Sezioni - in evidenza (ico+cat+titoletto+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230009,'PRS','Scheda lista politica (ruolo+titolo+img) ','
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<p class="mb8">$ruolo</p>
<article class="scheda-persona scheda-round">
	<div class="scheda-testo">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4> 		
	</div>
	<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230012,'PRS','Contatti (nome+cognome)','
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.nome.text $content.cognome.text</a>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230010,'PRS','Scheda lista politica (ruolo+titolo)','
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<p class="articolo-titoletto">$ruolo</p>
<div class="row articolo-ulterioriinfo">
		<div class="col-md-12">
				<div class="argomenti persone">
						<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="badge badge-pill badge-persone">$content.nome.text $content.cognome.text</a>
				</div>
		</div>
</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230008,'PRS','Rubrica (nome+cognome+tel+uff+serv)','
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4>
	<p>
		<br/>
		#if ($content.email.text != "")
			<strong>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_email"):</strong> $content.email.text<br/>
		#end
		#if ($content.telefono.text != "")
			<strong>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_telefono"):</strong> $content.telefono.text<br />
		#end		
		#if ($content.coll_liv1.size() > 0)
			<strong>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_area"):</strong>								
			#foreach ($coll in $content.coll_liv1)
				<a href="$coll.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $coll.text">$coll.text</a>
			#end
		#end
	</p>
</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230011,'PRS','Dettaglio completo volante','
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.sezione.mapKey != "")
							#set($codicePaginaContenuto = $content.sezione.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end												
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($incaricoscaduto = false)
#if ($content.data_fine.fullDate != "")
	#set($incaricoscaduto = $info.getExpiredDate($content.data_fine.getFormattedDate("dd/MM/yyyy")))
#end
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.nome.text $content.cognome.text</h2>
					#if($content.ruolo.mapKey != "")
					<h4>$content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length())</h4>
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#foreach ($Category in $content.getCategories())
								#if($Category.getParent().code == "arg")
									#set($argPage = $Category.code.substring(4,$Category.code.length()))
									<a href="$content.getContentOnPageLink($argPage)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
		<figure>
			<img src="$content.immagine.getImagePath("0")" alt="$content.immagine.text" class="img-fluid" />
			<figcaption>$content.immagine.text</figcaption>
		</figure>
	</div>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-ruolo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")</a>
						#if ($content.telefono.text != "" || $content.email.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-contatti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")</a>
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != "") || ($content.compensi.size() > 0) || ($content.importi.size() > 0 ) || ($content.altre_car.size() > 0) || ($content.atto_nom.size() > 0) || ($content.patrim_sit.size() > 0) || ($content.redditi.size() > 0) || ($content.spese_elet.size() > 0) || ($content.patrim_var.size() > 0) || ($content.redditi_pa.size() > 0) || ($content.insussist.size() > 0) || ($content.emolumenti.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-ruolo"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")</h4>
						</div>
					</div>
					#if ($content.ruolo_desc.text != "")
					<div class="row">
						<div class="offset-md-1 col-md-8">
							<div class="testolungo">
								$content.ruolo_desc.text
							</div>
						</div>
					</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 deleghe">
							#if ($content.deleghe.text != "")
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_deleghe")</p>
								$content.deleghe.text
							#end
							#if ($content.coll_liv1.size() > 0)
								#if ($content.tipologia.mapKey == "PP")
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_partedi")</p>
									<div class="schede">
										#foreach ($coll in $content.coll_liv1)
											#if ($foreach.count % 2 != 0)
												<div class="row row-eq-height">
											#end
											<div class="col-md-6">
												#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkCont'',''220019'')" data-ng-bind-html="renderContent[''$linkCont''][''220019'']"></div>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.coll_liv1.size())
												</div>
											#end
										#end
									</div>
								#else
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_partedi")</p>								
									<div class="listaluoghi">
										<div class="row">
										#foreach ($coll in $content.coll_liv1)									
											<div class="col-lg-6">
												<article class="scheda-ufficio-contatti scheda-round">
													<div class="scheda-ufficio-testo">
														#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
														<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
													</div>
												</article>
											</div>									
										#end
										</div>
									</div>
								#end
							#end
							#if ($content.coll_liv2.size() > 0)
								#if ($content.tipologia.mapKey == "PP")
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_membro")</p>
									<div class="schede">
										#foreach ($coll in $content.coll_liv2)
											#if ($foreach.count % 2 != 0)
												<div class="row row-eq-height">
											#end
											<div class="col-md-6">
												#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkCont'',''220019'')" data-ng-bind-html="renderContent[''$linkCont''][''220019'']"></div>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.coll_liv2.size())
												</div>
											#end
										#end
									</div>
								#else
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_uffici")</p>
									<div class="listaluoghi">
										<div class="row">
											#foreach ($coll in $content.coll_liv2)
												<div class="col-lg-6">
													<article class="scheda-ufficio-contatti scheda-round">
														<div class="scheda-ufficio-testo">
															#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
														</div>
													</article>
												</div>
											#end
										</div>
									</div>
								#end
							#end
							#if ($content.data_insed.fullDate != "")
								<div class="row">
									<div class="col-md-6">
										<p>
											#if ($content.tipologia.mapKey == "PP")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_insed")</span><br />
											#end
											#if ($content.tipologia.mapKey == "PA")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_incarico")</span><br />
											#end
											$content.data_insed.getFormattedDate("dd/MM/yyyy")
										</p>	
									</div>
									#if ($incaricoscaduto)
									<div class="col-md-6">
										<p>
											#if ($content.tipologia.mapKey == "PP")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_fine")</span><br />
											#end
											#if ($content.tipologia.mapKey == "PA")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_fine_incarico")</span><br />
											#end
											$content.data_fine.getFormattedDate("dd/MM/yyyy")
										</p>	
									</div>
									#end
								</div>
							#end
						</div>
					</div>
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 paragrafo">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.telefono.text != "" || $content.email.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-contatti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">
								<div class="row">
									<div class="col-md-9">
										<article class="scheda-ufficio-contatti scheda-round">
											<div class="scheda-ufficio-testo">
												<p>
													#if ($content.telefono.text != "")
														$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text
													#end
													#if ($content.email.text != "")
														<br>
														$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a class="scheda-contatti-email" href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a>
													#end
												</p>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					#end
					#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != "") || ($content.compensi.size() > 0) || ($content.importi.size() > 0 ) || ($content.altre_car.size() > 0) || ($content.atto_nom.size() > 0) || ($content.patrim_sit.size() > 0) || ($content.redditi.size() > 0) || ($content.spese_elet.size() > 0) || ($content.patrim_var.size() > 0) || ($content.redditi_pa.size() > 0) || ($content.insussist.size() > 0) || ($content.emolumenti.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")</h4>
							</div>
						</div>
						#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != ''''))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<div class="row allegati-riga">
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													#if ($content.cv.attachPath != '''')
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#set ($fileNameSplit = $content.cv.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$content.cv.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $content.cv.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $content.cv.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $content.cv.resource.instance.mimeType, $content.cv.resource.instance.fileLength)" target="_blank">
																$content.cv.text
															</a>
														</h4>
													#else
														<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_CV_non_disponibile")<br /><span>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</span></h4>
													#end
												</div>
											</article>
										</div>
									</div>
								</div>
							</div>
						#end
						#if ($content.atto_nom.size() > 0)
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_atto_nom")</p>
									#foreach ($elem in $content.atto_nom)
										#if ($foreach.count % 2 != 0)
											<div class="row allegati-riga">
										#end
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($elem.attachPath != '''')
															#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																$elem.text
															</a>
														#end
													</h4>
												</div>
											</article>
										</div>
										#if ($foreach.count % 2 == 0 || $foreach.count == $content.atto_nom.size())
											</div>
										#end
									#end								
								</div>
							</div>
						#end
						#if (($content.tipologia.mapKey == "PP"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_spese_elet")</p>
									#if ($content.spese_elet.size() > 0)
										#foreach ($elem in $content.spese_elet)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.spese_elet.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.compensi.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_compensi")</p>
									#if ($content.compensi.size() > 0)
										#foreach ($elem in $content.compensi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.compensi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.importi.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_importi")</p>
									#if ($content.importi.size() > 0)
										#foreach ($elem in $content.importi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.importi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.altre_car.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_altre_car")</p>
									#if ($content.altre_car.size() > 0)
										#foreach ($elem in $content.altre_car)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.altre_car.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (!$incaricoscaduto)
							#if	(($content.tipologia.mapKey == "PP") || ($content.ruolo.mapKey == "PA6") || ($content.ruolo.mapKey == "PA21"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_patrim_sit")</p>
									#if ($content.patrim_sit.size() > 0)
										#foreach ($elem in $content.patrim_sit)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.patrim_sit.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($incaricoscaduto)
							#if	(($content.tipologia.mapKey == "PP") || ($content.ruolo.mapKey == "PA6") || ($content.ruolo.mapKey == "PA21"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_patrim_var")</p>
									#if ($content.patrim_var.size() > 0)
										#foreach ($elem in $content.patrim_var)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.patrim_var.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.redditi.size() > 0)
							#if	(($content.ruolo.mapKey != "PA7") && ($content.ruolo.mapKey != "PA10") && ($content.ruolo.mapKey != "PA26"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_redditi")</p>
									#if ($content.redditi.size() > 0)
										#foreach ($elem in $content.redditi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.redditi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (!$incaricoscaduto)
							#if	(($content.ruolo.mapKey != "PA7") && ($content.ruolo.mapKey != "PA10") && ($content.ruolo.mapKey != "PA26"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_redditi_pa")</p>
									#if ($content.redditi_pa.size() > 0)
										#foreach ($elem in $content.redditi_pa)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.redditi_pa.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.insussist.size() > 0)
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_insussist")</p>
									#if ($content.insussist.size() > 0)
										#foreach ($elem in $content.insussist)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.insussist.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.emolumenti.size() > 0)
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_emolumenti")</p>
									#if ($content.emolumenti.size() > 0)
										#foreach ($elem in $content.emolumenti)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.emolumenti.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.ult_info.text != "")
								$content.ult_info.text
							#end
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230005,'PRS','Scheda lista politica (titolo+incarico+img)','
<article class="scheda-persona scheda-round">
	<div class="scheda-testo">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4>
 		<p>$content.descr.text</p>
	</div>
	<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230007,'PRS','Gestione (nome+cognome)','
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="badge badge-pill badge-persone">$content.nome.text $content.cognome.text</a>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230006,'PRS','Argomento (ico+cat+titolo+abs+img)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small mr130">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.sezione.mapValue
	</div>
	<div class="scheda-testo">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4>
 		$content.ruolo_desc.text
	</div>
	<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230003,'PRS','Sezioni - In evidenza (cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL("$content.sezione.mapKey")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.sezione.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230014,'PRS','Lista ricerca (ico + cat + titolo + abs + link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.sezione.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> <span>$content.sezione.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230001,'PRS','Dettaglio completo','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($incaricoscaduto = false)
#if ($content.data_fine.fullDate != "")
	#set($incaricoscaduto = $info.getExpiredDate($content.data_fine.getFormattedDate("dd/MM/yyyy")))
#end
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.nome.text $content.cognome.text</h2>
					#if($content.ruolo.mapKey != "")
					<h4>$content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length())</h4>
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#foreach ($Category in $content.getCategories())
								#if($Category.getParent().code == "arg")
									#set($argPage = $Category.code.substring(4,$Category.code.length()))
									<a href="$content.getContentOnPageLink($argPage)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
		<figure>
			<img src="$content.immagine.getImagePath("0")" alt="$content.immagine.text" class="img-fluid" />
			<figcaption>$content.immagine.text</figcaption>
		</figure>
	</div>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-ruolo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")</a>
						#if ($content.telefono.text != "" || $content.email.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-contatti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")</a>
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != "") || ($content.compensi.size() > 0) || ($content.importi.size() > 0 ) || ($content.altre_car.size() > 0) || ($content.atto_nom.size() > 0) || ($content.patrim_sit.size() > 0) || ($content.redditi.size() > 0) || ($content.spese_elet.size() > 0) || ($content.patrim_var.size() > 0) || ($content.redditi_pa.size() > 0) || ($content.insussist.size() > 0) || ($content.emolumenti.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-ruolo"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")</h4>
						</div>
					</div>
					#if ($content.ruolo_desc.text != "")
					<div class="row">
						<div class="offset-md-1 col-md-8">
							<div class="testolungo">
								$content.ruolo_desc.text
							</div>
						</div>
					</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 deleghe">
							#if ($content.deleghe.text != "")
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_deleghe")</p>
								$content.deleghe.text
							#end
							#if ($content.coll_liv1.size() > 0)
								#if ($content.tipologia.mapKey == "PP")
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_partedi")</p>
									<div class="schede">
										#foreach ($coll in $content.coll_liv1)
											#if ($foreach.count % 2 != 0)
												<div class="row row-eq-height">
											#end
											<div class="col-md-6">
												#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkCont'',''220019'')" data-ng-bind-html="renderContent[''$linkCont''][''220019'']"></div>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.coll_liv1.size())
												</div>
											#end
										#end
									</div>
								#else
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_partedi")</p>								
									<div class="listaluoghi">
										<div class="row">
										#foreach ($coll in $content.coll_liv1)									
											<div class="col-lg-6">
												<article class="scheda-ufficio-contatti scheda-round">
													<div class="scheda-ufficio-testo">
														#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
														<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
													</div>
												</article>
											</div>									
										#end
										</div>
									</div>
								#end
							#end
							#if ($content.coll_liv2.size() > 0)
								#if ($content.tipologia.mapKey == "PP")
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_membro")</p>
									<div class="schede">
										#foreach ($coll in $content.coll_liv2)
											#if ($foreach.count % 2 != 0)
												<div class="row row-eq-height">
											#end
											<div class="col-md-6">
												#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkCont'',''220019'')" data-ng-bind-html="renderContent[''$linkCont''][''220019'']"></div>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.coll_liv2.size())
												</div>
											#end
										#end
									</div>
								#else
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_uffici")</p>
									<div class="listaluoghi">
										<div class="row">
											#foreach ($coll in $content.coll_liv2)
												<div class="col-lg-6">
													<article class="scheda-ufficio-contatti scheda-round">
														<div class="scheda-ufficio-testo">
															#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
														</div>
													</article>
												</div>
											#end
										</div>
									</div>
								#end
							#end
							#if ($content.data_insed.text != "")
								<div class="row">
									<div class="col-md-6">
										<p>
											#if ($content.tipologia.mapKey == "PP")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_insed")</span><br />
											#end
											#if ($content.tipologia.mapKey == "PA")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_incarico")</span><br />
											#end
											$content.data_insed.getFormattedDate("dd/MM/yyyy")
										</p>	
									</div>
									#if ($incaricoscaduto)
									<div class="col-md-6">
										<p>
											#if ($content.tipologia.mapKey == "PP")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_fine")</span><br />
											#end
											#if ($content.tipologia.mapKey == "PA")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_fine_incarico")</span><br />
											#end
											$content.data_fine.getFormattedDate("dd/MM/yyyy")
										</p>	
									</div>
									#end
								</div>
							#end
						</div>
					</div>
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 paragrafo">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.telefono.text != "" || $content.email.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-contatti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">
								<div class="row">
									<div class="col-md-9">
										<article class="scheda-ufficio-contatti scheda-round">
											<div class="scheda-ufficio-testo">
												<p>
													#if ($content.telefono.text != "")
														$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text
													#end
													#if ($content.email.text != "")
														<br>
														$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a class="scheda-contatti-email" href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a>
													#end
												</p>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					#end
					#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != "") || ($content.compensi.size() > 0) || ($content.importi.size() > 0 ) || ($content.altre_car.size() > 0) || ($content.atto_nom.size() > 0) || ($content.patrim_sit.size() > 0) || ($content.redditi.size() > 0) || ($content.spese_elet.size() > 0) || ($content.patrim_var.size() > 0) || ($content.redditi_pa.size() > 0) || ($content.insussist.size() > 0) || ($content.emolumenti.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")</h4>
							</div>
						</div>
						#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != ''''))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<div class="row allegati-riga">
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													#if ($content.cv.attachPath != '''')
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#set ($fileNameSplit = $content.cv.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$content.cv.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $content.cv.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $content.cv.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $content.cv.resource.instance.mimeType, $content.cv.resource.instance.fileLength)" target="_blank">
																$content.cv.text
															</a>
														</h4>
													#else
														<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_CV_non_disponibile")<br /><span>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</span></h4>
													#end
												</div>
											</article>
										</div>
									</div>
								</div>
							</div>
						#end
						#if ($content.atto_nom.size() > 0)
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_atto_nom")</p>
									#foreach ($elem in $content.atto_nom)
										#if ($foreach.count % 2 != 0)
											<div class="row allegati-riga">
										#end
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($elem.attachPath != '''')
															#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																$elem.text
															</a>
														#end
													</h4>
												</div>
											</article>
										</div>
										#if ($foreach.count % 2 == 0 || $foreach.count == $content.atto_nom.size())
											</div>
										#end
									#end								
								</div>
							</div>
						#end
						#if (($content.tipologia.mapKey == "PP"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_spese_elet")</p>
									#if ($content.spese_elet.size() > 0)
										#foreach ($elem in $content.spese_elet)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.spese_elet.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.compensi.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_compensi")</p>
									#if ($content.compensi.size() > 0)
										#foreach ($elem in $content.compensi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.compensi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.importi.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_importi")</p>
									#if ($content.importi.size() > 0)
										#foreach ($elem in $content.importi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.importi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.altre_car.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_altre_car")</p>
									#if ($content.altre_car.size() > 0)
										#foreach ($elem in $content.altre_car)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.altre_car.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (!$incaricoscaduto)
							#if	(($content.tipologia.mapKey == "PP") || ($content.ruolo.mapKey == "PA6") || ($content.ruolo.mapKey == "PA21"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_patrim_sit")</p>
									#if ($content.patrim_sit.size() > 0)
										#foreach ($elem in $content.patrim_sit)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.patrim_sit.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($incaricoscaduto)
							#if	(($content.tipologia.mapKey == "PP") || ($content.ruolo.mapKey == "PA6") || ($content.ruolo.mapKey == "PA21"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_patrim_var")</p>
									#if ($content.patrim_var.size() > 0)
										#foreach ($elem in $content.patrim_var)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.patrim_var.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.redditi.size() > 0)
							#if	(($content.ruolo.mapKey != "PA7") && ($content.ruolo.mapKey != "PA10") && ($content.ruolo.mapKey != "PA26"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_redditi")</p>
									#if ($content.redditi.size() > 0)
										#foreach ($elem in $content.redditi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.redditi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (!$incaricoscaduto)
							#if	(($content.ruolo.mapKey != "PA7") && ($content.ruolo.mapKey != "PA10") && ($content.ruolo.mapKey != "PA26"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_redditi_pa")</p>
									#if ($content.redditi_pa.size() > 0)
										#foreach ($elem in $content.redditi_pa)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.redditi_pa.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.insussist.size() > 0)
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_insussist")</p>
									#if ($content.insussist.size() > 0)
										#foreach ($elem in $content.insussist)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.insussist.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.emolumenti.size() > 0)
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_emolumenti")</p>
									#if ($content.emolumenti.size() > 0)
										#foreach ($elem in $content.emolumenti)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.emolumenti.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.ult_info.text != "")
								$content.ult_info.text
							#end
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230013,'PRS','Sezioni - in evidenza (titolo+incarico+img)','
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-testo mt16">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4>
		<p>$content.descr.text</p>
</div>
<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230004,'PRS','Sezioni - in evidenza (ico+cat+titoletto+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.sezione.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.sezione.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230002,'PRS','Sezioni - in evidenza (ico+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230016,'PRS','Amm. trasparente (nominativo+carica+inizio+fine)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<th scope="row" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_NOMINATIVO'')">
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-piu" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEEALL''): $content.titolo.text" onclick="espandiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add"></use></svg>
	</button>
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-meno" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEELESS''): $content.titolo.text" onclick="chiudiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-remove"></use></svg>
	</button>
	<a href="$content.getContentLink()" title="$i18n.getLabel(''CITTAMETRO_PORTAL_GOTOPAGE''): $content.titolo.text">$content.cognome.text $content.nome.text</a>
</th>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_CARICA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_CARICA'')">$ruolo</td>

<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INIZIOCARICA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INIZIOCARICA'')">$content.data_insed.getFormattedDate(''dd MMM yyyy'')</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_FINECARICA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_FINECARICA'')">$content.data_fine.getFormattedDate(''dd MMM yyyy'')</td>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230017,'PRS','Amm. trasparente (nominativo+incarico+fine)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<th scope="row" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_NOMINATIVO'')">
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-piu" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEEALL''): $content.titolo.text" onclick="espandiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add"></use></svg>
	</button>
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-meno" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEELESS''): $content.titolo.text" onclick="chiudiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-remove"></use></svg>
	</button>
	<a href="$content.getContentLink()" title="$i18n.getLabel(''CITTAMETRO_PORTAL_GOTOPAGE''): $content.titolo.text">$content.cognome.text $content.nome.text</a>
</th>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INCARICO'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INCARICO'')">$ruolo</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_FINECARICA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_FINECARICA'')">
	$content.data_fine.getFormattedDate(''dd MMM yyyy'')
</td>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (230015,'PRS','Amm. trasparente (nominativo+incarico+area)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<th scope="row" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_NOMINATIVO'')">
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-piu" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEEALL''): $content.titolo.text" onclick="espandiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add"></use></svg>
	</button>
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-meno" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEELESS''): $content.titolo.text" onclick="chiudiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-remove"></use></svg>
	</button>
	<a href="$content.getContentLink()" title="$i18n.getLabel(''CITTAMETRO_PORTAL_GOTOPAGE''): $content.titolo.text">$content.cognome.text $content.nome.text</a>
</th>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INCARICO'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INCARICO'')">$ruolo</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_AREA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_AREA'')">
	#if ($content.coll_liv1.size() > 0)
		#foreach ($coll in $content.coll_liv1)
			<a href="$coll.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $coll.text">$coll.text</a><br/>
		#end
	#end
</td>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800003,'SRV','Homepage - In evidenza (cat+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLSERVIZI")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800005,'SRV','Sezioni - in evidenza (ico+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800008,'SRV','Dettaglio completo','
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						#if ($content.micro.booleanValue)
							<li class="breadcrumb-item"><a href="$content.parte_di.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.parte_di.text"><strong>$content.parte_di.text</strong></a><span class="separator">/</span></li>
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#else
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#end
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>

#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					#if($content.sottotit.text != "")
					<h4>$content.sottotit.text</h4>
					#end
					<p>$content.descr.text</p>
					#if($content.stato.text == "Disattivo")
					<div class="alert alert-warning mt16" role="alert">
						<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-error_outline"></use></svg> $content.stato_mot.text
					</div>
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
	<figure>
		<img src="$content.immagine.getImagePath("0")" alt="$content.immagine.text" class="img-fluid" />
		<figcaption>$content.immagine.text</figcaption>
	</figure>
  </div>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.descr_est.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-descr_est" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_descr_est")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_descr_est")</a>
						#end
						#if ($content.desc_dest.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-desc_dest" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_desc_dest")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_desc_dest")</a>
						#end
						#if (($content.come_si_fa.text != "") || ($content.esito.text != "") || ($content.proc_esito.testo.text != "") || ($content.can_digit.testo.text != ""))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-come_si_fa" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_come_si_fa")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_come_si_fa")</a>
						#end
						#if ($content.uffici.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-uffici" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_uffici")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_uffici")</a>
						#end
						#if ($content.area.destination != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_area")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_area")</a>
						#end
						#if ($content.cosa_serve && $content.cosa_serve.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-cosa_serve" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_cosa_serve")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_cosa_serve")</a>
						#end
						#if (($content.costi.text != "") || ($content.vincoli.text != ""))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-costi_vinc" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_costi_vinc")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_costi_vinc")</a>
						#end
						#if ($content.scadenze.text != "" || ($content.scad_fasi && $content.scad_fasi.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-scadenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_scadenze")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_scadenze")</a>
						#end
						#if ($content.casi_part.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-casi_part" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_casi_part")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_casi_part")</a>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_documenti")</a>
						#end
						#if ($content.link && $content.link.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-link" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_link")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_link")</a>
						#end
						<div data-ng-init="setParameters(''999'', ''999'', ''SRV'', '''', '''', '' '', '''', ''(attributeFilter=true;key=parte_di;value=$content.getId())+(order=ASC;attributeFilter=true;key=titolo)'')">
							<a data-ng-if="contents.length > 0" class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-micro_serv" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_micro_serv")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_micro_serv")</a>
						</div>
						#if ($content.parte_di.destination != "" && $content.micro.booleanValue)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-parte_di" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_parte_di")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_parte_di")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.descr_est.text != "")							
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-descr_est"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_descr_est")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.descr_est.text								
							</div>							
						</div>
					#end
					#if ($content.desc_dest.text != "")							
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-desc_dest"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_desc_dest")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.desc_dest.text								
							</div>							
						</div>
					#end
					#if ($content.cop_geo.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.cop_geo.text								
							</div>							
						</div>
					#end
					#if (($content.come_si_fa.text != "") || ($content.esito.text != "") || ($content.proc_esito.testo.text != "") || ($content.can_digit.testo.text != ""))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-come_si_fa"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_come_si_fa")</h4>
							</div>
						</div>
						#if ($content.come_si_fa.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									$content.come_si_fa.text								
								</div>							
							</div>
						#end
						#if ($content.esito.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_esito")</p>
									<div class="articolo-mt0">
										$content.esito.text		
									</div>								
								</div>							
							</div>
						#end
						#if ($content.proc_esito.testo.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_proc_esito")</p>
									<div class="articolo-mt0">
										$content.proc_esito.testo.text
									</div>
									#if ($content.proc_esito.link_sch.destination != "")
										<div class="row">
										#set($linkScheda = $content.proc_esito.link_sch.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("ORG"))
											<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''220018'')" data-ng-bind-html="renderContent[''$linkScheda''][''220018'']"></div>
										#end
										#if ($linkScheda.contains("LGO"))
											<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''120004'')" data-ng-bind-html="renderContent[''$linkScheda''][''120004'']"></div>
										#end
										</div>
									#end
								</div>							
							</div>
						#end
						#if ($content.can_digit.testo.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_can_digit")</p>
									<div class="articolo-mt0">
										$content.can_digit.testo.text
									</div>
									#if ($content.can_digit.link.destination != "")
										<p class="mt16 mb32">
											<a class="btn btn-default btn-iscriviti" href="$content.can_digit.link.destination" #if($content.can_digit.link.symbolicLink.destType == 1) target="_blank"#end>$content.can_digit.link.text</a>
										</p>
									#end
								</div>							
							</div>
						#end
					#end
					#if ($content.autenticaz.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.autenticaz.text								
							</div>							
						</div>
					#end
					#if (($content.can_fisico.text != "") || ($content.can_prenot.destination != ""))
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_can_fisico")</p>
								<div class="articolo-mt0">
									$content.can_fisico.text
								</div>
								#if ($content.can_prenot.destination != "")
									<div class="row">
										<div class="col-md-12">
											<p class="mt16">
												<a class="btn btn-default btn-iscriviti" href="$content.can_prenot.destination">$content.can_prenot.text</a>
											</p>
										</div>
									</div>
								#end
							</div>
						</div>
					#end
					#if ($content.uffici.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-uffici"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_uffici")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">	
								<div class="row">
									#foreach ($sede in $content.uffici)
										#if ($sede.destination != "")
											#set($linkScheda = $sede.destination.replaceAll("#|C;|!",""))
											#if ($linkScheda.contains("ORG"))
												<div class="col-xl-6 col-lg-12 col-md-12">
													<article class="scheda-ufficio-contatti scheda-round">
													  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
															<div class="scheda-ufficio-testo">
																<div data-ng-init="getContent(''$linkScheda'',''220012'')" data-ng-bind-html="renderContent[''$linkScheda''][''220012'']"></div>
															</div>
													</article>
												</div>
											#end
										#end
									#end
								</div>
							</div>
						</div>
					#end
					#if ($content.area.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-area"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_area")</h4>
							</div>
						</div>
						<div class="row listaluoghi">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									<div class="col-lg-12">
										#set($linkStruttura = $content.area.destination.replaceAll("#|C;|!",""))
										#if ($linkStruttura.contains("ORG"))
											<div data-ng-init="getContent(''$linkStruttura'',''220005'')" data-ng-bind-html="renderContent[''$linkStruttura''][''220005'']"></div>
										#end
									</div>
								</div>
							</div>
						</div>
					#end
					#if ($content.cosa_serve && $content.cosa_serve.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-cosa_serve"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_cosa_serve")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8">
								<div class="callout important callout-punti">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($box in $content.cosa_serve)
										<div class="callout-punto">
											<div class="callout-punto-icona">
												<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
											</div>
											<div class="callout-punto-testo">
												$box.testo.text
												#if ($box.documento.attachPath != '''')
													<p>
														#set ($fileNameSplit = $box.documento.resource.instance.fileName.split("\."))
														#set ($last = $fileNameSplit.size() - 1)
														#set ($fileExt = $fileNameSplit[$last])	
														<a href="$box.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $box.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $box.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $box.documento.resource.instance.fileLength)" target="_blank">
															<strong>$box.documento.text</strong>
														</a>
													</p>
												#end
												#if ($box.link.destination != '''')
													<p>
														<a href="$box.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $box.link.text" class="wbreak">
															<strong>$box.link.text</strong>
														</a>
													</p>
												#end
											</div>
										</div>
									#end
								</div>
							</div>
						</div>
					#end
					#if (($content.costi.text != "") || ($content.vincoli.text != ""))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-costi_vinc"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_costi_vinc")</h4>
							</div>
						</div>
						#if ($content.costi.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									$content.costi.text									
								</div>								
							</div>
						#end
						#if ($content.vincoli.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									$content.vincoli.text									
								</div>								
							</div>
						#end
					#end
					#if (($content.scadenze.text != "") || ($content.scad_fasi && $content.scad_fasi.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-scadenze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_scadenze")</h4>
							</div>
						</div>
						#if ($content.scadenze.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.scadenze.text
							</div>
						</div>
						#end
						#if ($content.scad_fasi && $content.scad_fasi.size() > 0)
						<div class="row steppertime">
							<div class="offset-md-1 col-md-11">
								#foreach ($fase in $content.scad_fasi)
									<div class="step">										
										<div class="date-step">
											#if ($fase.data.fullDate != "")
											<span class="date-step-giorno">$fase.data.getFormattedDate("dd")</span><span class="date-step-mese">$fase.data.getFormattedDate("MMM")/$fase.data.getFormattedDate("yy")</span>
											#else
											<span class="date-step-giorno"></span><span class="date-step-mese"></span>
											#end
											<span class="pallino"></span>
										</div>
										<div class="testo-step">
											<div class="scheda-step">
												<div class="scheda-step-testo">
													$fase.testo.text
													#if ($fase.micro_serv.destination != "")
														<p>
															<a href="$fase.micro_serv.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.microserv.link.text">
																<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
																$fase.micro_serv.text
															</a>
														</p>
													#end
												</div>
											</div>
										</div>
									</div>
								#end
							</div>
						</div>
						#end
					#end
					#if ($content.casi_part.text != "")							
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-casi_part"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_casi_part")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.casi_part.text								
							</div>							
						</div>
					#end
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
										<div class="col-md-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($allegato.link.destination != '''')
															<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $allegato.link.text">
																$allegato.link.text
															</a>
														#else
															#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																$allegato.documento.text
															</a>
														#end
														<br />
														<span>$allegato.descr.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.link && $content.link.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-link"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_link")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								#foreach ($sitoesterno in $content.link)
									#if ($sitoesterno.destination != '''')
										#set ($tipofile = $sitoesterno.symbolicLink.getDestType())
										#if ($tipofile == 1)
											#set ($icona = "ponmetroca.svg#ca-open_in_new")
										#end
										#if ($tipofile > 1)
											#set ($icona = "ponmetroca.svg#ca-link")
										#end
										<a href="$sitoesterno.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $sitoesterno.text" #if ($tipofile == 1)target="_blank" #end><svg class="icon icon-link"><use xlink:href="$imgURL/$icona"></use></svg>$sitoesterno.text</a><br />
									#end
								#end
							</div>
						</div>
					#end
					<div data-ng-if="contents.length > 0">
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-micro_serv"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_micro_serv")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 lista-eventi-figli">
								<div class="row">
									<div class="col-lg-6 col-md-12 mb16" data-ng-repeat="elem in contents" data-ng-init="getContent(elem.$,''800009'')" data-ng-bind-html="renderContent[elem.$][''800009'']"></div>
								</div>
							</div>
						</div>
					</div>
					
					#if ($content.parte_di.destination != "" && $content.micro.booleanValue)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-parte_di"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_parte_di")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 lista-eventi-figli">
								<div class="row">
									<div class="col-lg-6 col-md-12 mb16">
										#set($linkServizio = $content.parte_di.destination.replaceAll("#|C;|!",""))
										#if ($linkServizio.contains("SRV"))
											<div data-ng-init="getContent(''$linkServizio'',''800001'')" data-ng-bind-html="renderContent[''$linkServizio''][''800001'']"></div>
										#end
									</div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p
										#end
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<span class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy,H:mm")</strong>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800002,'SRV','Sezioni - In evidenza (cat+titolo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800006,'SRV','Argomento - In evidenza (cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800009,'SRV','Altri servizi (cat+titolo)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800001,'SRV','Sezioni - In evidenza (cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800010,'SRV','Lista ricerca (ico + cat + titolo + abs + link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="#if($content.tipologia.mapKey!='')$content.getPageURL($content.tipologia.mapKey)#end" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800004,'SRV','Argomento (ico+cat+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> #if ($content.tipologia.mapKey != "") $content.tipologia.mapValue #end
	</div>
	<div class="scheda-testo scheda-testo-large">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800007,'SRV','Sezioni - in evidenza (ico+cat+titoletto+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
		#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (800011,'SRV','Link lista (titolo)','
<a href="$content.getContentLink()">$content.titolo.text</a><br />
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (8000013,'SRV','Lista (Tabella), titolo,abs,area (per test)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<th class="tabella-campo-coll" data-th="SERVIZIO" data-thead="SERVIZIO"><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></th>
<td class="tabella-campo-coll" data-th="DESCRIZIONE" data-thead="DESCRIZIONE">$content.descr.text</td>
<td class="tabella-campo-coll" data-th="AREA" data-thead="AREA">$content.area.text</td>
<td class="tabella-campo-coll" data-th="ARGOMENTO" data-thead="ARGOMENTO">	
   <div class="argomenti-sezione-elenco"> 	#set ($argomenti = $content.argomenti.values)
	#foreach ($Category in $content.getCategories())  #if($argomenti.contains($Category.code))($Category.title) #end
	#end
    </div>
</td>
<td class="tabella-campo-coll" data-th="DOCUMENTI" data-thead="DOCUMENTI">	
#if ($content.cosa_serve && $content.cosa_serve.size() > 0)
	#foreach ($box in $content.cosa_serve)
    #if ($box.link.destination != '')
		<a href="$box.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $box.link.text" class="wbreak">$box.link.text</a><br/>
		#end
    #if ($box.documento.attachPath != '')
		  #set ($fileNameSplit = $box.documento.resource.instance.fileName.split("\."))
			#set ($last = $fileNameSplit.size() - 1)
			#set ($fileExt = $fileNameSplit[$last])	
			<a href="$box.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $box.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $box.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $box.documento.resource.instance.fileLength)" target="_blank">
$box.documento.text</a><br/>
		  #end
   #end
#end
</td>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (170002,'STM','Argomento - In evidenza  (ico+titolo+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-bundle-homepage/static/img")
<article class="scheda-sito $content.colore.text">
	#if ($content.icona.text != '''')
    <div class="icona-sito">
      <svg class="icon $content.icona.text"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
    </div>
  #end
	<p>
		<a href="$content.url_sito.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.titolo.text [$i18n.getLabel("CITTAMETRO_PORTAL_EXTERNALSITE")]">
			<strong>$content.titolo.text</strong><br>
			$content.descr.text
		</a>
	</p>
</article>

','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (170003,'STM','Dettaglio completo','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-bundle-homepage/static/img")
<div class="bg-argomento $content.colore.text">
</div>
<div class="container sitotematico-titolo">
	<div class="row">
		<div class="offset-lg-1 col-lg-10 col-md-12">
			<h2>
				#if ($content.icona.text != '')
					<svg class="icon $content.icona.text"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
				#end
				$content.titolo.text
		  </h2>
		</div>
	</div>
</div>
<div class="container" data-ng-cloak data-ng-controller="FiltriController">
	<div class="box-argomento box-sitotematico">
		<div class="row">
			<div class="col-lg-7 col-md-12" id="briciole">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Home"><strong>Home</strong></a><span class="separator">/</span></li>
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
				  </ol>
				</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7 col-md-12">
				<div class="titolo-sezione">
					#set($linkNotizia = $content.notizia.destination.replaceAll("#|C;|!",""))
					<div data-ng-init="getContent(''$linkNotizia'',''100011'')" data-ng-bind-html="renderContent[''$linkNotizia''][''100011'']"></div>
				</div>
			</div>
			<div class="col-lg-5 col-md-12">
				#if($content.gest_pag.size() > 0)
					<aside id="menu-area">
						<p>$i18n.getLabel("CITTAMETRO_CONTENT_MANAGE_THEMES")</p>
							#foreach ($gest in $content.gest_pag)
								#set($linkGest = $gest.destination.replaceAll("#|C;|!",""))
								#if ($linkGest.contains("ORG"))
									<div data-ng-init="getContent(''$linkGest'',''220005'')" data-ng-bind-html="renderContent[''$linkGest''][''220005'']"></div>
								#end
							#end
					</aside>
				#end
			</div>
		</div>
	</div>
</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (170001,'STM','Homepage - In evidenza (ico+titolo+abs)','#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = "")
#if ($content.url_sito.destination != '''')
	#set($linkpage = $content.url_sito.destination)
#end

<div class="scheda-sito $content.colore.text">
		#if ($content.icona.text != '''')
    	<div class="icona-sito">
        <svg class="icon $content.icona.text"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
      </div>
    #end
	<p>
  	#if ($content.url_sito.destination != '''')
		<a href="$content.url_sito.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.titolo.text [$i18n.getLabel("CITTAMETRO_PORTAL_EXTERNALSITE")]" #if($content.url_sito.symbolicLink.destType == 1)target="_blank" #end>
    #else 
    <a>
    #end
			<strong>$content.titolo.text</strong><br>
			$content.descr.text
		</a>
	</p>
</div>

',NULL);
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200011,'TGN','Intro sezioni (titolo + abs+pulsantiverdi)','
<h2>$content.titolo.text</h2>
<p>$content.descr.text</p>
#if ($content.link.size() > 0)
<p>
#foreach ($bottone in $content.link)
	<a class="btn btn-default btn-verde" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPAGE") $bottone.text" href="$bottone.destination">$bottone.text</a>
#end
</p>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200002,'TGN','Intro sezioni (titolo + abs)','
<h2>$content.titolo.text</h2>
<p>$content.descr.text</p>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200003,'TGN','Sito tematico (foto)','
<article class="scheda scheda-round">
	<div class="scheda-testo nopadd">
  	#if ($content.link_pag.destination != "")
		<a href="$content.link_pag.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.link.get(0).text">
    	#if ($content.immagine.getImagePath(''0'') != "")
			<img src="$content.immagine.getImagePath(''3'')" alt="$content.immagine.text" class="img-fluid" />
      #else
      	$content.titolo.text
      #end
		</a>
    #else
    <a>
    	#if ($content.immagine.getImagePath(0) != "")
			<img src="$content.immagine.getImagePath(3)" alt="$content.immagine.text" class="img-fluid" />
      #else
      	$content.titolo.text
      #end
		</a>
    #end
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200009,'TGN','Sezioni - in evidenza (titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
						<h4>
            		<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
            </h4>
						<p>$content.descr.text</p>
					</div>
					<div class="scheda-footer">
            		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.titolo.text" href="$linkpage" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
					</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200006,'TGN','Sezioni - in evidenza (ico+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
            <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200005,'TGN','Sito tematico (titolo+abs+ico)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<div class="scheda-gestione">
	<p>
		<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text"><strong>$content.titolo.text</strong></a><br />
		<span>$content.descr.text</span>
	</p>
	<div class="thumbs-round thumbs-round-icon">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
	</div>
</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200004,'TGN','Sito tematico (titolo+abs+ico+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
		<h4><a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p class="scheda-testo-w70">$content.descr.text</p>
		<div class="scheda-icona scheda-icona-inline">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
		</div>
	</div>
	<div class="scheda-footer">
			<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_SHOWMORE") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200010,'TGN','Lista ricerca (ico + cat + titolo + abs + link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
#set($icona = "ca-subject")
#if ($content.icona.text != "")
	#set($icona = $content.icona.text)
#end
<article>
	<div class="cerca-risultato-item">
  	#if ($content.sezione.mapKey != "")
			<a href="$content.getPageURL($content.sezione.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue" class="categoria">
    #else
    	<a class="categoria">
    #end
      	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$icona"></use></svg>
    #if ($content.sezione.mapKey != "")
        <span>$content.sezione.mapValue</span>
    #end
      </a>
		<h4><a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>

','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200001,'TGN','Dettaglio completo','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
          <p>$content.descr.text</p>
				</div>
			</div>
      <div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						#if ($content.argomenti.values != "[]")
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
							<div class="argomenti-sezione-elenco">
								#set ($argomenti = $content.argomenti.values)
								#foreach ($Category in $content.getCategories())
									#if($argomenti.contains($Category.code))
										<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
									#end
								#end
							</div>
						#end
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.paragrafo && $content.paragrafo.size() > 0)
	<section id="articolo-dettaglio-testo">
		<div class="container">	
			<div class="row">
				<div class="linetop-lg"></div>
				<div class="col-lg-3 col-md-4 lineright">
					<aside id="menu-sinistro">
						<h4 class="dropdown">
							<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
								$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
								<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
							</a>
						</h4>
						<div class="menu-separatore"><div class="bg-oro"></div></div>
						<div id="lista-paragrafi" class="list-group collapse show">
							#foreach ($paragrafo in $content.paragrafo)
								#if ($paragrafo.titolo.text != "")
									<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-$foreach.count" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $paragrafo.titolo.text">$paragrafo.titolo.text</a>
								#end
							#end
							#if ($content.allegati && $content.allegati.size() > 0)
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-allegati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_TGN_allegati")">$i18n.getLabel("CITTAMETRO_CONTENT_TGN_allegati")</a>
							#end
							#if ($content.link && $content.link.size() > 0)
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-linkcorrelati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_TGN_linkcorrelati")">$i18n.getLabel("CITTAMETRO_CONTENT_TGN_linkcorrelati")</a>
							#end
						</div>
					</aside>
				</div>
				<div class="col-lg-9 col-md-8 pt8">
					#if (($content.paragrafo && $content.paragrafo.size() > 0) || ($content.link && $content.link.size() > 0) || ($content.allegati && $content.allegati.size() > 0))
						<div class="articolo-paragrafi articolo-paragrafi-link">
							#foreach ($paragrafo in $content.paragrafo)
								#if ($paragrafo.titolo.text != "")
									<div class="row">
										<div class="offset-md-1 col-md-11 paragrafo">
											<a id="articolo-par-$foreach.count"> </a>
											<h4>
												#if ($paragrafo.link.destination != "")
													#if ($paragrafo.link.destination.contains(''mailto''))
														#set ($titleintro = $i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAILTO"))
													#else 
														#set ($titleintro = $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"))
													#end
													<a href="$paragrafo.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $paragrafo.link.text">$paragrafo.titolo.text</a>
												#else
													$paragrafo.titolo.text
												#end
											</h4>
										</div>
									</div>
								#end
								#if (($paragrafo.testo.text != "") || ($paragrafo.link_txt.destination != ""))
									<div class="row">
									  <div class="offset-md-1 col-md-8 testolungo">
										$paragrafo.testo.text
										#if ($paragrafo.link_txt.destination != "")
											#if ($paragrafo.link_txt.destination.contains(''mailto''))
												#set ($titleintro = $i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAILTO"))
											#else 
												#set ($titleintro = $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"))
											#end
											<p><a href="$paragrafo.link_txt.destination" title="$titleintro: $paragrafo.link_txt.text" class="wbreak" #if($paragrafo.link_txt.symbolicLink.destType == 1)target="_blank" #end>$paragrafo.link_txt.text</a></p>
										#end
									  </div>
									</div>
								#end
							#end
              
              #if ($content.video.destination != "")
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo" data-ng-cloak data-ng-controller="FiltriController">
									<div class="videoplayer">
									#set($linkVideo = $content.video.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
									</div>
								</div>
							</div>
							#end
              
							#if ($content.allegati && $content.allegati.size() > 0)
								<div class="row">
									<div class="offset-md-1 col-md-11 paragrafo">
										<a id="articolo-par-allegati"> </a>
										<h4>$i18n.getLabel("CITTAMETRO_CONTENT_TGN_allegati")</h4>
									</div>
								</div>
								<div class="row schede">
									<div class="offset-md-1 col-md-11">
										#set ($contadoc = 1)
										#foreach ($allegato in $content.allegati)
											#if ($contadoc % 2 != 0)
												<div class="row allegati-riga">
											#end
											#if ($allegato.attachPath != '''')
												<div class="col-lg-6">
													<article class="allegato">
														<div class="scheda-allegato">
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.resource.instance.fileLength"
																	aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.resource.instance.fileLength)" target="_blank">
																	$allegato.text
																</a>
															</h4>
														</div>
													</article>
												</div>
												#set ($contadoc = $contadoc + 1)
											#end
											#if ($contadoc % 2 != 0 || $foreach.count==$content.allegati.size())
												</div>
											#end
										#end
									</div>
								</div>
							#end
							#if ($content.link && $content.link.size() > 0)
								<div class="row">
									<div class="offset-md-1 col-md-11 paragrafo">
										<a id="articolo-par-linkcorrelati"> </a>
										<h4>$i18n.getLabel("CITTAMETRO_CONTENT_TGN_linkcorrelati")</h4>
									</div>
								</div>
								<div class="row articolo-ulterioriinfo">
									<div class="offset-md-1 col-md-8">
										#set ($contalink = 1)
										#foreach ($altrilink in $content.link)
											#if ($altrilink.destination != '''')
												#if ($foreach.count % 2 != 0)
													<div class="row">
												#end
												<div class="col-lg-6">
													<article class="scheda-verde-link scheda-round">
														<a href="$altrilink.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $altrilink.text" #if($altrilink.symbolicLink.destType == 1)target="_blank" #end><strong>$altrilink.text</strong></a>
													</article>
												</div>
												#set ($contalink = $contalink + 1)
											#end
											#if ($contalink % 2 != 0 || $foreach.count==$content.link.size())
												</div>
											#end
										#end
										</p>
									</div>
								</div>
							#end
						</div>
					#end
				</div>
			</div>
		</div>
	</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200007,'TGN','Area personale - cittadino (ico+cat+tit+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end

<article class="scheda scheda-slide">
	<div class="scheda-icona">
		<a>
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg> 
		</a>
	</div>
	<div class="scheda-icona-testo">$i18n.getLabel("CITTAMETRO_PORTAL_SERVICE_CITIZEN")</div>
	<div class="scheda-testo">
		<h4>
			<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
		</h4>
		<p>$content.descr.text</p>
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200012,'TGN','Area personale - residente (ico+cat+tit+abs)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end

<article class="scheda scheda-slide">
	<div class="scheda-icona">
		<a>
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg> 
		</a>
	</div>
	<div class="scheda-icona-testo">$i18n.getLabel("CITTAMETRO_PORTAL_SERVICE_RESIDENT")</div>
	<div class="scheda-testo">
		<h4>
			<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
		</h4>
		<p>$content.descr.text</p>
	</div>
</article>

','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200015,'TGN','Sezioni - in evd residente (ico+titolo+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
			$i18n.getLabel("CITTAMETRO_PORTAL_SERVICE_RESIDENT")
	</div>
	<div class="scheda-testo-small">
			<h4>
            <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200014,'TGN','Sezioni - in evd cittadino (ico+cat+tit+abs+link)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
      $i18n.getLabel("CITTAMETRO_PORTAL_SERVICE_CITIZEN")
	</div>
	<div class="scheda-testo-small">
			<h4>
            <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200008,'TGN','Intro sezioni (titolo+testo+argomenti)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")		
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-7">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 offset-lg-1 offset-md-1 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						#if ($content.argomenti.values != "[]")
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
							<div class="argomenti-sezione-elenco">
								#set ($argomenti = $content.argomenti.values)
								#foreach ($Category in $content.getCategories())
									#if($argomenti.contains($Category.code))
										<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
									#end
								#end
							</div>
						#end
					</div>
				</aside>
			</div>
		</div>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (200013,'TGN','Intro sezioni (condividi+azioni+argomenti)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")				
        <aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						#if ($content.argomenti.values != "[]")
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
							<div class="argomenti-sezione-elenco">
								#set ($argomenti = $content.argomenti.values)
								#foreach ($Category in $content.getCategories())
									#if($argomenti.contains($Category.code))
										<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
									#end
								#end
							</div>
						#end
					</div>
				</aside>
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (190002,'VID','Player interno','
#if (($content.cod_video.text != "") || ($content.cod_canale.text != ""))
  #if ($content.cod_video.text != "")
	<script>
		if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept"){
			$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/$content.cod_video.text");
		}else{
			$("#iframe-$content.getId()").attr("src", "https://www.youtube-nocookie.com/embed/$content.cod_video.text");
		}	
	</script>
  #else
	<script>
		if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept"){
			$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/live_stream?channel=$content.cod_canale.text");
		}else{
			$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/live_stream?channel=$content.cod_canale.text");
		}	
	</script>
  #end
<div class="video-interno">
<iframe id="iframe-$content.getId()" width="100%" height="480" src="" frameborder="0" allowfullscreen></iframe>
</div>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (190003,'VID','Dettaglio completo','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<section id="intro" data-ng-controller="FiltriController">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
		<div class="row mt40">
			<div class="offset-xl-1 col-xl-2 offset-lg-1 col-lg-3 col-md-3">
				<p class="data-articolo"><span>$i18n.getLabel("CITTAMETRO_CONTENT_DATE"):</span><br /><strong>$content.data.getFormattedDate("dd MMMM yyyy")</strong></p>
			</div>
			<div class="offset-xl-1 col-xl-2 offset-lg-1 col-lg-3 offset-md-1 col-md-3">
				
			</div>
		</div>
	</div>
</section>

<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_VID_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_VID_documenti")</a>
						#end
						#if ($content.link && $content.link.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-linkcorrelati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_VID_linkcorrelati")">$i18n.getLabel("CITTAMETRO_CONTENT_VID_linkcorrelati")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if (($content.cod_video.text != "") || ($content.cod_canale.text != ""))
						<div id="videoplayer">
							#if ($content.cod_video.text != "")
								<script>
									$(document).ready(function () {
										if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept"){
											$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/$content.cod_video.text");
										}else{
											$("#iframe-$content.getId()").attr("src", "https://www.youtube-nocookie.com/embed/$content.cod_video.text");
										}
									});
								</script>
							#else
								<script>
									$(document).ready(function () {
										if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept"){
											$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/live_stream?channel=$content.cod_canale.text");
										}else{
											$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/live_stream?channel=$content.cod_canale.text");
										}
									});
								</script>
							#end
							<div class="video-interno">
								<iframe id="iframe-$content.getId()" width="100%" height="480" src="" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
						<div id="videoplayerh"></div>
					#end
					#if ($content.testo.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.testo.text
							</div>
						</div>
					#end
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_VID_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.documenti.attachPath != '''' || $allegato.link.destination != '''')
										<div class="col-md-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($allegato.documenti.attachPath != '''')
															#set ($fileNameSplit = $allegato.documenti.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$allegato.documenti.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documenti.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documenti.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documenti.resource.instance.fileLength)" target="_blank">
																$allegato.documenti.text
															</a>
														#end
														#if ($allegato.link.destination != '''')
														<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.link.text">
															$allegato.link.text
														</a>
														#end
														<br />
														<span>$allegato.desc.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.link && $content.link.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-linkcorrelati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_VID_linkcorrelati")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								#set ($contalink = 1)
								#foreach ($altrilink in $content.link)
									#if ($altrilink.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$altrilink.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $altrilink.text" #if($altrilink.symbolicLink.destType == 1)target="_blank" #end><strong>$altrilink.text</strong></a>
											</article>
										</div>
										#set ($contalink = $contalink + 1)
									#end
									#if ($contalink % 2 != 0 || $foreach.count==$content.link.size())
										</div>
									#end
								#end
								</p>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Servizi">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Novità">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Documenti">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','');
INSERT INTO contentmodels (modelid,contenttype,descr,model,stylesheet) VALUES (190001,'VID','Elenco (ico+titolo+abs+img)','
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-video scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
      $content.data.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo">
  	
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
 	
	</div>
	<div class="scheda-anteprima img-fit-cover">
		<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.foto.text">
        <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-play"></use></svg>
 			</figure>
 		</a>
	</div>
</article>
','');
