INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('single_frame_page','Single Frame Page','<frames>
	<frame pos="0">
		<descr>Sample Frame</descr>
		<sketch x1="0" y1="0" x2="11" y2="1" />
	</frame>
</frames>',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title><@wp.currentPage param="title" /></title>
</head>
<body>
<h1><@wp.currentPage param="title" /></h1>
<a href="<@wp.url page="homepage" />" >Home</a><br>
<div><@wp.show frame=0 /></div>
</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('home','Home Page',NULL,NULL,NULL);
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('1-column','1 Column','<?xml version="1.0" encoding="UTF-8"?>
<frames>
  <frame pos="0">
      <descr>Logo</descr>
      <sketch x1="0" y1="0" x2="2" y2="0" />
      <defaultWidget code="logo" />
  </frame>
  <frame pos="1">
      <descr>Navigation bar</descr>
      <sketch x1="3" y1="0" x2="5" y2="0" />
      <defaultWidget code="navigation-menu" />
  </frame>
  <frame pos="2">
      <descr>Search</descr>
      <sketch x1="6" y1="0" x2="8" y2="0" />
      <defaultWidget code="search_form" />
  </frame>
  <frame pos="3">
      <descr>Login</descr>
      <sketch x1="9" y1="0" x2="11" y2="0" />
      <defaultWidget code="keycloak-login" />
  </frame>
  <frame pos="4" main="true">
      <descr>Frame 1</descr>
      <sketch x1="0" y1="1" x2="11" y2="1" />
  </frame>
  <frame pos="5">
      <descr>Frame 2</descr>
      <sketch x1="0" y1="2" x2="11" y2="2" />
  </frame>
  <frame pos="6">
      <descr>Frame 3</descr>
      <sketch x1="0" y1="3" x2="11" y2="3" />
  </frame>
  <frame pos="7">
      <descr>Frame 4</descr>
      <sketch x1="0" y1="4" x2="11" y2="4" />
  </frame>
  <frame pos="8">
      <descr>Footer</descr>
      <sketch x1="0" y1="5" x2="11" y2="5" />
  </frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.info key="systemParam" paramName="applicationBaseURL" var="appUrl" />

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            <@wp.currentPage param="title" />
        </title>
        <meta name="viewport" content="width=device-width,  user-scalable=no" />
        <link rel="icon" href="${appUrl}favicon.png" type="image/png" />
        <!-- Custom OOTB page template styles -->
        <link rel="stylesheet" href="<@wp.resourceURL />static/css/ootb/page-templates/index.css" rel="stylesheet">
        <!-- Carbon Design System -->
        <@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />
        <@wp.fragment code="keycloak_auth" escapeXml=false />

        <@wp.outputHeadInfo type="CSS">
            <link rel="stylesheet" type="text/css" href="<@wp.cssURL /><@wp.printHeadInfo />" />
        </@wp.outputHeadInfo>
        </head>
        <body>
          <header-fragment app-url="${appUrl}">
            <template>
                <@wp.show frame=0 />
                <@wp.show frame=1 />
                <@wp.show frame=2 />
                <@wp.show frame=3 />
            </template>
          </header-fragment>
          <div class="bx--grid Homepage__body">
            <div class="bx--row"><@wp.show frame=4 /></div>
            <div class="bx--row"><@wp.show frame=5 /></div>
            <div class="bx--row"><@wp.show frame=6 /></div>
            <div class="bx--row"><@wp.show frame=7 /></div>
          </div>
          <div class="Homepage__footer"><@wp.show frame=8 /></div>
        </body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('1-2x4-1-column','1-2x4-1 Columns','<?xml version="1.0" encoding="UTF-8"?>
<frames>
  <frame pos="0">
      <descr>Logo</descr>
      <sketch x1="0" y1="0" x2="2" y2="0" />
      <defaultWidget code="logo" />
  </frame>
  <frame pos="1">
      <descr>Navigation Menu</descr>
      <sketch x1="3" y1="0" x2="5" y2="0" />
      <defaultWidget code="navigation-menu" />
  </frame>
  <frame pos="2">
      <descr>Search</descr>
      <sketch x1="6" y1="0" x2="8" y2="0" />
      <defaultWidget code="search_form" />
  </frame>
  <frame pos="3">
      <descr>Login</descr>
      <sketch x1="9" y1="0" x2="11" y2="0" />
      <defaultWidget code="keycloak-login" />
  </frame>
  <frame pos="4">
      <descr>Frame 1</descr>
      <sketch x1="0" y1="1" x2="5" y2="1" />
  </frame>
  <frame pos="5">
      <descr>Frame 2</descr>
      <sketch x1="6" y1="1" x2="11" y2="1" />
  </frame>
  <frame pos="6">
      <descr>Frame 3</descr>
      <sketch x1="0" y1="2" x2="5" y2="2" />
  </frame>
  <frame pos="7">
      <descr>Frame 4</descr>
      <sketch x1="6" y1="2" x2="11" y2="2" />
  </frame>
  <frame pos="8">
      <descr>Frame 5</descr>
      <sketch x1="0" y1="3" x2="5" y2="3" />
  </frame>
  <frame pos="9">
      <descr>Frame 6</descr>
      <sketch x1="6" y1="3" x2="11" y2="3" />
  </frame>
  <frame pos="10">
      <descr>Frame 7</descr>
      <sketch x1="0" y1="4" x2="5" y2="4" />
  </frame>
  <frame pos="11">
      <descr>Frame 8</descr>
      <sketch x1="6" y1="4" x2="11" y2="4" />
  </frame>
  <frame pos="12">
      <descr>Footer</descr>
      <sketch x1="0" y1="5" x2="11" y2="5" />
  </frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.info key="systemParam" paramName="applicationBaseURL" var="appUrl" />

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            <@wp.currentPage param="title" />
        </title>
        <meta name="viewport" content="width=device-width,  user-scalable=no" />
        <link rel="icon" href="${appUrl}favicon.png" type="image/png" />
        <!-- Custom OOTB page template styles -->
        <link rel="stylesheet" href="<@wp.resourceURL />static/css/ootb/page-templates/index.css" rel="stylesheet">
        <!-- Carbon Design System -->
        <@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />
        <@wp.fragment code="keycloak_auth" escapeXml=false />

        <@wp.outputHeadInfo type="CSS">
            <link rel="stylesheet" type="text/css" href="<@wp.cssURL /><@wp.printHeadInfo />" />
        </@wp.outputHeadInfo>
        </head>
        <body>
          <header-fragment app-url="${appUrl}">
            <template>
                <@wp.show frame=0 />
                <@wp.show frame=1 />
                <@wp.show frame=2 />
                <@wp.show frame=3 />
            </template>
          </header-fragment>
          <div class="bx--grid Homepage__body">
            <div class="bx--row">
              <div class="bx--col-sm-4 bx--col-md-4 bx--no-gutter">
                  <div><@wp.show frame=4 /></div>
                  <div><@wp.show frame=6 /></div>
                  <div><@wp.show frame=8 /></div>
                  <div><@wp.show frame=10 /></div>
              </div>
              <div class="bx--col-sm-4 bx--col-md-4 bx--no-gutter">
                  <div><@wp.show frame=5 /></div>
                  <div><@wp.show frame=7 /></div>
                  <div><@wp.show frame=9 /></div>
                  <div><@wp.show frame=11 /></div>
                </div>
            </div>
          </div>
          <div class="Homepage__footer"><@wp.show frame=12 /></div>
        </body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('1-2-column','1-2 Columns','<?xml version="1.0" encoding="UTF-8"?>
<frames>
  <frame pos="0">
      <descr>Logo</descr>
      <sketch x1="0" y1="0" x2="2" y2="0" />
      <defaultWidget code="logo" />
  </frame>
  <frame pos="1">
      <descr>Navigation Menu</descr>
      <sketch x1="3" y1="0" x2="5" y2="0" />
      <defaultWidget code="navigation-menu" />
  </frame>
  <frame pos="2">
      <descr>Search</descr>
      <sketch x1="6" y1="0" x2="8" y2="0" />
      <defaultWidget code="search_form" />
  </frame>
  <frame pos="3">
      <descr>Login</descr>
      <sketch x1="9" y1="0" x2="11" y2="0" />
      <defaultWidget code="keycloak-login" />
  </frame>
  <frame pos="4">
      <descr>Frame 1</descr>
      <sketch x1="0" y1="1" x2="5" y2="1" />
  </frame>
  <frame pos="5">
      <descr>Frame 2</descr>
      <sketch x1="6" y1="1" x2="11" y2="1" />
  </frame>
  <frame pos="6">
      <descr>Frame 3</descr>
      <sketch x1="0" y1="2" x2="5" y2="2" />
  </frame>
  <frame pos="7">
      <descr>Frame 4</descr>
      <sketch x1="6" y1="2" x2="11" y2="2" />
  </frame>
  <frame pos="8">
      <descr>Frame 5</descr>
      <sketch x1="0" y1="3" x2="5" y2="3" />
  </frame>
  <frame pos="9">
      <descr>Frame 6</descr>
      <sketch x1="6" y1="3" x2="11" y2="3" />
  </frame>
  <frame pos="10">
      <descr>Frame 7</descr>
      <sketch x1="0" y1="4" x2="5" y2="4" />
  </frame>
  <frame pos="11">
      <descr>Frame 8</descr>
      <sketch x1="6" y1="4" x2="11" y2="4" />
  </frame>
  <frame pos="12">
      <descr>Frame 9</descr>
      <sketch x1="0" y1="5" x2="5" y2="5" />
  </frame>
  <frame pos="13">
      <descr>Frame 10</descr>
      <sketch x1="6" y1="5" x2="11" y2="5" />
  </frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.info key="systemParam" paramName="applicationBaseURL" var="appUrl" />

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            <@wp.currentPage param="title" />
        </title>
        <meta name="viewport" content="width=device-width,  user-scalable=no" />
        <link rel="icon" href="${appUrl}favicon.png" type="image/png" />
        <!-- Custom OOTB page template styles -->
        <link rel="stylesheet" href="<@wp.resourceURL />static/css/ootb/page-templates/index.css" rel="stylesheet">
        <!-- Carbon Design System -->
        <@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />
        <@wp.fragment code="keycloak_auth" escapeXml=false />

        <@wp.outputHeadInfo type="CSS">
            <link rel="stylesheet" type="text/css" href="<@wp.cssURL /><@wp.printHeadInfo />" />
        </@wp.outputHeadInfo>
        </head>
        <body>
          <header-fragment app-url="${appUrl}">
            <template>
                <@wp.show frame=0 />
                <@wp.show frame=1 />
                <@wp.show frame=2 />
                <@wp.show frame=3 />
            </template>
          </header-fragment>
          <div class="bx--grid Homepage__body">
            <div class="bx--row">
              <div class="bx--col-sm-4 bx--col-md-4 bx--no-gutter">
                  <div><@wp.show frame=4 /></div>
                  <div><@wp.show frame=6 /></div>
                  <div><@wp.show frame=8 /></div>
                  <div><@wp.show frame=10 /></div>
                  <div><@wp.show frame=12/></div>
              </div>
              <div class="bx--col-sm-4 bx--col-md-4 bx--no-gutter">
                  <div><@wp.show frame=5 /></div>
                  <div><@wp.show frame=7 /></div>
                  <div><@wp.show frame=9 /></div>
                  <div><@wp.show frame=11 /></div>
                  <div><@wp.show frame=13 /></div>
                </div>
            </div>
          </div>
        </body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('1-2x2-1-column','1-2x2-1 Columns','<?xml version="1.0" encoding="UTF-8"?>
<frames>
  <frame pos="0">
      <descr>Logo</descr>
      <sketch x1="0" y1="0" x2="2" y2="0" />
      <defaultWidget code="logo" />
  </frame>
  <frame pos="1">
      <descr>Navigation Menu</descr>
      <sketch x1="3" y1="0" x2="5" y2="0" />
      <defaultWidget code="navigation-menu" />
  </frame>
  <frame pos="2">
      <descr>Search</descr>
      <sketch x1="6" y1="0" x2="8" y2="0" />
      <defaultWidget code="search_form" />
  </frame>
  <frame pos="3">
      <descr>Login</descr>
      <sketch x1="9" y1="0" x2="11" y2="0" />
      <defaultWidget code="keycloak-login" />
  </frame>
  <frame pos="4">
      <descr>Frame 1</descr>
      <sketch x1="0" y1="1" x2="5" y2="2" />
  </frame>
  <frame pos="5">
      <descr>Frame 2</descr>
      <sketch x1="6" y1="1" x2="11" y2="1" />
  </frame>
  <frame pos="6">
      <descr>Frame 3</descr>
      <sketch x1="6" y1="2" x2="11" y2="2" />
  </frame>
  <frame pos="7">
      <descr>Frame 4</descr>
      <sketch x1="0" y1="3" x2="5" y2="4" />
  </frame>
  <frame pos="8">
      <descr>Frame 5</descr>
      <sketch x1="6" y1="3" x2="11" y2="3" />
  </frame>
  <frame pos="9">
      <descr>Frame 6</descr>
      <sketch x1="6" y1="4" x2="11" y2="4" />
  </frame>
  <frame pos="10">
      <descr>Footer</descr>
      <sketch x1="0" y1="5" x2="11" y2="5" />
  </frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.info key="systemParam" paramName="applicationBaseURL" var="appUrl" />

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            <@wp.currentPage param="title" />
        </title>
        <meta name="viewport" content="width=device-width,  user-scalable=no" />
        <link rel="icon" href="${appUrl}favicon.png" type="image/png" />
        <!-- Custom OOTB page template styles -->
        <link rel="stylesheet" href="<@wp.resourceURL />static/css/ootb/page-templates/index.css" rel="stylesheet">
        <!-- Carbon Design System -->
        <@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />
        <@wp.fragment code="keycloak_auth" escapeXml=false />

        <@wp.outputHeadInfo type="CSS">
            <link rel="stylesheet" type="text/css" href="<@wp.cssURL /><@wp.printHeadInfo />" />
        </@wp.outputHeadInfo>
        </head>
        <body>
          <header-fragment app-url="${appUrl}">
            <template>
                <@wp.show frame=0 />
                <@wp.show frame=1 />
                <@wp.show frame=2 />
                <@wp.show frame=3 />
            </template>
          </header-fragment>
          <div class="bx--grid Homepage__body">
            <div class="bx--row">
              <div class="bx--col-sm-4 bx--col-md-4 bx--no-gutter">
                  <div><@wp.show frame=4 /></div>
                  <div><@wp.show frame=7 /></div>
              </div>
              <div class="bx--col-sm-4 bx--col-md-4 bx--no-gutter">
                  <div><@wp.show frame=5 /></div>
                  <div><@wp.show frame=6 /></div>
                  <div><@wp.show frame=8 /></div>
                  <div><@wp.show frame=9 /></div>
                </div>
            </div>
          </div>
          <div class="Homepage__footer"><@wp.show frame=10 /></div>
        </body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('content-page','Content Page','<?xml version="1.0" encoding="UTF-8"?>
<frames>
  <frame pos="0">
      <descr>Logo</descr>
      <sketch x1="0" y1="0" x2="2" y2="0" />
      <defaultWidget code="logo" />
  </frame>
  <frame pos="1">
      <descr>Navigation bar</descr>
      <sketch x1="3" y1="0" x2="5" y2="0" />
      <defaultWidget code="navigation-menu" />
  </frame>
  <frame pos="2">
      <descr>Search</descr>
      <sketch x1="6" y1="0" x2="8" y2="0" />
      <defaultWidget code="search_form" />
  </frame>
  <frame pos="3">
      <descr>Login</descr>
      <sketch x1="9" y1="0" x2="11" y2="0" />
      <defaultWidget code="keycloak-login" />
  </frame>
  <frame pos="4" main="true">
      <descr>Content Frame</descr>
      <defaultWidget code="content_viewer">
        <properties>
          <property key="modelId">10002</property>
        </properties>
      </defaultWidget>
      <sketch x1="0" y1="1" x2="11" y2="1" />
  </frame>
  <frame pos="5">
      <descr>Frame 2</descr>
      <sketch x1="0" y1="2" x2="11" y2="2" />
  </frame>
  <frame pos="6">
      <descr>Frame 3</descr>
      <sketch x1="0" y1="3" x2="11" y2="3" />
  </frame>
  <frame pos="7">
      <descr>Frame 4</descr>
      <sketch x1="0" y1="4" x2="11" y2="4" />
  </frame>
  <frame pos="8">
      <descr>Footer</descr>
      <sketch x1="0" y1="5" x2="11" y2="5" />
  </frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.info key="systemParam" paramName="applicationBaseURL" var="appUrl" />

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>
            <@wp.currentPage param="title" />
        </title>
        <meta name="viewport" content="width=device-width,  user-scalable=no" />
        <link rel="icon" href="${appUrl}favicon.png" type="image/png" />
        <!-- Custom OOTB page template styles -->
        <link rel="stylesheet" href="<@wp.resourceURL />static/css/ootb/page-templates/index.css" rel="stylesheet">
        <!-- Carbon Design System -->
        <@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />
        <@wp.fragment code="keycloak_auth" escapeXml=false />

        <@wp.outputHeadInfo type="CSS">
            <link rel="stylesheet" type="text/css" href="<@wp.cssURL /><@wp.printHeadInfo />" />
        </@wp.outputHeadInfo>
        </head>
        <body>
          <header-fragment app-url="${appUrl}">
            <template>
                <@wp.show frame=0 />
                <@wp.show frame=1 />
                <@wp.show frame=2 />
                <@wp.show frame=3 />
            </template>
          </header-fragment>
          <div class="bx--grid Homepage__body">
            <div class="bx--row"><@wp.show frame=4 /></div>
            <div class="bx--row"><@wp.show frame=5 /></div>
            <div class="bx--row"><@wp.show frame=6 /></div>
            <div class="bx--row"><@wp.show frame=7 /></div>
          </div>
          <div class="Homepage__footer"><@wp.show frame=8 /></div>
        </body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_argumentpage','Cittametro - Argomento','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Argomento</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>In evidenza</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Amministrazione</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Servizi</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Novita</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="7">
		<descr>Documenti</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="8">
		<descr>Gallerie</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="9">
		<descr>Altri contenuti</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
	<frame pos="10" main="true">
		<descr>Dettaglio finto pagina volante</descr>
		<sketch x1="0" y1="9" x2="11" y2="9" />
	</frame>
	<frame pos="11">
		<descr>Footer</descr>
		<sketch x1="0" y1="10" x2="11" y2="10" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />

<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
					<#--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cittametro_template_header_navigation_menu.jsp" />
					-->
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 						    <@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
				<div class="bg-grigio">
					<section id="intro-argomenti">
						<@wp.show frame=2 />
					</section>
					<section id="argomenti-inevidenza" class="sezioni">
						<div class="container">
							<@wp.show frame=3 />
						</div>
					</section>
				</div>
				<section id="argomenti-amministrazione">
					<div class="container">
						<@wp.show frame=4 />
					</div>
				</section>
				<section id="argomenti-servizi">
					<div class="container">
						<@wp.show frame=5 />
					</div>
				</section>
				<section id="argomenti-novita" class="sezioni bg-grigio">
					<div class="container">
						<@wp.show frame=6 />
					</div>
				</section>
				<section id="argomenti-documenti">
					<div class="container">
						<@wp.show frame=7 />
					</div>
				</section>
				<section id="argomenti-gallerie">
					<div class="container">
						<@wp.show frame=8 />
					</div>
				</section>
				<@wp.show frame=9 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=11 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
	</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_internalpage','Cittametro - Dettaglio','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>main</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Servizio 1</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Servizio 2</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Footer</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="home.css" />
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />

<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	
		<#--
		<@wp.fragment code="keycloak_auth" escapeXml=false />
	    <@wp.fragment code="cagliari_template_googletagmanagerbody" escapeXml=false />
		-->
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
                      <#--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cagliari_template_header_navigation_menu.jsp" />
                      -->
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cagliari_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
                <@wp.fragment code="cittametro_briciole" escapeXml=false />
            				<@wp.show frame=2 />
				<section id="sezioni-servizi-interno">
	                <div class="container">
	                    <@wp.show frame=3 />
				    </div>
                </section>
                <section id="sezioni-eventi-interno">
                    <div class="container">
				        <@wp.show frame=4 />
				    </div>
                </section>
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=5 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
		
	</body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_internalpagesection2','Cittametro - Sezioni interne 7 frame con menu','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Contenuto principale</descr>
		<sketch x1="0" y1="1" x2="7" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Menu sezione</descr>
		<sketch x1="8" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="4">
		<descr>Altro contenuto 1</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="5">
		<descr>Altro contenuto 2</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="6">
		<descr>Altro contenuto 4</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="7">
		<descr>Altro contenuto 4</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="8">
		<descr>Footer</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />
			    
<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
					 <!--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cittametro_template_header_navigation_menu.jsp" />
 						-->
						 <ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
			    <@wp.fragment code="cittametro_template_briciole" escapeXml=false />
				<section id="intro">
                    <div class="container">
                    	<div class="row">
                    		<div class="offset-lg-1 col-lg-6 col-md-7">
                    			<div class="titolo-sezione">
                    				<@wp.show frame=2 />
                    			</div>
                    		</div>
                    		<div class="offset-lg-1 col-lg-3 offset-lg-1 offset-md-1 col-md-4">
                    			<aside id="menu-sezione">
                    				<@wp.show frame=3 />
                    			</aside>
                    		</div>
                    	</div>
                    </div>
				</section>
				<section class="mb32">
                	<div class="container">
                		<@wp.show frame=4 />
                	</div>
                </section>
                <section class="mb32">
	                <div class="container">
	                    <@wp.show frame=5 />
				    </div>
                </section>
                <section class="mb32">
	                <div class="container">
	                    <@wp.show frame=6 />
				    </div>
                </section>
                <section class="mb32">
	                <div class="container">
	                    <@wp.show frame=7 />
				    </div>
                </section>
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=8 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
	</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_sectionpage','Cittametro - Sezioni principali','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2" main="true">
		<descr>Intro sezione</descr>
		<sketch x1="0" y1="1" x2="7" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Cerca</descr>
		<sketch x1="0" y1="2" x2="7" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Menu sezione</descr>
		<sketch x1="8" y1="1" x2="11" y2="2" />
	</frame>
	<frame pos="5">
		<descr>In evidenza</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="6">
		<descr>Macro aree - Notizie</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="7">
		<descr>Comunicati stampa</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="8">
		<descr>Avvisi</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="9">
		<descr>Eventi</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="10">
		<descr>Argomenti</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
	<frame pos="11">
		<descr>Altro</descr>
		<sketch x1="0" y1="9" x2="11" y2="9" />
	</frame>
	<frame pos="12">
		<descr>Footer</descr>
		<sketch x1="0" y1="10" x2="11" y2="10" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />
<@wp.headInfo type="CSS_CA_INT" info="home.css" />
  
<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	
		<#--
		<@wp.fragment code="keycloak_auth" escapeXml=false />
	    <@wp.fragment code="cagliari_template_googletagmanagerbody" escapeXml=false />
		-->
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
                      <#--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cagliari_template_header_navigation_menu.jsp" />
                      -->
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cagliari_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
                <@wp.fragment code="cittametro_briciole" escapeXml=false />
			    <section id="intro">
                    <div class="container">
                    	<div class="row">
                    		<div class="offset-lg-1 col-lg-6 col-md-7">
                    			<div class="titolo-sezione">
                    				<@wp.show frame=2 />
                    				<@wp.show frame=3 />
                    			</div>
                    		</div>
                    		<div class="offset-lg-1 col-lg-3 offset-lg-1 offset-md-1 col-md-4">
                    			<aside id="menu-sezione">
                    				<@wp.show frame=4 />
                    			</aside>
                    		</div>
                    	</div>
                    </div>
				</section>
				<section id="sezioni-inevidenza" class="bg-grigio">
                	<div class="container">
                		<@wp.show frame=5 />
                	</div>
                </section>
                <section id="sezioni-servizi">
	                <div class="container">
	                    <@wp.show frame=6 />
				    </div>
                </section>
                <section id="sezioni-eventi">
                    <div class="container">
				        <@wp.show frame=7 />
				    </div>
                </section>
				<section id="sezioni-avvisi">
	                <div class="container">        
				        <@wp.show frame=8 />
				    </div>
                </section>
				<section id="sezioni-comunicati">
	                <div class="container">        
				        <@wp.show frame=9 />
				    </div>
                </section>
				<section id="sezioni-argomenti">
	                <div class="container">
				        <@wp.show frame=10 />
				    </div>
                </section>
                <@wp.show frame=11 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=12 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
		
	</body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_internalpagedetail','Cittametro - Dettaglio con gallerie o correlati vl','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2" main="true">
		<descr>Contenuti</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Contenuti 2</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Contenuti 3</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Footer</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>
<#assign jpseo=JspTaglibs["/jpseo-aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign jacms=JspTaglibs["/jacms-aps-core"]>

 	<html lang="<@wp.info key="currentLang" />">
 	<head>
		<base href="<@wp.info key="systemParam" paramName="applicationBaseURL" />">
 		<!--[if IE]><script nonce="<@wp.cspNonce />" type="text/javascript" >
 			(function() {
 				var baseTag = document.getElementsByTagName(''base'')[0];
 				baseTag.href = baseTag.href;
 			})();
 		</script><![endif]-->
		<@wp.i18n key="CITTAMETRO_PORTAL_TITLE" var="siteName"/>
  		<@wp.currentPage param="code" var="pageCode" />    
  		<@wp.currentPage param="title" var="pageTitle" />

		<@ca.cagliariPageInfoTag pageCode="${pageCode}" info="isPublishOnFly" var="isPublishOnFly"/>
		<#if (isPublishOnFly == "true")>
			<@ca.cagliariMainFrameContentTag var="content"/>
			<#if content??>
				<#if (content.getAttributeByRole(''jpsocial:title''))??>
					<#assign title=content.getAttributeByRole(''jpsocial:title'').text>
					<#if title??>
						<#assign pageTitle = title>
					</#if>
				</#if>
				<#if (content.getAttributeByRole(''jpsocial:description''))??>
					<#assign description=content.getAttributeByRole(''jpsocial:description'').text>    
				</#if>
				<#if (content.getAttributeByRole(''jpsocial:image''))??>
					<#assign image=content.getAttributeByRole(''jpsocial:image'').getImagePath("4")>
				</#if>
			</#if>
		</#if>

		<#if (pageCode == "homepage")>
			<title>${siteName}</title>
		<#else>
			<#if (pageCode == "arg_01_dett")>
				<@cw.currentArgument var="currentArg" />
				<#if (currentArg??)>
					<@cw.contentList listName="contentList" contentType="ARG" category="${currentArg}"/>
					<#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
						<#list contentList as contentId>
							<title>${siteName} | <@jacms.content contentId="${contentId}" modelId="400012" /></title>
							<#assign description><@jacms.content contentId="${contentId}" modelId="400013" /></#assign>
						</#list>
					</#if>
					<#assign contentList="">
				</#if>
			<#else>
				<title>${siteName} | ${pageTitle}</title>
			</#if>
		</#if>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

 		<@jpseo.currentPage param="description" var="metaDescr" />
 		<#if metaDescr?? && metaDescr?has_content>
			<meta name="description" content="${metaDescr}" />
		<#else>
			<#if description??>
				<meta name="description" content="${description}" />
			<#else>
				<@ca.cagliariMainFrameContentTag var="content"/>  <#--   DA CAMBIARE -->
				<#if content??>
					<#assign description=content.getAttributeByRole(''jacms:description'').text>
					<meta name="description" content="${description}" />
				</#if>
			</#if>
		</#if>
		<@jpseo.currentPage param="keywords" var="metaKeyword" />
		<#if metaKeyword??>
			<meta name="keywords" content="${metaKeyword}" />
		</#if>

 		<link rel="apple-touch-icon" sizes="57x57" href="<@wp.imgURL />apple-icon-57x57.png" />
 		<link rel="apple-touch-icon" sizes="60x60" href="<@wp.imgURL />apple-icon-60x60.png" />
 		<link rel="apple-touch-icon" sizes="72x72" href="<@wp.imgURL />apple-icon-72x72.png" />
 		<link rel="apple-touch-icon" sizes="76x76" href="<@wp.imgURL />apple-icon-76x76.png" />
 		<link rel="apple-touch-icon" sizes="114x114" href="<@wp.imgURL />apple-icon-114x114.png" />
 		<link rel="apple-touch-icon" sizes="120x120" href="<@wp.imgURL />apple-icon-120x120.png" />
 		<link rel="apple-touch-icon" sizes="144x144" href="<@wp.imgURL />apple-icon-144x144.png" />
 		<link rel="apple-touch-icon" sizes="152x152" href="<@wp.imgURL />apple-icon-152x152.png" />
 		<link rel="apple-touch-icon" sizes="180x180" href="<@wp.imgURL />apple-icon-180x180.png" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-32x32.png" sizes="32x32" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />android-chrome-192x192.png" sizes="192x192" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-96x96.png" sizes="96x96" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-16x16.png" sizes="16x16" />





 <!--   ##########BUONO############   -->
<meta charset="utf-8" />
      	<title><@wp.currentPage param="title" /></title>
      	<meta name="viewport" content="width=device-width,  user-scalable=no" />
 		<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery-3.3.1.min.js" ></script>        
		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/angular-material.cagliari.css"  />         
		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/angular-material.css"  />  
		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/font/fonts.min.css" />
        <link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/bootstrap-italia_1.2.0.min.css" />

 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/cittametro.css" rel="stylesheet" type="text/css" />
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/areapersonale.css" rel="stylesheet" type="text/css" />
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/cagliari-print.css" rel="stylesheet" type="text/css" media="print" />
 		<@wp.outputHeadInfo type="CSS_CA_INT">
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_EXT">
 		<link href="<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_PLGN">
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 
 		<@wp.outputHeadInfo type="JS_CA_TOP_EXT">nonce="<@wp.cspNonce />" src="<@wp.printHeadInfo />" ></script></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="JS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="JS_CA_TOP"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-hompepage-bundle/static/js/<@wp.printHeadInfo />" ></script></@wp.outputHeadInfo>


  		<@wp.i18n key="CAGLIARI_PORTAL_TITLE" var="siteName"/>
  		<@wp.currentPage param="code" var="pageCode" />    
  		<@wp.currentPage param="title" var="pageTitle" />
		<#if pageCode?? && pageCode != "">
		<#else>
			<#assign pageCode=''homepage''>
		</#if>
  		<#if pageCode == ''homepage''>
  		 	<#assign pageTitle=siteName>
  		</#if>
     
  		<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" var="description"/>
        <#--
  		<@wp.info key="systemParam" paramName="applicationBaseURL" var="baseURL"/>
		<#assign baseURL = baseURL?substring(0,baseURL?last_index_of("/portale"))>
		 FACEBOOK 
		<meta property="og:site_name" content="${siteName}" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="${pageTitle}" />
		<#if description??>
			<meta property="og:description" content="${description}"/>
		<#else>
			<meta property="og:description" content="-"/>
		</#if>
		<#if image?? && image != "">
			<meta property="og:image" content="${baseURL}${image}" />
		<#else>
			<meta property="og:image" content="${baseURL}<@wp.imgURL/>logo_cagliari_print.png" />
		</#if>
		<meta property="fb:app_id" content="409032892503811" />

		 TWITTER 
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:site" content="@Comune_Cagliari" />
		<meta name="twitter:title" content="${pageTitle}" />
		<#if description??>
			<meta name="twitter:description" content="${description}"/>
		<#else>
			<meta name="twitter:description" content="-"/>
		</#if>    
		<#if image?? && image != "">
			<meta name="twitter:image" content="${baseURL}${image}" />
		<#else>
			<meta name="twitter:image" content="${baseURL}<@wp.resourceURL />cittametro-homepage-bundle/static/img/logo_cittametro_cagliari.png" />
		</#if>
		<@wp.fragment code="cagliari_template_googletagmanager" escapeXml=false />
        
		-->
 	</head>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('service','Service Template','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Simple frame</descr>
		<sketch x1="0" y1="0" x2="11" y2="0" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title><@wp.currentPage param="title" /></title>
</head>
<body data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
<h1><@wp.currentPage param="title" /></h1>
<a href="<@wp.url page="homepage" />" >Home</a><br>
<div><@wp.show frame=0 /></div>
</body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_reservedpage','Cittametro - Area Personale','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Contenuto principale</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Altri contenuti 1</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Altri contenuti 2</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Altri contenuti con section</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Altri contenuti con section 1</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="7">
		<descr>Altri contenuti con section 2</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="8">
		<descr>Altri contenuti con section 3</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="9">
		<descr>Altri contenuti con section 4</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
	<frame pos="10">
		<descr>Footer</descr>
		<sketch x1="0" y1="9" x2="11" y2="9" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />

<@wp.currentPage param="code" var="currentViewCode" />

<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
    <@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
                     <!--
 						<@c.import url="/WEB-INF/aps/jsp/widgets"cittametro_template_header_navigation_menu.jsp" />
                         -->
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container" class="areapersonale <#if (currentViewCode == ''ap'')>areapersonale-home</#if>">
			    <section class="intestazione bg-grigio">
			        <@wp.fragment code="cittametro_areapersonale-intestazione" escapeXml=false />
                </section>
                <@wp.show frame=2 />
                <@wp.show frame=3 />
                <@wp.show frame=4 />
                <section class="areapersonale-sezione">
	                <div class="container">
		                <@wp.show frame=5 />
		            </div>
                </section>
		        <@wp.show frame=6 />
		        <@wp.show frame=7 />
		        <@wp.show frame=8 />
		        <@wp.show frame=9 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=10 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
	</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('ilpatto_template','Il Patto Template page','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Il Patto - Pulsanti Gestione</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
		<defaultWidget code="cittametro_navbar_orizzontale" />
	</frame>
	<frame pos="3">
		<descr>Il Patto - Statistiche Progetti</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Il Patto - Aree di Intervento</descr>
		<sketch x1="0" y1="3" x2="3" y2="4" />
	</frame>
	<frame pos="5">
		<descr>Il Patto - Mappa dei Comuni</descr>
		<sketch x1="4" y1="3" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Altro 1</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="7">
		<descr>Altro 2</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="8">
		<descr>Altro 3</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="9">
		<descr>Footer</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<!--
<@wp.headInfo type="CSS_CA_INT" info="home.css" />
-->
<!doctype html>
    <@wp.fragment code="cittametro_template_head" escapeXml=false />
    <body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
    <@wp.fragment code="keycloak_auth" escapeXml=false />
        <#--
        <@wp.fragment code="cagliari_template_googletagmanagerbody" escapeXml=false />
        -->

        <div class="body_wrapper push_container clearfix" id="page_top">
            <@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
        
            <header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
                <@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
                <!-- Button Menu -->
                 <button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar icon-bar1"></span>
                    <span class="icon-bar icon-bar2"></span>
                    <span class="icon-bar icon-bar3"></span>
                </button>
                <!--End Button Menu -->
 
                 <!-- Menu -->
                 <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
                     <div class="cbp-menu-wrapper clearfix">
                      <#--
                         <@c.import url="/WEB-INF/aps/jsp/widgets/cagliari_template_header_navigation_menu.jsp" />
                      -->
                         <ul class="utilitymobile">
                             <@wp.show frame=1 />
                         </ul>
                         <ul class="list-inline socialmobile">
                             <@wp.fragment code="cagliari_template_header_social_mobile" escapeXml=false />
                         </ul>
                     </div>
                 </nav>
                 <!-- End Menu -->
                 
                 <@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
                 
                <@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
                <section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
                                    <@wp.show frame=0 />
                                </ul>
                            </div>
                            <div class="col-lg-6 col-md-4 pull-right text-right">
                                <ul class="list_link-utili">
                                <@wp.show frame=1 />
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </header>

            <main id="main_container" class="container-fluid">
                <@wp.fragment code="cittametro_briciole" escapeXml=false />
                <div class="row mt-2">
                    <div class="col-12 col-md-2 m-0"></div>
                    <section 
                        id="ilpatto-pulsanti-gestione" 
                        class="col-12 col-md-8 m-0">
                        <@wp.show frame=2 />
                    </section>
                    <div class="col-12 col-md-2 m-0"></div>

                </div>
               
                <div class="row mt-2">
                    <div class="col-12 col-md-2"></div>
                    <section 
                         id="ilpatto-aree-di-intervento"
                        class="col-12 col-md-8">
                        <@wp.show frame=3 />
                    </section>
                    <div class="col-12 col-md-2"></div>

                </div>
                
                <div class="row mt-2">
                    <div class="col-12 col-md-1"></div>
                    <section 
                         id="ilpatto-aree-di-intervento"
                        class="col-12 col-md-2">
                        <@wp.show frame=4 />
                    </section>
                  
                    <section 
                         id="ilpatto-mappa-dei-comuni"
                        class="col-12 col-md-8">
                        <@wp.show frame=5 />
                    </section>
                    <div class="col-12 col-md-1"></div>
                </div>

                <div class="row mt-2">
                    <div class="col-12">
                                    <@wp.show frame=6 />

                    </div>
                    <div class="col-12">
                                    <@wp.show frame=7 />

                    </div>
                    <div class="col-12">
                                    <@wp.show frame=8 />

                    </div>
                </div>
            </main>
            <footer id="footer">
                <div class="container">
                    <@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
                    <section class="lista-sezioni">
                        <div class="row">
                            <@wp.show frame=9 />
                        </div>
                    </section>
                    <section class="lista-linkutili">
                        <div class="row">
                            <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
                        </div>
                    </section>
                    <@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
                </div>
            </footer>
        </div>
        <@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
        <@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
    </body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_themesitepage','Cittametro - Sito tematico home','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Sito tematico</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>In evidenza</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Servizi</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Banner</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Documenti</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="7">
		<descr>Amministrazione</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="8">
		<descr>Argomenti</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="9">
		<descr>Altri contenuti</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
	<frame pos="10">
		<descr>Altri contenuti</descr>
		<sketch x1="0" y1="9" x2="11" y2="9" />
	</frame>
	<frame pos="11">
		<descr>Footer</descr>
		<sketch x1="0" y1="10" x2="11" y2="10" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />

<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
		<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
					 <!--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cittametro_template_header_navigation_menu.jsp" />
 						-->
						 <ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
                <div class="bg-grigio">
                    <section id="intro-sititematici">
				        <@wp.show frame=2 />
				    </section>
				    <section id="argomenti-inevidenza" class="sezioni">
		                <div class="container">
				            <@wp.show frame=3 />
				        </div>
				    </section>
				</div>
				<section id="sezioni-servizi" class="sezioni-servizi-tema">
	                <div class="container">
				        <@wp.show frame=4 />
				    </div>
				</section>
				<section id="centroinfo">
				    <@wp.show frame=5 />
				</section>
				<section id="sezioni-documenti" class="sezioni-documenti-tema bg-grigio">
	                <div class="container">
				        <@wp.show frame=6 />
				    </div>
				</section>
				<section id="sezioni-amministrazione" class="sezioni-amministrazione-tema">
	                <div class="container">
				        <@wp.show frame=7 />
				    </div>
				</section>
				<section id="sezioni-argomenti">
	                <div class="container">
				        <@wp.show frame=8 />
				    </div>
				</section>
				<@wp.show frame=9 />
				<@wp.show frame=10 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=11 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
	</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_internalpagesection','Cittametro - Sezioni interne con 4 frame','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Contenuto principale</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>In evidenza</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Altri contenuti 1</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Altri contenuti 2</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Footer</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />

<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
      
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
                      	<@wp.fragment code="cittametro_template_header_navigation_menu" escapeXml=false />
						 <ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
			    <@wp.fragment code="cittametro_briciole" escapeXml=false />
				<section id="intro">
                    <div class="container">
                    	<@wp.show frame=2 />
                    </div>
				</section>
				<section id="sezioni-inevidenza" class="bg-grigio">
                	<div class="container">
                		<@wp.show frame=3 />
                	</div>
                </section>
                <section id="sezioni-infoutili">
	                <div class="container">
	                    <@wp.show frame=4 />
				    </div>
                </section>
		        <@wp.show frame=5 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=6 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
	</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_citizensservices','Cittametro - Servizi online cittadini','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Contenuto principale</descr>
		<sketch x1="0" y1="1" x2="7" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Menu sezione</descr>
		<sketch x1="8" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="4">
		<descr>Contenuto riservato 1</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="5">
		<descr>Contenuto riservato 2</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="6">
		<descr>Contenuto riservato 3</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="7">
		<descr>Altro contenuto pubblico</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="8">
		<descr>Footer</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign comCa=JspTaglibs["/WEB-INF/tld/reservedPonMetro.tld"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_BTM" info="interne.css" />
			    
<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
                     <!--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cittametro_template_header_navigation_menu.jsp" />
 						-->
                         <ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
			    <#assign leafletjs_css>
                    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
                </#assign>
                <#assign leafletjs_js>
                    <script nonce="<@wp.cspNonce />" src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" ></script>
                </#assign>
                <@wp.headInfo type="CSS_CA_EXT_GLB" info="${leafletjs_css}" />
                <@wp.headInfo type="JS_CA_EXT_GLB" info="${leafletjs_js}" />
                <@wp.headInfo type="JS_CA_TOP" info="leafletjs_config.min.js" />
			    <@wp.headInfo type="CSS_CA_BTM" info="sezioni.css" />
                <@wp.headInfo type="CSS_CA_BTM" info="interne.css" />

                <@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
                <@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.css" />
                <@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.css" />
                <@wp.headInfo type="JS_CA_BTM" info="filtri-controller.js" />
                <@wp.headInfo type="JS_CA_BTM" info="filtri-service.js" />

                <#assign js_slide="$(document).ready(function() {
                	var owl = $(''#owl-correlati'');
                	owl.owlCarousel({
                		nav:false,
                		startPosition: 0,
                		autoPlay:false,
                		responsiveClass:true,
                		responsive:{
                				0:{
                					items:1,
                				},
                				576: {
                					items:2,
                				},
                				768: {
                					items:2,
                				},
                				991: {
                					items:4,
                				},
                			}
                	});

                	var owl2 = $(''#owl-galleria'');
                	owl2.owlCarousel({
                		nav:false,
                		startPosition: 0,
                		autoPlay:false,
                		responsiveClass:true,
                		responsive:{
                				0:{
                					items:1,
                				},
                				576: {
                					items:2,
                				},
                				768: {
                					items:2,
                				},
                				991: {
                					items:3,
                				},
                			}
                	});

                });" />
                <@wp.headInfo type="JS_CA_BTM_INC" info="${js_slide}" />
			
			    <@wp.fragment code="cittametro_template_briciole" escapeXml=false />
				<section id="intro">
                    <div class="container">
                    	<div class="row">
                    		<div class="offset-lg-1 col-lg-6 col-md-7">
                    			<div class="titolo-sezione">
                    				<@wp.show frame=2 />
                    			</div>
                    		</div>
                    		<div class="offset-lg-1 col-lg-3 offset-lg-1 offset-md-1 col-md-4">
                    			<aside id="menu-sezione">
                    				<@wp.show frame=3 />
                    			</aside>
                    		</div>
                    	</div>
                    </div>
				</section>
				<@comCa.serviziOnlineAuthorityChecker authLevel="cittadini" var="testlogin">
					<section>
						<div class="container">
							<@wp.show frame=4 />
						</div>
					</section>
					<section>
						<div class="container">
							<@wp.show frame=5 />
						</div>
					</section>
					<section>
						<div class="container">
							<@wp.show frame=6 />
						</div>
					</section>
				</@comCa.serviziOnlineAuthorityChecker>
				<@wp.freemarkerTemplateParameter var="testlogin" valueName="testlogin" removeOnEndTag=true >
					<#if testlogin?? && !testlogin>
						<@wp.fragment code="cittametro_unauthorized_alert_service_cittadino" escapeXml=false />
					</#if>
				</@wp.freemarkerTemplateParameter>
                <section>
	                <div class="container">
	                    <@wp.show frame=7 />
				    </div>
                </section>
			</main>
			<footer id="footer" class="mt100">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=8 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
		<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_residentsservices','Cittametro - Servizi online residenti','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Contenuto principale</descr>
		<sketch x1="0" y1="1" x2="7" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Menu sezione</descr>
		<sketch x1="8" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="4">
		<descr>Contenuto riservato 1</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="5">
		<descr>Contenuto riservato 2</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="6">
		<descr>Contenuto riservato 3</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="7">
		<descr>Altro contenuto pubblico</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="8">
		<descr>Footer</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign comCa=JspTaglibs["/WEB-INF/tld/reservedPonMetro.tld"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_BTM" info="interne.css" />
			    
<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
					 <!--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cittametro_template_header_navigation_menu.jsp" />
 						-->
						 <ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
			    <#assign leafletjs_css>
                    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
                </#assign>
                <#assign leafletjs_js>
                    <script nonce="<@wp.cspNonce />" src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" ></script>
                </#assign>
                <@wp.headInfo type="CSS_CA_EXT_GLB" info="${leafletjs_css}" />
                <@wp.headInfo type="JS_CA_EXT_GLB" info="${leafletjs_js}" />
                <@wp.headInfo type="JS_CA_TOP" info="leafletjs_config.min.js" />
			    <@wp.headInfo type="CSS_CA_BTM" info="sezioni.css" />
                <@wp.headInfo type="CSS_CA_BTM" info="interne.css" />

                <@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
                <@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.css" />
                <@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.css" />
                <@wp.headInfo type="JS_CA_BTM" info="filtri-controller.js" />
                <@wp.headInfo type="JS_CA_BTM" info="filtri-service.js" />

                <#assign js_slide="$(document).ready(function() {
                	var owl = $(''#owl-correlati'');
                	owl.owlCarousel({
                		nav:false,
                		startPosition: 0,
                		autoPlay:false,
                		responsiveClass:true,
                		responsive:{
                				0:{
                					items:1,
                				},
                				576: {
                					items:2,
                				},
                				768: {
                					items:2,
                				},
                				991: {
                					items:4,
                				},
                			}
                	});

                	var owl2 = $(''#owl-galleria'');
                	owl2.owlCarousel({
                		nav:false,
                		startPosition: 0,
                		autoPlay:false,
                		responsiveClass:true,
                		responsive:{
                				0:{
                					items:1,
                				},
                				576: {
                					items:2,
                				},
                				768: {
                					items:2,
                				},
                				991: {
                					items:3,
                				},
                			}
                	});

                });" />
                <@wp.headInfo type="JS_CA_BTM_INC" info="${js_slide}" />
			
			    <@wp.fragment code="cittametro_template_briciole" escapeXml=false />
				<section id="intro">
                    <div class="container">
                    	<div class="row">
                    		<div class="offset-lg-1 col-lg-6 col-md-7">
                    			<div class="titolo-sezione">
                    				<@wp.show frame=2 />
                    			</div>
                    		</div>
                    		<div class="offset-lg-1 col-lg-3 offset-lg-1 offset-md-1 col-md-4">
                    			<aside id="menu-sezione">
                    				<@wp.show frame=3 />
                    			</aside>
                    		</div>
                    	</div>
                    </div>
				</section>
				<@comCa.serviziOnlineAuthorityChecker authLevel="residente" var="testlogin">
					<section>
						<div class="container">
							<@wp.show frame=4 />
						</div>
					</section>
					<section>
						<div class="container">
							<@wp.show frame=5 />
						</div>
					</section>
					<section>
						<div class="container">
							<@wp.show frame=6 />
						</div>
					</section>
				</@comCa.serviziOnlineAuthorityChecker>
				<@wp.freemarkerTemplateParameter var="testlogin" valueName="testlogin" removeOnEndTag=true >
					<#if testlogin?? && !testlogin>
						<@wp.fragment code="cittametro_unauthorized_alert_service_residente" escapeXml=false /> 
					</#if>
				</@wp.freemarkerTemplateParameter>
                <section>
	                <div class="container">
	                    <@wp.show frame=7 />
				    </div>
                </section>
			</main>
			<footer id="footer" class="mt100">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=8 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
	</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_internalpagegallery','Cittametro - Dettaglio con gallerie o correlati','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Contenuti</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Contenuti 2</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Contenuti 3</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Footer</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />
<@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
<@wp.headInfo type="CSS_CA_INT" info="owl.carousel.css" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.css" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />
                
                <#assign js_slide="$(document).ready(function() {        
                	var owl = $(''#owl-correlati'');
                	owl.owlCarousel({
                		nav:false,
                		startPosition: 0,
                		autoPlay:false,
                		responsiveClass:true,
                		responsive:{
                				0:{
                					items:1,
                				},
                				576: {
                					items:2,
                				},
                				768: {
                					items:2,
                				},
                				991: {
                					items:4,
                				},
                			}
                	});
                	
                	var owl2 = $(''#owl-galleria'');
                	owl2.owlCarousel({
                		nav:false,
                		startPosition: 0,
                		autoPlay:false,
                		responsiveClass:true,
                		responsive:{
                				0:{
                					items:1,
                				},
                				576: {
                					items:2,
                				},
                				768: {
                					items:2,
                				},
                				991: {
                					items:3,
                				},
                			}
                	});
                	
                });" />
<@wp.headInfo type="JS_CA_BTM_INC" info="${js_slide}" />
<@wp.headInfo type="JS_CA_BTM" info="file-saver.min.js" />
<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
		<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
                     <!--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cittametro_template_header_navigation_menu.jsp" />
                         -->
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
			    <@wp.fragment code="cittametro_template_briciole" escapeXml=false />

				<@wp.show frame=2 />
				<@wp.show frame=3 />
				<@wp.show frame=4 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=5 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
	</body>
</html>');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_homepage','Cittametro - Homepage','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Allerta meteo</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Notizia principale</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Ultime notizie</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Calendario</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Argomenti in evidenza</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="7">
		<descr>Gallerie</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="8">
		<descr>Siti tematici</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="9">
		<descr>Cerca servizi</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
	<frame pos="10">
		<descr>Altro 1</descr>
		<sketch x1="0" y1="9" x2="11" y2="9" />
	</frame>
	<frame pos="11">
		<descr>Altro 2</descr>
		<sketch x1="0" y1="10" x2="11" y2="10" />
	</frame>
	<frame pos="12">
		<descr>Altro 3</descr>
		<sketch x1="0" y1="11" x2="11" y2="11" />
	</frame>
	<frame pos="13">
		<descr>Footer</descr>
		<sketch x1="0" y1="12" x2="11" y2="12" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="home.css" />

<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style;no-unsafe-eval">
	
		<#--
		<@wp.fragment code="keycloak_auth" escapeXml=false />
	    <@wp.fragment code="cagliari_template_googletagmanagerbody" escapeXml=false />
		-->
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
                      <#--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cagliari_template_header_navigation_menu.jsp" />
                      -->
                      
 							<@wp.fragment code="cittametro_template_header_navigation_menu" escapeXml=false />
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
			    <section id="allerta_meteo">
				    <@wp.show frame=2 />
				</section>
				<section id="novita_evidenza">
				    <@wp.show frame=3 />
				</section>
				<section id="home-novita">
                    <div class="container">
                        <div class="mt-88n">
                            <@wp.show frame=4 />
                    	</div>		
	                </div>
				</section>
				<section id="calendario">
				    <@wp.show frame=5 />
				</section>
				<section id="argomenti_evidenza">
				    <@wp.show frame=6 />
				</section>
				<@wp.show frame=7 />
				<section id="siti_tematici">
				    <div class="container">
				        <@wp.show frame=8 />
				    </div>
				</section>
				<section class="bg-servizi" id="home-servizi">
				    <@wp.show frame=9 />
				</section>
				<@wp.show frame=10 />
				<@wp.show frame=11 />
				<@wp.show frame=12 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=13 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
              
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
		
	</body>
</html>
');
INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cittametro_focuspage','Cittametro - Focus','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu Orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
	</frame>
	<frame pos="2">
		<descr>Argomento</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>In evidenza</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Amministrazione</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Servizi</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Novita</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="7">
		<descr>Documenti</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="8">
		<descr>Gallerie</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="9">
		<descr>Altri contenuti</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
	<frame pos="10" main="true">
		<descr>Dettaglio finto pagina volante</descr>
		<sketch x1="0" y1="9" x2="11" y2="9" />
	</frame>
	<frame pos="11">
		<descr>Footer</descr>
		<sketch x1="0" y1="10" x2="11" y2="10" />
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />

<!doctype html>
	<@wp.fragment code="cittametro_template_head" escapeXml=false />
	<@wp.fragment code="cittametro_template_footer_js" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cittametro_template_googletagmanagerbody" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cittametro_template_skiplink" escapeXml=false />
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cittametro_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
					<#--
 						<@c.import url="/WEB-INF/aps/jsp/widgets/cittametro_template_header_navigation_menu.jsp" />
					-->
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 						    <@wp.fragment code="cittametro_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cittametro_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cittametro_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
				<div class="bg-grigio">
					<section id="intro-argomenti">
						<@wp.show frame=2 />
					</section>
					<section id="argomenti-inevidenza" class="sezioni">
						<div class="container">
							<@wp.show frame=3 />
						</div>
					</section>
				</div>
				<section id="argomenti-amministrazione">
					<div class="container">
						<@wp.show frame=4 />
					</div>
				</section>
				<section id="argomenti-servizi">
					<div class="container">
						<@wp.show frame=5 />
					</div>
				</section>
				<section id="argomenti-novita" class="sezioni bg-grigio">
					<div class="container">
						<@wp.show frame=6 />
					</div>
				</section>
				<section id="argomenti-documenti">
					<div class="container">
						<@wp.show frame=7 />
					</div>
				</section>
				<section id="argomenti-gallerie">
					<div class="container">
						<@wp.show frame=8 />
					</div>
				</section>
				<@wp.show frame=9 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cittametro_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=11 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cittametro_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cittametro_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cittametro_template_backtotop" escapeXml=false />
	</body>
</html>');
