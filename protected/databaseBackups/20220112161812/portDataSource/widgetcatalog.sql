INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('messages_system','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">System Messages</property>
<property key="it">Messaggi di Sistema</property>
</properties>',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'system','font-awesome:fa-envelope-square');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('login_form','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Legacy Login Form</property>
<property key="it">Form di Login Legacy</property>
</properties>',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'system','font-awesome:fa-sign-in');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('formAction','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Internal Servlet</property>
<property key="it">Invocazione di una Servlet Interna</property>
</properties>','<config>
	<parameter name="actionPath">
		Path to an action or to a JSP. You must prepend ''/ExtStr2'' to any Struts2 action path
	</parameter>
	<action name="configSimpleParameter"/>
</config>',NULL,NULL,NULL,1,NULL,NULL,NULL,0,'system','asset:ent-form-action');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('entando_apis','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">APIs</property>
<property key="it">APIs</property>
</properties>
',NULL,NULL,'formAction','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="actionPath">/ExtStr2/do/Front/Api/Resource/list.action</property>
</properties>
',1,'free',NULL,NULL,1,'system','asset:ent-entando-apis');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('userprofile_editCurrentUser','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Edit Profile and Password</property>
<property key="it">Edita Profilo e Password</property>
</properties>',NULL,NULL,'formAction','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="actionPath">/ExtStr2/do/Front/CurrentUser/edit.action</property>
</properties>',1,NULL,NULL,NULL,1,'user','asset:ent-user-profile-edit');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('userprofile_editCurrentUser_password','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Edit User Password</property>
<property key="it">Edita Password Utente</property>
</properties>',NULL,NULL,'formAction','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="actionPath">/ExtStr2/do/Front/CurrentUser/editPassword.action</property>
</properties>',1,NULL,NULL,NULL,1,'user','font-awesome:fa-key');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('userprofile_editCurrentUser_profile','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Edit User Profile</property>
<property key="it">Edita Profilo Utente</property>
</properties>',NULL,NULL,'formAction','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="actionPath">/ExtStr2/do/Front/CurrentUser/Profile/edit.action</property>
</properties>',1,NULL,NULL,NULL,1,'user','font-awesome:fa-address-card');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('search_form','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Search Form</property>
<property key="it">Barra ricerca</property>
</properties>',NULL,'jacms',NULL,NULL,0,'free',NULL,NULL,0,'cms','font-awesome:fa-search');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Content</property>
<property key="it">Singolo Contenuto</property>
</properties>','<config>
	<parameter name="contentId">Content ID</parameter>
	<parameter name="modelId">Content Model ID</parameter>
	<action name="viewerConfig"/>
</config>','jacms',NULL,NULL,1,NULL,NULL,NULL,0,'cms','asset:ent-content-viewer');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('search_result','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Search Results</property>
<property key="it">Risultati della Ricerca</property>
</properties>',NULL,'jacms',NULL,NULL,1,NULL,NULL,NULL,0,'cms','font-awesome:fa-list');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('content_viewer_list','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Content Search Query</property>
<property key="it">Elenco dinamico di contenuti</property>
</properties>','<config>
	<parameter name="contentType">Content Type (mandatory)</parameter>
	<parameter name="modelId">Content Model</parameter>
	<parameter name="userFilters">Front-End user filter options</parameter>
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="listViewerConfig"/>
</config>','jacms',NULL,NULL,1,NULL,NULL,NULL,0,'cms','font-awesome:fa-filter');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('row_content_viewer_list','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Content List</property>
<property key="it">Elenco di Contenuti</property>
</properties>','<config>
	<parameter name="contents">Contents to Publish (mandatory)</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="rowListViewerConfig" />
</config>','jacms',NULL,NULL,1,'free',NULL,NULL,0,'cms','font-awesome:fa-list-alt');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('breadcrumb','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Breadcrumbs</property>
<property key="it">Briciole di pane</property>
</properties>',NULL,NULL,NULL,NULL,1,'free',NULL,NULL,0,'navigation','asset:ent-breadcrumb');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('logo','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Logo</property>
<property key="it">Logo</property>
</properties>',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'page','font-awesome:fa-cube');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('language','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Language</property>
<property key="it">Linguaggio</property>
</properties>',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'page','font-awesome:fa-language');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('keycloak-login','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Login</property>
<property key="it">Login</property>
</properties>',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'system','font-awesome:fa-sign-in');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('navigation-menu','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Navigation Menu</property>
<property key="it">Menu di Navigazione</property>
</properties>','<config>
	<parameter name="navSpec">Rules for the Page List auto-generation</parameter>
	<action name="navigatorConfig" />
</config>',NULL,NULL,NULL,1,NULL,NULL,NULL,0,'navigation','font-awesome:fa-bars');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('sitemap','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Sitemap</property>
<property key="it">Mappa del Sito</property>
</properties>',NULL,NULL,NULL,NULL,1,'free',NULL,NULL,0,'navigation','font-awesome:fa-sitemap');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('NWS_Latest','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">News Latest</property>
<property key="it">Notizie Ultime</property>
</properties>',NULL,NULL,'content_viewer_list','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="maxElements">4</property>
<property key="filters">(order=DESC;attributeFilter=true;key=date)</property>
<property key="title_it">Notizie</property>
<property key="linkDescr_it">Archivio</property>
<property key="pageLink">news</property>
<property key="title_en">News</property>
<property key="contentType">NWS</property>
<property key="modelId">10022</property>
<property key="linkDescr_en">Archive</property>
</properties>',0,NULL,NULL,NULL,0,'cms','asset:ent-nws-latest');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('NWS_Archive','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">News Archive</property>
<property key="it">Notizie Archivio</property>
</properties>',NULL,NULL,'content_viewer_list','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="maxElemForItem">10</property>
<property key="title_it">Archivio Notizie</property>
<property key="userFilters">(attributeFilter=false;key=fulltext)+(attributeFilter=true;key=date)</property>
<property key="filters">(order=DESC;attributeFilter=true;key=date)</property>
<property key="title_en">News Archive</property>
<property key="contentType">NWS</property>
<property key="modelId">10021</property>
</properties>',0,NULL,NULL,NULL,0,'cms','font-awesome:fa-archive');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('jpsolr_facetResults','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Facets Search Result</property>
<property key="it">Risultati Ricerca Faccette</property>
</properties>','<config>
	<parameter name="contentTypesFilter">Content Type (optional)</parameter>
	<action name="solrFacetNavResultConfig"/>
</config>','jpsolr',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('jpsolr_facetTree','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Facets Tree</property>
<property key="it">Albero delle faccette</property>
</properties>','<config>
	<parameter name="facetRootNodes">Facet Category Root</parameter>
	<parameter name="contentTypesFilter">Content Type (optional)</parameter>
	<action name="solrFacetNavTreeConfig"/>
</config>','jpsolr',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('jpseo_content_viewer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Content SEO Meta-description</property>
<property key="it">Content SEO Meta-description</property>
</properties>','<config>
	<parameter name="contentId">Content ID</parameter>
	<parameter name="modelId">Content Model ID</parameter>
	<action name="viewerConfig"/>
</config>','jpseo',NULL,NULL,1,NULL,NULL,NULL,0,'seo','asset:ent-jpseo-content-viewer');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_widget_inevidenza','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cagliari - Contenuti in 3 colonne</property><property key="it">Cittametro - Contenuti in 3 colonne</property></properties>','<config>
	<parameter name="contents">Contents to Publish (mandatory)</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="rowListViewerConfig" />
</config>','whitelabel',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cagliari_widget_advsearch_results','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">CITTAMETRO - Ricerca avanzata - Risultati</property><property key="it">CITTAMETRO - Ricerca avanzata - Risultati</property></properties>','<config>
    <parameter name="facetRootNodes">Facet Category Root</parameter>
    <parameter name="contentTypesFilter">Content Type (optional)</parameter>
    <action name="solrFacetNavTreeConfig"/>
</config>','whitelabel',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_widget_argomenti_homepage','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cittametro - Argomenti homepage</property>
<property key="it">Cittametro - Argomenti homepage</property>
</properties>

','<config>
	<parameter name="contents">Contents to Publish (mandatory)</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="rowListViewerConfig" />
</config>','whitelabel',NULL,NULL,1,'free','',NULL,0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_widget_menu_link_utili','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">CITTAMETRO - Menu link Argomenti</property>
<property key="it">CITTAMETRO - Menu link Argomenti</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_pulsanti_gestione','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">CITTAMETRO Buttons manager</property>
<property key="it">CITTAMETRO manager buttons</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free','{"resources":["cittametro-homepage-bundle/widgets/navBar-config/static/js/runtime.js","cittametro-homepage-bundle/widgets/navBar-config/static/js/vendor.js","cittametro-homepage-bundle/widgets/navBar-config/static/js/main.js"],"customElement":"navbar-config"}','cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_widget_inevidenza_homepage','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cittametro - In evidenza homepage</property><property key="it">Cittametro - In evidenza homepage</property></properties>','<config>
	<parameter name="contents">Contents to Publish (mandatory)</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="rowListViewerConfig" />
</config>','whitelabel',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_navbar_orizzontale','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">CITTAMETRO - navbar oriz</property>
<property key="it">CITTAMETRO - navbar oriz</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free','{"resources":["cittametro-homepage-bundle/widgets/navBar-config/static/js/runtime.js","cittametro-homepage-bundle/widgets/navBar-config/static/js/vendor.js","cittametro-homepage-bundle/widgets/navBar-config/static/js/main.js"],"customElement":"navbar-config"}','cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_widget_gallerie_homepage','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">CITTAMETRO - Gallerie homepage</property>
<property key="it">CITTAMETRO - Gallerie homepage</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('login_prova','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Login button prova</property>
<property key="it">Bottone Login prova</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_briciole','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">CITTAMETRO - bread crumbs</property>
<property key="it">CITTAMETRO - briciole di pane</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_widget_cerca_servizi_home','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">CITTAMETRO - Cerca servizi homepage</property>
<property key="it">CITTAMETRO - Cerca servizi homepage</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_cerca_contenuti','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">CITTAMETRO - Cerca Contenuti</property>
<property key="it">CITTAMETRO - Cerca Contenuti</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_footer','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">CITTAMETRO - footer</property>
<property key="it">CITTAMETRO - footer</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free','{"resources":["cittametro-homepage-bundle/widgets/navBar-config/static/js/runtime.js","cittametro-homepage-bundle/widgets/navBar-config/static/js/vendor.js","cittametro-homepage-bundle/widgets/navBar-config/static/js/main.js"],"customElement":"navbar-config"}','cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_navigation_child','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">CITTAMETRO - navigation child</property>
<property key="it">CITTAMETRO - navigation child</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('ilpatto_gestione_aree','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Il Patto - Area Management</property>
<property key="it">Il Patto - Gestione Aree</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'ilpatto-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('ilpatto_stats_aree','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Il Patto - Areas Statistics</property>
<property key="it">Il Patto - Statistiche Aree</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'ilpatto-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('ilpatto_gestione_settori','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Il Patto - Manage Sectors</property>
<property key="it">Il Patto - Gestione Settori</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'ilpatto-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('ilpatto_gestione_progetti','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Il Patto - Manage projects</property>
<property key="it">Il Patto - Gestione progetti</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'ilpatto-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('ilpatto_mappa_comuni','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Il Patto - Comuni Map</property>
<property key="it">Il Patto - Mappa Comuni</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'ilpatto-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('ilpatto_stats_stato','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Il Patto - Project State Statistics</property>
<property key="it">Il Patto - Statistiche Stato Progetti</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'ilpatto-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cagliari_widget_argomento_dettaglio','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cittametro - Argomenti dettaglio</property>
<property key="it">Cittametro - Argomenti dettaglio</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free','',NULL,0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cagliari_widget_argomenti_inevidenza_img','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cittametro - Argomenti in evidenza foto</property>
<property key="it">Cittametro - Argomenti in evidenza foto</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cagliari_widget_lista_multicontent','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cittametro - Lista multi-content</property>
<property key="it">Cittametro - Lista multi-content</property>
</properties>

','<config>
	<parameter name="contentTypes">Content Type (mandatory)</parameter>
    <parameter name="layout">Layout of content list</parameter>	
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />	
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="multiContentListViewerConfig"/>
</config>','whitelabel',NULL,NULL,1,'free','',NULL,0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('prova_lingua','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Prova_lingua</property>
<property key="it">Prova_lingua</property>
</properties>

',NULL,NULL,NULL,NULL,0,'administrators',NULL,NULL,0,NULL,'font-awesome:fa-500px');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_aiuto_uffici','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cittametro - Aiuto Uffici</property>
<property key="it">Cittametro - Aiuto Uffici</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,'cittametro-homepage-bundle',0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cagliari_widget_calendario_homepage','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cittametro - Calendario homepage con filtri</property>
<property key="it">Cittametro - Calendario homepage con filtri</property>
</properties>

','<config>
	<parameter name="contentType">Content Type (mandatory)</parameter>
	<parameter name="modelId">Content Model</parameter>
	<parameter name="userFilters">Front-End user filter options</parameter>
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="listViewerConfig"/>
</config>','whitelabel',NULL,NULL,1,'free','',NULL,0,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cagliari_widget_argomenti_consigliati','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cittametro - Argomenti consigliati (piccoli)</property><property key="it">Cittametro - Argomenti consigliati (piccoli)</property></properties>','<config>
	<parameter name="contentType">Content Type (mandatory)</parameter>
	<parameter name="modelId">Content Model</parameter>
	<parameter name="userFilters">Front-End user filter options</parameter>
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="listViewerConfig"/>
</config>','whitelabel',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cittametro_mappa_territorio','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cittametro - Mappa territorio</property>
<property key="it">Cittametro - Mappa territorio</property>
</properties>

',NULL,NULL,NULL,NULL,0,'free',NULL,NULL,0,NULL,'font-awesome:fa-map');
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup,configui,bundleid,readonlypagewidgetconfig,widgetcategory,icon) VALUES ('cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cagliari - Lista multi-layout</property><property key="it">Cagliari - Lista multi-layout</property></properties>','<config>
	<parameter name="contentType">Content Type (mandatory)</parameter>
    <parameter name="layout">Layout of content list</parameter>
	<parameter name="modelId">Content Model</parameter>
	<parameter name="modelId2">Content Model 2</parameter>
	<parameter name="userFilters">Front-End user filter options</parameter>
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />	
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="listViewerConfigExt"/>
</config>','whitelabel',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL);
