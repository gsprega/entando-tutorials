INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_editCurrentUser_profile','userprofile_editCurrentUser_profile',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<h1><@wp.i18n key="userprofile_EDITPROFILE_TITLE" /></h1>
<#if (Session.currentUser != "guest") >
	<form action="<@wp.action path="/ExtStr2/do/Front/CurrentUser/Profile/save.action" />" method="post" class="form-horizontal">
	<@s.if test="hasFieldErrors()">
		<div class="alert alert-block">
			<p><strong><@wp.i18n key="userprofile_MESSAGE_TITLE_FIELDERRORS" /></strong></p>
			<ul class="unstyled">
				<@s.iterator value="fieldErrors">
					<@s.iterator value="value">
						<li><@s.property escapeHtml=false /></li>
					</@s.iterator>
				</@s.iterator>
			</ul>
		</div>
	</@s.if>
	<@s.set var="lang" value="defaultLang" />
	<@s.iterator value="userProfile.attributeList" var="attribute">
		<@s.if test="%{#attribute.active}">
			<@wpsa.tracerFactory var="attributeTracer" lang="%{#lang.code}" />
				<@s.set var="i18n_attribute_name">userprofile_<@s.property value="userProfile.typeCode" />_<@s.property value="#attribute.name" /></@s.set>
				<@s.set var="attribute_id">userprofile_<@s.property value="#attribute.name" /></@s.set>
				<@wp.fragment code="userprofile_is_IteratorAttribute" escapeXml=false />
		</@s.if>
	</@s.iterator>

	<p class="form-actions">
		<@wp.i18n key="userprofile_SAVE_PROFILE" var="userprofile_SAVE_PROFILE" />
		<@wpsf.submit useTabindexAutoIncrement=true value="%{#attr.userprofile_SAVE_PROFILE}" cssClass="btn btn-primary" />
	</p>

	</form>
<#else>
	<p>
		<@wp.i18n key="userprofile_PLEASE_LOGIN" />
	</p>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-DateAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<#assign currentLangVar ><@wp.info key="currentLang" /></#assign>

<@s.if test="#attribute.failedDateString == null">
<@s.set var="dateAttributeValue" value="#attribute.getFormattedDate(''dd/MM/yyyy'')" />
</@s.if>
<@s.else>
<@s.set var="dateAttributeValue" value="#attribute.failedDateString" />
</@s.else>
<@wpsf.textfield
useTabindexAutoIncrement=true id="%{attribute_id}"
name="%{#attributeTracer.getFormFieldName(#attribute)}"
value="%{#dateAttributeValue}" maxlength="10" cssClass="text userprofile-date" />
&#32;
<#assign js_for_datepicker="jQuery(function($){
	$.datepicker.regional[''it''] = {
		closeText: ''Chiudi'',
		prevText: ''&#x3c;Prec'',
		nextText: ''Succ&#x3e;'',
		currentText: ''Oggi'',
		monthNames: [''Gennaio'',''Febbraio'',''Marzo'',''Aprile'',''Maggio'',''Giugno'',
			''Luglio'',''Agosto'',''Settembre'',''Ottobre'',''Novembre'',''Dicembre''],
		monthNamesShort: [''Gen'',''Feb'',''Mar'',''Apr'',''Mag'',''Giu'',
			''Lug'',''Ago'',''Set'',''Ott'',''Nov'',''Dic''],
		dayNames: [''Domenica'',''Luned&#236'',''Marted&#236'',''Mercoled&#236'',''Gioved&#236'',''Venerd&#236'',''Sabato''],
		dayNamesShort: [''Dom'',''Lun'',''Mar'',''Mer'',''Gio'',''Ven'',''Sab''],
		dayNamesMin: [''Do'',''Lu'',''Ma'',''Me'',''Gi'',''Ve'',''Sa''],
		weekHeader: ''Sm'',
		dateFormat: ''dd/mm/yy'',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''''};
});

jQuery(function($){
	if (Modernizr.touch && Modernizr.inputtypes.date) {
		$.each(	$(''input.userprofile-date''), function(index, item) {
			item.type = ''date'';
		});
	} else {
		$.datepicker.setDefaults( $.datepicker.regional[''${currentLangVar}''] );
		$(''input.userprofile-date'').datepicker({
      			changeMonth: true,
      			changeYear: true,
      			dateFormat: ''dd/mm/yyyy''
    		});
	}
});" >

<@wp.headInfo type="JS" info="entando-misc-html5-essentials/modernizr-2.5.3-full.js" />
<@wp.headInfo type="JS_EXT" info="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" />
<@wp.headInfo type="CSS_EXT" info="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.min.css" />
<@wp.headInfo type="JS_RAW" info="${js_for_datepicker}" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front_attributeInfo-help-block',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@s.set var="validationRules" value="#attribute.validationRules.ognlValidationRule" />
<@s.set var="hasValidationRulesVar" value="%{#validationRules != null && #validationRules.expression != null}" />

<@s.if test="%{#hasValidationRulesVar || #attribute.type == ''Date'' || (#attribute.textAttribute && (#attribute.minLength != -1 || #attribute.maxLength != -1))}">
		<span class="help-block">
		<@s.if test="#attribute.type == ''Date''">dd/MM/yyyy&#32;</@s.if>
		<@s.if test="%{#validationRules.helpMessageKey != null}">
			<@s.set var="label" scope="page" value="#validationRules.helpMessageKey" /><@wp.i18n key="${label}" />
		</@s.if>
		<@s.elseif test="%{#validationRules.helpMessage != null}">
			<@s.property value="#validationRules.helpMessage" />
		</@s.elseif>
		<@s.if test="#attribute.minLength != -1">
			&#32;
			<abbr title="<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MINLENGTH_FULL" />&#32;<@s.property value="#attribute.minLength" />">
				<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MINLENGTH_SHORT" />:&#32;<@s.property value="#attribute.minLength" />
			</abbr>
		</@s.if>
		<@s.if test="#attribute.maxLength != -1">
			&#32;
			<abbr title="<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MAXLENGTH_FULL" />&#32;<@s.property value="#attribute.maxLength" />">
				<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MAXLENGTH_SHORT" />:&#32;<@s.property value="#attribute.maxLength" />
			</abbr>
		</@s.if>
	</span>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_passwordChanged',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign s=JspTaglibs["/struts-tags"]>

<h1><@wp.i18n key="userprofile_EDITPASSWORD_TITLE" /></h1>
<p class="alert alert-success"><@wp.i18n key="userprofile_PASSWORD_UPDATED" /></p>
<@s.if test="!#session.currentUser.credentialsNonExpired">
	<p class="alert alert-info">
		<a href="<@s.url namespace="/do" action="logout" />" ><@wp.i18n key="userprofile_PLEASE_LOGIN_AGAIN" /></a>
	</p>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-HypertextAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@wpsf.textarea
	useTabindexAutoIncrement=true
	cols="50"
	rows="3"
	id="%{#attribute_id}"
	name="%{#attributeTracer.getFormFieldName(#attribute)}"
	value="%{#attribute.textMap[#lang.code]}"  />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-NumberAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@s.if test="#attribute.failedNumberString == null">
	<@s.set var="numberAttributeValue" value="#attribute.value"></@s.set>
</@s.if>
<@s.else>
	<@s.set var="numberAttributeValue" value="#attribute.failedNumberString"></@s.set>
</@s.else>
<@wpsf.textfield
		useTabindexAutoIncrement=true
		id="%{#attribute_id}"
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		value="%{#numberAttributeValue}"
		maxlength="254" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-BooleanAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<label class="radio inline" for="<@s.property value="%{#attribute_id + ''-true''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-true''}"
		value="true"
		checked="%{#attribute.value == true}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_YES" />
</label>
&#32;
<label class="radio inline" for="<@s.property value="%{#attribute_id+''-false''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-false''}"
		value="false"
		checked="%{#attribute.value == false}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_NO" />
</label>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Enumer',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<div class="control-group">
	<label for="${formFieldNameVar}" class="control-label"><@wp.i18n key="${i18n_Attribute_Key}" /></label>
	<div class="controls">
		<select name="${formFieldNameVar}" id="${formFieldNameVar}" class="input-xlarge">
			<option value=""><@wp.i18n key="ALL" /></option>
			<#list userFilterOptionVar.attribute.items as enumeratorItemVar>
			<option value="${enumeratorItemVar}" <#if (formFieldValue??) && (enumeratorItemVar == formFieldValue)>selected="selected"</#if> >${enumeratorItemVar}</option>
			</#list>
		</select>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-ThreeStateAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<label class="radio inline" for="<@s.property value="%{#attribute_id + ''-none''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-none''}"
		value=""
		checked="%{#attribute.booleanValue == null}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_BOTH_YES_AND_NO" />
</label>
&#32;
<label class="radio inline" for="<@s.property value="%{#attribute_id + ''-true''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-true''}"
		value="true"
		checked="%{#attribute.booleanValue != null && #attribute.booleanValue == true}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_YES" />
</label>
&#32;
<label class="radio inline" for="<@s.property value="%{#attribute_id + ''-false''}" />">
	<@wpsf.radio
		useTabindexAutoIncrement=true
		name="%{#attributeTracer.getFormFieldName(#attribute)}"
		id="%{#attribute_id + ''-false''}"
		value="false"
		checked="%{#attribute.booleanValue != null && #attribute.booleanValue == false}"
		cssClass="radio" />
		<@wp.i18n key="userprofile_NO" />
</label>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front_AttributeInfo',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@s.if test="#attribute.required">
	<abbr class="icon icon-asterisk" title="<@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MANDATORY_FULL" />"><span class="noscreen"><@wp.i18n key="userprofile_ENTITY_ATTRIBUTE_MANDATORY_SHORT" /></span></abbr>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front_AllList_operationModule',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@s.if test="null == #operationButtonDisabled">
	<@s.set var="operationButtonDisabled" value="false" />
</@s.if>
<div class="btn-toolbar">
	<div class="btn-group btn-group-sm">
		<@wpsa.actionParam action="moveListElement" var="actionName" >
			<@wpsa.actionSubParam name="attributeName" value="%{#attribute.name}" />
			<@wpsa.actionSubParam name="listLangCode" value="%{#lang.code}" />
			<@wpsa.actionSubParam name="elementIndex" value="%{#elementIndex}" />
			<@wpsa.actionSubParam name="movement" value="UP" />
		</@wpsa.actionParam>
		<@wpsf.submit disabled="%{#operationButtonDisabled}" action="%{#actionName}" type="button" cssClass="btn btn-default" title="%{getText(''label.moveInPositionNumber'')}: %{#elementIndex}">
		<span class="icon fa fa-sort-desc"></span>
		<span class="sr-only"><@s.text name="label.moveInPositionNumber" />: <@s.property value="%{#elementIndex}" /></span>
		</@wpsf.submit>

		<@wpsa.actionParam action="moveListElement" var="actionName" >
			<@wpsa.actionSubParam name="attributeName" value="%{#attribute.name}" />
			<@wpsa.actionSubParam name="listLangCode" value="%{#lang.code}" />
			<@wpsa.actionSubParam name="elementIndex" value="%{#elementIndex}" />
			<@wpsa.actionSubParam name="movement" value="DOWN" />
		</@wpsa.actionParam>
		<@wpsf.submit disabled="%{#operationButtonDisabled}" action="%{#actionName}" type="button" cssClass="btn btn-default" title="%{getText(''label.moveInPositionNumber'')}: %{#elementIndex+2}">
		<span class="icon fa fa-sort-asc"></span>
		<span class="sr-only"><@s.text name="label.moveInPositionNumber" />: <@s.property value="%{#elementIndex}" /></span>
		</@wpsf.submit>
	</div>
	<div class="btn-group btn-group-sm">
		<@wpsa.actionParam action="removeListElement" var="actionName" >
			<@wpsa.actionSubParam name="attributeName" value="%{#attribute.name}" />
			<@wpsa.actionSubParam name="listLangCode" value="%{#lang.code}" />
			<@wpsa.actionSubParam name="elementIndex" value="%{#elementIndex}" />
		</@wpsa.actionParam>
		<@wpsf.submit disabled="%{#operationButtonDisabled}" action="%{#actionName}" type="button" cssClass="btn btn-default btn-warning" title="%{getText(''label.remove'')}: %{#elementIndex}">
		<span class="icon fa fa-times-circle-o"></span>
		<span class="sr-only"><@s.text name="label.remove" />: <@s.property value="%{#elementIndex}" /></span>
		</@wpsf.submit>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-AllList-addElementButton',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@s.set var="add_label"><@wp.i18n key="userprofile_ADDITEM_LIST" /></@s.set>

<@wpsa.actionParam action="addListElement" var="actionName" >
	<@wpsa.actionSubParam name="attributeName" value="%{#attribute.name}" />
	<@wpsa.actionSubParam name="listLangCode" value="%{#lang.code}" />
</@wpsa.actionParam>
<@s.set var="iconImagePath" id="iconImagePath"><@wp.resourceURL/>administration/common/img/icons/list-add.png</@s.set>
<@wpsf.submit
	cssClass="btn"
	useTabindexAutoIncrement=true
	action="%{#actionName}"
	value="%{add_label}"
	title="%{i18n_attribute_name}%{'': ''}%{add_label}" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_IteratorAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<#assign i18n_attribute_name ><@s.property value="#i18n_attribute_name" /></#assign>
<@s.if test="#attribute.type == ''Boolean''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-BooleanAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.if>
<@s.elseif test="#attribute.type == ''CheckBox''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-CheckboxAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Composite''">
	<div class="well well-small">
		<fieldset class=" <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
			<legend class="margin-medium-top">
				<@wp.i18n key="${i18n_attribute_name}" />
				<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
			</legend>
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
			<@wp.fragment code="userprofile_is_front-CompositeAttribute" escapeXml=false />
		</fieldset>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Date''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-DateAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Enumerator''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-EnumeratorAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''EnumeratorMap''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-EnumeratorMapAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Hypertext''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-HypertextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''List''">
	<div class="well well-small">
		<fieldset class=" <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
			<legend class="margin-medium-top">
				<@wp.i18n key="${i18n_attribute_name}" />
					<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
			</legend>
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
			<@wp.fragment code="userprofile_is_front-MonolistAttribute" escapeXml=false />
		</fieldset>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Longtext''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-LongtextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Monolist''">
	<div class="well well-small">
		<fieldset class=" <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
			<legend class="margin-medium-top"><@wp.i18n key="${i18n_attribute_name}" />
				<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
			</legend>
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
			<@wp.fragment code="userprofile_is_front-MonolistAttribute" escapeXml=false />
		</fieldset>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Monotext''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Number''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-NumberAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''Text''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.elseif test="#attribute.type == ''ThreeState''">
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-ThreeStateAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.elseif>
<@s.else> <#-- for all other types, insert a simple label and a input[type="text"] -->
	<div class="control-group <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="attribute_id" />">
			<@wp.i18n key="${i18n_attribute_name}" />
			<@wp.fragment code="userprofile_is_front_AttributeInfo" escapeXml=false />
		</label>
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</div>
</@s.else>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_editCurrentUser_password','userprofile_editCurrentUser_password',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<h1><@wp.i18n key="userprofile_EDITPASSWORD" /></h1>

<#if (Session.currentUser != "guest") >

	<form action="<@wp.action path="/ExtStr2/do/Front/CurrentUser/changePassword.action" />" method="post" class="form-horizontal">

	<@s.if test="hasFieldErrors()">
		<div class="alert alert-block">
			<p><strong><@wp.i18n key="userprofile_MESSAGE_TITLE_FIELDERRORS" /></strong></p>
			<ul class="unstyled">
				<@s.iterator value="fieldErrors">
					<@s.iterator value="value">
						<li><@s.property escapeHtml=false /></li>
					</@s.iterator>
				</@s.iterator>
			</ul>
		</div>
	</@s.if>

	<p class="noscreen">
		<wpsf:hidden name="username" />
	</p>

	<div class="control-group">
		<label for="userprofile-old-password" class="control-label"><@wp.i18n key="userprofile_OLDPASSWORD" /></label>
		<div class="controls">
			<@wpsf.password
				useTabindexAutoIncrement=true
				name="oldPassword"
				id="userprofile-old-password" />
		</div>
	</div>

	<div class="control-group">
		<label for="userprofile-new-password" class="control-label"><@wp.i18n key="userprofile_NEWPASS" /></label>
		<div class="controls">
			<@wpsf.password
				useTabindexAutoIncrement=true
				name="password"
				id="userprofile-new-password" />
		</div>
	</div>

	<div class="control-group">
		<label for="userprofile-new-password-confirm" class="control-label"><@wp.i18n key="userprofile_CONFIRM_NEWPASS" /></label>
		<div class="controls">
			<@wpsf.password
				useTabindexAutoIncrement=true
				name="passwordConfirm"
				id="userprofile-new-password-confirm" />
		</div>
	</div>

	<p class="form-actions">
		<@wp.i18n key="userprofile_SAVE_PASSWORD" var="userprofile_SAVE_PASSWORD" />
		<@wpsf.submit
			useTabindexAutoIncrement=true
			value="%{#attr.userprofile_SAVE_PASSWORD}"
			cssClass="btn btn-primary" />
	</p>

	</form>

<#else>
	<p>
		<@wp.i18n key="userprofile_PLEASE_LOGIN_TO_EDIT_PASSWORD" />
	</p>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-MonolistAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@s.if test="#attribute.attributes.size() != 0">
	<ul class="unstyled">
</@s.if>

<@s.set var="masterListAttributeTracer" value="#attributeTracer" />
<@s.set var="masterListAttribute" value="#attribute" />

<@s.iterator value="#attribute.attributes" var="attribute" status="elementStatus">
	<@s.set var="attributeTracer" value="#masterListAttributeTracer.getMonoListElementTracer(#elementStatus.index)"></@s.set>
	<@s.set var="elementIndex" value="#elementStatus.index" />
	<@s.set var="i18n_attribute_name">userprofile_ATTR<@s.property value="#attribute.name" /></@s.set>
	<@s.set var="attribute_id">userprofile_<@s.property value="#attribute.name" />_<@s.property value="#elementStatus.count" /></@s.set>

	<li class="control-group  <@s.property value="%{'' attribute-type-''+#attribute.type+'' ''}" />">
		<label class="control-label" for="<@s.property value="#attribute_id" />">
			<@s.if test="#attribute.type == ''Composite''">
				<@s.property value="#elementStatus.count" /><span class="noscreen">&#32;<@s.text name="label.compositeAttribute.element" /></span>
				&#32;
				<@s.if test="#lang.default">
					<@wp.fragment code="userprofile_is_front_AllList_operationModule" escapeXml=false />
				</@s.if>
			</@s.if>
			<@s.else>
				<@s.property value="#elementStatus.count" />
				&#32;
				<@wp.fragment code="userprofile_is_front_AllList_operationModule" escapeXml=false />
			</@s.else>
		</label>
		<div class="controls">
			<@s.if test="#attribute.type == ''Boolean''">
				<@wp.fragment code="userprofile_is_front-BooleanAttribute" escapeXml=false />
			</@s.if>
			<@s.elseif test="#attribute.type == ''CheckBox''">
				<@wp.fragment code="userprofile_is_front-CheckboxAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Composite''">
				<@wp.fragment code="userprofile_is_front-CompositeAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Date''">
				<@wp.fragment code="userprofile_is_front-DateAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Enumerator''">
				<@wp.fragment code="userprofile_is_front-EnumeratorAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''EnumeratorMap''">
				<@wp.fragment code="userprofile_is_front-EnumeratorMapAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Hypertext''">
				<@wp.fragment code="userprofile_is_front-HypertextAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Longtext''">
				<@wp.fragment code="userprofile_is_front-LongtextAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Monotext''">
				<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Number''">
				<@wp.fragment code="userprofile_is_front-NumberAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''ThreeState''">
				<@wp.fragment code="userprofile_is_front-ThreeStateAttribute" escapeXml=false />
			</@s.elseif>
			<@s.elseif test="#attribute.type == ''Text''">
				<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			</@s.elseif>
			<@s.else>
				<@wp.fragment code="userprofile_is_front-MonotextAttribute" escapeXml=false />
			</@s.else>
			<@wp.fragment code="userprofile_is_front_attributeInfo-help-block" escapeXml=false />
		</div>
	</li>
</@s.iterator>

<@s.set var="attributeTracer" value="#masterListAttributeTracer" />
<@s.set var="attribute" value="#masterListAttribute" />
<@s.set var="elementIndex" value="" />
<@s.if test="#attribute.attributes.size() != 0">
</ul>
</@s.if>
<@s.if test="#lang.default">
	<div class="control-group">
		<div class="controls">
			<@wp.fragment code="userprofile_is_front-AllList-addElementButton" escapeXml=false />
		</div>
	</div>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_profileChangeConfirmation',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<h1><@wp.i18n key="userprofile_EDITPROFILE_TITLE" /></h1>
<p><@wp.i18n key="userprofile_PROFILE_UPDATED" /></p>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_resource_detail_include',NULL,NULL,'','<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@s.if test="#methodVar == null">
	<p>
		<@s.property value="#currentMethodNameVar" />,&#32;<@wp.i18n key="ENTANDO_API_METHOD_KO" />
	</p>
</@s.if>
<@s.else>
	<dl class="dl-horizontal">
		<dt>
			<@wp.i18n key="ENTANDO_API_METHOD" />
		</dt>
			<dd>
				<@wp.i18n key="ENTANDO_API_METHOD_OK" />
			</dd>
		<@s.if test="#methodVar != null">
			<dt>
				<@wp.i18n key="ENTANDO_API_DESCRIPTION" />
			</dt>
				<dd><@s.property value="#methodVar.description" /></dd>
			<dt>
				<@wp.i18n key="ENTANDO_API_METHOD_AUTHORIZATION" />
			</dt>
				<dd>
					<@s.if test="%{null != #methodVar.requiredPermission}">
						<@s.iterator value="methodAuthorityOptions" var="permission"><@s.if test="#permission.key==#methodVar.requiredPermission"><@s.property value="#permission.value" /></@s.if></@s.iterator>
					</@s.if>
					<@s.elseif test="%{#methodVar.requiredAuth}">
						<@wp.i18n key="ENTANDO_API_METHOD_AUTH_SIMPLE" />
					</@s.elseif>
					<@s.else>
						<@wp.i18n key="ENTANDO_API_METHOD_AUTH_FREE" />
					</@s.else>
				</dd>
			<@s.if test=''%{!#methodVar.resourceName.equalsIgnoreCase("getService")}'' >
			<dt>
				<@wp.i18n key="ENTANDO_API_METHOD_SCHEMAS" />
			</dt>
				<dd class="schemas">
					<@s.if test=''%{#methodVar.httpMethod.toString().equalsIgnoreCase("POST") || #methodVar.httpMethod.toString().equalsIgnoreCase("PUT")}''>
						<@wp.action path="/ExtStr2/do/Front/Api/Resource/requestSchema.action" var="requestSchemaURLVar" >
							<@wp.parameter name="resourceName"><@s.property value="#methodVar.resourceName" /></@wp.parameter>
							<@wp.parameter name="namespace"><@s.property value="#methodVar.namespace" /></@wp.parameter>
							<@wp.parameter name="httpMethod"><@s.property value="#methodVar.httpMethod" /></@wp.parameter>
						</@wp.action>
						<a href="${requestSchemaURLVar}" >
							<@wp.i18n key="ENTANDO_API_METHOD_SCHEMA_REQ" />
						</a>
						<br />
					</@s.if>
						<@wp.action path="/ExtStr2/do/Front/Api/Resource/responseSchema.action" var="responseSchemaURLVar" >
							<@wp.parameter name="resourceName"><@s.property value="#methodVar.resourceName" /></@wp.parameter>
							<@wp.parameter name="namespace"><@s.property value="#methodVar.namespace" /></@wp.parameter>
							<@wp.parameter name="httpMethod"><@s.property value="#methodVar.httpMethod" /></@wp.parameter>
						</@wp.action>
						<a href="${responseSchemaURLVar}" >
							<@wp.i18n key="ENTANDO_API_METHOD_SCHEMA_RESP" />
						</a>
				</dd>
			</@s.if>
		</@s.if>
	</dl>
	<@s.if test="#methodVar != null">
		<@s.set var="methodParametersVar" value="#methodVar.parameters" />
		<@s.if test="null != #methodParametersVar && #methodParametersVar.size() > 0">
			<table class="table table-striped table-bordered table-condensed">
				<caption><@wp.i18n key="ENTANDO_API_METHOD_REQUEST_PARAMS" /></caption>
				<tr>
					<th><@wp.i18n key="ENTANDO_API_PARAM_NAME" /></th>
					<th><@wp.i18n key="ENTANDO_API_PARAM_DESCRIPTION" /></th>
					<th><@wp.i18n key="ENTANDO_API_PARAM_REQUIRED" /></th>
				</tr>
				<@s.iterator value="#methodParametersVar" var="apiParameter" >
					<tr>
						<td><@s.property value="#apiParameter.key" /></td>
						<td><@s.property value="#apiParameter.description" /></td>
						<td class="icon required_<@s.property value="#apiParameter.required" />">
							<@s.if test="#apiParameter.required">
								<@wp.i18n key="YES" />
							</@s.if>
							<@s.else>
								<@wp.i18n key="NO" />
							</@s.else>
						</td>
					</tr>
				</@s.iterator>
			</table>
		</@s.if>
	</@s.if>
</@s.else>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_entryCurrentProfile',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<#if (Session.currentUser != "guest") >
<form action="<@wp.action path="/ExtStr2/do/Front/CurrentUser/Profile/save.action" />" method="post" class="form-horizontal">
	<@s.if test="hasFieldErrors()">
		<div class="alert alert-block">
			<p><strong><@wp.i18n key="userprofile_MESSAGE_TITLE_FIELDERRORS" /></strong></p>
			<ul class="unstyled">
				<@s.iterator value="fieldErrors">
					<@s.iterator value="value">
						<li><@s.property escapeHtml=false /></li>
					</@s.iterator>
				</@s.iterator>
			</ul>
		</div>
	</@s.if>
	<@s.set var="lang" value="defaultLang" />
	<@s.iterator value="userProfile.attributeList" var="attribute">
		<@s.if test="%{#attribute.active}">
			<@wpsa.tracerFactory var="attributeTracer" lang="%{#lang.code}" />
			<@s.set var="i18n_attribute_name">userprofile_<@s.property value="userProfile.typeCode" />_<@s.property value="#attribute.name" /></@s.set>
			<@s.set var="attribute_id">userprofile_<@s.property value="#attribute.name" /></@s.set>
			<@wp.fragment code="userprofile_is_IteratorAttribute" escapeXml=false />
		</@s.if>
	</@s.iterator>
	<p class="form-actions">
		<@wp.i18n key="userprofile_SAVE_PROFILE" var="userprofile_SAVE_PROFILE" />
		<@wpsf.submit useTabindexAutoIncrement=true value="%{#attr.userprofile_SAVE_PROFILE}" cssClass="btn btn-primary" />
	</p>
</form>
<#else>
	<p><@wp.i18n key="userprofile_PLEASE_LOGIN" /></p>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_currentWithoutProfile',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<h1><@wp.i18n key="userprofile_EDITPROFILE_TITLE" /></h1>
<p class="label label-info">
	<@wp.i18n key="userprofile_CURRENT_USER_WITHOUT_PROFILE" />
</p>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-CheckboxAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>

<@wpsf.checkbox useTabindexAutoIncrement=true
	name="%{#attributeTracer.getFormFieldName(#attribute)}"
	id="%{attribute_id}" value="%{#attribute.value == true}"/>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-MonotextAttribute',NULL,NULL,NULL,'<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@wpsf.textfield useTabindexAutoIncrement=true id="%{attribute_id}"
	name="%{#attributeTracer.getFormFieldName(#attribute)}" value="%{#attribute.getTextForLang(#lang.code)}"
	maxlength="254" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-LongtextAttribute',NULL,NULL,NULL,'<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@wpsf.textarea useTabindexAutoIncrement=true cols="30" rows="5" id="%{attribute_id}" name="%{#attributeTracer.getFormFieldName(#attribute)}" value="%{#attribute.getTextForLang(#lang.code)}" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-CompositeAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpsa=JspTaglibs["/apsadmin-core"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@s.set var="i18n_parent_attribute_name" value="#attribute.name" />
<@s.set var="masterCompositeAttributeTracer" value="#attributeTracer" />
<@s.set var="masterCompositeAttribute" value="#attribute" />
<@s.iterator value="#attribute.attributes" var="attribute">
	<@s.set var="attributeTracer" value="#masterCompositeAttributeTracer.getCompositeTracer(#masterCompositeAttribute)"></@s.set>
	<@s.set var="parentAttribute" value="#masterCompositeAttribute"></@s.set>
	<@s.set var="i18n_attribute_name">userprofile_ATTR<@s.property value="%{i18n_parent_attribute_name}" /><@s.property value="#attribute.name" /></@s.set>
	<@s.set var="attribute_id">userprofile_<@s.property value="%{i18n_parent_attribute_name}" /><@s.property value="#attribute.name" />_<@s.property value="#elementIndex" /></@s.set>
	<@wp.fragment code="userprofile_is_IteratorAttribute" escapeXml=false />
</@s.iterator>
<@s.set var="attributeTracer" value="#masterCompositeAttributeTracer" />
<@s.set var="attribute" value="#masterCompositeAttribute" />
<@s.set var="parentAttribute" value=""></@s.set>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-EnumeratorAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@wpsf.select useTabindexAutoIncrement=true
	name="%{#attributeTracer.getFormFieldName(#attribute)}"
	id="%{attribute_id}"
	headerKey="" headerValue=""
	list="#attribute.items" value="%{#attribute.getText()}" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('userprofile_is_front-EnumeratorMapAttribute',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<@wpsf.select useTabindexAutoIncrement=true
	name="%{#attributeTracer.getFormFieldName(#attribute)}"
	id="%{attribute_id}"
	headerKey="" headerValue=""
	list="#attribute.mapItems" value="%{#attribute.getText()}" listKey="key" listValue="value" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_resource_list','entando_apis',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<h2><@wp.i18n key="ENTANDO_API_RESOURCES" /></h2>
<@s.if test="hasActionErrors()">
	<div class="alert alert-block alert-error">
		<h3 class="alert-heading"><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionErrors">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.set var="resourceFlavoursVar" value="resourceFlavours" />

<@s.if test="#resourceFlavoursVar.size() > 0">
	<@s.set var="icon_free"><span class="icon icon-ok"></span><span class="noscreen sr-only"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_FREE" /></span></@s.set>
	<@s.set var="title_free"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_FREE" />. <@wp.i18n key="ENTANDO_API_GOTO_DETAILS" /></@s.set>

	<@s.set var="icon_auth"><span class="icon icon-user"></span><span class="noscreen sr-only"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_AUTH" /></span></@s.set>
	<@s.set var="title_auth"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_AUTH" />. <@wp.i18n key="ENTANDO_API_GOTO_DETAILS" /></@s.set>

	<@s.set var="icon_lock"><span class="icon icon-lock"></span><span class="noscreen sr-only"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_LOCK" /></span></@s.set>
	<@s.set var="title_lock"><@wp.i18n key="ENTANDO_API_METHOD_STATUS_LOCK" />. <@wp.i18n key="ENTANDO_API_GOTO_DETAILS" /></@s.set>

	<@s.iterator var="resourceFlavourVar" value="#resourceFlavoursVar" status="resourceFlavourStatusVar">
		<table class="table table-striped table-bordered table-condensed">
			<@s.iterator value="#resourceFlavourVar" var="resourceVar" status="statusVar" >
				<@s.if test="#statusVar.first">
					<@s.if test="#resourceVar.source==''core''"><@s.set var="captionVar"><@s.property value="#resourceVar.source" escapeHtml=false /></@s.set></@s.if>
					<@s.else><@s.set var="captionVar"><@s.property value="%{getText(#resourceVar.sectionCode+''.name'')}" escapeHtml=false /></@s.set></@s.else>
					<caption>
						<@s.property value="#captionVar" />
					</caption>
					<tr>
						<th class="span3"><@wp.i18n key="ENTANDO_API_RESOURCE" /></th>
						<th><@wp.i18n key="ENTANDO_API_DESCRIPTION" /></th>
						<th class="text-center span1">GET</th>
						<th class="text-center span1">POST</th>
						<th class="text-center span1">PUT</th>
						<th class="text-center span1">DELETE</th>
					</tr>
				</@s.if>
				<tr>
					<td>
						<@wp.action path="/ExtStr2/do/Front/Api/Resource/detail.action" var="detailActionURL">
							<@wp.parameter name="resourceName"><@s.property value="#resourceVar.resourceName" /></@wp.parameter>
							<@wp.parameter name="namespace"><@s.property value="#resourceVar.namespace" /></@wp.parameter>
						</@wp.action>
						<a title="<@wp.i18n key="ENTANDO_API_GOTO_DETAILS" />:&#32;/<@s.property value="%{#resourceVar.namespace.length()>0?#resourceVar.namespace+''/'':''''}" /><@s.property value="#resourceVar.resourceName" />" href="${detailActionURL}" ><@s.property value="#resourceVar.resourceName" /></a>
					</td>
					<td><@s.property value="#resourceVar.description" /></td>
					<td class="text-center">
						<@s.if test="#resourceVar.getMethod != null && #resourceVar.getMethod.active && (!#resourceVar.getMethod.hidden)" >
							<@s.if test="#resourceVar.getMethod.requiredPermission != null" ><@s.set var="icon" value="#icon_lock" /><@s.set var="title" value="#title_lock" /></@s.if>
							<@s.elseif test="#resourceVar.getMethod.requiredAuth" ><@s.set var="icon" value="#icon_auth" /><@s.set var="title" value="#title_auth" /></@s.elseif>
							<@s.else><@s.set var="icon" value="#icon_free" /><@s.set var="title" value="#title_free" /></@s.else>
							<a href="${detailActionURL}#api_method_GET" title="<@s.property value="#title" />">
								<@s.property value="#icon" escapeHtml=false />
							</a>
						</@s.if>
						<@s.else><abbr title="<@wp.i18n key="ENTANDO_API_METHOD_STATUS_NA" />">&ndash;</abbr></@s.else>
					</td>
					<td class="text-center">
						<@s.if test="#resourceVar.postMethod != null && #resourceVar.postMethod.active && (!#resourceVar.postMethod.hidden)" >
							<@s.if test="#resourceVar.postMethod.requiredPermission != null" ><@s.set var="icon" value="#icon_lock" /><@s.set var="title" value="#title_lock" /></@s.if>
							<@s.elseif test="#resourceVar.postMethod.requiredAuth" ><@s.set var="icon" value="#icon_auth" /><@s.set var="title" value="#title_auth" /></@s.elseif>
							<@s.else><@s.set var="icon" value="#icon_free" /><@s.set var="title" value="#title_free" /></@s.else>
							<a href="${detailActionURL}#api_method_POST" title="<@s.property value="#title" />">
								<@s.property value="#icon" escapeHtml=false />
							</a>
						</@s.if>
						<@s.else><abbr title="<@wp.i18n key="ENTANDO_API_METHOD_STATUS_NA" />">&ndash;</abbr></@s.else>
					</td>
					<td class="text-center">
						<@s.if test="#resourceVar.putMethod != null && #resourceVar.putMethod.active && (!#resourceVar.putMethod.hidden)" >
							<@s.if test="#resourceVar.putMethod.requiredPermission != null" ><@s.set var="icon" value="#icon_lock" /><@s.set var="title" value="#title_lock" /></@s.if>
							<@s.elseif test="#resourceVar.putMethod.requiredAuth" ><@s.set var="icon" value="#icon_auth" /><@s.set var="title" value="#title_auth" /></@s.elseif>
							<@s.else><@s.set var="icon" value="#icon_free" /><@s.set var="title" value="#title_free" /></@s.else>
							<a href="${detailActionURL}#api_method_PUT" title="<@s.property value="#title" />">
								<@s.property value="#icon" escapeHtml=false />
							</a>
						</@s.if>
						<@s.else><abbr title="<@wp.i18n key="ENTANDO_API_METHOD_STATUS_NA" />">&ndash;</abbr></@s.else>
					</td>
					<td class="text-center">
						<@s.if test="#resourceVar.deleteMethod != null && #resourceVar.deleteMethod.active && (!#resourceVar.deleteMethod.hidden)" >
							<@s.if test="#resourceVar.deleteMethod.requiredPermission != null" ><@s.set var="icon" value="#icon_lock" /><@s.set var="title" value="#title_lock" /></@s.if>
							<@s.elseif test="#resourceVar.deleteMethod.requiredAuth" ><@s.set var="icon" value="#icon_auth" /><@s.set var="title" value="#title_auth" /></@s.elseif>
							<@s.else><@s.set var="icon" value="#icon_free" /><@s.set var="title" value="#title_free" /></@s.else>
							<a href="${detailActionURL}#api_method_DELETE" title="<@s.property value="#title" />">
								<@s.property value="#icon" escapeHtml=false />
							</a>
						</@s.if>
						<@s.else><abbr title="<@wp.i18n key="ENTANDO_API_METHOD_STATUS_NA" />">&ndash;</abbr></@s.else>
					</td>
				</tr>
			</@s.iterator>
		</table>

		<@s.if test="#resourceVar.source==''core''">
			<a href="<@wp.action path="/ExtStr2/do/Front/Api/Service/list.action" />" class="btn btn-primary pull-right"><@wp.i18n key="ENTANDO_API_GOTO_SERVICE_LIST" /></a>
		</@s.if>
	</@s.iterator>
</@s.if>
<@s.else>
	<p><@wp.i18n key="ENTANDO_API_NO_RESOURCES" /></p>
</@s.else>
<script nonce="<@wp.cspNonce />" >
  $(function () {
    $(''#api-togglers a:first'').tab(''show'');
  })
</script>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_resource_detail','entando_apis',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@s.set var="apiResourceVar" value="apiResource" />
<@s.set var="GETMethodVar" value="#apiResourceVar.getMethod" />
<@s.set var="POSTMethodVar" value="#apiResourceVar.postMethod" />
<@s.set var="PUTMethodVar" value="#apiResourceVar.putMethod" />
<@s.set var="DELETEMethodVar" value="#apiResourceVar.deleteMethod" />
<@s.set var="apiNameVar" value="(#apiResourceVar.namespace!=null && #apiResourceVar.namespace.length()>0 ? ''/'' + #apiResourceVar.namespace : '''')+''/''+#apiResourceVar.resourceName" />
<section>
<p>
	<a href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>
<h2><@wp.i18n key="ENTANDO_API_RESOURCE" />&#32;<@s.property value="#apiNameVar" /></h2>
<@s.if test="hasActionMessages()">
	<div class="alert alert-box alert-success">
		<h3 class="alert-heading"><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionMessages">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.if test="hasActionErrors()">
	<div class="alert alert-box alert-error">
		<h3 class="alert-heading"><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionErrors">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<!-- DESCRIPTION -->
<p><@s.property value="#apiResourceVar.description" /></p>

<!-- INFO -->
<dl class="dl-horizontal">
	<dt><@wp.i18n key="ENTANDO_API_RESOURCE_NAME" /></dt>
		<dd><@s.property value="#apiResourceVar.resourceName" /></dd>
	<dt><span lang="en"><@wp.i18n key="ENTANDO_API_RESOURCE_NAMESPACE" /></span></dt>
		<dd>/<@s.property value="#apiResourceVar.namespace" /></dd>
	<dt><@wp.i18n key="ENTANDO_API_RESOURCE_SOURCE" /></dt>
		<dd>
			<@s.property value="#apiResourceVar.source" /><@s.if test="%{#apiResourceVar.pluginCode != null && #apiResourceVar.pluginCode.length() > 0}">,&#32;<@s.property value="%{getText(#apiResourceVar.pluginCode+''.name'')}" />&#32;(<@s.property value="%{#apiResourceVar.pluginCode}" />)</@s.if>
		</dd>
	<dt><@wp.i18n key="ENTANDO_API_RESOURCE_URI" /></dt>
		<dd>
			<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />legacyapi/rs/<@wp.info key="currentLang" /><@s.if test="null != #apiResourceVar.namespace">/<@s.property value="#apiResourceVar.namespace" /></@s.if>/<@s.property value="#apiResourceVar.resourceName" />"><@wp.info key="systemParam" paramName="applicationBaseURL" />legacyapi/rs/<@wp.info key="currentLang" /><@s.if test="null != #apiResourceVar.namespace">/<@s.property value="#apiResourceVar.namespace" /></@s.if>/<@s.property value="#apiResourceVar.resourceName" /></a>
		</dd>
	<dt>
		<@wp.i18n key="ENTANDO_API_EXTENSION" />
	</dt>
		<dd>
			<@wp.i18n key="ENTANDO_API_EXTENSION_NOTE" />
		</dd>
</dl>

	<@s.set var="methodVar" value="#GETMethodVar" />
	<@s.set var="currentMethodNameVar" value="%{''GET''}" />
	<h3 id="api_method_GET">GET</h3>
	<@wp.fragment code="entandoapi_is_resource_detail_include" escapeXml=false />

	<@s.set var="methodVar" value="#POSTMethodVar" />
	<@s.set var="currentMethodNameVar" value="%{''POST''}" />
	<h3 id="api_method_POST">POST</h3>
	<@wp.fragment code="entandoapi_is_resource_detail_include" escapeXml=false />

	<@s.set var="methodVar" value="#PUTMethodVar" />
	<@s.set var="currentMethodNameVar" value="%{''PUT''}" />
	<h3 id="api_method_PUT">PUT</h3>
	<@wp.fragment code="entandoapi_is_resource_detail_include" escapeXml=false />

	<@s.set var="methodVar" value="#DELETEMethodVar" />
	<@s.set var="currentMethodNameVar" value="%{''DELETE''}" />
	<h3 id="api_method_DELETE">DELETE</h3>
	<@wp.fragment code="entandoapi_is_resource_detail_include" escapeXml=false />
<p>
	<a href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>
</section>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_service_list','entando_apis',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<section>

<p>
	<a href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>

<h2><@wp.i18n key="ENTANDO_API_GOTO_SERVICE_LIST" /></h2>
<@s.if test="hasActionErrors()">
	<div class="alert alert-block alert-error">
		<h3 class="alert-heading"><@s.text name="message.title.ActionErrors" /></h3>
		<ul>
			<@s.iterator value="actionErrors">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.if test="hasFieldErrors()">
	<div class="alert alert-block alert-error">
		<h3 class="alert-heading"><@s.text name="message.title.FieldErrors" /></h3>
		<ul>
			<@s.iterator value="fieldErrors">
				<@s.iterator value="value">
				<li><@s.property escapeHtml=false /></li>
				</@s.iterator>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.if test="hasActionMessages()">
	<div class="alert alert-block alert-info">
		<h3 class="alert-heading"><@s.text name="messages.confirm" /></h3>
		<ul>
			<@s.iterator value="actionMessages">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.set var="resourceFlavoursVar" value="resourceFlavours" />
<@s.set var="serviceFlavoursVar" value="serviceFlavours" />

<@s.if test="#serviceFlavoursVar != null && #serviceFlavoursVar.size() > 0">
<div class="tabbable tabs-left">
	<ul class="nav nav-tabs">
		<@s.iterator var="resourceFlavour" value="#resourceFlavoursVar" status="statusVar">
			<@s.set var="serviceGroupVar" value="#resourceFlavour.get(0).getSectionCode()" />
			<@s.set var="servicesByGroupVar" value="#serviceFlavoursVar[#serviceGroupVar]" />
			<@s.if test="null != #servicesByGroupVar && #servicesByGroupVar.size() > 0">
				<@s.if test="#serviceGroupVar == ''core''"><@s.set var="captionVar" value="%{#serviceGroupVar}" /></@s.if>
				<@s.else><@s.set var="captionVar" value="%{getText(#serviceGroupVar + ''.name'')}" /></@s.else>
				<li<@s.if test="#statusVar.first"> class="active"</@s.if>>
					<a href="#api-flavour-<@s.property value=''%{#captionVar.toLowerCase().replaceAll("[^a-z0-9-]", "")}'' />" data-toggle="tab"><@s.property value=''%{#captionVar}'' /></a>
				</li>
			</@s.if>
		</@s.iterator>
	</ul>

  <div class="tab-content">
	<@s.iterator var="resourceFlavour" value="#resourceFlavoursVar" status="moreStatusVar">
		<@s.set var="serviceGroupVar" value="#resourceFlavour.get(0).getSectionCode()" />
		<@s.set var="servicesByGroupVar" value="#serviceFlavoursVar[#serviceGroupVar]" />
		<@s.if test="null != #servicesByGroupVar && #servicesByGroupVar.size() > 0">
			<@s.if test="#serviceGroupVar == ''core''"><@s.set var="captionVar" value="%{#serviceGroupVar}" /></@s.if>
			<@s.else><@s.set var="captionVar" value="%{getText(#serviceGroupVar + ''.name'')}" /></@s.else>
			<div class="tab-pane<@s.if test="#moreStatusVar.first"> active</@s.if>" id="api-flavour-<@s.property value=''%{#captionVar.toLowerCase().replaceAll("[^a-z0-9]", "")}'' />">
			<table class="table table-striped table-bordered table-condensed">
				<caption>
					<@s.property value="#captionVar" />
				</caption>
				<tr>
					<th><@wp.i18n key="ENTANDO_API_SERVICE" /></th>
					<th><@wp.i18n key="ENTANDO_API_DESCRIPTION" /></th>
				</tr>
				<@s.iterator var="serviceVar" value="#servicesByGroupVar" >
					<tr>
						<td class="monospace">
							<@wp.action path="/ExtStr2/do/Front/Api/Service/detail.action" var="detailActionURL">
								<@wp.parameter name="serviceKey"><@s.property value="#serviceVar.key" /></@wp.parameter>
							</@wp.action>
							<a href="${detailActionURL}"><@s.property value="#serviceVar.key" /></a>
						</td>
						<td><@s.property value="#serviceVar.value" /></td>
					</tr>
				</@s.iterator>
			</table>
			</div>
		</@s.if>
	</@s.iterator>
	</div>
</div>
</@s.if>
<@s.else>
<div class="alert alert-block alert-info">
	<p><@wp.i18n key="ENTANDO_API_NO_SERVICES" escapeXml=false /></p>
</div>
</@s.else>

<p>
	<a href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />" class="btn btn-primary"><i class="icon-arrow-left icon-white"></i>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>

</section>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entandoapi_is_service_detail','entando_apis',NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@wp.headInfo type="CSS" info="widgets/api.css"/>
<@s.set var="apiServiceVar" value="%{getApiService(serviceKey)}" />
<div class="entando-api api-resource-detail">
<h2><@wp.i18n key="ENTANDO_API_SERVICE" />&#32;<@s.property value="serviceKey" /></h2>
<@s.if test="hasActionMessages()">
	<div class="message message_confirm">
		<h3><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionMessages">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>
<@s.if test="hasActionErrors()">
	<div class="message message_error">
		<h3><@wp.i18n key="ENTANDO_API_ERROR" /></h3>
		<ul>
			<@s.iterator value="actionErrors">
				<li><@s.property escapeHtml=false /></li>
			</@s.iterator>
		</ul>
	</div>
</@s.if>

<p class="description"><@s.property value="getTitle(serviceKey, #apiServiceVar.description)" /></p>

<@s.set var="masterMethodVar" value="#apiServiceVar.master" />

<dl class="dl-horizontal">
	<dt><@wp.i18n key="ENTANDO_API_SERVICE_KEY" /></dt>
		<dd><@s.property value="serviceKey" /></dd>
	<dt><@wp.i18n key="ENTANDO_API_SERVICE_PARENT_API" /></dt>
		<dd><@s.property value="#masterMethodVar.description" />&#32;(/<@s.if test="#masterMethodVar.namespace!=null && #masterMethodVar.namespace.length()>0"><@s.property value="#masterMethodVar.namespace" />/</@s.if><@s.property value="#masterMethodVar.resourceName" />)</dd>
	<dt>
		<@wp.i18n key="ENTANDO_API_SERVICE_AUTHORIZATION" />
	</dt>
		<dd>
			<@s.if test="%{!#apiServiceVar.requiredAuth}" >
				<@wp.i18n key="ENTANDO_API_SERVICE_AUTH_FREE" />
			</@s.if>
			<@s.elseif test="%{null == #apiServiceVar.requiredPermission && null == #apiServiceVar.requiredGroup}">
				<@wp.i18n key="ENTANDO_API_SERVICE_AUTH_SIMPLE" />
			</@s.elseif>
			<@s.else>
				<@s.set var="serviceAuthGroupVar" value="%{getGroup(#apiServiceVar.requiredGroup)}" />
				<@s.set var="serviceAuthPermissionVar" value="%{getPermission(#apiServiceVar.requiredPermission)}" />
				<@s.if test="%{null != #serviceAuthPermissionVar}">
					<@wp.i18n key="ENTANDO_API_SERVICE_AUTH_WITH_PERM" />&#32;<@s.property value="#serviceAuthPermissionVar.description" />
				</@s.if>
				<@s.if test="%{null != #serviceAuthGroupVar}">
					<@s.if test="%{null != #serviceAuthPermissionVar}"><br /></@s.if>
					<@wp.i18n key="ENTANDO_API_SERVICE_AUTH_WITH_GROUP" />&#32;<@s.property value="#serviceAuthGroupVar.descr" />
				</@s.if>
			</@s.else>
		</dd>
	<dt><@wp.i18n key="ENTANDO_API_SERVICE_URI" /></dt>
		<dd>
			<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />legacyapi/rs/<@wp.info key="currentLang" />/getService?key=<@s.property value="serviceKey" />"><@wp.info key="systemParam" paramName="applicationBaseURL" />legacyapi/rs/<@wp.info key="currentLang" />/getService?key=<@s.property value="serviceKey" /></a>
		</dd>
	<dt>
		<@wp.i18n key="ENTANDO_API_EXTENSION" />
	</dt>
		<dd>
			<@wp.i18n key="ENTANDO_API_EXTENSION_NOTE" />
		</dd>
	<dt>
		<@wp.i18n key="ENTANDO_API_SERVICE_SCHEMAS" />
	</dt>
		<dd class="schemas">
			<@wp.action path="/ExtStr2/do/Front/Api/Service/responseSchema.action" var="responseSchemaURLVar" >
				<@wp.parameter name="serviceKey"><@s.property value="serviceKey" /></@wp.parameter>
			</@wp.action>
			<a href="${responseSchemaURLVar}" >
				<@wp.i18n key="ENTANDO_API_SERVICE_SCHEMA_RESP" />
			</a>
		</dd>
</dl>

<@s.if test="%{null != #apiServiceVar.freeParameters && #apiServiceVar.freeParameters.length > 0}" >
<table class="table table-striped table-bordered table-condensed" summary="<@wp.i18n key="ENTANDO_API_SERVICE_PARAMETERS_SUMMARY" />">
	<caption><span><@wp.i18n key="ENTANDO_API_SERVICE_PARAMETERS" /></span></caption>
	<tr>
		<th><@wp.i18n key="ENTANDO_API_SERVICE_PARAM_NAME" /></th>
		<th><@wp.i18n key="ENTANDO_API_SERVICE_PARAM_DESCRIPTION" /></th>
		<th><@wp.i18n key="ENTANDO_API_SERVICE_PARAM_REQUIRED" /></th>
		<th><@wp.i18n key="ENTANDO_API_SERVICE_PARAM_DEFAULT_VALUE" /></th>
	</tr>
	<@s.iterator value="#apiServiceVar.freeParameters" var="apiParameterNameVar" >
		<@s.set var="apiParameterValueVar" value="%{#apiServiceVar.parameters[#apiParameterNameVar]}" />
		<@s.set var="apiParameterVar" value="%{#apiServiceVar.master.getParameter(#apiParameterNameVar)}" />
		<@s.set var="apiParameterRequiredVar" value="%{#apiParameterVar.required && null == #apiParameterValueVar}" />
		<tr>
			<td><label for="<@s.property value="#apiParameterNameVar" />"><@s.property value="#apiParameterNameVar" /></label></td>
			<td><@s.property value="%{#apiParameterVar.description}" /></td>
			<td class="icon required_<@s.property value="#apiParameterRequiredVar" />">
				<@s.if test="#apiParameterRequiredVar" ><@wp.i18n key="YES" /></@s.if>
				<@s.else><@wp.i18n key="NO" /></@s.else>
			</td>
			<td><@s.if test="null != #apiParameterValueVar"><@s.property value="#apiParameterValueVar" /></@s.if><@s.else>-</@s.else></td>
		</tr>
	</@s.iterator>
</table>
</@s.if>
<p class="api-back">
	<a class="btn btn-primary" href="<@wp.action path="/ExtStr2/do/Front/Api/Resource/list.action" />"><span class="icon-arrow-left icon-white"></span>&#32;<@wp.i18n key="ENTANDO_API_GOTO_LIST" /></a>
</p>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('login_form','login_form',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<h1><@wp.i18n key="RESERVED_AREA" /></h1>
<#if (Session.currentUser.username != "guest") >
	<p><@wp.i18n key="WELCOME" />, <em>${Session.currentUser}</em>!</p>
	<#if (Session.currentUser.entandoUser) >
	<table class="table table-condensed">
		<tr>
			<th><@wp.i18n key="USER_DATE_CREATION" /></th>
			<th><@wp.i18n key="USER_DATE_ACCESS_LAST" /></th>
			<th><@wp.i18n key="USER_DATE_PASSWORD_CHANGE_LAST" /></th>
		</tr>
		<tr>
			<td>${Session.currentUser.creationDate?default("-")}</td>
			<td>${Session.currentUser.lastAccess?default("-")}</td>
			<td>${Session.currentUser.lastPasswordChange?default("-")}</td>
		</tr>
	</table>
		<#if (!Session.currentUser.credentialsNotExpired) >
		<div class="alert alert-block">
			<p><@wp.i18n key="USER_STATUS_EXPIRED_PASSWORD" />: <a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/editPassword.action"><@wp.i18n key="USER_STATUS_EXPIRED_PASSWORD_CHANGE" /></a></p>
		</div>
		</#if>
	</#if>
	<@wp.ifauthorized permission="enterBackend">
	<div class="btn-group">
		<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/main.action?request_locale=<@wp.info key="currentLang" />" class="btn"><@wp.i18n key="ADMINISTRATION" /></a>
	</div>
	</@wp.ifauthorized>
	<p class="pull-right"><a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action" class="btn"><@wp.i18n key="LOGOUT" /></a></p>
	<@wp.pageWithWidget widgetTypeCode="userprofile_editCurrentUser" var="userprofileEditingPageVar" listResult=false />
	<#if (userprofileEditingPageVar??) >
	<p><a href="<@wp.url page="${userprofileEditingPageVar.code}" />" ><@wp.i18n key="userprofile_CONFIGURATION" /></a></p>
	</#if>
<#else>
	<#if (accountExpired?? && accountExpired == true) >
	<div class="alert alert-block alert-error">
		<p><@wp.i18n key="USER_STATUS_EXPIRED" /></p>
	</div>
	</#if>
	<#if (wrongAccountCredential?? && wrongAccountCredential == true) >
	<div class="alert alert-block alert-error">
		<p><@wp.i18n key="USER_STATUS_CREDENTIALS_INVALID" /></p>
	</div>
	</#if>
	<form action="<@wp.url/>" method="post" class="form-horizontal margin-medium-top">
		<#if (RequestParameters.returnUrl??) >
		<input type="hidden" name="returnUrl" value="${RequestParameters.returnUrl}" />
		</#if>
		<div class="control-group">
			<label for="username" class="control-label"><@wp.i18n key="USERNAME" /></label>
			<div class="controls">
				<input id="username" type="text" name="username" class="input-xlarge" />
			</div>
		</div>
		<div class="control-group">
			<label for="password" class="control-label"><@wp.i18n key="PASSWORD" /></label>
			<div class="controls">
				<input id="password" type="password" name="password" class="input-xlarge" />
			</div>
		</div>
		<div class="form-actions">
			<input type="submit" value="<@wp.i18n key="SIGNIN" />" class="btn btn-primary" />
		</div>
	</form>
</#if>',0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('internal_servlet_generic_error',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.i18n key="GENERIC_ERROR" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('internal_servlet_user_not_allowed',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.i18n key="USER_NOT_ALLOWED" />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('messages_system','messages_system',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<#assign currentPageCode><@wp.currentPage param="code" /></#assign>

<#if (currentPageCode == ''notfound'')>
<div class="alert alert-error alert-block">
	<h1 class="alert-heading"><@wp.i18n key="PAGE_NOT_FOUND" escapeXml=false /></h1>
</div>
</#if>
<#if (currentPageCode == ''errorpage'')>
<div class="alert alert-error alert-block">
	<h1 class="alert-heading"><@wp.i18n key="GENERIC_ERROR" escapeXml=false /></h1>
</div>
</#if>',0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('default_pagerBlock',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<#if (group.size > group.max)>
	<div class="pagination pagination-centered">
		<ul>
		<#if (1 != group.currItem)>
			<#if (group.advanced)>
				<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >1</@wp.parameter></@wp.url>" title="<@wp.i18n key="PAGER_FIRST" />"><i class="icon-fast-backward"></i></a></li>
				<#if (1 != group.beginItemAnchor)>
					<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.currItem - group.offset}</@wp.parameter></@wp.url>" title="<@wp.i18n key="PAGER_STEP_BACKWARD" />&#32;${group.offset}"><i class="icon-step-backward"></i></a></li>
				</#if>
			</#if>
			<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.prevItem}</@wp.parameter></@wp.url>"><@wp.i18n key="PAGER_PREV" /></a></li>
		</#if>
		<#list group.items as item>
		<#if (item_index >= (group.beginItemAnchor-1)) && (item_index <= (group.endItemAnchor-1))>
			<#if (item == group.currItem)>
			<li class="active"><a href="#">${item}</a></li>
			<#else>
			<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${item}</@wp.parameter></@wp.url>">${item}</a></li>
			</#if>
		</#if>
		</#list>
		<#if (group.maxItem != group.currItem)>
			<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.nextItem}</@wp.parameter></@wp.url>"><@wp.i18n key="PAGER_NEXT" /></a></li>
			<#if (group.advanced)>
				<#if (group.maxItem != group.endItemAnchor)>
					<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.currItem + group.offset}</@wp.parameter></@wp.url>" title="<@wp.i18n key="PAGER_STEP_FORWARD" />&#32;${group.offset}"><i class="icon-step-forward"></i></a></li>
				</#if>
				<li><a href="<@wp.url paramRepeat=true ><@wp.parameter name="${group.paramItemName}" >${group.maxItem}</@wp.parameter></@wp.url>" title="<@wp.i18n key="PAGER_LAST" />"><i class="icon-fast-forward"></i></a></li>
			</#if>
		</#if>
		</ul>
	</div>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('default_pagerInfo_is',NULL,NULL,NULL,'<#assign s=JspTaglibs["/struts-tags"]>
<p><@s.text name="note.searchIntro" />&#32;<@s.property value="#group.size" />&#32;<@s.text name="note.searchOutro" />.<br />
<@s.text name="label.page" />: [<@s.property value="#group.currItem" />/<@s.property value="#group.maxItem" />].</p>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('default_pagerFormBlock_is',NULL,NULL,NULL,'<#assign wpsf=JspTaglibs["/apsadmin-form"]>
<#assign s=JspTaglibs["/struts-tags"]>
<@s.if test="#group.size > #group.max">
<ul class="pagination">
	<@s.if test="null != #group.pagerId">
		<@s.set var="pagerIdMarker" value="#group.pagerId" />
	</@s.if>
	<@s.else>
		<@s.set var="pagerIdMarker">pagerItem</@s.set>
	</@s.else>
	<@s.if test="#group.advanced">
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_1''}" type="button" disabled="%{1 == #group.currItem}" title="%{getText(''label.goToFirst'')}">
			<span class="icon fa fa-step-backward"></span>
		</@wpsf.submit>
	</li>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + (#group.currItem - #group.offset) }" type="button" disabled="%{1 == #group.beginItemAnchor}" title="%{getText(''label.jump'') + '' '' + #group.offset + '' '' + getText(''label.backward'')}">
			<span class="icon fa fa-fast-backward"></span>
		</@wpsf.submit>
	</li>
	</@s.if>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + #group.prevItem}" type="button" title="%{getText(''label.prev.full'')}" disabled="%{1 == #group.currItem}">
			<span class="icon fa fa-long-arrow-left"></span>
		</@wpsf.submit>
	</li>
	<@s.subset source="#group.items" count="#group.endItemAnchor-#group.beginItemAnchor+1" start="#group.beginItemAnchor-1">
		<@s.iterator id="item">
			<li>
				<@wpsf.submit name="%{#pagerIdMarker + ''_'' + #item}" type="button" disabled="%{#item == #group.currItem}">
					<@s.property value="%{#item}" />
				</@wpsf.submit>
			</li>
		</@s.iterator>
	</@s.subset>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + #group.nextItem}" type="button" title="%{getText(''label.next.full'')}" disabled="%{#group.maxItem == #group.currItem}">
			<span class="icon fa fa-long-arrow-right"></span>
		</@wpsf.submit>
	</li>
	<@s.if test="#group.advanced">
	<@s.set var="jumpForwardStep" value="#group.currItem + #group.offset"></@s.set>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + (#jumpForwardStep)}" type="button" disabled="%{#group.maxItem == #group.endItemAnchor}" title="%{getText(''label.jump'') + '' '' + #group.offset + '' '' + getText(''label.forward'')}">
			<span class="icon fa fa-fast-forward"></span>
		</@wpsf.submit>
	</li>
	<li>
		<@wpsf.submit name="%{#pagerIdMarker + ''_'' + #group.size}" type="button" disabled="%{#group.maxItem == #group.currItem}" title="%{getText(''label.goToLast'')}">
			<span class="icon fa fa-step-forward"></span>
		</@wpsf.submit>
	</li>
	</@s.if>
	<@s.set var="pagerIdMarker" value="null" />
</ul>
</@s.if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('search_form','search_form','jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.pageWithWidget var="searchResultPageVar" widgetTypeCode="search_result" />
<@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />
<search-bar-widget
   action-url="<#if (searchResultPageVar??) ><@wp.url page="${searchResultPageVar.code}" /></#if>"
   placeholder="<@wp.i18n key="ESSF_SEARCH" />"
></search-bar-widget>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_EnumerMap',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<div class="control-group">
	<label for="${formFieldNameVar}" class="control-label"><@wp.i18n key="${i18n_Attribute_Key}" /></label>
	<div class="controls">
		<select name="${formFieldNameVar}" id="${formFieldNameVar}" class="input-xlarge">
			<option value=""><@wp.i18n key="ALL" /></option>
			<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
			<option value="${enumeratorMapItemVar.key}" <#if (formFieldValue??) && (enumeratorMapItemVar.key == formFieldValue)>selected="selected"</#if> >${enumeratorMapItemVar.value}</option>
			</#list>
		</select>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Number',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<fieldset>
<legend>
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<@wp.i18n key="${i18n_Attribute_Key}" />
</legend>
<div class="control-group">
	<#assign formFieldStartNameVar = userFilterOptionVar.formFieldNames[0] >
	<#assign formFieldStartValueVar = userFilterOptionVar.getFormFieldValue(formFieldStartNameVar) >
	<label for="${formFieldStartNameVar}" class="control-label">
		<@wp.i18n key="NUMBER_FROM" />
	</label>
	<div class="controls">
		<input id="${formFieldStartNameVar}" name="${formFieldStartNameVar}" value="${formFieldStartValueVar?default("")}" type="number" class="input-medium" />
	</div>
</div>
<div class="control-group">
	<#assign formFieldEndNameVar = userFilterOptionVar.formFieldNames[1] >
	<#assign formFieldEndValueVar = userFilterOptionVar.getFormFieldValue(formFieldEndNameVar) >
	<label for="${formFieldEndNameVar}" class="control-label">
		<@wp.i18n key="NUMBER_TO" />
	</label>
	<div class="controls">
		<input id="${formFieldEndNameVar}" name="${formFieldEndNameVar}" value="${formFieldEndValueVar?default("")}" type="number" class="input-medium" />
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilters',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#if (userFilterOptionsVar??) && userFilterOptionsVar?has_content && (userFilterOptionsVar?size > 0)>
<div class="row-fluid"><div class="span12 padding-medium-top">
<#assign hasUserFilterError = false >
<#list userFilterOptionsVar as userFilterOptionVar>
<#if (userFilterOptionVar.formFieldErrors??) && userFilterOptionVar.formFieldErrors?has_content && (userFilterOptionVar.formFieldErrors?size > 0)>
<#assign hasUserFilterError = true >
</#if>
</#list>
<#if (hasUserFilterError)>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert" href="#"><i class="icon-remove"></i></a>
	<h2 class="alert-heading"><@wp.i18n key="ERRORS" /></h2>
	<ul>
		<#list userFilterOptionsVar as userFilterOptionVar>
			<#if (userFilterOptionVar.formFieldErrors??) && (userFilterOptionVar.formFieldErrors?size > 0)>
			<#assign formFieldErrorsVar = userFilterOptionVar.formFieldErrors >
			<#list formFieldErrorsVar?keys as formFieldErrorKey>
			<li>
			<@wp.i18n key="jacms_LIST_VIEWER_FIELD" />&#32;<em>${formFieldErrorsVar[formFieldErrorKey].attributeName}</em><#if (formFieldErrorsVar[formFieldErrorKey].rangeFieldType??)>:&#32;<em><@wp.i18n key="${formFieldErrorsVar[formFieldErrorKey].rangeFieldType}" /></em></#if>&#32;<@wp.i18n key="${formFieldErrorsVar[formFieldErrorKey].errorKey}" />
			</li>
			</#list>
			</#if>
		</#list>
	</ul>
</div>
</#if>
<#assign hasUserFilterError = false >
<p><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#content-viewer-list-filters"><@wp.i18n key="SEARCH_FILTERS_BUTTON" /> <i class="icon-zoom-in icon-white"></i></button></p>
<form action="<@wp.url />" method="post" class="form-horizontal collapse" id="content-viewer-list-filters">
	<#list userFilterOptionsVar as userFilterOptionVar>
		<@wp.freemarkerTemplateParameter var="userFilterOptionVar" valueName="userFilterOptionVar" removeOnEndTag=true >
		<#if !userFilterOptionVar.attributeFilter && (userFilterOptionVar.key == "fulltext" || userFilterOptionVar.key == "category")>
			<@wp.fragment code="jacms_content_viewer_list_userfilter_met_${userFilterOptionVar.key}" escapeXml=false />
		</#if>
		<#if userFilterOptionVar.attributeFilter >
			<#if userFilterOptionVar.attribute.type == "Monotext" || userFilterOptionVar.attribute.type == "Text" || userFilterOptionVar.attribute.type == "Longtext" || userFilterOptionVar.attribute.type == "Hypertext">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Text" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "Enumerator" >
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Enumer" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "EnumeratorMap" >
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_EnumerMap" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "Number">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Number" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "Date">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Date" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "Boolean">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Boolean" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "CheckBox">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_CheckBox" escapeXml=false />
			</#if>
			<#if userFilterOptionVar.attribute.type == "ThreeState">
				<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_ThreeSt" escapeXml=false />
			</#if>
		</#if>
		</@wp.freemarkerTemplateParameter>
	</#list>
	<p class="form-actions">
		<input type="submit" value="<@wp.i18n key="SEARCH" />" class="btn btn-primary" />
	</p>
</form>
</div></div>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_met_fulltext',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<div class="control-group">
    <label for="${formFieldNameVar}" class="control-label"><@wp.i18n key="TEXT" /></label>
    <div class="controls">
        <input name="${formFieldNameVar}" id="${formFieldNameVar}" value="${formFieldValue}" type="text" class="input-xlarge"/>
    </div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_met_category',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign userFilterCategoryCodeVar = userFilterOptionVar.userFilterCategoryCode?default("") >
<@wp.categories var="systemCategories" titleStyle="prettyFull" root="${userFilterCategoryCodeVar}" />
<div class="control-group">
	<label for="category" class="control-label"><@wp.i18n key="CATEGORY" /></label>
	<div class="controls">
		<select id="category" name="${formFieldNameVar}" class="input-xlarge">
			<option value=""><@wp.i18n key="ALL" /></option>
			<#list systemCategories as systemCategory>
			<option value="${systemCategory.key}" <#if (formFieldValue == systemCategory.key)>selected="selected"</#if> >${systemCategory.value}</option>
			</#list>
		</select>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Text',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<div class="control-group">
	<label for="${formFieldNameVar}" class="control-label"><@wp.i18n key="${i18n_Attribute_Key}" /></label>
	<div class="controls">
		<input name="${formFieldNameVar}" id="${formFieldNameVar}" value="${formFieldValue}" type="text" class="input-xlarge"/>
	</div>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer','content_viewer','jacms',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@jacms.contentInfo param="authToEdit" var="canEditThis" />
<@jacms.contentInfo param="contentId" var="myContentId" />
<#if (canEditThis?? && canEditThis)>
	<style>
		.bar-content-edit {
				position: absolute;
				transform: translate(4px, 20px);
		}
		.bar-content-edit .btn {
				color: white;
				padding: 12px 32px;
				background: #33B5E5;
				text-transform: uppercase;
				text-decoration: none;
				border-radius: 2px;
				box-shadow: 4px 4px 8px 1px rgba(0,0,0,0.5);
		}
		.bar-content-edit-container:not(:hover) .bar-content-edit {
				display: none;
		}
	</style>
	<div class="bar-content-edit-container">
		<div class="bar-content-edit">
			<a href="<@wp.info key="systemParam" paramName="appBuilderBaseURL" />cms/content/edit/<@jacms.contentInfo param="contentId" />" class="btn btn-info">
			<@wp.i18n key="EDIT_THIS_CONTENT" /> <i class="icon-edit icon-white"></i></a>
		</div>
		<@jacms.content publishExtraTitle=true />
	</div>
<#else>
  <@jacms.content publishExtraTitle=true />
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list','content_viewer_list','jacms',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@wp.headInfo type="JS_EXT" info="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js" />
<@jacms.contentList listName="contentList" titleVar="titleVar"
	pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />
<#if (titleVar??)>
	<h1>${titleVar}</h1>
</#if>
<@wp.freemarkerTemplateParameter var="userFilterOptionsVar" valueName="userFilterOptionsVar" removeOnEndTag=true >
<@wp.fragment code="jacms_content_viewer_list_userfilters" escapeXml=false />
</@wp.freemarkerTemplateParameter>
<#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
	<@wp.pager listName="contentList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
		<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
		<@wp.fragment code="default_pagerBlock" escapeXml=false />
<#list contentList as contentId>
<#if (contentId_index >= groupContent.begin) && (contentId_index <= groupContent.end)>
	<@jacms.content contentId="${contentId}" />
</#if>
</#list>
		<@wp.fragment code="default_pagerBlock" escapeXml=false />
		</@wp.freemarkerTemplateParameter>
	</@wp.pager>
<#else>
		<p class="alert alert-info"><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
</#if>
<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
	<p class="text-right"><a class="btn btn-primary" href="<@wp.url page="${pageLinkVar}"/>">${pageLinkDescriptionVar}</a></p>
</#if>
<#assign contentList="">',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Date',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<#assign currentLangVar ><@wp.info key="currentLang" /></#assign>

<#assign js_for_datepicker="jQuery(function($){
	$.datepicker.regional[''it''] = {
		closeText: ''Chiudi'',
		prevText: ''&#x3c;Prec'',
		nextText: ''Succ&#x3e;'',
		currentText: ''Oggi'',
		monthNames: [''Gennaio'',''Febbraio'',''Marzo'',''Aprile'',''Maggio'',''Giugno'',
			''Luglio'',''Agosto'',''Settembre'',''Ottobre'',''Novembre'',''Dicembre''],
		monthNamesShort: [''Gen'',''Feb'',''Mar'',''Apr'',''Mag'',''Giu'',
			''Lug'',''Ago'',''Set'',''Ott'',''Nov'',''Dic''],
		dayNames: [''Domenica'',''Luned&#236'',''Marted&#236'',''Mercoled&#236'',''Gioved&#236'',''Venerd&#236'',''Sabato''],
		dayNamesShort: [''Dom'',''Lun'',''Mar'',''Mer'',''Gio'',''Ven'',''Sab''],
		dayNamesMin: [''Do'',''Lu'',''Ma'',''Me'',''Gi'',''Ve'',''Sa''],
		weekHeader: ''Sm'',
		dateFormat: ''yy-mm-dd'',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''''};
});

jQuery(function($){
	if (Modernizr.touch && Modernizr.inputtypes.date) {
		$.each(	$(''input[data-isdate=true]''), function(index, item) {
			item.type = ''date'';
		});
	} else {
		$.datepicker.setDefaults( $.datepicker.regional[''${currentLangVar}''] );
		$(''input[data-isdate=true]'').datepicker({
      			changeMonth: true,
      			changeYear: true,
      			dateFormat: ''yyyy-mm-dd''
    		});
	}
});" >

<@wp.headInfo type="JS" info="entando-misc-html5-essentials/modernizr-2.5.3-full.js" />
<@wp.headInfo type="JS_EXT" info="http://code.jquery.com/ui/1.10.0/jquery-ui.min.js" />
<@wp.headInfo type="CSS_EXT" info="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.min.css" />
<@wp.headInfo type="JS_RAW" info="${js_for_datepicker}" />
<fieldset>
<legend>
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<@wp.i18n key="${i18n_Attribute_Key}" />
</legend>
<div class="control-group">
	<#assign formFieldStartNameVar = userFilterOptionVar.formFieldNames[0] >
	<#assign formFieldStartValueVar = userFilterOptionVar.getFormFieldValue(formFieldStartNameVar) >
	<label for="${formFieldStartNameVar}" class="control-label">
		<@wp.i18n key="DATE_FROM" />
	</label>
	<div class="controls">
		<input id="${formFieldStartNameVar}" name="${formFieldStartNameVar}" value="${formFieldStartValueVar?default("")}" type="text" data-isdate="true" class="input-xlarge" placeholder="YYYY-MM-DD" />
	</div>
</div>
<div class="control-group">
	<#assign formFieldEndNameVar = userFilterOptionVar.formFieldNames[1] >
	<#assign formFieldEndValueVar = userFilterOptionVar.getFormFieldValue(formFieldEndNameVar) >
	<label for="${formFieldEndNameVar}" class="control-label">
		<@wp.i18n key="DATE_TO" />
	</label>
	<div class="controls">
		<input id="${formFieldEndNameVar}" name="${formFieldEndNameVar}" value="${formFieldEndValueVar?default("")}" type="text" data-isdate="true" class="input-xlarge" placeholder="YYYY-MM-DD" />
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Boolean',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<fieldset>
<legend><@wp.i18n key="${i18n_Attribute_Key}" /></legend>
<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Bool_io" escapeXml=false />
<div class="control-group">
	<div class="controls">
		<label for="${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="true_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "true")>checked="checked"</#if> value="true" type="radio" />
		<@wp.i18n key="YES"/></label>
	</div>
	<div class="controls">
		<label for="false_${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="false_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "false")>checked="checked"</#if> value="false" type="radio" />
		<@wp.i18n key="NO"/></label>
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_Bool_io',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameControlVar = userFilterOptionVar.formFieldNames[2] >
<input name="${formFieldNameControlVar}" type="hidden" value="true" />
<#assign formFieldNameIgnoreVar = userFilterOptionVar.formFieldNames[1] >
<#assign formFieldIgnoreValue = userFilterOptionVar.getFormFieldValue(formFieldNameIgnoreVar) >
<#assign formFieldControlValue = userFilterOptionVar.getFormFieldValue(formFieldNameControlVar) >
<div class="controls">
	<label for="ignore_${formFieldNameIgnoreVar}" class="checkbox">
	<input id="ignore_${formFieldNameIgnoreVar}" name="${formFieldNameIgnoreVar}" <#if (formFieldIgnoreValue?? && formFieldIgnoreValue == "true")>checked="checked"</#if> value="true" type="checkbox" />
	<@wp.i18n key="IGNORE" /></label>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_CheckBox',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<fieldset>
<legend><@wp.i18n key="${i18n_Attribute_Key}" /></legend>
<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Bool_io" escapeXml=false />
<div class="control-group">
	<div class="controls">
		<label for="true_${formFieldNameVar}" class="checkbox">
		<input name="${formFieldNameVar}" id="true_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "true")>checked="checked"</#if> value="true" type="checkbox" />
		<@wp.i18n key="YES"/></label>
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_content_viewer_list_userfilter_ent_ThreeSt',NULL,'jacms',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<fieldset>
<legend><@wp.i18n key="${i18n_Attribute_Key}" /></legend>
<@wp.fragment code="jacms_content_viewer_list_userfilter_ent_Bool_io" escapeXml=false />
<div class="control-group">
	<div class="controls">
		<label for="true_${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="true_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "true")>checked="checked"</#if> value="true" type="radio" />
		<@wp.i18n key="YES"/></label>
		<label for="false_${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="false_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "false")>checked="checked"</#if> value="false" type="radio" />
		<@wp.i18n key="NO"/></label>
		<label for="both_${formFieldNameVar}" class="radio">
		<input name="${formFieldNameVar}" id="both_${formFieldNameVar}" <#if (formFieldValue??) && (formFieldValue == "both")>checked="checked"</#if> value="both" type="radio" />
		<@wp.i18n key="BOTH"/></label>
	</div>
</div>
</fieldset>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('ilpatto_mappa_comuni','ilpatto_mappa_comuni',NULL,'<#assign wp=JspTaglibs[ "/aps-core"]>

<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react@17.0.1/umd/react.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react-dom@17.0.1/umd/react-dom.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />ilpatto-bundle/js/widgets/ilpatto-microfrontends/build.js" ></script>

<ilpatto-service-url value="http://metroca.mshome.net/cmca/ilpatto-microservice/0-1-3-snapshot" />
<ilpatto-mappa-comuni ></ilpatto-mappa-comuni>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('search_result','search_result','jacms',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<h1><@wp.i18n key="SEARCH_RESULTS" /></h1>
<#if (RequestParameters.search?? && RequestParameters.search!='''')>
<@jacms.searcher listName="contentListResult" />
</#if>
<p><@wp.i18n key="SEARCHED_FOR" />: <em><strong><#if (RequestParameters.search??)>${RequestParameters.search}</#if></strong></em></p>
<#if (contentListResult??) && (contentListResult?has_content) && (contentListResult?size > 0)>
<@wp.pager listName="contentListResult" objectName="groupContent" max=10 pagerIdFromFrame=true advanced=true offset=5>
	<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
	<p><em><@wp.i18n key="SEARCH_RESULTS_INTRO" /> <!-- infamous whitespace hack -->
	${groupContent.size}
	<@wp.i18n key="SEARCH_RESULTS_OUTRO" /> [${groupContent.begin + 1} &ndash; ${groupContent.end + 1}]:</em></p>
	<@wp.fragment code="default_pagerBlock" escapeXml=false />
	<#list contentListResult as contentId>
	<#if (contentId_index >= groupContent.begin) && (contentId_index <= groupContent.end)>
		<@jacms.content contentId="${contentId}" modelId="list" />
	</#if>
	</#list>
	<@wp.fragment code="default_pagerBlock" escapeXml=false />
	</@wp.freemarkerTemplateParameter>
</@wp.pager>
<#else>
<p class="alert alert-info"><@wp.i18n key="SEARCH_NOTHING_FOUND" /></p>
</#if>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jacms_row_content_viewer_list','row_content_viewer_list','jacms',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar"
	pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />
<#if (titleVar??)>
	<h1>${titleVar}</h1>
</#if>
<#if (contentInfoList??) && (contentInfoList?has_content) && (contentInfoList?size > 0)>
	<@wp.pager listName="contentInfoList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
	<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
	<@wp.fragment code="default_pagerBlock" escapeXml=false />
	<#list contentInfoList as contentInfoVar>
	<#if (contentInfoVar_index >= groupContent.begin) && (contentInfoVar_index <= groupContent.end)>
		<#if (contentInfoVar[''modelId'']??)>
		<@jacms.content contentId="${contentInfoVar[''contentId'']}" modelId="${contentInfoVar[''modelId'']}" />
		<#else>
		<@jacms.content contentId="${contentInfoVar[''contentId'']}" />
		</#if>
	</#if>
	</#list>
	<@wp.fragment code="default_pagerBlock" escapeXml=false />
	</@wp.freemarkerTemplateParameter>
	</@wp.pager>
</#if>
<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
	<p class="text-right"><a class="btn btn-primary" href="<@wp.url page="${pageLinkVar}"/>">${pageLinkDescriptionVar}</a></p>
</#if>
<#assign contentInfoList="">',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('breadcrumb','breadcrumb',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<@wp.currentPage param="code" var="currentViewCode" />

<div class="navbar-position">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">

            <span class="noscreen">
                <@wp.i18n key="ESNB_YOU_ARE_HERE" />
            </span>
            <#assign first=true>
            <@wp.nav spec="current.path" var="currentTarget">
            <#assign currentCode=currentTarget.code>

            <!--            <#if first>
                        <span class="divider">/</span>
                        </#if>-->

            <#if !currentTarget.voidPage>
            <#if (currentCode == currentViewCode)>
            <li class="breadcrumb-item active" aria-current="page">${currentTarget.title}</li>
            <#else>
            <li class="breadcrumb-item">
                <a href="${currentTarget.url}">${currentTarget.title}</a>
            </li>
            </#if>
            <#else>
            ${currentTarget.title}
            </#if>
            <#assign first=false>
            </@wp.nav>
        </ol>
    </nav>
</div>
',NULL,1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('logo','logo',NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<@wp.info key="systemParam" paramName="applicationBaseURL" var="appUrl" />
<img src="${appUrl}resources/static/img/Entando_light.svg" aria-label="Entando" alt="Logo" role="logo" />','<#assign wp=JspTaglibs["/aps-core"]>

<@wp.info key="systemParam" paramName="applicationBaseURL" var="appUrl" />
<img src="${appUrl}resources/static/img/Entando_light.svg" aria-label="Entando" alt="Entando" role="logo" />',0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('language','language',NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<@wp.info key="langs" var="langsVar" />
<@wp.info key="currentLang" var="currentLangVar" />

<@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />

<#assign langstrs = [] />
<#list langsVar as curLangVar>
  <#assign langurl><@wp.url lang="${curLangVar.code}" paramRepeat=true /></#assign>
  <#assign langdesc><@wp.i18n key="LANG_${curLangVar.code?upper_case}" /></#assign>
  <#assign langstr = ["{\"code\": \"" + curLangVar.code + "\", \"descr\": \"" + langdesc + "\", \"url\": \""+ langurl +"\"}"]  />
  <#assign langstrs = langstrs + langstr />
</#list>
<#assign lang_json_string = langstrs?join(", ") />

<choose-language-widget
  currentLang="${currentLangVar}"
  languages="[${lang_json_string?html}]"
></choose-language-widget>','<#assign wp=JspTaglibs["/aps-core"]>

<@wp.info key="langs" var="langsVar" />
<@wp.info key="currentLang" var="currentLangVar" />

<@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />

<#assign langstrs = [] />
<#list langsVar as curLangVar>
  <#assign langurl><@wp.url lang="${curLangVar.code}" paramRepeat=true /></#assign>
  <#assign langstr = ["{\"code\": \"" + curLangVar.code + "\", \"descr\": \"" + curLangVar.descr + "\", \"url\": \""+ langurl +"\"}"]  />
  <#assign langstrs = langstrs + langstr />
</#list>
<#assign lang_json_string = langstrs?join(", ") />

<choose-language-widget
  currentLang="${currentLangVar}"
  languages="[${lang_json_string?html}]"
></choose-language-widget>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('keycloak-login','keycloak-login',NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />

<#assign sessionUser = "" />
<#assign userDisplayName = "" />
<#if (Session.currentUser.username != "guest") >
   <#assign sessionUser = Session.currentUser.username />
   <#if (Session.currentUser.profile??) && (Session.currentUser.profile.displayName??)>
      <#assign userDisplayName = Session.currentUser.profile.displayName />
   <#else>
      <#assign userDisplayName = Session.currentUser />
   </#if>
</#if>

<login-button-widget
   admin-url="<@wp.info key="systemParam" paramName="appBuilderBaseURL" />"
   user-display-name="${userDisplayName}"
   redirect-url="<@wp.url />"
></login-button-widget>','<#assign wp=JspTaglibs["/aps-core"]>

<@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />

<#assign sessionUser = "" />
<#assign userDisplayName = "" />
<#if (Session.currentUser.username != "guest") >
   <#assign sessionUser = Session.currentUser.username />
   <#if (Session.currentUser.profile??) && (Session.currentUser.profile.displayName??)>
      <#assign userDisplayName = Session.currentUser.profile.displayName />
   <#else>
      <#assign userDisplayName = Session.currentUser />
   </#if>
</#if>

<login-button-widget
   admin-url="<@wp.info key="systemParam" paramName="appBuilderBaseURL" />"
   user-display-name="${userDisplayName}"
   redirect-url="<@wp.url />"
></login-button-widget>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('navigation-menu','navigation-menu',NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<@wp.currentPage param="code" var="currentPageCode" />

<@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />

<#assign navstrs = [] />
<@wp.nav var="page">
  <#assign navstr = ["{\"code\": \"" + page.code + "\", \"title\": \"" + page.title + "\", \"level\": \"" + page.level + "\", \"url\": \""+ page.url +"\", \"voidPage\": " + page.voidPage?string("true", "false") + "}"]  />
  <#assign navstrs = navstrs + navstr />
</@wp.nav>
<#assign nav_json_string = navstrs?join(", ") />

<navigation-bar-widget
  current-page="${currentPageCode}"
  nav-items="[${nav_json_string?html}]"
></navigation-bar-widget>','<#assign wp=JspTaglibs["/aps-core"]>

<@wp.currentPage param="code" var="currentPageCode" />

<@wp.fragment code="entando_ootb_carbon_include" escapeXml=false />

<#assign navstrs = [] />
<@wp.nav var="page">
  <#assign navstr = ["{\"code\": \"" + page.code + "\", \"title\": \"" + page.title + "\", \"level\": \"" + page.level + "\", \"url\": \""+ page.url +"\", \"voidPage\": " + page.voidPage?string("true", "false") + "}"]  />
  <#assign navstrs = navstrs + navstr />
</@wp.nav>
<#assign nav_json_string = navstrs?join(", ") />

<navigation-bar-widget
  current-page="${currentPageCode}"
  nav-items="[${nav_json_string?html}]"
></navigation-bar-widget>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('sitemap','sitemap',NULL,NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@jacms.contentList listName="contentList" contentType="NWS" />
<@wp.currentPage param="code" var="currentPageCode" />
<@wp.freemarkerTemplateParameter var="currentPageCode" valueName="currentPageCode" />
<link rel="stylesheet" type="text/css" href="<@wp.resourceURL />ootb-widgets/static/css/sitemap.css">

<div class="well well-small sitemap">
<h2>Sitemap</h2>

<ul class="nav nav-list">
<@wp.nav spec="code(homepage).subtree(50)" var="page">
   <#if (previousPage?? && previousPage.code??)>
	<#assign previousLevel=previousPage.level>
	<#assign level=page.level>
	<@wp.freemarkerTemplateParameter var="previousLevel" valueName="previousLevel" />
	<@wp.freemarkerTemplateParameter var="level" valueName="level" />
	<@wp.fragment code="sitemap_menu_include" escapeXml=false />
   </#if>
   <@wp.freemarkerTemplateParameter var="previousPage" valueName="page" />
</@wp.nav>
<#if (previousPage??)>
   <#assign previousLevel = previousPage.level>
   <#assign level=0>
   <@wp.freemarkerTemplateParameter var="previousLevel" valueName="previousLevel" />
   <@wp.freemarkerTemplateParameter var="level" valueName="level" />
   <@wp.fragment code="sitemap_menu_include" escapeXml=false />
   <#if (previousLevel != 0)>
	<#list 0..(previousLevel - 1) as ignoreMe>
		</ul></li>
	</#list>
   </#if>
</#if>
<ul class="nav nav-list">
     <li class="nav-header">
     <strong>News</strong>
<ul class="nav-list">
<#list contentList as contentId>
	<@jacms.content contentId="${contentId}" modelId="10020" />
</#list>
</ul>
</li>
</ul>
</div>
<@wp.freemarkerTemplateParameter var="previousPage" valueName="" removeOnEndTag=true />
',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('sitemap_menu_include',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign liClass="">
<#assign aClass="">
<#if previousPage.voidPage>
 <#assign liClass='' class="nav-header" ''>
    <#assign aClass='' class="a-void" ''>
</#if>
<li ${liClass}>
<#if previousLevel != 0>
<#if (!previousPage.voidPage)>
<a href="${previousPage.url}"  ${aClass}>
<#else>
<a ${aClass}>
</#if>
${previousPage.title}</a>
<#else>
<strong>Pages</strong>
</#if>
<#if (previousLevel == level)>
</li>
</#if>
<#if (previousLevel < level)>
<ul class="nav-list">
</#if>
<#if (previousLevel > level)>
<#list 1..(previousLevel - level) as ignoreMe>
</li></ul>
</#list>
</#if>
',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('entando_ootb_carbon_include',NULL,NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<script src="<@wp.resourceURL />ootb-widgets/static/js/2.ootb.chunk.js" nonce="<@wp.cspNonce />"></script>
<script src="<@wp.resourceURL />ootb-widgets/static/js/main.ootb.chunk.js" nonce="<@wp.cspNonce />"></script>
<script src="<@wp.resourceURL />ootb-widgets/static/js/runtime-main.ootb.js" nonce="<@wp.cspNonce />"></script>
<link href="<@wp.resourceURL />ootb-widgets/static/css/main.ootb.chunk.css" rel="stylesheet">
',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('keycloak_auth_with_redirect',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<script nonce="<@wp.cspNonce />" >
  (function () {
    const consolePrefix = ''[ENTANDO-KEYCLOAK]'';
    const keycloakConfigEndpoint = ''<@wp.info key="systemParam" paramName="applicationBaseURL" />keycloak.json'';
    let keycloakConfig;
    function dispatchKeycloakEvent(eventType) {
      console.info(consolePrefix, ''Dispatching'', eventType, ''custom event'');
      return window.dispatchEvent(new CustomEvent(''keycloak'', { detail: { eventType } }));
    };
    function initKeycloak() {
      const keycloak = new Keycloak(keycloakConfig);
      keycloak.onReady = function() {
        dispatchKeycloakEvent(''onReady'');
      };
      keycloak.onAuthSuccess = function() {
        dispatchKeycloakEvent(''onAuthSuccess'');
      };
      keycloak.onAuthError = function() {
        dispatchKeycloakEvent(''onAuthError'');
      };
      keycloak.onAuthRefreshSuccess = function() {
        dispatchKeycloakEvent(''onAuthRefreshSuccess'');
      };
      keycloak.onAuthRefreshError = function() {
        dispatchKeycloakEvent(''onAuthRefreshError'');
      };
      keycloak.onAuthLogout = function() {
        dispatchKeycloakEvent(''onAuthLogout'');
      };
      keycloak.onTokenExpired = function() {
        dispatchKeycloakEvent(''onTokenExpired'');
      };
      window.entando = {
        ...(window.entando || {}),
        keycloak,
      };
      window.entando.keycloak
        .init({ onLoad: ''login-required'', promiseType: ''native'', enableLogging: true })
        .catch(function (e) {
          console.error(e);
          console.error(consolePrefix, ''Failed to initialize Keycloak'');
        });
    };
    function onKeycloakScriptError(e) {
      console.error(e);
      console.error(consolePrefix, ''Failed to load keycloak.js script'');
    };
    function addKeycloakScript(keycloakConfig) {
      const script = document.createElement(''script'');
      script.src = keycloakConfig[''auth-server-url''] + ''/js/keycloak.js'';
      script.async = true;
      script.addEventListener(''load'', initKeycloak);
      script.addEventListener(''error'', onKeycloakScriptError);
      document.body.appendChild(script);
    };
    fetch(keycloakConfigEndpoint)
      .then(function (response) {
        return response.json();
      })
      .then(function (config) {
        keycloakConfig = config;
        if (!keycloakConfig.clientId) {
          keycloakConfig.clientId = keycloakConfig.resource;
        }
        addKeycloakScript(keycloakConfig);
      })
      .catch(function (e) {
        console.error(e);
        console.error(consolePrefix, ''Failed to fetch Keycloak configuration'');
      });
  })();</script>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('keycloak_auth',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<script nonce="<@wp.cspNonce />" >
  (function () {
    const consolePrefix = ''[ENTANDO-KEYCLOAK]'';
    const keycloakConfigEndpoint = ''<@wp.info key="systemParam" paramName="applicationBaseURL" />keycloak.json'';
    let keycloakConfig;
    function dispatchKeycloakEvent(eventType) {
      console.info(consolePrefix, ''Dispatching'', eventType, ''custom event'');
      return window.dispatchEvent(new CustomEvent(''keycloak'', { detail: { eventType } }));
    };
    function initKeycloak() {
      const keycloak = new Keycloak(keycloakConfig);
      keycloak.onReady = function() {
        dispatchKeycloakEvent(''onReady'');
      };
      keycloak.onAuthSuccess = function() {
        dispatchKeycloakEvent(''onAuthSuccess'');
      };
      keycloak.onAuthError = function() {
        dispatchKeycloakEvent(''onAuthError'');
      };
      keycloak.onAuthRefreshSuccess = function() {
        dispatchKeycloakEvent(''onAuthRefreshSuccess'');
      };
      keycloak.onAuthRefreshError = function() {
        dispatchKeycloakEvent(''onAuthRefreshError'');
      };
      keycloak.onAuthLogout = function() {
        dispatchKeycloakEvent(''onAuthLogout'');
      };
      keycloak.onTokenExpired = function() {
        dispatchKeycloakEvent(''onTokenExpired'');
      };
      function onKeycloakInitialized(isAuthenticated) {
        if (isAuthenticated) {
          console.info(consolePrefix, ''Keycloak initialized, user authenticated'');
        } else {
          console.info(consolePrefix, ''Keycloak initialized, user not authenticated'');
        }
      };
      window.entando = {
        ...(window.entando || {}),
        keycloak,
      };
      window.entando.keycloak
        .init({ onLoad: ''check-sso'', promiseType: ''native'', enableLogging: true })
        .then(onKeycloakInitialized)
        .catch(function (e) {
          console.error(e);
          console.error(consolePrefix, ''Failed to initialize Keycloak'');
        });
    };
    function onKeycloakScriptError(e) {
      console.error(e);
      console.error(consolePrefix, ''Failed to load keycloak.js script'');
    };
    function addKeycloakScript(keycloakConfig) {
      const script = document.createElement(''script'');
      script.src = keycloakConfig[''auth-server-url''] + ''/js/keycloak.js'';
      script.async = true;
      script.addEventListener(''load'', initKeycloak);
      script.addEventListener(''error'', onKeycloakScriptError);
      document.body.appendChild(script);
    };
    fetch(keycloakConfigEndpoint)
      .then(function (response) {
        return response.json();
      })
      .then(function (config) {
        keycloakConfig = config;
        if (!keycloakConfig.clientId) {
          keycloakConfig.clientId = keycloakConfig.resource;
        }
        addKeycloakScript(keycloakConfig);
      })
      .catch(function (e) {
        console.error(e);
        console.error(consolePrefix, ''Failed to fetch Keycloak configuration'');
      });
  })();</script>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jpseo_content_viewer','jpseo_content_viewer','jpseo',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign jpseo=JspTaglibs["/jpseo-aps-core"]>
<@jacms.contentInfo param="authToEdit" var="canEditThis" />
<@jacms.contentInfo param="contentId" var="myContentId" />
<#if (canEditThis?? && canEditThis)>
	<div class="bar-content-edit">
		<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/jacms/Content/edit.action?contentId=<@jacms.contentInfo param="contentId" />" class="btn btn-info">
		<@wp.i18n key="EDIT_THIS_CONTENT" /> <i class="icon-edit icon-white"></i></a>
	</div>
</#if>
<@jpseo.content publishExtraTitle=true publishExtraDescription=true />',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jpseo_model_meta_info',NULL,'jpseo',NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign jpseo=JspTaglibs["/jpseo-aps-core"]>

<@jpseo.currentPage param="description" var="metaDescrVar" />
<#if (metaDescrVar??)>
<meta name="description" content="<@c.out value="${metaDescrVar}" />" />
</#if>

<#-- EXAMPLE OF meta infos on page -->
<#--
<@jpseo.seoMetaTag key="author" var="metaAuthorVar" />
<#if (metaAuthorVar??)>
<meta name="author" content="<@c.out value="${metaAuthorVar}" />" />
</#if>

<@jpseo.seoMetaTag key="keywords" var="metaKeywords" />
<#if (metaKeywords??)>
<meta name="keywords" content="<@c.out value="${metaKeywords}" />" />
</#if>
-->
',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_widget_advsearch_results','cagliari_widget_advsearch_results','whitelabel','<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<@wp.currentWidget param="config" configParam="contentTypesFilter" var="tipiContenuto" />
<#if (RequestParameters[''cercatxt'']??)><#assign cercatxt = RequestParameters[''cercatxt'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign cercatxt = ""></#if>
<#if (RequestParameters[''argomenti'']??)><#assign argomenti = RequestParameters[''argomenti'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign argomenti = ""></#if>
<#if (RequestParameters[''amministrazione'']??)><#assign amministrazione = RequestParameters[''amministrazione'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign amministrazione = ""></#if>
<#if (RequestParameters[''servizi'']??)><#assign servizi = RequestParameters[''servizi'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign servizi = ""></#if>
<#if (RequestParameters[''novita'']??)><#assign novita = RequestParameters[''novita'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign novita = ""></#if>
<#if (RequestParameters[''documenti'']??)><#assign documenti = RequestParameters[''documenti'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign documenti = ""></#if>
<#if (RequestParameters[''attivi'']??)><#assign attivi = RequestParameters[''attivi'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign attivi = ""></#if>
<#if (attivi == "")><#assign attivi = ''false''/></#if>
<#if (RequestParameters[''inizio'']??)><#assign inizio = RequestParameters[''inizio'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign inizio = ""></#if>
<#if (RequestParameters[''fine'']??)><#assign fine = RequestParameters[''fine'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign fine = ""></#if>

<div class="progress-spinner progress-spinner-active" data-ng-show="::false">
	<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_LOADING" />...</span>
</div>

<div data-ng-cloak data-ng-controller="ctrlRicerca as ctrl">
	<data-ng-container data-ng-init="ctrl.Ricerca.setContentTypes(''${tipiContenuto}'');
		ctrl.Ricerca.setArgsSelected(''${argomenti?replace(''"'', '''')}''); 
		ctrl.Ricerca.setCatSelected(''amministrazione'',''${amministrazione?replace(''"'', '''')}''); 
		ctrl.Ricerca.setCatSelected(''servizi'',''${servizi?replace(''"'', '''')}'');
		ctrl.Ricerca.setCatSelected(''novita'',''${novita?replace(''"'', '''')}'');
		ctrl.Ricerca.setCatSelected(''documenti'',''${documenti?replace(''"'', '''')}'');
		ctrl.Ricerca.setCercatxt(''${cercatxt}'');
		ctrl.Ricerca.activeChk = ${attivi};
		ctrl.Ricerca.dataInizio = ''${inizio}'';
		ctrl.Ricerca.dataFine = ''${fine}''">
	</data-ng-container>
	
	<section id="introricerca" data-ng-init="setSearchCookie(true); ctrl.Ricerca.getContents(); showall=false; showfilter=false">
		<div class="container">
			<div class="row">
				<div class="offset-lg-1 col-lg-10 col-md-12">
					<div class="titolo-sezione">
						<h2><#if (cercatxt!="")>${cercatxt}<#else><@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT" /></#if></h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="articolo-dettaglio-testo">
		<div class="container">
			<div class="row">
				<div class="linetop-lg"></div>
				<div class="col-lg-3 col-md-4 col-sm-12">
					<div class="cerca-risultati d-block d-sm-block d-md-none d-lg-none d-xl-none">
						<#assign morethan><@wp.i18n key="CITTAMETRO_PORTAL_MORETHAN" /></#assign>
						<span data-ng-if="ctrl.Ricerca.getContentsVar().length > 0"><span data-ng-show="ctrl.Ricerca.getTotalItems() == ''100000''">${morethan?cap_first} </span>{{ctrl.Ricerca.getTotalItems()}} <@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /></span>
						<span data-ng-if="ctrl.Ricerca.getContentsVar().length == 0"><@wp.i18n key="LIST_VIEWER_EMPTY" /></span>
						<a class="show-filters" data-ng-click="showfilter=false" data-ng-show="showfilter" href=""><strong><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></strong></a>
						<a class="show-filters" data-ng-click="showfilter=true" data-ng-hide="showfilter" href=""><strong><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></strong></a>
						<span class="float-right" data-ng-if="ctrl.Ricerca.getContentsVar().length > 0 && !showfilter">
							<select class="ordinamento" name="ordinamento" data-ng-model="ctrl.Ricerca.orderBy" data-ng-change="ctrl.Ricerca.getContents()" title="<@wp.i18n key="CITTAMETRO_PORTAL_CHOOSE_OPTION" />">
								<option value="data_last_mod"><@wp.i18n key="CITTAMETRO_PORTAL_LAST_MODIFIED" /></option>
								<option value="data_pubb"><@wp.i18n key="CITTAMETRO_PORTAL_PUBLICATION_DATE" /></option>
								<option value="titolo_asc"><@wp.i18n key="CITTAMETRO_PORTAL_INCREASING_TITLE" /></option>
								<option value="titolo_desc"><@wp.i18n key="CITTAMETRO_PORTAL_DESCENDING_TITLE" /></option>
							</select>
						</span>
						<button data-ng-show="showfilter" data-ng-click="showfilter=false" class="confirm btn btn-default btn-trasparente float-right"><@wp.i18n key="CITTAMETRO_PORTAL_CONFIRM" /></button>
					</div>
					<aside id="menu-sinistro-cerca" class="d-md-block" data-ng-class="showfilter ? ''d-sm-block d-block'' : ''d-sm-none d-none''">
						<h4><@wp.i18n key="CITTAMETRO_PORTAL_CATEGORIES" /></h4>
						<div class="search-filter-ckgroup">
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" />"
									data-ng-checked="ctrl.Ricerca.ammins_isChecked()"
									md-indeterminate="ctrl.Ricerca.ammins_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.ammins_toggleAll(); ctrl.Ricerca.getContents()"><label><@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" /></label>
								</md-checkbox>
							</div>
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" />"
									data-ng-checked="ctrl.Ricerca.servizi_isChecked()"
									md-indeterminate="ctrl.Ricerca.servizi_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.servizi_toggleAll(); ctrl.Ricerca.getContents()"><label><@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></label>
								</md-checkbox>
							</div>
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" />"
									data-ng-checked="ctrl.Ricerca.novita_isChecked()"
									md-indeterminate="ctrl.Ricerca.novita_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.novita_toggleAll(); ctrl.Ricerca.getContents()"><label><@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" /></label>
								</md-checkbox>
							</div>
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" />"
									data-ng-checked="ctrl.Ricerca.docs_isChecked()"
									md-indeterminate="ctrl.Ricerca.docs_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.docs_toggleAll(); ctrl.Ricerca.getContents()"><label><@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" /></label>
								</md-checkbox>
							</div>
						</div>
						
						<h4><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></h4>
						<div class="search-filter-ckgroup">
							<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()">
								<div data-ng-if = "$index <= 9">
									<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
									<label data-ng-bind-html="item.name"></label></md-checkbox>
								</div>
								<div data-ng-if = "$index >= 10">									
									<div data-ng-show="showallarg">
										<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
										<label data-ng-bind-html="item.name"></label></md-checkbox>
									</div>
								</div>
							</div>
							<a href="" class="search-mostra" data-ng-hide="showallarg" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
							<a href="" class="search-mostra" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
						</div>
					</aside>
				</div>
				<div class="col-lg-9 col-md-8 d-md-block" data-ng-class="showfilter ? ''d-sm-none d-none'' : ''d-sm-block d-block''">
					<div class="articolo-paragrafi">
						<div class="row">
							<div class="col-md-12 cerca-risultati d-md-block d-none">
								<span data-ng-if="ctrl.Ricerca.getContentsVar().length > 0"><@wp.i18n key="CITTAMETRO_PORTAL_FOUND" /> <span data-ng-show="ctrl.Ricerca.getTotalItems() == ''100000''"><@wp.i18n key="CITTAMETRO_PORTAL_MORETHAN" /> </span>{{ctrl.Ricerca.getTotalItems()}} <@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /></span>
								<span data-ng-if="ctrl.Ricerca.getContentsVar().length == 0"><@wp.i18n key="LIST_VIEWER_EMPTY" /></span>
								<div class="float-right d-sm-none d-md-block" data-ng-if="ctrl.Ricerca.getContentsVar().length > 0">
									<label for="ordinamento"><@wp.i18n key="CITTAMETRO_PORTAL_SORT_BY" /></label>
									<select id="ordinamento" class="ordinamento" name="ordinamento" data-ng-model="ctrl.Ricerca.orderBy" data-ng-change="ctrl.Ricerca.getContents()" title="<@wp.i18n key="CITTAMETRO_PORTAL_CHOOSE_OPTION" />">
										<option value="data_last_mod"><@wp.i18n key="CITTAMETRO_PORTAL_LAST_MODIFIED" /></option>
										<option value="data_pubb"><@wp.i18n key="CITTAMETRO_PORTAL_PUBLICATION_DATE" /></option>
										<option value="titolo_asc"><@wp.i18n key="CITTAMETRO_PORTAL_INCREASING_TITLE" /></option>
										<option value="titolo_desc"><@wp.i18n key="CITTAMETRO_PORTAL_DESCENDING_TITLE" /></option>
									</select>
								</div>
							</div>
							<div class="linetop-lg"></div>
						</div>
						
						<div data-ng-if="ctrl.Ricerca.getContentsVar().length > 0" data-ng-repeat="elem in ctrl.Ricerca.getContentsVar()">
							<div class="row" data-ng-if="($index+1)%3 == 0 || ($index+1) == ctrl.Ricerca.getContentsVar().length || ($index+1) == 15 || (($index+1) + (15* ctrl.Ricerca.getCurrentPage)) == ctrl.Ricerca.getContentsVar().length">
								<div class="col-lg-4 col-md-12" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="ctrl.Ricerca.getRenderContent(ctrl.Ricerca.getContentsVar((ctrl.Ricerca.getContentsVar().indexOf(elem)-2)))"></div>
								<div class="col-lg-4 col-md-12" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="ctrl.Ricerca.getRenderContent(ctrl.Ricerca.getContentsVar((ctrl.Ricerca.getContentsVar().indexOf(elem)-1)))"></div>
								<div class="col-lg-4 col-md-12" data-ng-bind-html="ctrl.Ricerca.getRenderContent(elem)"></div>
							</div>
						</div>
						
						<div class="row" data-ng-if="ctrl.Ricerca.getContentsVar().length > 0 && ctrl.Ricerca.getPages().length > 1">
							<div class="col-md-12">
								<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" />">
									<ul class="pagination">
										<li class="page-item">
											<button class="page-link" data-ng-disabled="ctrl.Ricerca.getCurrentPage() == 0" data-ng-click="ctrl.Ricerca.goToPage(ctrl.Ricerca.getCurrentPage()); ctrl.Ricerca.getContents(ctrl.Ricerca.getCurrentPage()+1)" 
												title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
												<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
												<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
											</button>
										</li>
										<li data-ng-repeat="elem in ctrl.Ricerca.getPages()" data-ng-if="elem-1<ctrl.Ricerca.getCurrentPage()+5 && elem-1>ctrl.Ricerca.getCurrentPage()-5" title="{{elem}}"
											data-ng-class="ctrl.Ricerca.getCurrentPage() == (elem-1) ? ''page-item active'' : ''page-item''">
											<button data-ng-show="ctrl.Ricerca.getCurrentPage() == (elem-1)" aria-current="page" class="page-link" data-ng-click="ctrl.Ricerca.goToPage(elem); ctrl.Ricerca.getContents(elem)">{{elem}}</button>
											<button data-ng-show="ctrl.Ricerca.getCurrentPage() != (elem-1)" class="page-link" data-ng-click="ctrl.Ricerca.goToPage(elem); ctrl.Ricerca.getContents(elem)">{{elem}}</button>
										</li>
										<li class="page-item">
											<button class="page-link" data-ng-disabled="ctrl.Ricerca.getCurrentPage()+1 == ctrl.Ricerca.getNumberOfPages()" data-ng-click="ctrl.Ricerca.goToPage(ctrl.Ricerca.getCurrentPage()+1); ctrl.Ricerca.getContents(ctrl.Ricerca.getCurrentPage()+2)"
												title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
												<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
												<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
											</button>
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>',NULL,1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_widget_calendario_homepage','cagliari_widget_calendario_homepage','whitelabel','<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cc=JspTaglibs["/cagliari-core"]>

<#assign utenteToken = '''' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
<#if (Session.currentUser.accessToken??)>
<#assign utenteToken = Session.currentUser.accessToken />
</#if>
</#if>

<@wp.currentWidget param="config" configParam="contentType" var="tipoContenuto" />
<@wp.currentWidget param="config" configParam="maxElements" var="elementiMax" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />
<@wp.currentWidget param="config" configParam="modelId" var="idModello" />
<@wp.currentWidget param="config" configParam="categories" var="categorie" />
<@wp.currentWidget param="config" configParam="userFilters" var="filtri" />
<@wp.currentWidget param="config" configParam="filters" var="ordinamento" />
<@wp.categories var="systemCategories" titleStyle="prettyFull" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />


<@jacms.contentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />

<@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.min.css" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.min.css" />

<#assign pathimg><@wp.imgURL/>ponmetroca.svg</#assign>
<#setting locale="it_IT">
<#assign actualdate = .now>
<#assign monthst = actualdate?string["MMMM"]?cap_first>
<#assign day = actualdate?string["d"]>
<#assign dayNum = day?number />
<#assign month = actualdate?string["MM"]>
<#assign monthNum = month?number />
<#assign yearst = actualdate?string["yyyy"]>
<#assign finemese><@cc.lastDayOfMonth /></#assign>
<#assign finemese = finemese?number />
<#assign dataend = "${finemese}/${month}/${yearst}"  />
<#assign datatoday = "${day}/${month}/${yearst}"/>

<#if (!elementiMax??)><#assign elementiMax = 999 /></#if>
<#if (!elementiMaxItem??)><#assign elementiMaxItem = 999 /></#if>
<#if (!ordinamento??)><#assign ordinamento = "" /></#if>
<#if (!categorie??)><#assign categorie = "" /></#if>

<#if (filtri??)>
	<#assign filtriUtente = [] />
	<#list filtri?split("+") as filtro>
		<#assign value>${filtro?remove_beginning("(attributeFilter=false;key=category;categoryCode=")}</#assign>
		<#assign value>${value?remove_ending(")")}</#assign>
		<#assign filtriUtente = filtriUtente + [value] />
	</#list>
	<#assign filtriCodeName = [] />
	<#list filtriUtente as filtro>
		<#list systemCategories as systemCategory>
			<#if (filtro == systemCategory.key)>
				<#assign filtriCodeName = filtriCodeName + [filtro + "/" + systemCategory.value?keep_after_last("/ ")] />
			</#if>
		</#list>
	</#list>
</#if>

<div class="widget" data-ng-cloak data-ng-controller="FiltriController" >
	<div class="container" data-ng-init="setCalendarParameters(''${dayNum}'',''${finemese}'',false, ''${elementiMaxItem}'', ''${elementiMax}'', ''${utenteToken}'')">
		<div class="row">
			<#if (titleVar??)>
				<div class="col-md-3">
					<h3>${titleVar}</h3>
				</div>
			</#if>
			<#if (filtriCodeName??) && (filtriCodeName?has_content) && (filtriCodeName?size > 0)>
				<div class="col-md-9 calendario-filtro">
					<button id="${tipoContenuto}" class="btn btn-default btn-trasparente" title="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />"
						data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie}'', ''${tipoContenuto}''); categorie=''${categorie}''">
						<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />
					</button>
					<#list filtriCodeName as filtro>
						<#assign categoryCode>${filtro?keep_before_last("/")}</#assign>
						<#assign categoryName>${filtro?keep_after_last("/")}</#assign>
						<button id="${categoryCode}" class="btn btn-default btn-trasparente" title="${categoryName}"
							data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie},${categoryCode}'', ''${categoryCode}''); categorie=''${categoryCode}''">
							${categoryName}
						</button>
					</#list>
				</div>
			</#if>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<h4 data-ng-show="oneMonth">${monthst} ${yearst}</h4>
				<h4 data-ng-hide="oneMonth">${monthst}/{{nextMonth}} ${yearst}</h4>
				<div id="owl-calendario" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist" data-ng-cloak>
					<data-ng-container data-ng-repeat="day in calendar" data-ng-cloak data-ng-if="full[day]" data-ng-show="!empty">
						<div class="scheda-calendario" data-ng-if="day >= ${dayNum}" data-ng-init="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie}'',
							''${tipoContenuto}'', getFilterDay(day, ''${month}'',''${yearst}''), day)">
							<div class="scheda-calendario-data">
								<strong>{{day}}</strong> {{dayName[day]}}
							</div>
							<div class="progress-spinner progress-spinner-active" data-ng-show="loader[day]">
								<span class="sr-only">Caricamento...</span>
							</div>
							<ul class="scheda-calendario-lista" data-ng-if="events[day].length > 0">
								<li data-ng-cloak data-ng-repeat="elem in events[day]"
									data-ng-show="categoryContent[day][elem.$].indexOf(categorie)>-1"
									data-ng-bind-html="renderEventContent[day][elem.$]">
								</li>
							</ul>
						</div>
						<div class="scheda-calendario" data-ng-if="day < ${dayNum}" data-ng-init="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie}'',
							''${tipoContenuto}'', getFilterDay(day, ''${monthNum + 1}'',''${yearst}''), day)">
							<div class="scheda-calendario-data">
								<strong>{{day}}</strong> {{dayName[day]}}
							</div>
							<div class="progress-spinner progress-spinner-active" data-ng-show="loader[day]">
								<span class="sr-only">Caricamento...</span>
							</div>
							<ul class="scheda-calendario-lista" data-ng-if="events[day].length > 0">
								<li data-ng-repeat="elem in events[day]"
									data-ng-show="categoryContent[day][elem.$].indexOf(categorie)>-1"
									data-ng-bind-html="renderEventContent[day][elem.$]">
								</li>
							</ul>
						</div>
					</data-ng-container>
				</div>
				<div data-ng-show="empty" class="mt24">
					<@wp.i18n key="CITTAMETRO_PORTAL_EVENTLISTEMPTY" />
				</div>
			</div>
		</div>
	</div>
</div>',NULL,1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_widget_lista_multilayout','cagliari_widget_lista_multilayout','whitelabel','<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<#assign utenteToken = '''' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
<#if (Session.currentUser.accessToken??)>
<#assign utenteToken = Session.currentUser.accessToken />
</#if>
</#if>

<@ca.widgetFrameNumber frameid="numeroFrame" />
<@cw.currentArgument var="currentArg" />
<@wp.currentWidget param="config" configParam="contentType" var="tipoContenuto" />
<@wp.currentWidget param="config" configParam="maxElements" var="elementiMax" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />
<@wp.currentWidget param="config" configParam="modelId" var="idModello" />
<@wp.currentWidget param="config" configParam="modelId2" var="idModello2" />
<@wp.currentWidget param="config" configParam="categories" var="categorie" />
<@wp.currentWidget param="config" configParam="userFilters" var="filtri" />
<@wp.currentWidget param="config" configParam="filters" var="ordinamento" />
<@wp.currentWidget param="config" configParam="layout" var="layout" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@wp.categories var="systemCategories" root="arg_argomenti" />

<@jacms.contentList listName="contentList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />
    
<#if (!elementiMax??)><#assign elementiMax = 999 /></#if>
<#if (!elementiMaxItem??)><#assign elementiMaxItem = 999 /></#if>
<#if (!ordinamento??)><#assign ordinamento = "" /></#if>
<#if (!categorie??)><#assign categorie = "" /></#if>
<#if (!idModello??)><#assign idModello = "list" /></#if>
<#if (!idModello2??)><#assign idModello2 = "list" /></#if>
<#if (currentArg??)>
	<#assign categorie = ''${currentArg},${categorie}'' />
<#else>
	<#assign currentArg = '''' />
</#if>

<#assign altriFiltri = false />
<#assign allCategories = false/>
<#assign numTipologie = 0 />
<#assign categoryIndex = 999 />
<#assign categoryEnumIndex = 999 />
<#assign tipologieIndex = 999 />
<#assign tipologiaName = '''' />
<#assign enumCategName = '''' />

<div id="frame${numeroFrame}" class="widget" data-ng-controller="FiltriController">
	<#if (userFilterOptionsVar??) && (userFilterOptionsVar?has_content) && (userFilterOptionsVar?size > 0)>
		<#list userFilterOptionsVar as userFilterOptionVar>
			<#if (userFilterOptionVar.attribute??)>
				<#if (userFilterOptionVar.attribute.mapItems??)>
					<#if (userFilterOptionVar.attribute.type != ''MultiEnumeratorMapCategory'')>
						<#if (tipologieIndex == 999)>
							<#assign tipologieIndex = userFilterOptionVar?index/>
						</#if>
					<#elseif (userFilterOptionVar.attribute.type == ''MultiEnumeratorMapCategory'')>
						<#assign categoryEnumIndex = userFilterOptionVar?index/>
					</#if>
				</#if>
			</#if>
		</#list>
	</#if>
	
	<#if (filtri??)>
		<#assign filtriRicerca = [] />
		<#list filtri?split("+") as filtro>
			<#if (filtro?contains("key=category"))>
				<#if (filtro?remove_beginning("(attributeFilter=false;key=category)") == "") || (filtro?contains("categoryCode=arg_argomenti"))>
					<#assign allCategories = true/>
				</#if>
				<#assign categoryIndex = filtro?index/>
			<#else>
				<#assign filtriRicerca = filtriRicerca + [filtro] />
			</#if>
		</#list>
		<#if (categoryIndex <= categoryEnumIndex)>
			<#if (allCategories) && (categoryIndex < categoryEnumIndex)>
				<#list systemCategories?sort_by("value") as systemCategory>
					<data-ng-container data-ng-init="setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>
				</#list>
			<#else>
				<#assign filtriCategorie = [] />
				<#assign filtriRicerca = [] />
				<#list filtri?split("+") as filtro>
					<#if (filtro?contains("key=category"))>
						<#assign value>${filtro?remove_beginning("(attributeFilter=false;key=category;categoryCode=")}</#assign>
						<#assign value>${value?remove_ending(")")}</#assign>
						<#assign filtriCategorie = filtriCategorie + [value] />
					<#else>
						<#assign filtriRicerca = filtriRicerca + [filtro] />
					</#if>
				</#list>
				<#list filtriCategorie as filtro>
					<#list systemCategories as systemCategory>
						<#if (filtro == systemCategory.key)>
							<data-ng-container data-ng-init="setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>
						</#if>
					</#list>	
				</#list>
			</#if>
		<#elseif (categoryEnumIndex < categoryIndex)>
			<#list userFilterOptionsVar as userFilterOptionVar>
				<#if (userFilterOptionVar.attribute??)>
					<#if (userFilterOptionVar.attribute.mapItems??)>
						<#if (userFilterOptionVar?index == categoryEnumIndex)>
							<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
								<data-ng-container data-ng-init="setArgomenti(''${enumeratorMapItemVar.value?replace("''", "\\''")}'', ''${enumeratorMapItemVar.key}'')"></data-ng-container>
							</#list>
						</#if>
					</#if>
				</#if>
			</#list>
		</#if>
	</#if>

	<div class="progress-spinner progress-spinner-active" data-ng-show="::false">
		<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_LOADING" />...</span>
	</div>
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', ''${elementiMax}'', ''${tipoContenuto}'', ''${idModello}'', ''${categorie}'', '' '', ''${idModello2}'', ''${ordinamento}'', '''', ''${utenteToken}'')"> 	
		<div class="row">
			<div class="col-md-12">
				<#if ((userFilterOptionsVar??) && (userFilterOptionsVar?has_content) && (userFilterOptionsVar?size > 0)) || ((filtriCategorie??) && (filtriCategorie?size > 0) || (allCategories))>
					<div class="titolosezione filtrisezione">
						<h3 class="float-left">
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
						<div class="filtro">
							<button id="${tipoContenuto}" class="btn btn-default btn-trasparente active" data-ng-class="class_tutti()" title="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />"
								data-ng-click="toggleAll(); cleanSearchParameter(''all''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), ''${tipoContenuto}'', ''${ordinamento}'')">
								<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />
							</button>
							<#if (((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories)) && (categoryIndex<tipologieIndex) && (categoryIndex<categoryEnumIndex) >
								<button data-ng-repeat="item in argomenti" data-ng-if="$index <= 2" id="{{item.code}}" class="btn btn-default btn-trasparente" data-ng-class="(getArgomentiList().indexOf(item.code) != -1)? ''active'': ''''" title="{{item.name}}" data-ng-click="toggle(item, args_selected); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
									{{item.name}}
								</button>
								<button data-ng-repeat="item in argomenti" data-ng-if="$index >= 3" id="{{item.code}}" class="btn btn-default btn-trasparente" data-ng-class="(getArgomentiList().indexOf(item.code) != -1)? ''active'': ''''" title="{{item.name}}" data-ng-click="toggle(item, args_selected); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')" data-ng-show="getArgomentiList().indexOf(item.code) != -1 && item.code != ''${currentArg}''">
									{{item.name}}
								</button>
							</#if>
							<#if (userFilterOptionsVar??) && (userFilterOptionsVar?has_content) && (userFilterOptionsVar?size > 0)>
								<#list userFilterOptionsVar as userFilterOptionVar>
									<#if (userFilterOptionVar.attribute??)>
										<#if (userFilterOptionVar.attribute.mapItems??)>
											<#if (userFilterOptionVar?index == tipologieIndex)>
												<#assign tipologiaName = userFilterOptionVar.attribute.name />
												<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
													<#if (enumeratorMapItemVar?index <= 1) && (tipologieIndex<categoryIndex) && (tipologieIndex<categoryEnumIndex)>
														<#if (enumeratorMapItemVar?index == 0)><#assign tipologia1 = enumeratorMapItemVar.key/></#if>
														<#if (enumeratorMapItemVar?index == 1)><#assign tipologia2 = enumeratorMapItemVar.key/></#if>
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index <= 1) && ((categoryIndex<tipologieIndex) || (categoryEnumIndex<tipologieIndex)) >
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''"
															data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index >= 2)>
														<#if (enumeratorMapItemVar?index == 2) && (tipologieIndex<categoryIndex) && (tipologieIndex<categoryEnumIndex) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" title="${enumeratorMapItemVar.value}" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" data-ng-hide="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' && searchParameters.${tipologiaName} != ''${tipologia1}'' && searchParameters.${tipologiaName} != ''${tipologia2}'' && searchParametersLength != 0"
																data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index == 2) && ((categoryIndex<tipologieIndex) || (categoryEnumIndex<tipologieIndex)) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index > 2)>
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
													</#if>
													<#assign numTipologie = enumeratorMapItemVar?index + 1 />
												</#list>
											<#elseif (userFilterOptionVar?index == categoryEnumIndex) && (categoryEnumIndex<categoryIndex)>
												<#assign enumCategName = userFilterOptionVar.attribute.name />
												<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
													<#if (enumeratorMapItemVar?index <= 1) && (categoryEnumIndex<tipologieIndex)>
														<#if (enumeratorMapItemVar?index == 0)><#assign categoria1 = enumeratorMapItemVar.key/></#if>
														<#if (enumeratorMapItemVar?index == 1)><#assign categoria2 = enumeratorMapItemVar.key/></#if>
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-class="(searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}'')? ''active'': ''''"
															data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index <= 1) && (categoryEnumIndex>tipologieIndex) >
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''"
															data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index >= 2)>
														<#if (enumeratorMapItemVar?index == 2) && (tipologieIndex>categoryEnumIndex) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" title="${enumeratorMapItemVar.value}" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" data-ng-hide="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' && searchParameters.${enumCategName} != ''${categoria1}'' && searchParameters.${enumCategName} != ''${categoria2}'' && searchParametersLength != 0"
																data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index == 2) && (categoryEnumIndex>tipologieIndex) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index > 2)>
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
													</#if>
												</#list>
											<#else>
												<#assign altriFiltri = true />
											</#if>
										</#if>
									</#if>
								</#list>
							</#if>
							<#if (((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories)) && (tipologieIndex<categoryIndex) && (categoryIndex<categoryEnumIndex)>
								<button data-ng-repeat="item in argomenti" id="{{item.code}}" class="btn btn-default btn-trasparente" title="{{item.name}}" data-ng-class="(getArgomentiList().indexOf(item.code) != -1)? ''active'': ''''" data-ng-click="toggle(item, args_selected); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')" data-ng-show="getArgomentiList().indexOf(item.code) != -1 && item.code != ''${currentArg}''">
									{{item.name}}
								</button>
							</#if>
							<button data-ng-if="key != ''${tipologiaName}'' && key != ''${enumCategName}''" data-ng-show="(value.value != null && value.value != '''') || (value.start != null && value.start != '''') || (value.end != null && value.end != '''')" data-ng-repeat="(key, value) in searchParameters"  class="btn btn-default btn-trasparente active" data-ng-click="cleanSearchParameter(key); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
								<span data-ng-if="value.type==''date''">
									<span data-ng-if="value.start && value.end">{{value.title}}: <@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{value.start}}&nbsp;<@wp.i18n key="CITTAMETRO_PORTAL_TO" />&nbsp;{{value.end}} </span>
									<span data-ng-if="value.start && !value.end">{{value.title}}: <@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{value.start}}</span>
									<span data-ng-if="!value.start && value.end">{{value.title}}: <@wp.i18n key="CITTAMETRO_PORTAL_UNTIL" />&nbsp;{{value.end}}</span>
								</span>
								<span data-ng-if="value.type==''text''">
									<span data-ng-if="key == ''fulltext''">{{value.value}}</span>
									<span data-ng-if="key != ''fulltext''">{{value.title}}: {{value.value}}</span>
								</span>
								<span data-ng-if="value.type==''select''">
									<span>{{value.title}}: {{value.value}}</span>
								</span>
								<span data-ng-if="value.type==''boolean''">
									<span>{{value.title}}</span>
								</span>
							</button>
							<#if (filtriRicerca??) && (filtriRicerca?has_content) && (filtriRicerca?size > 0)>
								<#list filtriRicerca as filtro>
									<#if !((filtro?contains("key=${tipologiaName}") && tipologiaName != '''') || ((filtro?contains("key=${enumCategName}") && enumCategName != '''')))>
										<#assign altriFiltri = true />
									</#if>
								</#list>
							</#if>
							<#if (numTipologie > 3) || (allCategories) || ((filtriCategorie??) && (filtriCategorie?size > 3)) || (altriFiltri) || (categoryEnumIndex<categoryIndex)>
								<#if (tipologieIndex<categoryIndex)>
									<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''categorie''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
								<#elseif (tipologieIndex>categoryIndex) || (categoryEnumIndex<categoryIndex)>
									<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''argomenti''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
								<#else>
									<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''opzioni''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
								</#if>
							</#if>
						</div>
					</div>
				<#else>
					<div class="titolosezione">
						<h3>
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
					</div>
				</#if>
			</div>
		</div>
		
		<#if (layout == ''1'')>
			<div class="row row-eq-height widget-mono">
				<div class="col-lg-4 col-md-6 animation-if" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem}"></div>
			</div>
		</#if>
		<#if (layout == ''2'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-md-4" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
				</div>
			</div>
		</#if>
		<#if (layout == ''3'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height widget-multi" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-lg-4 col-md-6 mb16" data-ng-if="($index+1)%3 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-6 mb16" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-6 mb16" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
				</div>
			</div>
		</#if>
		<#if (layout == ''4'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height" data-ng-if="($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-lg-4 col-md-12 1a" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) >= 4" data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 2a" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) >= 3" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 3a" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 4a" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) <= 3 && contents.length - (''${elementiMaxItem}''* currentPage) >= 2"
						data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 5a" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) <= 2" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 6a" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 3" data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
				</div>
			</div>
		</#if>
		<#if (layout == ''5'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height" data-ng-if="($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-lg-4 col-md-12 1b" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) >= 5" data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello2}'']"></div>						
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 3b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 5b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4" data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 6b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 7b" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) == 3" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 8b" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) <= 3 && contents.length - (''${elementiMaxItem}''* currentPage) >= 2"
						data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 9b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) <= 3" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>	
				</div>
			</div>
		</#if>
		
		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && ${elementiMax}>${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" /><#if (titleVar??)> - ${titleVar}</#if>">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="goToPage(currentPage-1); scrollTo(''frame${numeroFrame}'')" title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}" data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage >= contents.length/pageSize - 1" data-ng-click="goToPage(currentPage+1); scrollTo(''frame${numeroFrame}'')" title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		
		<div class="row row-eq-height" data-ng-if="contents.length == 0">
			<div class="col-md-12">
				<p><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
			</div>
		</div>
	</div>

	<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
		<div class="row">
			<div class="col-md-12 veditutti">
				<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde">${pageLinkDescriptionVar} <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
			</div>
		</div>
	</#if>
	<#assign contentList="">
	
	<#if (numTipologie > 3) || (allCategories) || ((filtriCategorie??) && (filtriCategorie?size > 3)) || ((filtriRicerca??) && (filtriRicerca?has_content) && (filtriRicerca?size > 0)) || (categoryEnumIndex<categoryIndex) >
		<div class="modal fade searchWidgetModal" id="searchWidgetModal${numeroFrame}" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle${numeroFrame}" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="ricerca-interna-${numeroFrame}">
						<div class="modal-header-fullsrc">
							<div class="container">
								<div class="row">
									<div class="col-sm-1" data-ng-class="{pb12: !General.getFiltriMode().filtri}">
										<button type="button" class="close" data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}''); General.setFiltriMode(false, false)" 
											data-dismiss="modal" aria-label="Chiudi filtri di ricerca">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
									</div>
									<div class="col-sm-11">
										<h1 class="modal-title" id="searchModalTitle${numeroFrame}">
											<@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" />
										</h1>
										<button data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}''); ctrl.General.setFiltriMode(false, false)" class="confirm btn btn-default btn-trasparente float-right" data-dismiss="modal">Conferma</button>
									</div>
								</div>
								<#if (filtriRicerca??) && (filtriRicerca?has_content) && (filtriRicerca?size > 0)>
									<#list filtriRicerca as filtro>
										<#if (filtro?contains("key=titolo"))>
											<div class="row">
												<div class="col-lg-12 col-md-12 col-sm-12">
													<div class="form-group-free">
														<label class="sr-only active"><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></label>
														<input type="text" class="cerca-txt-free form-control" name="cercatxt" id="cerca-txt${numeroFrame}" placeholder="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /> <#if (titleVar??)>${titleVar?lower_case}</#if>" data-ng-model=''searchParameters.titolo.value'' data-ng-change="searchParameters.titolo.title = ''Titolo''; searchParameters.titolo.type = ''text''; searchParameters.titolo.value == ''''? cleanSearchParameter(''titolo''):''''">
													</div>
												</div>
											</div>
										<#elseif !((filtro?contains("key=${tipologiaName}") && tipologiaName != '''') || ((filtro?contains("key=${enumCategName}") && enumCategName != '''')))>
											<#assign altriFiltri = true />
										</#if>
									</#list>
								</#if>
								<#if (numTipologie > 0) || ((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories) || (altriFiltri) || (categoryEnumIndex<categoryIndex)>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div class="search-filter-type ng-hide" data-ng-show="General.getFiltriMode().filtri">
												<ul class="nav nav-tabs" role="tablist">
													<#if (numTipologie > 0)>
														<li role="presentation">
															<a href="" aria-controls="categoriaTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''categorie'')" data-ng-class="categoriaTab"><@wp.i18n key="CITTAMETRO_CONTENT_TYPES" /></a>
														</li>
													</#if>
													<#if ((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories) || (categoryEnumIndex<categoryIndex)>
														<li role="presentation" data-ng-if="argomenti.length > 0">
															<a href="" aria-controls="argomentoTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''argomenti'')" data-ng-class="argomentoTab"><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></a>
														</li>
													</#if>
													<#if (altriFiltri)>
														<li role="presentation">
															<a href="" aria-controls="opzioniTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''opzioni'')" data-ng-class="opzioniTab"><@wp.i18n key="CITTAMETRO_CONTENT_OPTIONS" /></a>
														</li>
													</#if>
												</ul>
											</div>
										</div>
									</div>
								</#if>
							</div>
						</div>

						<#if (numTipologie > 0) || ((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories) || (altriFiltri) || (categoryEnumIndex<categoryIndex)>
							<div class="modal-body-search">
								<div class="container">
									<div role="tabpanel" data-ng-show="General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">
										<div class="tab-content">
											<#if (numTipologie > 0)>
												<div role="tabpanel" data-ng-class="categoriaTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="categoriaTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12">
															<div class="search-filter-ckgroup">
																<#assign mostraTutto = false />
																<#list userFilterOptionsVar as userFilterOptionVar>
																	<#if (userFilterOptionVar.attribute??)>
																		<#if (userFilterOptionVar.attribute.mapItems??)>
																			<#if (userFilterOptionVar?index == tipologieIndex)>
																				<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
																					<#if (enumeratorMapItemVar?index <= 11)>
																						<div class="flex-100">
																							<md-checkbox data-ng-checked="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																					<#if (enumeratorMapItemVar?index >= 12)>
																						<#assign mostraTutto = true />
																						<div class="flex-100" data-ng-show="showalltipol">
																							<md-checkbox data-ng-checked="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																				</#list>
																			</#if>
																		</#if>
																	</#if>
																</#list>
																<#if (mostraTutto)>
																	<a href="" data-ng-hide="showalltipol" data-ng-click="showalltipol=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
																	<a href="" data-ng-show="showalltipol" data-ng-click="showalltipol=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
																</#if>
															</div>
														</div>
													</div>
												</div>
											</#if>
											<#if (((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories)) && (categoryIndex<categoryEnumIndex)>
												<div data-ng-if="argomenti.length > 0" role="tabpanel" data-ng-class="argomentoTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="argomentoTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12">
															<div class="search-filter-ckgroup">
																<div class="flex-100" data-ng-repeat="item in argomenti">
																	<div data-ng-if = "$index <= 11 && item.code != ''${currentArg}''">
																		<md-checkbox data-ng-checked="exists(item, args_selected)" data-ng-click="toggle(item, args_selected)" aria-label="{{item.name}}">
																		<label data-ng-bind-html="item.name"></label></md-checkbox>
																	</div>
																	<div data-ng-if = "$index >= 12 && item.code != ''${currentArg}''">									
																		<div data-ng-show="showallarg">
																			<md-checkbox data-ng-checked="exists(item, args_selected)" data-ng-click="toggle(item, args_selected)" aria-label="{{item.name}}">
																			<label data-ng-bind-html="item.name"></label></md-checkbox>
																		</div>
																	</div>
																</div>
																<a href="" data-ng-hide="showallarg || argomenti.length <= 12" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
																<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
															</div>
														</div>
													</div>
												</div>
											<#elseif (categoryEnumIndex<categoryIndex)>
												<div role="tabpanel" data-ng-class="argomentoTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="argomentoTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12">
															<div class="search-filter-ckgroup">
																<#assign mostraTuttoCat = false />
																<#list userFilterOptionsVar as userFilterOptionVar>
																	<#if (userFilterOptionVar.attribute??)>
																		<#if (userFilterOptionVar.attribute.mapItems??)>
																			<#if (userFilterOptionVar?index == categoryEnumIndex)>
																				<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
																					<#if (enumeratorMapItemVar?index <= 11)>
																						<div class="flex-100">
																							<md-checkbox data-ng-checked="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																					<#if (enumeratorMapItemVar?index >= 12)>
																						<#assign mostraTuttoCat = true />
																						<div class="flex-100" data-ng-show="showallarg">
																							<md-checkbox data-ng-checked="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																				</#list>
																			</#if>
																		</#if>
																	</#if>
																</#list>
																<#if (mostraTuttoCat)>
																	<a href="" data-ng-hide="showallarg" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
																	<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
																</#if>
															</div>
														</div>
													</div>
												</div>
											</#if>
											<#if (altriFiltri)>
												<div role="tabpanel" data-ng-class="opzioniTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="opzioniTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12 mt48">
															<div class="row">
															<@wp.freemarkerTemplateParameter var="userFilterOptionsVar" valueName="userFilterOptionsVar" removeOnEndTag=true >
																<@wp.fragment code="cagliari_widget_lista_multilayout_uf" escapeXml=false />
															</@wp.freemarkerTemplateParameter>
															</div>
														</div>
													</div>
												</div>
											</#if>
										</div>
									</div>
								</div>
							</div>
						</#if>
					</form>
				</div>
			</div>
		</div>
	</#if>
</div>',NULL,1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_widget_inevidenza_homepage','cittametro_widget_inevidenza_homepage','whitelabel','<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@wp.currentWidget param="config" configParam="contents" var="contenuti" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />

<#assign elementiMaxItem = 3 />
<#if (!contenuti??)><#assign contenuti = "" /></#if>

<div class="widget" data-ng-controller="FiltriController">
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', '''', '''', '''', '''', '''', '''', '''', ''${contenuti}'')"> 	
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem}">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">	
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
		
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} == 0" data-ng-repeat="elem in contents | startFrom:currentPage*contents.length | limitTo: contents.length">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == contents.length">
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
	</div>
	<#assign contentList="">
</div>',NULL,1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_widget_lista_multicontent','cagliari_widget_lista_multicontent','whitelabel','<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<#assign utenteToken = '''' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
	<#if (Session.currentUser.accessToken??)>
		<#assign utenteToken = Session.currentUser.accessToken />
	</#if>
</#if>

<@ca.widgetFrameNumber frameid="numeroFrame" />
<@cw.currentArgument var="currentArg" />
<@wp.currentWidget param="config" configParam="contentTypes" var="contentTypes" />
<@wp.currentWidget param="config" configParam="maxElements" var="elementiMax" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />
<@wp.currentWidget param="config" configParam="modelId" var="idModello" />
<@wp.currentWidget param="config" configParam="modelId2" var="idModello2" />
<@wp.currentWidget param="config" configParam="categories" var="categorie" />
<@wp.currentWidget param="config" configParam="orClauseCategoryFilter" var="orCategory" />
<@wp.currentWidget param="config" configParam="userFilters" var="filtri" />
<@wp.currentWidget param="config" configParam="filters" var="filters" />
<@wp.currentWidget param="config" configParam="layout" var="layout" />
<@wp.currentWidget param="config" configParam="title_it" var="titleVar" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@wp.categories var="systemCategories" root="arg_argomenti" />

	
<#if (!elementiMax??)><#assign elementiMax = 999 /></#if>
<#if (!elementiMaxItem??)><#assign elementiMaxItem = 999 /></#if>
<#if (!categorie??)><#assign categorie = "" /></#if>
<#if (!orCategory??)><#assign orCategory = "false" /></#if>
<#if (!filters??)><#assign filters = "" /></#if>
<#if (!idModello??)><#assign idModello = "list" /></#if>
<#if (!idModello2??)><#assign idModello2 = "list" /></#if>
<#if (currentArg??)>
	<#assign categorie = ''${currentArg},${categorie}'' />
<#else>
	<#assign currentArg = '''' />
</#if>

<#assign altriFiltri = false />
<#assign allCategories = false/>
<#assign numTipologie = 0 />
<#assign categoryIndex = 999 />
<#assign categoryEnumIndex = 999 />
<#assign tipologieIndex = 999 />
<#assign tipologiaName = '''' />
<#assign enumCategName = '''' />

<#assign contentTypes = contentTypes?keep_after("[")?keep_before("]")?split("},") />

<#assign contents = [] />
<#list contentTypes as contentType>
	<#assign contents = contents + [contentType?keep_after("{")?keep_before("}")] />
</#list>

<#assign tipiContenuto = "" />
<#list contents as content>
	<#assign typeCode = content?keep_after("contentType=")?keep_before(",")/>
	<#if (content?index == 0)>
		<#assign tipiContenuto += typeCode />
	<#else>
		<#assign tipiContenuto += '','' + typeCode />
	</#if>
</#list>

<#assign userFilters = [] />
<#list contents as content>
	<#list content?keep_after("userFilters=")?keep_before("}")?split("+") as userFilter>
		<#if (userFilter != '''')>
			<#assign userFilters = userFilters + [{''contentType'': content?keep_after("contentType=")?keep_before(","), ''filter'':userFilter}] />
		</#if>
	</#list>
</#list>

<div id="frame${numeroFrame}" class="widget" data-ng-controller="FiltriController">
    <div class="progress-spinner progress-spinner-active" data-ng-show="::false">
		<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_LOADING" />...</span>
	</div>
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', ''${elementiMax}'', ''${tipiContenuto}'', ''${idModello}'', ''${categorie}'', '' '', ''${orCategory}'', ''${filters}'', ''multi'', ''${utenteToken}'')"> 	
		<div class="row">
			<div class="col-md-12">
				<#if (userFilters?size > 0)>
					<div class="titolosezione filtrisezione">
						<h3 class="float-left">
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
						<#if (titleVar??)>
								
                            <#assign "different_all"= "CITTAMETRO_PORTAL_ALL" + titleVar?upper_case/>
                        <#else>
                            <#assign "different_all"= "CITTAMETRO_PORTAL_ALL" + paginaCorrente ?upper_case/>
						</#if>
                        

						<div class="filtro">
							<button id="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />${numeroFrame}" class="btn btn-default btn-trasparente active" data-ng-class="class_tutti()" title="<@wp.i18n key=different_all />"
								data-ng-click="toggleAll(); cleanSearchParameter(''all''); getMultiContents(''${tipiContenuto}'', getArgomentiList(), ''<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />${numeroFrame}'', ''${filters}'')">
								<@wp.i18n key=different_all />
							</button>
							<#assign numTipologie = 0/>
							<#list userFilters as userFilter>
								<#assign typeCode= userFilter.filter?keep_after("value=")?keep_before(";")/>
								<#assign typeKey= userFilter.filter?keep_after("key=")?keep_before(")")/>
								<#if (typeCode!= '''')>
									<@wp.pageInfo pageCode="${typeCode}" info="title" var="typeName" />	
									<#if (numTipologie == 0)><#assign tip1 = typeCode/></#if>
									<#if (numTipologie == 1)><#assign tip2 = typeCode/></#if>
									<button id="${typeCode}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.key == ''${typeCode}''? ''active'': ''''" title="${typeName}" data-ng-click="searchParameters.key != ''${typeCode}'' ? searchParameters = {type:''tipologia'', key:''${typeCode}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')" <#if (numTipologie == 2)>data-ng-hide="searchParameters.key != ''${typeCode}'' && searchParameters.key != ''${tip1}'' && searchParameters.key != ''${tip2}'' && searchParametersLength != 0"</#if> <#if (numTipologie > 2)>data-ng-if="searchParameters.key == ''${typeCode}''"</#if> >
										${typeName}
									</button>
									<#assign numTipologie ++/>
								<#else>
									<#if (typeKey?contains("contentType"))>
										<#if (numTipologie == 0)><#assign tip1 = userFilter.contentType/></#if>
										<#if (numTipologie == 1)><#assign tip2 = userFilter.contentType/></#if>
										<button id="${userFilter.contentType}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.key == ''${userFilter.contentType}''? ''active'': ''''" title="<@wp.i18n key=''CITTAMETRO_CONTENT_${userFilter.contentType}''/>" data-ng-click="searchParameters.key != ''${userFilter.contentType}'' ? searchParameters = {type:''contentType'', key:''${userFilter.contentType}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')" <#if (numTipologie == 2)> data-ng-hide="searchParameters.key != ''${userFilter.contentType}'' && searchParameters.key != ''${tip1}'' && searchParameters.key != ''${tip2}'' && searchParametersLength != 0"</#if> <#if (numTipologie > 2)>data-ng-if="searchParameters.key == ''${userFilter.contentType}''"</#if>>
											<@wp.i18n key="CITTAMETRO_CONTENT_${userFilter.contentType}"/>
										</button>
										<#assign numTipologie ++/>
									<#else>
										<#assign pageStart= ''homepage''/>
										<#if (userFilter.contentType== ''NVT'')><#assign pageStart= ''novita''/></#if>
										<#if (userFilter.contentType== ''EVN'')><#assign pageStart= ''nov_03''/></#if>
										<#if (userFilter.contentType== ''DOC'')><#assign pageStart= ''documenti''/></#if>
										<#if (userFilter.contentType== ''SRV'')><#assign pageStart= ''servizi''/></#if>
										<@wp.nav var="pagina" spec="code(${pageStart}).subtree(3)">
											<#if (pagina.level >= 1)>
												<#if (numTipologie == 0)><#assign tip1 = pagina.code/></#if>
												<#if (numTipologie == 1)><#assign tip2 = pagina.code/></#if>
												<button id="${pagina.code}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.key == ''${pagina.code}''? ''active'': ''''" title="${pagina.title}" data-ng-click="searchParameters.key != ''${pagina.code}'' ? searchParameters = {type:''tipologia'', key:''${pagina.code}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')" <#if (numTipologie == 2)>data-ng-hide="searchParameters.key != ''${pagina.code}'' && searchParameters.key != ''${tip1}'' && searchParameters.key != ''${tip2} && searchParametersLength != 0''"</#if> <#if (numTipologie > 2)>data-ng-if="searchParameters.key == ''${pagina.code}''"</#if>>
													${pagina.title}
												</button>
												<#assign numTipologie ++/>
											</#if>
										</@wp.nav>
									</#if>
								</#if>
							</#list>
							<#if (numTipologie > 3)>
								<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''categorie''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
							</#if>
						</div>
					</div>
				<#else>
					<div class="titolosezione">
						<h3>
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
					</div>
				</#if>
			</div>
		</div>
		
		<#if (layout == ''1'')>
			<div class="row row-eq-height widget-mono">
				<#list contents as content>
					<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
					<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
					<div class="col-lg-4 col-md-6 animation-if" data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents"></div>
				</#list>
			</div>
		</#if>
		<#if (layout == ''2'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<div class="col-md-4" data-ng-if="($index+1)%3 == 0 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
						<div class="col-md-4" data-ng-if="(($index+1)%3 == 0 || ($index+1)%3 == 2) && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-md-4" data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
					</#list>
				</div>
			</div>
		</#if>
		<#if (layout == ''3'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height widget-multi" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<div class="col-lg-4 col-md-6 mb16" data-ng-if="($index+1)%3 == 0 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-6 mb16" data-ng-if="(($index+1)%3 == 0 || ($index+1)%3 == 2) && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-6 mb16" data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
					</#list>
				</div>
			</div>
		</#if>
		<#if (layout == ''4'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height" data-ng-if="($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />						
						<div class="col-lg-4 col-md-12 1a" data-ng-if="contents.length >= 4 && contents[contents.indexOf(elem)-3].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-3], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 2a" data-ng-if="contents.length >= 3 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
					</#list>
						<div class="col-lg-4 col-md-12 3a" data-ng-if="contents.length >= 4">
							<#list contents as content>
								<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
								<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
								<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />	
								<div data-ng-if="contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello2}'']"></div>
								<div data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
							</#list>
						</div>
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 4a" data-ng-if="contents.length <= 3 && contents.length >= 2 && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1"
						data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 5a" data-ng-if="contents.length <= 2 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 6a" data-ng-if="contents.length == 3 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
					</#list>
				</div>
			</div>
		</#if>
		<#if (layout == ''5'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height" data-ng-if="($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 1b" data-ng-if="contents.length >= 5 && contents[contents.indexOf(elem)-4].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-4], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4]][''${idModello}'']"></div>
					</#list>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="contents.length >= 5">
						<#list contents as content>
							<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
							<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
							<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
							<div data-ng-if="contents[contents.indexOf(elem)-3].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-3], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3]][''${idModello2}'']"></div>						
							<div data-ng-if="contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello2}'']"></div>
						</#list>
					</div>
					<div class="col-lg-4 col-md-12 3b" data-ng-if="contents.length >= 5">
						<#list contents as content>
							<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
							<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
							<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
							<div data-ng-if="contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello2}'']"></div>
							<div data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
						</#list>
					</div>
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 5b" data-ng-if="contents.length == 4 && contents[contents.indexOf(elem)-3].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-3], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3]][''${idModello}'']"></div>
					</#list>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="contents.length == 4">
						<#list contents as content>
							<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
							<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
							<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
							<div data-ng-if="contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello2}'']"></div>
							<div data-ng-if="contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello2}'']"></div>
						</#list>
					</div>
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 6b" data-ng-if="contents.length == 4 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
						<div class="col-lg-4 col-md-12 7b" data-ng-if="contents.length == 3 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>					
						<div class="col-lg-4 col-md-12 8b" data-ng-if="contents.length <= 3 && contents.length >= 2 && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 9b" data-ng-if="contents.length <= 3 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>	
					</#list>	
				</div>
			</div>
		</#if>
		
		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && ${elementiMax}>${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" /><#if (titleVar??)> - ${titleVar}</#if>">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, currentPage); scrollTo(''frame${numeroFrame}'')" 
								title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}"
							data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, elem); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, elem); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage+1 == numberOfPages" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, currentPage+2); scrollTo(''frame${numeroFrame}'')"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		
		<div class="row row-eq-height" data-ng-if="contents.length == 0">
			<div class="col-md-12">
				<p><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
			</div>
		</div>
	</div>

	<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
		<div class="row">
			<div class="col-md-12 veditutti">
				<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde">${pageLinkDescriptionVar} <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
			</div>
		</div>
	</#if>
	<#assign contentList="">
	
	<#if (numTipologie > 3)>
		<div class="modal fade searchWidgetModal" id="searchWidgetModal${numeroFrame}" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle${numeroFrame}" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="ricerca-interna-${numeroFrame}">
						<div class="modal-header-fullsrc">
							<div class="container">
								<div class="row">
									<div class="col-sm-1" data-ng-class="{pb12: !General.getFiltriMode().filtri}">
										<button type="button" class="close" data-dismiss="modal" aria-label="Chiudi filtri di ricerca" data-ng-click="General.setFiltriMode(false, false)">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
									</div>
									<div class="col-sm-11">
										<h1 class="modal-title" id="searchModalTitle${numeroFrame}">
											<@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" />
										</h1>
										<button class="confirm btn btn-default btn-trasparente float-right" data-dismiss="modal" data-ng-click="General.setFiltriMode(false, false)">Conferma</button>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="search-filter-type ng-hide" data-ng-show="General.getFiltriMode().filtri">
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation">
													<a href="" aria-controls="categoriaTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''categorie'')" data-ng-class="categoriaTab"><@wp.i18n key="CITTAMETRO_CONTENT_TYPES" /></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="modal-body-search">
							<div class="container">
								<div role="tabpanel" data-ng-show="General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">
									<div class="tab-content">
										<div role="tabpanel" data-ng-class="categoriaTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="categoriaTab${numeroFrame}">
											<div class="row">
												<div class="offset-md-1 col-md-10 col-sm-12">
													<div class="search-filter-ckgroup">
														<#assign mostraTutto = false />
														<#assign numTipologie = 0/>
														<#list userFilters as userFilter>
															<#assign typeCode= userFilter.filter?keep_after("value=")?keep_before(";")/>
															<#assign typeKey= userFilter.filter?keep_after("key=")?keep_before(")")/>
															<#if (typeCode!= '''')>
																<@wp.pageInfo pageCode="${typeCode}" info="title" var="typeName" />		
																<#if (numTipologie <= 11)>
																	<div class="flex-100">
																		<md-checkbox data-ng-checked="searchParameters.key == ''${typeCode}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${typeCode}'' ? searchParameters = {type:''tipologia'', key:''${typeCode}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																		<label>${typeName}</label></md-checkbox>
																	</div>
																<#else>
																	<#assign mostraTutto = true />
																	<div class="flex-100" data-ng-show="showalltipol">
																		<md-checkbox data-ng-checked="searchParameters.key == ''${typeCode}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${typeCode}'' ? searchParameters = {type:''tipologia'', key:''${typeCode}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																		<label>${typeName}</label></md-checkbox>
																	</div>
																</#if>
																<#assign numTipologie ++/>
															<#else>
																<#if (typeKey?contains("contentType"))>
																	<#if (numTipologie <= 11)>
																		<div class="flex-100">
																			<md-checkbox data-ng-checked="searchParameters.key == ''${userFilter.contentType}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${userFilter.contentType}'' ? searchParameters = {type:''contentType'', key:''${userFilter.contentType}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																			<label><@wp.i18n key="CITTAMETRO_CONTENT_${userFilter.contentType}" /></label></md-checkbox>
																		</div>
																	<#else>
																		<#assign mostraTutto = true />
																		<div class="flex-100" data-ng-show="showalltipol">
																			<md-checkbox data-ng-checked="searchParameters.key == ''${userFilter.contentType}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${userFilter.contentType}'' ? searchParameters = {type:''contentType'', key:''${userFilter.contentType}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																			<label><@wp.i18n key="CITTAMETRO_CONTENT_${userFilter.contentType}" /></label></md-checkbox>
																		</div>
																	</#if>
																	<#assign numTipologie ++/>
																<#else>
																	<#assign pageStart= ''homepage''/>
																	<#if (userFilter.contentType== ''NVT'')><#assign pageStart= ''novita''/></#if>
																	<#if (userFilter.contentType== ''EVN'')><#assign pageStart= ''nov_03''/></#if>
																	<#if (userFilter.contentType== ''DOC'')><#assign pageStart= ''documenti''/></#if>
																	<#if (userFilter.contentType== ''SRV'')><#assign pageStart= ''servizi''/></#if>
																	<@wp.nav var="pagina" spec="code(${pageStart}).subtree(3)">
																		<#if (pagina.level >= 1)>
																			<#if (numTipologie <= 11)>
																				<div class="flex-100">
																					<md-checkbox data-ng-checked="searchParameters.key == ''${pagina.code}''" aria-label="${pagina.title}" data-ng-click="searchParameters.key != ''${pagina.code}'' ? searchParameters = {type:''tipologia'', key:''${pagina.code}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																					<label>${pagina.title}</label></md-checkbox>
																				</div>
																			<#else>
																				<#assign mostraTutto = true />
																				<div class="flex-100" data-ng-show="showalltipol">
																					<md-checkbox data-ng-checked="searchParameters.key == ''${pagina.code}''" aria-label="${pagina.title}" data-ng-click="searchParameters.key != ''${pagina.code}'' ? searchParameters = {type:''tipologia'', key:''${pagina.code}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																					<label>${pagina.title}</label></md-checkbox>
																				</div>
																			</#if>
																			<#assign numTipologie ++/>
																		</#if>
																	</@wp.nav>
																</#if>
															</#if>
														</#list>
														<#if (mostraTutto)>
															<a href="" data-ng-hide="showalltipol" data-ng-click="showalltipol=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
															<a href="" data-ng-show="showalltipol" data-ng-click="showalltipol=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
														</#if>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</#if>
</div>',NULL,1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_widget_gallerie_homepage','cittametro_widget_gallerie_homepage',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.min.css" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.min.css" />

<#assign js_slide="$(document).ready(function() {        
	var owl = $(''#owl-galleria-home'');
	owl.owlCarousel({
		nav:false,
		startPosition: 0,
		autoPlay:false,
		responsiveClass:true,
		responsive:{
				0:{
					items:1,
				},
				576: {
					items:1,
				},
				768: {
					items:2,
				},
				991: {
					items:3,
				},
			}
	});
});" />
<@wp.headInfo type="JS_CA_BTM_INC" info="${js_slide}" />

<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />
<section id="gallerie">
<div class="container">
	<div class="widget">
		<#if (titleVar??)>
			<div class="row">
				<div class="col-md-12">
					<div class="titolosezione">
						<h3>${titleVar}</h3>
					</div>
				</div>
			</div>
		</#if>
		<#if (contentInfoList??) && (contentInfoList?has_content) && (contentInfoList?size > 0)>
			<@wp.pager listName="contentInfoList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
				<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
					<div id="owl-galleria-home" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
						<#list contentInfoList as contentInfoVar>
							<#assign cid = contentInfoVar[''contentId'']>
							<#if (cid?contains("GAL"))>
								<@jacms.content contentId="${contentInfoVar[''contentId'']}" modelId="300002" />
							</#if>
							<#if (cid?contains("VID"))>
								<@jacms.content contentId="${contentInfoVar[''contentId'']}" modelId="190005" />
							</#if>
						</#list>
					</div>
				</@wp.freemarkerTemplateParameter>
			</@wp.pager>
		</#if>
		<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
			<div class="row">
				<div class="col-md-12 mt16">
					<div class="text-center">
						<#if (pageLinkVar??)>
							<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde mr0">${pageLinkDescriptionVar}</a>
						<#else>
							${titleVar}
						</#if>
					</div>
				</div>
			</div>
		</#if>
		<#assign contentInfoList="">
	</div>
</div>
</section>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_widget_argomenti_homepage','cittametro_widget_argomenti_homepage','whitelabel','<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<div class="widget">
<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />
	<div class="argomenti-foto">
		<div class="container">
			<#if (titleVar??)>
				<div class="row">
					<div class="col-md-12">
						<h3 class="titolosezione">
							${titleVar}
						</h3>
					</div>
				</div>
			</#if>
		</div>
	</div>
	<div class="container">
		<#if (contentInfoList??) && (contentInfoList?has_content) && (contentInfoList?size > 0)>
			<@wp.pager listName="contentInfoList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
				<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
				<#assign count= 0>
				<div class="row row-eq-height mt-128n">
					<#list contentInfoList as contentInfoVar>
						<#if (count < 3)>
							<div class="col-md-4">
								<#assign cid = contentInfoVar[''contentId'']>
								<#if (cid?contains("ARG"))>
									<#if (count < 3)>
										<@jacms.content contentId="${contentInfoVar[''contentId'']}" modelId="400003" />
									</#if>
									<#assign count = count + 1>
								</#if>
							</div>
						</#if>	
					</#list>
				</div>
				<div class="row">
					<div class="offset-md-2 col-md-8 offset-md-2 col-sm-12">
						<div class="argomenti altri-argomenti">
							<div class="altri-argomenti-titolo">
								<h4><@wp.i18n key="CITTAMETRO_PORTAL_ARGUMENTS_OTHER" /></h4>
							</div>
							<#assign count= 0>
							<div class="altri-argomenti-elenco">
								<#list contentInfoList as contentInfoVar>
									<#if (count > 2)>
										<#assign cid = contentInfoVar[''contentId'']>
										<#if (cid?contains("ARG"))>
											<@jacms.content contentId="${contentInfoVar[''contentId'']}" modelId="400001" />
											<#assign count = count + 1>
										</#if>
									</#if>
									<#assign count = count + 1>
								</#list>
								<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOARGUMENT" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="badge badge-pill badge-argomenti">${pageLinkDescriptionVar}</a>
								</#if>
							</div>
						</div>
					</div>
				</div>
				</@wp.freemarkerTemplateParameter>
			</@wp.pager>
		</#if>
	</div>
<#assign contentInfoList="">
</div>',NULL,1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_cerca_contenuti','cittametro_cerca_contenuti',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@wp.currentPage param="title" var="titolo" />
<@wp.currentPage param="code" var="page" />
<@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" />
<@cw.configWidget pageCode="${searchWidgetPage.code}" widgetCode="cagliari_widget_advsearch_results" configParam="contentTypesFilter" var="tipiContenuto" />

<form data-ng-cloak data-ng-controller="ctrlRicerca as ctrl" action="<@wp.url page="${searchWidgetPage.code}"/>" method="post">
	<div class="form-group" data-ng-init="ctrl.Ricerca.setContentTypes(''${tipiContenuto}'')">
		<label class="sr-only" for="cerca-intro"><@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /> ${titolo}</label>
		<input type="text" id="cerca-intro" name="cercatxt" class="form-control" data-ng-model="ctrl.Ricerca.searchStringRicercaTxt" placeholder="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /> ${titolo?lower_case}">
		<button type="submit" class="ico-sufix" value="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" />" title="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" />">
			<svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
		</button>
	</div>
	<div class="form-filtro">
		<div class="form-filtro-titoletto"><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></div>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_tutti()" data-ng-click="ctrl.Ricerca.toggleAll()"><@wp.i18n key="CITTAMETRO_PORTAL_ALL" /></a>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_ammins_selected(), ctrl.Ricerca.get_ammins_chk())" data-ng-click="ctrl.Ricerca.ammins_toggleAll()" data-ng-if="ctrl.Ricerca.ammins_isChecked()"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-account_balance"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" /></a>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_servizi_selected(), ctrl.Ricerca.get_servizi_chk())" data-ng-click="ctrl.Ricerca.servizi_toggleAll()" data-ng-if="ctrl.Ricerca.servizi_isChecked()"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-settings"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></a>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_novita_selected(), ctrl.Ricerca.get_novita_chk())" data-ng-click="ctrl.Ricerca.novita_toggleAll()" data-ng-if="ctrl.Ricerca.novita_isChecked()"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" /></a>
		<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_docs_selected(), ctrl.Ricerca.get_docs_chk())" data-ng-click="ctrl.Ricerca.docs_toggleAll()" data-ng-if="ctrl.Ricerca.docs_isChecked()"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-description"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" /></a>
		<div data-ng-if="!ctrl.Ricerca.ammins_isChecked()" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_ammins_selected())" data-ng-repeat="item in ctrl.Ricerca.get_ammins_chk()" class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_ammins_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> {{ item.name }}"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="!ctrl.Ricerca.servizi_isChecked()" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-repeat="item in ctrl.Ricerca.get_servizi_chk()" class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_servizi_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> {{ item.name }}"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="!ctrl.Ricerca.novita_isChecked()" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_novita_selected())" data-ng-repeat="item in ctrl.Ricerca.get_novita_chk()" class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_novita_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> {{ item.name }}"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="!ctrl.Ricerca.docs_isChecked()" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_docs_selected())" data-ng-repeat="item in ctrl.Ricerca.get_docs_chk()" class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_docs_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> {{ item.name }}"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>		
		<div data-ng-if="ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()" 
			class="btn-badge active">
			<span data-ng-bind-html="item.name">-</span>
			<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_args_selected())" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" />  {{ item.name }}">
				<svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg>
			</a>
		</div>
		<div data-ng-if="ctrl.Ricerca.activeChk" class="btn-badge active">
			<@wp.i18n key="CITTAMETRO_CONTENT_ACTIVE_CONTENT" /> <a href="" data-ng-click="ctrl.Ricerca.activeChk = false" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="btn-badge active">
			<@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<@wp.i18n key="CITTAMETRO_PORTAL_TO" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> <@wp.i18n key="CITTAMETRO_PORTAL_FROM" /> <@wp.i18n key="CITTAMETRO_PORTAL_TO" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="ctrl.Ricerca.dataInizio && !ctrl.Ricerca.dataFine" class="btn-badge active">
			<@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" /> <@wp.i18n key="CITTAMETRO_PORTAL_START_DATE" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<div data-ng-if="!ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="btn-badge active">
			<@wp.i18n key="CITTAMETRO_PORTAL_UNTIL" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close" title="<@wp.i18n key="CITTAMETRO_PORTAL_DELETE_FILTER" />" <@wp.i18n key="CITTAMETRO_PORTAL_END_DATE" />><svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
		</div>
		<a href="" class="btn btn-default btn-trasparente" data-ng-click="ctrl.Ricerca.setActive(); ctrl.General.setFiltriMode(true, true, ''${page}'')" aria-label="<@wp.i18n key="SEARCH_FILTERS_BUTTON" />" data-toggle="modal" data-target="#searchModal">...</a>
	</div>
	<input type="hidden" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">
	<input type="hidden" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">
	<input type="hidden" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">
	<input type="hidden" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">
	<input type="hidden" name="argomenti" value="{{ctrl.Ricerca.get_args_selected()}}">
	<input type="hidden" name="attivi" value="{{ctrl.Ricerca.activeChk}}">
	<input type="hidden" name="inizio" value="{{ctrl.Ricerca.dataInizio}}">
	<input type="hidden" name="fine" value="{{ctrl.Ricerca.dataFine}}">
</form>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_footer','cittametro_footer',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<#assign "livelloPrecedente" = -1 />
<#assign "npagine" = 0 />
<#assign "paginapadre" = ""/>

<@wp.nav var="pagina">

<#assign "livello" = pagina.level/>
<#if livello == 0>
  <#if -1 < livelloPrecedente >
    </ul>
    </div>
  </#if>

  <div class="col-lg-3 col-md-3 col-sm-6">
    <h4>
      <#if pagina.code == "amministrazione" > <#assign icona="account_balance"/> </#if>
      <#if pagina.code == "servizi" > <#assign icona="settings"/> </#if>
      <#if pagina.code == "novita" > <#assign icona="event"/> </#if>
      <#if pagina.code == "documenti" > <#assign icona="description"/> </#if>
      <!--
      <a href="${pagina.url}" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />:  ${pagina.title}"> ${pagina.title} </a>
      -->
      <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-${icona}"></use></svg>${pagina.title}
      <#assign paginapadre = pagina.title/>
    </h4>
    <ul class="footer-list clearfix">
  </#if>
    <#if livello gt 0 >
    <li> 
      <a href="${pagina.url}" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${paginapadre}" > ${pagina.title}</a>
    </li>
      <#assign npagine = npagine + 1 />
    </#if>
      <#assign livelloPrecedente = livello />
    </@wp.nav>
    <#if npagine gt 0 >
    </ul>
  </div>
</#if>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_head',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>

<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>
<#assign jpseo=JspTaglibs["/jpseo-aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign jacms=JspTaglibs["/jacms-aps-core"]>

 	<html lang="<@wp.info key="currentLang" />">
 	<head>
		<base href="<@wp.info key="systemParam" paramName="applicationBaseURL" />">
 		<!--[if IE]><script nonce="<@wp.cspNonce />" type="text/javascript" >
 			(function() {
 				var baseTag = document.getElementsByTagName(''base'')[0];
 				baseTag.href = baseTag.href;
 			})();
 		</script><![endif]-->
		<@wp.i18n key="CITTAMETRO_PORTAL_TITLE" var="siteName"/>
  		<@wp.currentPage param="code" var="pageCode" />    
  		<@wp.currentPage param="title" var="pageTitle" />

		<@ca.cagliariPageInfoTag pageCode="${pageCode}" info="isPublishOnFly" var="isPublishOnFly"/>
		<#if (isPublishOnFly == "true")>
			<@ca.cagliariMainFrameContentTag var="content"/>
			<#if content??>
				<#if (content.getAttributeByRole(''jpsocial:title''))??>
					<#assign title=content.getAttributeByRole(''jpsocial:title'').text>
					<#if title??>
						<#assign pageTitle = title>
					</#if>
				</#if>
				<#if (content.getAttributeByRole(''jpsocial:description''))??>
					<#assign description=content.getAttributeByRole(''jpsocial:description'').text>    
				</#if>
				<#if (content.getAttributeByRole(''jpsocial:image''))??>
					<#assign image=content.getAttributeByRole(''jpsocial:image'').getImagePath("4")>
				</#if>
			</#if>
		</#if>

		<#if (pageCode == "homepage")>
			<title>${siteName}</title>
		<#else>
			<#if (pageCode == "arg_01_dett")>
				<@cw.currentArgument var="currentArg" />
				<#if (currentArg??)>
					<@cw.contentList listName="contentList" contentType="ARG" category="${currentArg}"/>
					<#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
						<#list contentList as contentId>
							<title>${siteName} | <@jacms.content contentId="${contentId}" modelId="400012" /></title>
							<#assign description><@jacms.content contentId="${contentId}" modelId="400013" /></#assign>
						</#list>
					</#if>
					<#assign contentList="">
				</#if>
			<#else>
				<title>${siteName} | ${pageTitle}</title>
			</#if>
		</#if>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

 		<@jpseo.currentPage param="description" var="metaDescr" />
 		<#if metaDescr?? && metaDescr?has_content>
			<meta name="description" content="${metaDescr}" />
		<#else>
			<#if description??>
				<meta name="description" content="${description}" />
			<#else>
				<@ca.cagliariMainFrameContentTag var="content"/>  <#--   DA CAMBIARE -->
				<#if content??>
					<#assign description=content.getAttributeByRole(''jacms:description'').text>
					<meta name="description" content="${description}" />
				</#if>
			</#if>
		</#if>
		<@jpseo.currentPage param="keywords" var="metaKeyword" />
		<#if metaKeyword??>
			<meta name="keywords" content="${metaKeyword}" />
		</#if>

 		<link rel="apple-touch-icon" sizes="57x57" href="<@wp.imgURL />apple-icon-57x57.png" />
 		<link rel="apple-touch-icon" sizes="60x60" href="<@wp.imgURL />apple-icon-60x60.png" />
 		<link rel="apple-touch-icon" sizes="72x72" href="<@wp.imgURL />apple-icon-72x72.png" />
 		<link rel="apple-touch-icon" sizes="76x76" href="<@wp.imgURL />apple-icon-76x76.png" />
 		<link rel="apple-touch-icon" sizes="114x114" href="<@wp.imgURL />apple-icon-114x114.png" />
 		<link rel="apple-touch-icon" sizes="120x120" href="<@wp.imgURL />apple-icon-120x120.png" />
 		<link rel="apple-touch-icon" sizes="144x144" href="<@wp.imgURL />apple-icon-144x144.png" />
 		<link rel="apple-touch-icon" sizes="152x152" href="<@wp.imgURL />apple-icon-152x152.png" />
 		<link rel="apple-touch-icon" sizes="180x180" href="<@wp.imgURL />apple-icon-180x180.png" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-32x32.png" sizes="32x32" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />android-chrome-192x192.png" sizes="192x192" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-96x96.png" sizes="96x96" />
 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-16x16.png" sizes="16x16" />





 <!--   ##########BUONO############   -->
<meta charset="utf-8" />
      	<title><@wp.currentPage param="title" /></title>
      	<meta name="viewport" content="width=device-width,  user-scalable=no" />
 		<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery-3.3.1.min.js" ></script>        
		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/angular-material.cagliari.css"  />         
		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/angular-material.css"  />  
		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/font/fonts.min.css" />
        <link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/bootstrap-italia_1.2.0.min.css" />

 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/cittametro.css" rel="stylesheet" type="text/css" />
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/areapersonale.css" rel="stylesheet" type="text/css" />
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/cagliari-print.css" rel="stylesheet" type="text/css" media="print" />
 		<@wp.outputHeadInfo type="CSS_CA_INT">
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_EXT">
 		<link href="<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_PLGN">
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 
 		<@wp.outputHeadInfo type="JS_CA_TOP_EXT">nonce="<@wp.cspNonce />" src="<@wp.printHeadInfo />" ></script></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="JS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="JS_CA_TOP"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/<@wp.printHeadInfo />" ></script></@wp.outputHeadInfo>


  		<@wp.i18n key="CAGLIARI_PORTAL_TITLE" var="siteName"/>
  		<@wp.currentPage param="code" var="pageCode" />    
  		<@wp.currentPage param="title" var="pageTitle" />
		<#if pageCode?? && pageCode != "">
		<#else>
			<#assign pageCode=''homepage''>
		</#if>
  		<#if pageCode == ''homepage''>
  		 	<#assign pageTitle=siteName>
  		</#if>
     
  		<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" var="description"/>
        <#--
  		<@wp.info key="systemParam" paramName="applicationBaseURL" var="baseURL"/>
		<#assign baseURL = baseURL?substring(0,baseURL?last_index_of("/portale"))>
		 FACEBOOK 
		<meta property="og:site_name" content="${siteName}" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="${pageTitle}" />
		<#if description??>
			<meta property="og:description" content="${description}"/>
		<#else>
			<meta property="og:description" content="-"/>
		</#if>
		<#if image?? && image != "">
			<meta property="og:image" content="${baseURL}${image}" />
		<#else>
			<meta property="og:image" content="${baseURL}<@wp.imgURL/>logo_cagliari_print.png" />
		</#if>
		<meta property="fb:app_id" content="409032892503811" />

		 TWITTER 
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:site" content="@Comune_Cagliari" />
		<meta name="twitter:title" content="${pageTitle}" />
		<#if description??>
			<meta name="twitter:description" content="${description}"/>
		<#else>
			<meta name="twitter:description" content="-"/>
		</#if>    
		<#if image?? && image != "">
			<meta name="twitter:image" content="${baseURL}${image}" />
		<#else>
			<meta name="twitter:image" content="${baseURL}<@wp.resourceURL />cittametro-homepage-bundle/static/img/logo_cittametro_cagliari.png" />
		</#if>
		<@wp.fragment code="cagliari_template_googletagmanager" escapeXml=false />
        
		-->
 	</head>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_backtotop',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>  
 
<div id="topcontrol" class="topcontrol bg-bluscuro" title="<@wp.i18n key="CITTAMETRO_PORTAL_BACKTOTOP" />">
 	<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_pulsanti_gestione','cittametro_pulsanti_gestione',NULL,'<#assign wp=JspTaglibs["/aps-core"]> <#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/metroca-core.tld"]>
<@wp.currentPage param="code" var="paginaCorrente" />
<@wp.freemarkerTemplateParameter var="paginaCorrente" valueName="paginaCorrente" />
<@uc.metrocaPageInfoTag pageCode="${paginaCorrente}" info="pathArray" var="pathCorrente"/> 

<@wp.nav var="pagina"> 
<#assign aperto="false"/>

<#list pathCorrente as pgitem > 
<#if pgitem == pagina.code> 
<#assign aperto="true"/>
 </#if>
</#list>

<#if aperto=="true">
<a class="btn btn-default btn-trasparente active" href="${pagina.url}" title="${pagina.title}"> ${pagina.title} </a> 

<#else>
<a class="btn btn-default btn-trasparente" href="${pagina.url}" title="${pagina.title}"> ${pagina.title} </a> 

</#if>

</@wp.nav>
</ul>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('ilpatto_stats_stato','ilpatto_stats_stato',NULL,'<#assign wp=JspTaglibs[ "/aps-core"]>

<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react@17.0.1/umd/react.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react-dom@17.0.1/umd/react-dom.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />ilpatto-bundle/js/widgets/ilpatto-microfrontends/build.js" ></script>

<ilpatto-service-url value="http://metroca.mshome.net/cmca/ilpatto-microservice/0-1-3-snapshot" />
<ilpatto-statistiche-progetti-stato ></ilpatto-statistiche-progetti-stato>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_skiplink',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>    
	
<!--[if lt IE 8]>
	<p class="browserupgrade"><@wp.i18n key="CAGLIARI_PORTAL_BROWSERUPDATE" escapeXml=false /></p>
<![endif]-->
<div class="skiplink sr-only">
	<ul>
		<li><a accesskey="2" href="<@wp.url />#main_container"><@wp.i18n key="CITTAMETRO_PORTAL_SKIPLINK_GOCONTENT" /></a></li>
		<li><a accesskey="3" href="<@wp.url />#menup"><@wp.i18n key="CITTAMETRO_PORTAL_SKIPLINK_GONAVIGATION" /></a></li>
		<li><a accesskey="4" href="<@wp.url />#footer"><@wp.i18n key="CITTAMETRO_PORTAL_SKIPLINK_GOFOOTER" /></a></li>
	</ul>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_widget_inevidenza','cittametro_widget_inevidenza','whitelabel','<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<@wp.currentWidget param="config" configParam="contents" var="contenuti" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />

<#if (!elementiMaxItem??)><#assign elementiMaxItem = 0 /></#if>
<#if (!contenuti??)><#assign contenuti = "" /></#if>

<#assign utenteToken = '''' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
<#if (Session.currentUser.accessToken??)>
<#assign utenteToken = Session.currentUser.accessToken />
</#if>
</#if>

<@ca.widgetFrameNumber frameid="numeroFrame" />

<div id="frame${numeroFrame}" class="widget" data-ng-controller="FiltriController">
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', '''', '''', '''', '''', '''', '''', '''', ''${contenuti}'',''${utenteToken}'')"> 
		<#if (titleVar??)>
			<div class="row">
				<div class="col-md-12">
					<div class="titolosezione">
						<h3>
							<#if (pageLinkVar??)>
								<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
							<#else>
								${titleVar}
							</#if>
						</h3>
					</div>
				</div>
			</div>
		</#if>
		
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem}">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">	
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
		
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} == 0" data-ng-repeat="elem in contents | startFrom:currentPage*contents.length | limitTo: contents.length">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == contents.length">
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
		
		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && contents.length > ${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" />">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="goToPage(currentPage-1); scrollTo(''frame${numeroFrame}'')" 
								title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}"
							data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage >= contents.length/pageSize - 1" data-ng-click="goToPage(currentPage+1); scrollTo(''frame${numeroFrame}'')"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>

	<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
		<div class="row">
			<div class="col-md-12 veditutti">
				<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde">${pageLinkDescriptionVar} <svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-arrow_forward"></use></svg></a>
			</div>
		</div>
	</#if>
	<#assign contentList="">
</div>',NULL,1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_navbar_orizzontale','cittametro_navbar_orizzontale',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/metroca-core.tld"]>

<@wp.currentPage param="code" var="paginaCorrente" />
<@wp.freemarkerTemplateParameter var="paginaCorrente" valueName="paginaCorrente" />
<@uc.metrocaPageInfoTag pageCode="${paginaCorrente}" info="pathArray" var="pathCorrente"/> 
<@wp.nav var="pagina">
 <#assign aperto="false"/>
 <#list pathCorrente as pgitem > 
<#if pgitem == pagina.code> 
<#assign aperto="true"/>
 </#if>
 </#list>
<li <#if aperto=="true">class="active"</#if> > 
<a href="${pagina.url}" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />:  ${pagina.title}"> ${pagina.title} </a> 
</li>
</@wp.nav>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_footer_intestazione',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>  
 
<section class="footer-logo">
 	<div class="row clearfix">
 		<div class="col-sm-12 intestazione">
 			<div class="logoimg">
 				<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />">
 					<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/logo_cittametro_cagliari.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT	" />"/>
 				</a>
 			</div>
 			<div class="logotxt">
 				<h3>
 					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" /> <@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" /></a>
 				</h3>
 			</div>
 		</div>
 	</div>
</section>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_footer_link',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>  

<section class="postFooter clearfix">
	<div class="link_piede_footer">
		<h3 class="sr-only"><@wp.i18n key="CITTAMETRO_FOOTER_LINKUTILI_TITLE" /></h3>
		<a href="<@wp.url page="privacy" />" title="<@wp.pageInfo pageCode="privacy" info="title" />" lang="en"><@wp.pageInfo pageCode="privacy" info="title" /></a> | 
		<a href="<@wp.url page="note_legali" />" title="<@wp.pageInfo pageCode="note_legali" info="title" />"><@wp.pageInfo pageCode="note_legali" info="title" /></a> | 
		<a aria-label="<@wp.i18n key="CITTAMETRO_DICH_ACCESSIBILITA_ARIA" />" title="<@wp.i18n key="CITTAMETRO_DICH_ACCESSIBILITA_TEXT_TITLE" />"  href="https://form.agid.gov.it/view/32e1145b-80b3-4895-9a2c-5bf70b406ceb" target="_blank"> <@wp.i18n key="CITTAMETRO_DICH_ACCESSIBILITA_TEXT" escapeXml=false /></a> | 
		<a href="<@wp.url page="redazione" />" title="<@wp.pageInfo pageCode="redazione" info="title" />"><@wp.pageInfo pageCode="redazione" info="title" /></a>
	</div>
	<div class="loghi_progetto">
		<div class="lista_loghi_progetto">
			<span class="loghi_progetto_img logo_progetto_1"><a target="blank" aria-label="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_PONMETRO_ARIA" />" href="http://www.ponmetro.it/home/ecosistema/viaggio-nei-cantieri-pon-metro/pon-metro-cagliari/" title="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_PONMETRO" />"><img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/footer_ponmetro.svg" alt="<@wp.i18n key="CITTAMETRO_FOOTER_LOGO_PONMETRO" />" /></a></span>
			<span class="loghi_progetto_img logo_progetto_2"><a target="blank" aria-label="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_REPUBBLICA_ARIA" />" href="http://www.agenziacoesione.gov.it" title="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_REPUBBLICA" />"><img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/footer_repubblica.svg" alt="<@wp.i18n key="CITTAMETRO_FOOTER_LOGO_REPUBBLICA" />" /></a></span>
			<span class="loghi_progetto_img logo_progetto_3"><a target="blank" aria-label="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_UE_ARIA" />" href="https://europa.eu/european-union/index_it" title="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_UE" />"><img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/footer_ue.svg" alt="<@wp.i18n key="CITTAMETRO_FOOTER_LOGO_UE" />" /></a></span>
		</div> 
		<div class="testo_progetto">
			<p><@wp.i18n key="CITTAMETRO_FOOTER_TXT_PROGETTO" /></p>
		</div>
	</div> 
</section>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_search_modal',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@wp.categories var="systemCategories" root="arg_argomenti" />
<@wp.currentPage param="code" var="code" />
<@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" />
<@cw.configWidget pageCode="${searchWidgetPage.code}" widgetCode="cagliari_widget_advsearch_results" configParam="contentTypesFilter" var="tipiContenuto" />
<#if (!tipiContenuto??)><#assign tipiContenuto = ""/></#if>

<div id="dvRicerca">
	<div data-ng-cloak data-ng-controller="ctrlRicerca as ctrl">
		<!-- CERCA -->
		<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle" aria-hidden="false">
			<script nonce="<@wp.cspNonce />">
				$(function() {
				  $("#datepicker_start").datepicker();
				  $("#datepicker_end").datepicker();
				});
			</script>
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="ricerca" action="<@wp.url page="${searchWidgetPage.code}"/>" method="post">
						<div class="modal-header-fullsrc">
							<div class="container">
								<div class="row">
									<div class="col-sm-1" data-ng-class="{pb12: !ctrl.General.getFiltriMode().filtri}">
										<button data-ng-hide="ctrl.General.getFiltriMode().filtri && !ctrl.General.getFiltriMode().internal" type="button" class="close" data-dismiss="modal" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_CLOSE_SEARCH_FILTERS" />" data-ng-click="ctrl.General.setFiltriMode(false, false)">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
										<button data-ng-show="ctrl.General.getFiltriMode().filtri && !ctrl.General.getFiltriMode().internal" type="button" class="close" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_CLOSE_SEARCH_FILTERS" />" data-ng-click="ctrl.General.setFiltriMode(false, false)">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
									</div>
									<div class="col-sm-11">
										<h1 class="modal-title" id="searchModalTitle">
											<span data-ng-hide="ctrl.General.getFiltriMode().filtri"><@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /></span>
											<span data-ng-show="ctrl.General.getFiltriMode().filtri"><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></span>
										</h1>
										<button data-ng-show="ctrl.General.getFiltriMode().filtri" class="confirm btn btn-default btn-trasparente float-right"><@wp.i18n key="CITTAMETRO_PORTAL_CONFIRM" /></button>
									</div>
								</div>
							</div>
							<div class="search-filter-type ng-hide" data-ng-show="ctrl.General.getFiltriMode().filtri">
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation">
										<a data-ng-if="ctrl.General.getFiltriMode().categoria == ''''" href="" aria-controls="categoriaTab" role="tab" data-toggle="tab" data-ng-click="ctrl.Ricerca.setActive(''categorie'')" data-ng-class="ctrl.Ricerca.get_categoriaTab()"><@wp.i18n key="CITTAMETRO_PORTAL_CATEGORIES" /></a>
										<a data-ng-if="ctrl.General.getFiltriMode().categoria != ''''" href="" aria-controls="categoriaTab" role="tab" data-toggle="tab" data-ng-click="ctrl.Ricerca.setActive(''categorie'')" data-ng-class="ctrl.Ricerca.get_categoriaTab()">{{ctrl.General.getFiltriMode().categoria}}</a>
									</li>
									<li role="presentation"><a href="" aria-controls="argomentoTab" role="tab" data-toggle="tab"
										data-ng-click="ctrl.Ricerca.setActive(''argomenti'')" data-ng-class="ctrl.Ricerca.get_argomentoTab()"><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></a></li>
									<li role="presentation"><a href="" aria-controls="opzioniTab" role="tab" data-toggle="tab"
										data-ng-click="ctrl.Ricerca.setActive(''opzioni'')" data-ng-class="ctrl.Ricerca.get_opzioniTab()"><@wp.i18n key="CITTAMETRO_CONTENT_OPTIONS" /></a></li>
								</ul>
							</div>
						</div>
						<div class="modal-body-search">
							<div class="container">
								<div class="row" data-ng-hide="ctrl.General.getFiltriMode().filtri">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="form-group" data-ng-init="ctrl.Ricerca.setContentTypes(''${tipiContenuto}'')">
											<label for="cerca-txt" class="sr-only active"><@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /></label>
											<md-autocomplete type="text" md-input-name ="cercatxt" md-input-id ="cerca-txt" placeholder="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" />" md-selected-item="ctrl.Ricerca.cercatxt"
												md-search-text="ctrl.Ricerca.searchStringRicercaTxt" md-selected-item-change="ctrl.Ricerca.selectedRicercaTxtItemChanged(item)" md-items="item in ctrl.Ricerca.getAutocompleteResults(ctrl.Ricerca.searchStringRicercaTxt)" md-item-text="item.contentName"	md-min-length="1" md-clear-button="true">
												<md-item-template>
													<div>
														<a data-ng-href="{{item.href}}" data-ng-if="!item.searchButton && item.searchButton != ''hidden''">
															<div>
																<svg class="icon icon-sm" data-ng-bind-html="item.icon"></svg>
																<span class="autocomplete-list-text">
																	<span data-md-highlight-text="ctrl.Ricerca.searchStringRicercaTxt" data-md-highlight-flags="i">{{item.contentName}}</span>
																	<em>{{item.category}}</em>
																</span>
															</div>
														</a>
														<div class="search-start" data-ng-if="item.searchButton && item.searchButton != ''hidden''" data-ng-click="item.contentName = ctrl.Ricerca.searchStringRicercaTxt" onclick="document.getElementById(''search-button'').click();">
															<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
															<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_THROUGHOUT_SITE" />
														</div>
														<div class="search-start-hidden" data-ng-if="item.searchButton == ''hidden''"> </div>
													</div>
												</md-item-template>
											</md-autocomplete>
											<svg class="icon ico-prefix"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
										</div>
										<div data-ng-hide="ctrl.Ricerca.getAutocompleteIsOpened()">
											<div class="form-filtro">
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_tutti(''categorie'')" data-ng-click="ctrl.Ricerca.toggleAll(''categorie'')"><@wp.i18n key="CITTAMETRO_PORTAL_ALL" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_ammins_selected(), ctrl.Ricerca.get_ammins_chk())" data-ng-click="ctrl.Ricerca.ammins_toggleAll()"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-account_balance"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_servizi_selected(), ctrl.Ricerca.get_servizi_chk())" data-ng-click="ctrl.Ricerca.servizi_toggleAll()"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-settings"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_novita_selected(), ctrl.Ricerca.get_novita_chk())" data-ng-click="ctrl.Ricerca.novita_toggleAll()"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_docs_selected(), ctrl.Ricerca.get_docs_chk())" data-ng-click="ctrl.Ricerca.docs_toggleAll()"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-description"></use></svg> <@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" /></a>
												<a href="" class="btn btn-default btn-trasparente" data-ng-click="ctrl.Ricerca.setActive(); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_FILTER_BY" />&nbsp;<@wp.i18n key="CITTAMETRO_PORTAL_CATEGORIES" />">...</a>
											</div>
											<div class="form-blockopz">
												<h2><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></h2>
												<a href="" data-ng-class="(ctrl.Ricerca.get_args_selected().length > 0 && ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length)? ''badge badge-pill badge-argomenti'' : ''badge badge-pill badge-argomenti active''" data-ng-click="ctrl.Ricerca.toggleAll(''argomenti'')">
													<@wp.i18n key="CITTAMETRO_CONTENT_ALLARGUMENTS" />
												</a>
												<div data-ng-if="ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()" class="badge badge-pill badge-argomenti active">
													<span data-ng-bind-html="item.name">-</span>
													<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_args_selected())" class="btn-badge-close">
														<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg>
													</a>
												</div>
												<a href="" class="badge badge-pill badge-argomenti puntini" data-ng-click="ctrl.Ricerca.setActive(''argomenti''); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_FILTER_BY" />&nbsp;<@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" />">...</a>
											</div>
											<div class="form-blockopz">
												<h2><@wp.i18n key="CITTAMETRO_CONTENT_OPTIONS" /></h2>
												<a href="" data-ng-class="(ctrl.Ricerca.activeChk || ctrl.Ricerca.dataInizio || ctrl.Ricerca.dataFine)? ''badge badge-pill badge-argomenti'' : ''badge badge-pill badge-argomenti active''" data-ng-click="ctrl.Ricerca.activeChk = false; ctrl.Ricerca.clean_date()">
													<@wp.i18n key="CITTAMETRO_CONTENT_ALLOPTIONS" />
												</a>
												<div data-ng-if="ctrl.Ricerca.activeChk" class="badge badge-pill badge-argomenti active">
													<@wp.i18n key="CITTAMETRO_CONTENT_ACTIVE_CONTENT" />&nbsp;<a href="" data-ng-click="ctrl.Ricerca.activeChk = false" class="btn-badge-close"><svg class="icon">
													<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
												</div>
												<div data-ng-if="ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">
													<@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<@wp.i18n key="CITTAMETRO_PORTAL_TO" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
												</div>
												<div data-ng-if="ctrl.Ricerca.dataInizio && !ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">
													<@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
												</div>
												<div data-ng-if="!ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">
													<@wp.i18n key="CITTAMETRO_PORTAL_UNTIL" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-clear"></use></svg></a>
												</div>
												<a href="" class="badge badge-pill badge-argomenti puntini" data-ng-click="ctrl.Ricerca.setActive(''opzioni''); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="SEARCH_FILTERS_BUTTON" />">...</a>
											</div>
											<button id="search-button" class="search-start" >
												<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
												<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_THROUGHOUT_SITE" />
											</button>
										</div>
										<input type="hidden" id="amministrazione" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">
										<input type="hidden" id="servizi" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">
										<input type="hidden" id="novita" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">
										<input type="hidden" id="documenti" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">
										<input type="hidden" id="argomenti" name="argomenti" value="{{ctrl.Ricerca.get_args_selected()}}">
										<input type="hidden" id="attivi" name="attivi" value="{{ctrl.Ricerca.activeChk}}">
										<input type="hidden" id="inizio" name="inizio" value="{{ctrl.Ricerca.dataInizio}}">
										<input type="hidden" id="fine" name="fine" value="{{ctrl.Ricerca.dataFine}}">
									</div>
								</div>
								
								<#list systemCategories?sort_by("value") as systemCategory>
									<data-ng-container data-ng-init="ctrl.Ricerca.setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>
								</#list>
								<@wp.nav var="pagina" spec="code(homepage).subtree(4)">
									<#if (pagina.level == 1)>
										<#assign padre = pagina.code />
									</#if>
									<#if (pagina.level == 2)>	
										<#assign padre2 = pagina.code />
										<#assign categoria = pagina.title />
										<#assign categoriaCode = pagina.code />
										<data-ng-container data-ng-init="ctrl.Ricerca.setCategorie(''${categoria?replace("''", "\\''")}'', ''${categoriaCode}'', ''${padre}'')"></data-ng-container>
									</#if>
									<#if (pagina.level > 2)>
										<#assign categoria = pagina.title />
										<#assign categoriaCode = pagina.code />
										<data-ng-container data-ng-init="ctrl.Ricerca.setCategorie(''${categoria?replace("''", "\\''")}'', ''${categoriaCode}'', ''${padre}'', ''${padre2}'')"></data-ng-container>
									</#if>
								</@wp.nav>
								
								<#if (code == ''amministrazione'')><data-ng-container data-ng-init="ctrl.Ricerca.ammins_toggleAll()"></data-ng-container></#if>
								<#if (code == ''servizi'')><data-ng-container data-ng-init="ctrl.Ricerca.servizi_toggleAll()"></data-ng-container></#if>
								<#if (code == ''novita'')><data-ng-container data-ng-init="ctrl.Ricerca.novita_toggleAll()"></data-ng-container></#if>
								<#if (code == ''documenti'')><data-ng-container data-ng-init="ctrl.Ricerca.docs_toggleAll()"></data-ng-container></#if>
								
								<div role="tabpanel" data-ng-show="ctrl.General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">
									<div class="tab-content">
										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_categoriaTab() == ''active''? ''tab-pane active'' : ''tab-pane''" id="categoriaTab" data-ng-init="ctrl.Ricerca.chkCategoryCode(''${code}'')">
											<div class="row">
												<div class="offset-md-1 col-md-10 col-sm-12">
													<div class="search-filter-ckgroup">
														<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" />"
															data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"
															data-ng-checked="ctrl.Ricerca.ammins_isChecked()"
															md-indeterminate="ctrl.Ricerca.ammins_isIndeterminate()"
															data-ng-click="ctrl.Ricerca.ammins_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" /></label>
														</md-checkbox>
														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_ammins_chk()"
															data-ng-if="ctrl.General.getFiltriMode().categoria == ''Amministrazione'' || ctrl.General.getFiltriMode().categoria == ''''">
															<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_ammins_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_ammins_selected())" 	aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label>
															</md-checkbox>
														</div>
													</div>
													<div class="search-filter-ckgroup">
														<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" />"
															data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"
															data-ng-checked="ctrl.Ricerca.servizi_isChecked()"
															md-indeterminate="ctrl.Ricerca.servizi_isIndeterminate()"
															data-ng-click="ctrl.Ricerca.servizi_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></label>
														</md-checkbox>
														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_servizi_chk()"
															data-ng-if="ctrl.General.getFiltriMode().categoria == ''Servizi'' || ctrl.General.getFiltriMode().categoria == ''''">
															<div data-ng-if = "$index <= 2">
																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_servizi_selected())" aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label>
																</md-checkbox>
															</div>
															<div data-ng-if = "$index >= 3">									
																<div data-ng-show="showallcat || ctrl.General.getFiltriMode().categoria != ''''">
																	<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_servizi_selected())" aria-label="{{item.name}}">
																	<label data-ng-bind-html="item.name"></label>
																	</md-checkbox>
																</div>
															</div>
														</div>
														<a href="" data-ng-hide="showallcat || ctrl.General.getFiltriMode().categoria != ''''" data-ng-click="showallcat=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
													</div>
													<div data-ng-show="showallcat || ctrl.General.getFiltriMode().categoria != ''''">
														<div class="search-filter-ckgroup">
															<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" />"
																data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"
																data-ng-checked="ctrl.Ricerca.novita_isChecked()"
																md-indeterminate="ctrl.Ricerca.novita_isIndeterminate()"
																data-ng-click="ctrl.Ricerca.novita_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" /></label>
															</md-checkbox>
															<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_novita_chk()"
																data-ng-if="ctrl.General.getFiltriMode().categoria == ''Novità'' || ctrl.General.getFiltriMode().categoria == ''''">
																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_novita_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_novita_selected())" aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label>
																</md-checkbox>
															</div>
														</div>
														<div class="search-filter-ckgroup">
															<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" />"
																data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"
																data-ng-checked="ctrl.Ricerca.docs_isChecked()"
																md-indeterminate="ctrl.Ricerca.docs_isIndeterminate()"
																data-ng-click="ctrl.Ricerca.docs_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" /></label>
															</md-checkbox>
															<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_docs_chk()"
																data-ng-if="ctrl.General.getFiltriMode().categoria == ''Documenti'' || ctrl.General.getFiltriMode().categoria == ''''">
																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_docs_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_docs_selected())" aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label>
																</md-checkbox>
															</div>
															<a href="" data-ng-hide="ctrl.General.getFiltriMode().categoria != ''''" data-ng-click="showallcat=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_argomentoTab() == ''active''? ''tab-pane active'' : ''tab-pane''" id="argomentoTab">
											<div class="row">
												<div class="offset-md-1 col-md-10 col-sm-12">
													<div class="search-filter-ckgroup">
														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()">
															<div data-ng-if = "$index <= 11">
																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
																<label data-ng-bind-html="item.name"></label></md-checkbox>
															</div>
															<div data-ng-if = "$index >= 12">									
																<div data-ng-show="showallarg">
																	<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
																	<label data-ng-bind-html="item.name"></label></md-checkbox>
																</div>
															</div>
														</div>
														<a href="" data-ng-hide="showallarg" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
														<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_opzioniTab() == ''active''? ''tab-pane active'' : ''tab-pane''" id="opzioniTab">
											<div class="row">
												<div class="offset-lg-1 col-lg-4 col-md-6 col-sm-12">
													<div class="form-check form-check-group">
														<div class="toggles">
															<label for="active_chk">
																<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT" />
																<input type="checkbox" id="active_chk" data-ng-model="ctrl.Ricerca.activeChk">
																<span class="lever"></span>
															</label>
														</div>
													</div>
													<p class="small"><@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT_INFO" /></p>
												</div>
												<div class="col-md-3 col-sm-12 search-filter-dategroup">
													<div class="form-group">
														<label for="datepicker_start"><@wp.i18n key="CITTAMETRO_PORTAL_START_DATE" /></label>
														<input type="text" class="form-control" id="datepicker_start" data-ng-model="ctrl.Ricerca.dataInizio" placeholder="gg/mm/aaaa" />
														<button aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" onclick="$(''#datepicker_start'').focus();" onkeypress="$(''#datepicker_start'').focus();"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg></button>
													</div>
												</div>
												<div class="col-md-3 col-sm-12 search-filter-dategroup">
													<div class="form-group">
														<label for="datepicker_end"><@wp.i18n key="CITTAMETRO_PORTAL_END_DATE" /></label>
														<input type="text" class="form-control" id="datepicker_end" data-ng-model="ctrl.Ricerca.dataFine" placeholder="gg/mm/aaaa" />
														<button aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" onclick="$(''#datepicker_end'').focus();" onkeypress="$(''#datepicker_end'').focus();"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_widget_cerca_servizi_home','cittametro_widget_cerca_servizi_home',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.currentPage param="title" var="titolo" />
<@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" />

<div class="widget" data-ng-cloak data-ng-controller="ctrlRicerca as ctrl"> 
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></h3>
			</div>
		</div>
		<div class="row">
			<div class="offset-lg-2 col-lg-8 offset-lg-2 offset-md-1 col-md-10 offset-md-1 col-sm-12">
				<div class="box-servizi">
					<form action="<@wp.url page="${searchWidgetPage.code}"/>" method="post">
						<div class="form-group">
							<label class="sr-only" for="cerca-txt-intro"><@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" /></label>
							<input type="text" id="cerca-txt-intro" name="cercatxt" class="form-control" data-ng-model="ctrl.Ricerca.searchStringRicercaTxt" placeholder="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" />">
							<button type="submit" class="ico-sufix" >
								<svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
							</button>
						</div>
						<div>
							<a class="btn btn-default btn-verde" href="<@wp.url page="doc_01" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="doc_01" info="title" />"><@wp.pageInfo pageCode="doc_01" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="amm_03" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amm_03" info="title" />"><@wp.pageInfo pageCode="amm_03" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="organizzazione" />?contentId=ORG9607" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: URP">URP </a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="persona" />?contentId=PRS11084" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: Sindaco">Sindaco</a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="amm_01_02" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amm_01_02" info="title" />"><@wp.pageInfo pageCode="amm_01_02" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="doc_09" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="doc_09" info="title" />"><@wp.pageInfo pageCode="doc_09" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="doc_09_02" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="doc_09_02" info="title" />"><@wp.pageInfo pageCode="doc_09_02" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="ts_10_01" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="ts_10_01" info="title" />"><@wp.pageInfo pageCode="ts_10_01" info="title" /></a>
							<a class="btn btn-default btn-verde" data-ng-click="ctrl.Ricerca.setActive(); ctrl.General.setFiltriMode(true, true)" aria-label="<@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" />" data-toggle="modal" data-target="#searchModal">...</a>
						</div>
						<input type="hidden" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">
						<input type="hidden" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">
						<input type="hidden" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">
						<input type="hidden" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">
						<input type="hidden" name="argomenti" value="{{ctrl.Ricerca.get_args_selected()}}">
						<input type="hidden" name="attivi" value="{{ctrl.Ricerca.active_chk}}">
						<input type="hidden" name="inizio" value="{{ctrl.Ricerca.data_inizio}}">
						<input type="hidden" name="fine" value="{{ctrl.Ricerca.data_fine}}">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_header_intestazione',NULL,NULL,'<#--
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
-->
<#assign wp=JspTaglibs["/aps-core"]>
 
<@wp.pageWithWidget var="searchResultPageVar" widgetTypeCode="jpfacetnav_results" listResult=false />

<#assign currentimgr= '''' />
<#assign accessToken = '''' />
<#assign refreshToken = '''' />
<#--
<#if (Session.currentimgr??)>
	<#assign currentimgr = Session.currentimgr />
	<#if (Session.currentimgr.accessToken??)>
		<#assign accessToken = Session.currentimgr.accessToken />
	</#if>
	<#if (Session.currentimgr.refreshToken??)>
		<#assign refreshToken = Session.currentimgr.refreshToken />
	</#if>
</#if>

<!-- Intestazione
<div class="container header" data-ng-controller="ctrlRicerca as ctrl" data-ng-init="ctrl.General.setCurrentimgr(''${currentimgr}''); ctrl.General.setAccessToken(''${accessToken}''); ctrl.General.setRefreshToken(''${refreshToken}'')">
 -->
<div class="container header">
	<div class="row clearfix">
		<div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 comune">
			<div class="logoprint">
				<h1>
					<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/logo_cittametro_cagliari.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />"/>
					<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" />
				</h1>
			</div>
			<div class="logoimg">
				<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"> 
					<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/logo_cittametro_cagliari.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />"/>
				</a>
			</div>
			<div class="logotxt">
				<h1>
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" /></a>
				</h1>
			</div>
			<!-- pulsante ricerca mobile -->
			<div class="p_cercaMobile clearfix">
				<button aria-label="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SEARCH" />" class="btn btn-default btn-cerca pull-right" data-target="#searchModal" data-toggle="modal" type="button">
					<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
				</button>
			</div>
			<!-- pulsante ricerca mobile -->
		</div>
		
		<div class="col-xl-4 col-lg-4 d-none d-lg-block d-md-none pull-right text-right">
			<!-- social-->
			<ul class="list-inline text-right social">
				<li class="small list-inline-item"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL" /></li>
				
				<li class="list-inline-item">
					<a 1target="_blank" 
						href="https://twitter.com/CittaCa?s=08" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Twitter - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-twitter"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.facebook.com/cittametropolitanadicagliari/" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Facebook - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-facebook"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.youtube.com/channel/UCuhKPaORUxFGD6rY5DLhIzg" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- YouTube - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-youtube"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.instagram.com/cittametroca/?utm_source=ig_profile_share&igshid=vz1lk7zotn77" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Instagram - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-instagram"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span>
					</a>
				</li>
			</ul>
			<!-- social-->
		</div>

		<div class="col-xl-2 col-lg-2 col-md-4 d-none d-md-block d-md-none text-right">
			<!-- ricerca -->
			<div class="cerca float-right">
				<span><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SEARCH" /></span>
				<button aria-label="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SEARCH" />" class="btn btn-default btn-cerca pull-right" type="button" data-toggle="modal" data-target="#searchModal">
					<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
				</button>
			</div>
			<!-- ricerca -->
		</div>
	</div>
</div>
<!-- Intestazione -->',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_mappa_territorio','cittametro_mappa_territorio',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cc=JspTaglibs["/cagliari-core"]>


<#assign leafletjs_css="<link rel=\"stylesheet\" href=\"https://unpkg.com/leaflet@1.5.1/dist/leaflet.css\"
  integrity=\"sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==\"
  crossorigin=\"\" />" />
<#assign leafletjs_js><script nonce="<@wp.cspNonce />" src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
  integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
  crossorigin=""></script></#assign>

<@wp.headInfo type="CSS_CA_EXT_GLB" info="${leafletjs_css}" />
<@wp.headInfo type="JS_CA_EXT_GLB" info="${leafletjs_js}" />
<@wp.headInfo type="JS_CA_TOP" info="leafletjs_config.min.js" />
<#--
<@wp.headInfo type="CSS_CA_INT" info="sezioni.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.css" />
<@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
<@wp.headInfo type="CSS_CA_INT" info="owl.carousel.css" />
<@wp.headInfo type="CSS_CA_INT" info="owl.theme.default.css" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />
-->




<script nonce="<@wp.cspNonce />">

  var cmMap = L.map(''cm-mappa'', {
    minZoom: 8,
    maxZoom: 15,
    zoomSnap: 0.25,
    zoomDelta: 0.25,
    wheelPxPerZoomLevel: 150,
    layers: [CartoDB]
  });

  var baseMaps = {
    "CartoDB": CartoDB,
    "Open Street Map": OSM,
    "Google Sat": Google,
    "Esri Sat": Esri_sat
  };

  <#--
  L.tileLayer(''https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}'', {
    attribution: ''Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'',
    maxZoom: 15,
    minZoom: 10,
    id: ''mapbox/streets-v11'',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: ''pk.eyJ1IjoiZ3NwcmVnYSIsImEiOiJja3cwZmkyaWgwdXk5Mm9udDZzc2JjMzRyIn0.hOqCl2fw8DJ6-ZQlS4XSUA''
  }).addTo(cmMap);
  -->   

  cmMap.setView([39.170, 9.150], 10.5);
      
  cmMap.setMaxBounds(cmMap.getBounds());
      
  function highlight(layer) {
    
    layer.setStyle({
      fillColor: ''#00572A'',
      weight: 2,
      opacity: 0.7,
      color: ''white'',
        fillOpacity: 0.9
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
      layer.bringToFront();
    }
  }

  function resetHighlight(layer) {

    if (selectedLayer === null || selectedLayer._leaflet_id !== layer._leaflet_id) {

      geojson.resetStyle(layer);
    }
  }
  
  var selectedLayer = null;

  function resetMap(e) {

    if (selectedLayer !== null) {

      geojson.resetStyle(selectedLayer);

      cmMap.setView([39.170, 9.150], 10.5);
      
    }
  }
  
  function select(layer) {

    if (selectedLayer !== null) {
      
      var previousLayer = selectedLayer;
    }
    
    cmMap.fitBounds(layer.getBounds());

    selectedLayer = layer;

    if (previousLayer) {

      resetHighlight(previousLayer);
    }
  }
  
  function onEachFeature(feature, layer) {
      layer.on({
          ''mouseover'': function (e) {
            highlight(e.target);
          },
          ''mouseout'': function (e) {
            resetHighlight(e.target);
          },
          ''click'': function (e) {
            select(e.target);
          }
      });
  }

  fetch("<@wp.resourceURL />cittametro-homepage-bundle/static/js/cittametroGeo.geojson")
    .then(function (response) {

      return response.json()
    })
    .then(function (data) {

      geojson = L.geoJson(data, {
        style: style,
        onEachFeature: onEachFeature
      }).bindPopup(function (layer) {

        return layer.feature.properties.nome;
      }).addTo(cmMap);


      var geoData = data.map(f => f.properties.nome);

      return
        <h2>
          <@wp.i18n key="CITTAMETRO_PORTAL_MAP_TITLE" />
        </h2>
        <div class="row">
          <div class="col-lg-9 col-md-8">
            <div id="cm-mappa" class="mt32" style="height:700px"></div>
            <#--
            <div id="cm-mappah" class="mt32"></div>
            -->

          </div>
          <div class="offset-lg-1 col-lg-2 offset-md-1 col-md-3">
            <aside id="menu-sezione">
              <ul class="list-group">
                <li class="list-group-item">
                  <a href="#">
                    AAA
                  </a>
                </li>
              </ul>
            </aside>
          </div>
        </div>
      ;
    });

  function style(feature) {
    return {
      fillColor: ''#006631'',
      weight: 2,
      opacity: 0.7,
      color: ''white'',
      fillOpacity: 0.8
    };
  }

  cmMap.createPane(''labels'');
  cmMap.getPane(''labels'').style.zIndex = 650;
  cmMap.getPane(''labels'').style.pointerEvents = ''none'';

  var positron = L.tileLayer(''https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png'', {
        attribution: ''©OpenStreetMap, ©CartoDB''
  }).addTo(cmMap);

  var positronLabels = L.tileLayer(''https://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png'', {
          attribution: ''©OpenStreetMap, ©CartoDB'',
          pane: ''labels''
  }).addTo(cmMap);

  cmMap.on(''click'', resetMap);

  function listaComuni() {


  }


</script>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('ilpatto_gestione_aree','ilpatto_gestione_aree',NULL,'<#assign wp=JspTaglibs[ "/aps-core"]>

<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react@17.0.1/umd/react.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react-dom@17.0.1/umd/react-dom.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />ilpatto-bundle/js/widgets/ilpatto-microfrontends/build.js" ></script>

<ilpatto-service-url value="http://metroca.mshome.net/cmca/ilpatto-microservice/0-1-3-snapshot" />
<ilpatto-gestione-aree ></ilpatto-gestione-aree>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('ilpatto_stats_aree','ilpatto_stats_aree',NULL,'<#assign wp=JspTaglibs[ "/aps-core"]>

<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react@17.0.1/umd/react.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react-dom@17.0.1/umd/react-dom.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />ilpatto-bundle/js/widgets/ilpatto-microfrontends/build.js" ></script>

<ilpatto-service-url value="http://metroca.mshome.net/cmca/ilpatto-microservice/0-1-3-snapshot" />
<ilpatto-statistiche-progetti-area ></ilpatto-statistiche-progetti-area>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('ilpatto_gestione_settori','ilpatto_gestione_settori',NULL,'<#assign wp=JspTaglibs[ "/aps-core"]>

<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react@17.0.1/umd/react.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react-dom@17.0.1/umd/react-dom.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />ilpatto-bundle/js/widgets/ilpatto-microfrontends/build.js" ></script>

<ilpatto-service-url value="http://metroca.mshome.net/cmca/ilpatto-microservice/0-1-3-snapshot" />
<ilpatto-gestione-settori ></ilpatto-gestione-settori>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('ilpatto_gestione_progetti','ilpatto_gestione_progetti',NULL,'<#assign wp=JspTaglibs[ "/aps-core"]>

<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react@17.0.1/umd/react.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="https://cdn.jsdelivr.net/npm/react-dom@17.0.1/umd/react-dom.production.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />ilpatto-bundle/js/widgets/ilpatto-microfrontends/build.js" ></script>

<ilpatto-service-url value="http://metroca.mshome.net/cmca/ilpatto-microservice/0-1-3-snapshot" />
<ilpatto-gestione-progetti ></ilpatto-gestione-progetti>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_footer_js',NULL,NULL,'<#assign wp=JspTaglibs[ "/aps-core"]>
<#-- FOOTER SCRIPTS -->
<script nonce="<@wp.cspNonce />">window.__PUBLIC_PATH__ ="<@wp.resourceURL />cittametro-homepage-bundle/static/font/" </script>
<#-- <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/bootstrap-italia.bundle_1.4.3.min.js" ></script> -->
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/cittametro.js" ></script>




<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/popper.min.js" ></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/bootstrap-italia_1.2.0sf.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery-ui.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/i18n/datepicker-it.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-animate.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-aria.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-messages.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-sanitize.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/app.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/ricerca.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/ricerca-service.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/general-service.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-material.cagliari.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery.cookie.min.js"></script>
<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery.cookiecuttr.min.js"></script>



<@wp.outputHeadInfo type="JS_CA_BTM_EXT"><script nonce="<@wp.cspNonce />" src="<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_BTM_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_BTM"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_BTM_PLGN"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>

<@wp.outputHeadInfo type="JS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_TOP"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/leafletjs_config.min.js"></script></@wp.outputHeadInfo>


<script nonce="<@wp.cspNonce />">
<@wp.outputHeadInfo type="JS_CA_BTM_INC">
<@wp.printHeadInfo />
</@wp.outputHeadInfo>
</script>



<!-- COOKIE BAR, ANALITICS e HOTJAR -->
<script nonce="<@wp.cspNonce />">
(function ($) {
$(document).ready(function () {
// activate cookie cutter
$.cookieCuttr({
cookieAnalytics: false,
cookieMessage: ''<p><@wp.i18n key="CITTAMETRO_FOOTER_COOKIES_TEXT" />&nbsp;<a href="<@wp.url page="privacy" />" class="text-white" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_PORTAL_PRIVACYPOLICY" />"><@wp.i18n key="CITTAMETRO_PORTAL_PRIVACYPOLICY" /></a>.<br/><@wp.i18n key="CITTAMETRO_FOOTER_COOKIES_QUESTION" /></p>'',
cookieAcceptButtonText: ''<@wp.i18n key="CITTAMETRO_FOOTER_ACCEPTS_COOKIES" />'',
cookieDiscreetPosition: "bottomleft",
cookieDeclineButton: true,
//cookieResetButton: true,
cookieDeclineButtonText: ''<@wp.i18n key="CITTAMETRO_FOOTER_DECLINES_COOKIES" />'',
//cookieResetButtonText: ''Cambia scelta cookies'',
cookieDomain: ''comune.cagliari.it''
});
});
})(jQuery);

if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept") {
(function(i,s,o,g,r,a,m){i[''GoogleAnalyticsObject'']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,''script'',''https://www.google-analytics.com/analytics.js'',''ga'');
ga(''create'', ''UA-27957186-1'', ''auto'');
ga(''set'', ''anonymizeIp'', true);
ga(''send'', ''pageview'');



(function(h,o,t,j,a,r){
h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
h._hjSettings={hjid:553641,hjsv:6};
a=o.getElementsByTagName(''head'')[0];
r=o.createElement(''script'');r.async=1;
r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
a.appendChild(r);
})(window,document,''https://static.hotjar.com/c/hotjar-'',''.js?sv='');
}
</script>
<@wp.fragment code="cittametro_template_search_modal" escapeXml=false />',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jpsolr_inc_facetNavTree',NULL,'jpsolr',NULL,'<#assign wpfp=JspTaglibs["/jpsolr-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<ul>
	<#list currFacetRoot.children as facetNode>
		<@wpfp.hasToViewFacetNode facetNodeCode="${facetNode.code}" requiredFacetsParamName="requiredFacets">
			<#if (occurrences[facetNode.code]??)>
				<li>
				<a href="<@wp.url><@wpfp.urlPar name="selectedNode" >${facetNode.code}</@wpfp.urlPar>
					<#list requiredFacets as requiredFacet>
					<@wpfp.urlPar name="facetNode_${requiredFacet_index + 1}" >${requiredFacet}</@wpfp.urlPar>
					</#list>
					</@wp.url>"><@wpfp.facetNodeTitle escapeXml=true facetNodeCode="${facetNode.code}" /></a>&#32;<abbr class="jpsolr_tree_occurences" title="<@wp.i18n key="jpsolr_OCCURRENCES_FOR" />&#32;<@wpfp.facetNodeTitle escapeXml=true facetNodeCode="${facetNode.code}" />:&#32;${occurrences[facetNode.code]}">${occurrences[facetNode.code]}</abbr>
				<@wpfp.hasToOpenFacetNode facetNodeCode="${facetNode.code}" requiredFacetsParamName="requiredFacets" occurrencesParamName="occurrences" >
					<@wp.freemarkerTemplateParameter var="currFacetRoot" valueName="facetNode" removeOnEndTag=true >
					<@wp.fragment code="jpsolr_inc_facetNavTree" escapeXml=false />
					</@wp.freemarkerTemplateParameter>
				</@wpfp.hasToOpenFacetNode>
				</li>
			</#if>
		</@wpfp.hasToViewFacetNode>
	</#list>
</ul>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jpsolr_facetTree','jpsolr_facetTree','jpsolr',NULL,'<#assign wpfp=JspTaglibs["/jpsolr-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@wp.headInfo type="CSS" info="../../plugins/jpsolr/static/css/jpsolr.css"/>
<div class="jpsolr">
	<h2 class="title"><@wp.i18n key="jpsolr_TITLE_TREE" /></h2>
	<@wpfp.facetNavTree requiredFacetsParamName="requiredFacets" facetsTreeParamName="facetsForTree" />
	<@wp.freemarkerTemplateParameter var="occurrences" valueName="occurrences" removeOnEndTag=true >
	<#list facetsForTree as facetRoot>
		<h3><@wpfp.facetNodeTitle facetNodeCode="${facetRoot.code}" /></h3>
		<#if (occurrences[facetRoot.code]??) && (occurrences[facetRoot.code]?has_content)>
			<ul>

				<#list facetRoot.childrenCodes as facetNode>


					<#if (occurrences[facetNode]??)>

						<li>
							<a href="<@wp.url><@wp.parameter name="selectedNode" >${facetNode}</@wp.parameter>
                                                        <#if (RequestParameters.search??)><@wp.parameter name="search" >${RequestParameters.search}</@wp.parameter></#if>
								<#list requiredFacets as requiredFacet>
								<@wpfp.urlPar name="facetNode_${requiredFacet_index + 1}" >${requiredFacet}</@wpfp.urlPar>
								</#list>
								</@wp.url>"><@wpfp.facetNodeTitle facetNodeCode="${facetNode}" /></a>&#32;<abbr class="jpsolr_tree_occurences" title="<@wp.i18n key="jpsolr_OCCURRENCES_FOR" />&#32;<@wpfp.facetNodeTitle facetNodeCode="${facetNode}" />:&#32;${occurrences[facetNode]}">${occurrences[facetNode]}</abbr>
							<@wpfp.hasToOpenFacetNode facetNodeCode="${facetNode}" requiredFacetsParamName="requiredFacets" occurrencesParamName="occurrences" >
								<@wp.freemarkerTemplateParameter var="currFacetRoot" valueName="facetNode" removeOnEndTag=true >
								<@wp.fragment code="jpsolr_inc_facetNavTree" escapeXml=false />
								</@wp.freemarkerTemplateParameter>
							</@wpfp.hasToOpenFacetNode>
						</li>
					</#if>


				</#list>


			</ul>
		<#else>
			<p><abbr title="<@wp.i18n key="jpsolr_EMPTY_TAG" />:&#32;<@wpfp.facetNodeTitle facetNodeCode="${facetRoot.code}" escapeXml=true />">&ndash;</abbr></p>
		</#if>
	</#list>
	</@wp.freemarkerTemplateParameter>
</div>

',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('jpsolr_facetResults','jpsolr_facetResults','jpsolr',NULL,'<#assign wpfp=JspTaglibs["/jpsolr-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpcms=JspTaglibs["/jacms-aps-core"]>

<@wp.headInfo type="CSS" info="../../plugins/jpsolr/jpsolr.css"/>
<div class="jpsolr">

<h2><@wp.i18n key="jpsolr_TITLE_FACET_RESULTS" /></h2>

<@wpfp.facetNavResult requiredFacetsParamName="requiredFacets"
	resultParamName="contentList" executeExtractRequiredFacets=false breadCrumbsParamName="breadCrumbs" />

<#if (breadCrumbs??) && (breadCrumbs?has_content)>
	<p><@wp.i18n key="SEARCHED_FOR" />:</p>
	<ul class="jpsolr_filterlist">
		<#list breadCrumbs as item>
		<li>
			<#list item.breadCrumbs as breadCrumb>
				<#if (breadCrumb_index != 0)>
					<#if (breadCrumb == item.requiredFacet)>
						<span class="jpfacetfilter jpsolr_requiredFacet"><@wpfp.facetNodeTitle facetNodeCode="${breadCrumb}" /></span>
                                                <#assign currentNodeTitle ><@wpfp.facetNodeTitle facetNodeCode="${breadCrumb}" /></#assign>
					<#elseif (breadCrumb == item.facetRoot)>
						<span class="jpfacetfilter jpsolr_facetRoot"><@wpfp.facetNodeTitle facetNodeCode="${item.facetRoot}" /></span>
                                                <#assign currentNodeTitle ><@wpfp.facetNodeTitle facetNodeCode="${item.facetRoot}" /></#assign>
					<#else>
						<a title="<@wp.i18n key="jpsolr_REMOVE_FILTER" />: <@wpfp.facetNodeTitle facetNodeCode="${breadCrumb}" />" class="jpfacetfilter" href="<@wp.url><@wpfp.urlPar name="selectedNode" >${breadCrumb}</@wpfp.urlPar>
						   <#list requiredFacets as requiredFacet>
						   <@wpfp.urlPar name="facetNode_${requiredFacet_index + 1}" >${requiredFacet}</@wpfp.urlPar>
						   </#list>
						   </@wp.url>"><@wpfp.facetNodeTitle facetNodeCode="${breadCrumb}" />
						</a>
                                                <#assign currentNodeTitle ><@wpfp.facetNodeTitle facetNodeCode="${breadCrumb}" /></#assign>
					</#if>
					<#if (item.breadCrumbs?size != (breadCrumb_index + 1))>&#32;/&#32;</#if>
				</#if>
			</#list>
			<span class="noscreen">|</span>&#32;<a class="jpsolrfilterremove" title="<@wp.i18n key="jpsolr_REMOVE_FILTER" />:&#32;${currentNodeTitle}" href="<@wp.url><#list requiredFacets as requiredFacet><@wpfp.urlPar name="facetNode_${requiredFacet_index + 1}" >${requiredFacet}</@wpfp.urlPar></#list><#list item.breadCrumbs as breadCrumb2><@wpfp.urlPar name="facetNodeToRemove_${breadCrumb2_index + 1}" >${breadCrumb2}</@wpfp.urlPar></#list></@wp.url>">
			<img src="<@wp.resourceURL />plugins/jpsolr/static/img/edit-delete.png" alt="<@wp.i18n key="jpsolr_REMOVE_FILTER" />" /></a>
		</li>
		</#list>
	</ul>
</#if>

<#if (contentList??) && (contentList?has_content)>
	<@wp.pager listName="contentList" objectName="groupContent" max=10 pagerIdFromFrame=true >
		<p><em><@wp.i18n key="SEARCH_RESULTS_INTRO" />&#32;${groupContent.size}&#32;<@wp.i18n key="SEARCH_RESULTS_OUTRO" />&#32;[${groupContent.begin + 1} &ndash; ${groupContent.end + 1}]:</em></p>
		<ol class="pureSize">
			<#list contentList as contentId>	
			<#if (contentId_index >= groupContent.begin) && (contentId_index <= groupContent.end)>
			<li><@wpcms.content contentId="${contentId}" modelId="list" /></li>
			</#if>
			</#list>
		</ol>
		<#if (groupContent.size > groupContent.max)>
			<div>
				<p class="paginazione">
					<#if (1 == groupContent.currItem)>
					&laquo;&#32;<@wp.i18n key="PREV" />
					<#else>
					<a href="<@wp.url paramRepeat=true ><@wp.parameter name="${groupContent.paramItemName}" >${groupContent.prevItem}</@wp.parameter><#list requiredFacets as requiredFacet><@wpfp.urlPar name="facetNode_${requiredFacet_index + 1}" >${requiredFacet}</@wpfp.urlPar></#list></@wp.url>">&laquo;&#32;<@wp.i18n key="PREV" /></a>
					</#if>
					<#list groupContent.items as item>
						<#if (item == groupContent.currItem)>
						&#32;[${item}]&#32;
						<#else>
						&#32;<a href="<@wp.url paramRepeat=true ><@wp.parameter name="${groupContent.paramItemName}" >${item}</@wp.parameter><#list requiredFacets as requiredFacet><@wpfp.urlPar name="facetNode_${requiredFacet_index + 1}" >${requiredFacet}</@wpfp.urlPar></#list></@wp.url>">${item}</a>&#32;
						</#if>
					</#list>
					<#if (groupContent.maxItem == groupContent.currItem)>
					<@wp.i18n key="NEXT" />&#32;&raquo;
					<#else>
					<a href="<@wp.url paramRepeat=true ><@wp.parameter name="${groupContent.paramItemName}" >${groupContent.nextItem}</@wp.parameter><#list requiredFacets as requiredFacet><@wpfp.urlPar name="facetNode_${requiredFacet_index + 1}" >${requiredFacet}</@wpfp.urlPar></#list></@wp.url>"><@wp.i18n key="NEXT" />&#32;&raquo;</a>
					</#if>
				</p>
			</div>
		</#if>
	</@wp.pager>
<#else>
	<p><em><@wp.i18n key="SEARCH_NOTHING_FOUND" /></em></p>
</#if>
</div>',1);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('login_prova','login_prova',NULL,'<#assign wp=JspTaglibs["/aps-core"]>

  <#if (Session.currentUser != "guest")>
   <div class="btn-group">   
   <button type="button" class="btn btn-default btn-accedi btn-accedi-ap-md" role="button" id="dropdownAP" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false"> ${Session.currentUser} <span class="sr-only">${Session.currentUser}</span>    
  </button>    

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
 <div class="link-list-wrapper">
<ul class="link-list">
  <@wp.ifauthorized permission="enterBackend">        
<li>
     <a class="list-item" href="/app-builder/">  <@wp.i18n key="Area Personale" />        </a> 
</li>
<li>
      <a class="list-item" href="/app-builder/user/edit/${Session.currentUser}" >  <@wp.i18n key="Gestione Utente" />    </a>      
    </@wp.ifauthorized>       
</li>
<li>
 <a class="list-item"  href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action">  <@wp.i18n key="ESLF_SIGNOUT" />    </a>
<@wp.pageWithWidget var="editProfilePageVar" widgetTypeCode="userprofile_editCurrentUser" />
<#if (editProfilePageVar??) >
<a class="list-item" href="<@wp.url page="${editProfilePageVar.code}" />" ><@wp.i18n key="ESLF_PROFILE_CONFIGURATION" /></a>
</#if>
</li>
</ul>
</div>
</div>
</div>

<#else>
<script nonce="<@wp.cspNonce />">
  function login() {
   window.entando.keycloak.login();
   location.href = ''<@wp.info key="systemParam" paramName="applicationBaseURL" />do/login?redirectTo=<@wp.url page="homepage"/>'';
  }
  document.addEventListener(''DOMContentLoaded'', function () {
    document.getElementById(''login-button'')
      .addEventListener(''click'', login);
  });
  </script>
  <script nonce="<@wp.cspNonce />">window.addEventListener("user.form.login", (evt) => {  if(evt.detail.payload){    login();  }});</script>
  <div class="btn-group login-drop">    
  <button id="login-button" type="button" class="btn btn-default btn-accedi btn-accedi-ap-md" >   
 <@wp.i18n key="ESLF_SIGNIN" />   
 </button>    
<!--
<a class="btn sign-up " style="line-height: 37px;" href="<@wp.url page="sign_up" />" >       
<@wp.i18n key="SIGN_UP" /> -->   
 </a>
 </div> 
</#if>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_googletagmanagerbody',NULL,NULL,'<!-- Google Tag Manager (noscript) -->
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_widget_argomenti_inevidenza_img','cagliari_widget_argomenti_inevidenza_img',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<#assign utenteToken = '''' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
<#if (Session.currentUser.accessToken??)>
<#assign utenteToken = Session.currentUser.accessToken />
</#if>
</#if>

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />
<@wp.categories var="systemCategories" titleStyle="prettyFull" />

<#assign tipoContenuto = ''ARG'' />
<#assign idModello = ''400006'' />
<#assign elementiMax = 999 />
<#assign elementiMaxItem = 7 />
<#assign ordinamento = "(order=ASC;attributeFilter=true;key=titolo)" />
<#assign categorie = "evd" />

<div class="widget" data-ng-controller="FiltriController">
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', ''${elementiMax}'', ''${tipoContenuto}'', ''${idModello}'', ''${categorie}'', ''${tipoContenuto}'', '''', ''${ordinamento}'', '''', ''${utenteToken}'')"> 
		<div class="row">
			<div class="col-md-12">
				<div class="titolosezione">
					<h3><@wp.i18n key="CITTAMETRO_PORTAL_INEVIDENCE" /></h3>
				</div>
			</div>
		</div>
		
		<div data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
			<div class="row row-eq-height" data-ng-if="($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
				<div class="col-md-4" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 1">
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 1" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 2">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(elem.$, ''400008'')" data-ng-bind-html="renderContent[elem.$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 3">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 6">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-5].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-5].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 7">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-6].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-6].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-5].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-5].$][''400008'']"></div>
					</div>
				</div>
				
				<div class="col-md-4" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 2">
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 3" data-ng-init="getContent(elem.$, ''400007'')" data-ng-bind-html="renderContent[elem.$][''400007'']"></div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400007'']"></div>
						<div data-ng-init="getContent(elem.$, ''400009'')" data-ng-bind-html="renderContent[elem.$][''400009'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''400007'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400009'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400009'']"></div>
						<div data-ng-init="getContent(elem.$, ''400007'')" data-ng-bind-html="renderContent[elem.$][''400007'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 6">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''400007'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''400009'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''400009'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400007'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 7">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$][''400007'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''400009'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''400009'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''400007'']"></div>
					</div>
				</div>
				
				<div class="col-md-4" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 6">
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 6" data-ng-init="getContent(elem.$, ''400008'')" data-ng-bind-html="renderContent[elem.$][''400008'']"></div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 7">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400008'']"></div>
						<div data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && ${elementiMax}>${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" /> - <@wp.i18n key="CITTAMETRO_PORTAL_INEVIDENCE" />">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="goToPage(currentPage-1)" 
								title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}"
							data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="goToPage(elem-1)">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="goToPage(elem-1)">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage >= contents.length/pageSize - 1" data-ng-click="goToPage(currentPage+1)"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		
		<div class="row row-eq-height" data-ng-if="contents.length == 0">
			<div class="col-md-12">
				<p><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
			</div>
		</div>
	</div>
	<#assign contentList="">
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_widget_argomento_dettaglio','cagliari_widget_argomento_dettaglio',NULL,'<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@cw.currentArgument var="currentArg" />
<#if (currentArg??)>
    <@cw.contentList listName="contentList" contentType="ARG" category="${currentArg}"/>
    <#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
        <#list contentList as contentId>
            <@jacms.content contentId="${contentId}" />
        </#list>
    <#else>
    	<p class="alert alert-info"><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
    </#if>
</#if>
<#assign contentList="">',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_menu_ap',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#--
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign jpavatar=JspTaglibs["/jpavatar-apsadmin-core"]>
-->

<@wp.ifauthorized groupName="cittadini" var="isCittadino"/>
<@wp.ifauthorized groupName="administrators" var="isAdmin"/>

<#--
<#if Session.currentUser?? && Session.currentUser != "guest" && isCittadino && !isAdmin>
	<@wp.currentPage param="code" var="paginaCorrente" />
	<@wp.currentUserProfileAttribute attributeRoleName="jpspid:codicefiscale" var="utenteCodiceFiscale" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:firstname" var="utenteName" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:surname" var="utenteSurname" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:sex" var="utenteSex" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:email" var="utenteEmail" />
	<#assign utenteFullname= ''''/>
	<#assign utenteShortname= ''''/>
	<#if utenteName?? && utenteSurname??>
		<#assign utenteFullname>${utenteName} ${utenteSurname}</#assign>
		<#assign utenteShortname>${utenteName?substring(0,1)}${utenteSurname?substring(0,1)}</#assign>
	<#elseif utenteCodiceFiscale??>
		<#assign utenteFullname>${utenteCodiceFiscale}</#assign>
		<#assign utenteShortname>${utenteCodiceFiscale?substring(3,4)}${utenteCodiceFiscale?substring(0,1)}</#assign>
	</#if>
    -->
	<#assign color= (.now?long % 9) + 1/>

	<!-- Menu AP -->
	<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="menuap">
		<div class="overlay"></div>
		<div class="cbp-menu-wrapper clearfix">
			<div class="user-burger bg-blu">
				<div class="usertitle-burger">
					<div class="avatar-wrapper">
						<div class="avatar size-lg bg${color}">
                        <#--
							<#if (currentAvatar?has_content)>
								<img src="${currentAvatar}" alt="${utenteShortname}" />
							<#else>
								${utenteShortname}
							</#if>
                        -->
							<!--<span class="avatar-notifiche">3 <em class="sr-only">nuove notifiche</em></span>-->
						</div>
					</div>
					<span>Bentornat
                    <#--
                    <#if utenteSex?? && utenteSex == ''F''>a<#else>o</#if><#if utenteName??><br /><strong>${utenteName}</strong></#if>!<span>
					<a href="<@wp.url page="ap_profilo" />" title="<@wp.i18nITTAMETRO" />: <@wp.pageInfo pageCode="ap_profilo" info="title" />">
						<svg class="icon"><use xlink:href="<@wp.imgURL />ponmetroca.svg#ca-settings"></use></svg>
					</a>
                    -->
                    </span>
				</div>
                <#--
				<#if utenteEmail??><div class="usermail-burger"><a>${utenteEmail}</a></div></#if>
                -->
			</div>
			<div class="logo-burger-user">
				<div class="logoimg-burger">
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"> 
						<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/logo_cittametro_cagliari.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />"/>
					</a>
				</div>
				<div class="logotxt-burger">
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" /></a>
				</div>
			</div>

			<h2 class="sr-only"><@wp.i18n key="CITTAMETRO_HEADER_MENU_TITLE" /> - <@wp.i18n key="CITTAMETRO_RESERVED_AREA" /></h2>
			
			<ul class="nav navmenu">
				<li>
					<a href="<@wp.url page="ap" />" class="list-item" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" escapeXml=false /> <@wp.pageInfo pageCode="ap" info="title" />">
							<@wp.pageInfo pageCode="ap" info="title" />
					</a>
				</li>
                <#--
				<@wp.nav var="page" spec="code(ap).subtree(1)">
					<li>
						<a href="<@wp.url page="${page.code}" />" class="list-item <#if (page.code == paginaCorrente)>current</#if>" title="<@wp.i18nITTAMETRO" escapeXml=false /> ${page.title}">
							${page.title}
						</a>
					</li>
				</@wp.nav>
				<li>
					<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action" class="list-item" title="<@wp.i18nITTAMETRO" escapeXml=false /> <@wp.i18n key="CITTAMETRO_RESERVEDAREA_LOGOUT" escapeXml=false />">
						<@wp.i18n key="CITTAMETRO_RESERVEDAREA_LOGOUT" escapeXml=false />
					</a>
				</li>
                -->
			</ul>
		</div>
	</nav>
	<!-- End Menu AP -->
    <#--
</#if>
-->',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_footer_info',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<div class="col-lg-3 col-md-3 col-sm-6 footer-ammtrasp footer-link">
 	<h4>
	 <!--
        <a href="<@wp.url page="amministrazione_trasparente" />" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />: 
            <@wp.pageInfo pageCode="amministrazione_trasparente" info="title" />">
            <@wp.pageInfo pageCode="amministrazione_trasparente" info="title" />
        </a>
	-->
	<@wp.i18n key="CITTAMETRO_FOOTER_TRASPARENZA" />
    </h4>
	<ul class="footer-list clearfix footer-link">
	    <li><p><@wp.i18n key="CITTAMETRO_FOOTER_AMMTRASP_TEXT" /></p></li>
		<li><a href="<@wp.url page="amministrazione_trasparente" />" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />: 
            <@wp.pageInfo pageCode="amministrazione_trasparente" info="title" />">
			<@wp.pageInfo pageCode="amministrazione_trasparente" info="title" />Amministrazione trasparente
        </a></li>
	</ul>
 	
	
</div>



<div class="col-lg-3 col-md-3 col-sm-6 footer-contatti">
 	<h4>
        <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_TITLE" />
    </h4>
 	<p>
 	 	<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" />:<br />
 	 	<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_ADDRESS" /><br />
 	 	<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_CODFISC" /> / <abbr title="<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_PIVA" />"><@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_PIVA_ABBR" />:</abbr> <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_PIVA_VAL" /><br />
 	 	 <abbr title="<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_IBAN_ACRO" />">IBAN</abbr>: <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_IBAN_VAL" />
 	</p>
 	<ul class="footer-list clearfix footer-link">
		<li><a href="<@wp.url page="posta_elettronica_certificata" />" title="<@wp.i18n key="CITTAMETRO_GO_TO_PAGE" />: <@wp.pageInfo pageCode="posta_elettronica_certificata" info="title" />"><@wp.pageInfo pageCode="posta_elettronica_certificata" info="title" />Posta elettronica certificata</a></li>
	</ul>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 footer-contatti">
 	<h4><span class="sr-only"><@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_NUMBERS" /></span>&nbsp;</h4>
 	<p class="footer-link"> 
	    <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_CENTRAL" /><br />
	  	<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_GREENNUM" /><br />
 	 	<@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_WHTPP" /><br />
 	</p>
<ul class="footer-list clearfix footer-link">
		<li><a href="<@wp.url page="organizzazione">
<#--
<@wp.parameter name="contentId" value="ORG9607" />
-->
</@wp.url>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_URP" />"><@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_URP" /></a></li>
		<li><a href="<@wp.url page="scrivi_comune" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="scrivi_comune" info="title" />"><@wp.pageInfo pageCode="scrivi_comune" info="title" />Scrivi alla Città Metropolitana</a></li>
		<li><a href="<@wp.url page="amm_05_01" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amm_05_01" info="title" />"><@wp.pageInfo pageCode="amm_05_01" info="title" />Rubrica dipendenti</a></li>
	</ul>
</div>




<div class="col-lg-3 col-md-3 col-sm-6 footer-seguici">
 	<h4><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL" /></h4>
 	<ul class="list-inline text-left social">
	 	<li class="list-inline-item">
					<a 1target="_blank" 
						href="https://twitter.com/CittaCa?s=08" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Twitter - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-twitter"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.facebook.com/cittametropolitanadicagliari/" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Facebook - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-facebook"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.youtube.com/channel/UCuhKPaORUxFGD6rY5DLhIzg" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- YouTube - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-youtube"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span>
					</a>
				</li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.instagram.com/cittametroca/?utm_source=ig_profile_share&igshid=vz1lk7zotn77" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Instagram - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-instagram"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span>
					</a>
				</li>
 	</ul>
<h4 class="mt48"><@wp.i18n key="CITTAMETRO_FOOTER_CONTACT_PARTNERSHIP" /></h4>
	<div class="loghi_partnership">
		<a target="blank" aria-label="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_PONMETRO_ARIA" />" href="https://www.consregsardegna.it/corecom/" title="<@wp.i18n key="CITTAMETRO_FOOTER_SITO_CORECOM" />"><img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/CORECOM_logo.svg" alt="<@wp.i18n key="CITTAMETRO_FOOTER_LOGO_CORECOM" />" /></a>
	</div>
</div>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_header_social_mobile',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<li class="small"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL" /></li>
<li class="">
    <a target="_blank" 
        aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - Twitter - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
        href="https://twitter.com/Comune_Cagliari" 
        title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" />">
            <svg class="icon"><use xlink:href="<@wp.resourceURL />/cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-twitter"></use>
            </svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TW" /></span></a></li>
<li>
    <a target="_blank" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - Facebook - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" href="https://www.facebook.com/comunecagliarinews.it" title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />/cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-facebook"></use></svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" /></span></a></li>
<li>
    <a target="_blank" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - YouTube - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" href="https://www.youtube.com/user/ComuneCagliariNews" title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />/cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-youtube"></use></svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span></a></li>
<li>
    <a target="_blank" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - Instagram - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" href="https://www.instagram.com/cittametroca/?utm_source=ig_profile_share&igshid=vz1lk7zotn77" title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_IG" />"><svg class="icon"><use xlink:href="<@wp.resourceURL />/cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-instagram"></use></svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_TG" /></span></a></li>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('prova_lingua','prova_lingua',NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@wp.headInfo type="JS" info="entando-misc-jquery/jquery-1.10.0.min.js" />
<@wp.headInfo type="JS" info="entando-misc-bootstrap/bootstrap.min.js" />
<@wp.info key="langs" var="langsVar" />
<@wp.info key="currentLang" var="currentLangVar" />


			<@wp.freemarkerTemplateParameter var="langsListVar" valueName="langsVar" removeOnEndTag=true >
			<#list langsListVar as curLangVar>
				<li
				<#if (curLangVar.code == currentLangVar)>class="active" </#if>>
				<a href="<@wp.url lang="${curLangVar.code}" paramRepeat=true />">
				<@wp.i18n key="A${curLangVar.code}" />
				</a>
				</li>
			</#list>
			</@wp.freemarkerTemplateParameter>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_header_preheader',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<#--
<#assign jpavatar=JspTaglibs["/jpavatar-apsadmin-core"]>
-->

<!-- Fascia Appartenenza -->
<section class="preheader bg-bluscuro">
	<div class="container">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 entesup">

				<a aria-label="<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_LINKTITLE" />" title="<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_TEXT_TITLE" />" href="http://www.regione.sardegna.it" target="_blank">
					<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_TEXT" escapeXml=false />
				</a>
				<div class="float-right">
					<!-- siti verticali -->
					<div class="sitiverticali float-left text-right">
						<a aria-label="<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_OLDSITE_ARIA" />" title="<@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_OLDSITE" />" href="<@wp.url page="old_site" />"><@wp.i18n key="CITTAMETRO_HEADER_PREHEADER_OLDSITE" /></a>
					</div>
					<!-- siti verticali -->


					<!-- accedi -->
<#--
					<div class="accedi float-left text-right">
                        <@wp.fragment code="login_prova" escapeXml=false />  
				</div>
	-->

			</div>
		</div>
	</div>       
</div>       
      
</section>
<!-- Fascia Appartenenza -->
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_widget_menu_link_utili','cittametro_widget_menu_link_utili',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign wpadmin=JspTaglibs["/apsadmin-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<li><a aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_CULTURE" />" href="<@wp.url page="arg_01_dett" />?categoryCode=317" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_CULTURE" />"><@wp.i18n key="CITTAMETRO_PORTAL_CULTURE" /></a></li>
<li><a aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_SPORT" />" href="<@wp.url page="arg_01_dett" />?categoryCode=4245" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_SPORT" />"><@wp.i18n key="CITTAMETRO_PORTAL_SPORT" /></a></li>
<li><a aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_TOURISM" />" href="<@wp.url page="arg_01_dett" />?categoryCode=4470" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_PORTAL_TOURISM" />"><@wp.i18n key="CITTAMETRO_PORTAL_TOURISM" /></a></li>
<li><a aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.i18n key="CITTAMETRO_CONTENT_ALLARGUMENTS" />" href="<@wp.url page="argomenti" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="argomenti" info="title" />"><@wp.i18n key="CITTAMETRO_CONTENT_ALLARGUMENTS" /></a></li>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_aiuto_uffici','cittametro_aiuto_uffici',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.min.css" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.min.css" />

<#assign js_slide="$(document).ready(function() {        
	var owl = $(''#owl-help'');
	owl.owlCarousel({
		nav:false,
		startPosition: 0,
		autoPlay:false,
		responsiveClass:true,
		responsive:{
				0:{
					items:1,
				},
				576: {
					items:1,
				},
				768: {
					items:2,
				},
				991: {
					items:2,
				},
			}
	});	
});" />
<@wp.headInfo type="JS_CA_BTM_INC" info="${js_slide}" />


<section id="help">
	<div class="bg-help">
		<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/foto/ufficiaperti.jpg" alt="Assitenza uffici aperti" />
	</div>
	<div class="container">
		<div class="row">
			<div class="offset-lg-3 col-lg-6 col-md-12 text-center">
				<h3><@wp.i18n key="CITTAMETRO_WIDGET_HELP_TITLE" /></h3>
				<p><@wp.i18n key="CITTAMETRO_WIDGET_HELP_DESC" /></p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row row-eq-height">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<div id="owl-help" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
					<div class="item">
						<article class="scheda scheda-help scheda-round">
							<div class="scheda-testo">
								<h4>
									<span class="scheda-icona-grande"><svg class="icon1"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-cittadini"></use></svg></span>
									<a title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_WIDGET_HELP_CITTADINI_TITLE" />"><@wp.i18n key="CITTAMETRO_WIDGET_HELP_CITTADINI" /></a>
								</h4>
								<p><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-phone_in_talk"></use></svg> <strong>800 016 058</strong></p>
								<p><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-whatsapp"></use></svg> <strong>+39 329 0582872</strong></p>
								<@jacms.content contentId="ORG9607" modelId="220022" />
							</div>
							<div class="scheda-footer">
								<span><@wp.i18n key="CITTAMETRO_WIDGET_HELP_COMPILA" /></span><br />
								<a href="<@wp.url page="scrivi_comune" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_WIDGET_HELP_CITTADINI_TITLE" />" class="tutte"><@wp.i18n key="CITTAMETRO_WIDGET_HELP_CITTADINI_TITLE" /> <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
							</div>
						</article>
					</div>
					<div class="item">
						<article class="scheda scheda-help scheda-round">
							<div class="scheda-testo">
								<h4>
									<span class="scheda-icona-grande"><svg class="icon2"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-pa"></use></svg></span>
									<a title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_WIDGET_HELP_PA_TITLE" />"><@wp.i18n key="CITTAMETRO_WIDGET_HELP_PA" /></a>
								</h4>
								<p><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-phone_in_talk"></use></svg> <strong>800 016 058</strong></p>
								<p><strong>&nbsp;</strong></p>
								<@jacms.content contentId="ORG9607" modelId="220022" />
							</div>
							<div class="scheda-footer">
								<span><@wp.i18n key="CITTAMETRO_WIDGET_HELP_COMPILA" /></span><br />
								<a title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_WIDGET_HELP_PA_TITLE" />" class="tutte"><@wp.i18n key="CITTAMETRO_WIDGET_HELP_PA_TITLE" /> <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
							</div>
						</article>
					</div>
				</div>
			</div>		
		</div>
	</div>
</section>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_briciole','cittametro_briciole',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_YOUAREIN" />">
					<ol class="breadcrumb">
						<@wp.currentPage param="code" var="currentViewCode" />
						<@wp.freemarkerTemplateParameter var="currentViewCode" valueName="currentViewCode" />
						<#assign first=true />
						<#assign lastPageTitle="" />
						
						<@uc.cagliariMainFrameContentTag var="content"/>
						<@uc.cagliariPageInfoTag pageCode="${currentViewCode}" info="isPublishOnFly" var="isPublishOnFly"/>
						<@wp.nav spec="current.path" var="currentTarget">
							<#assign currentCode = currentTarget.code />
							<#if !currentTarget.voidPage>
								<#if currentCode == currentViewCode>
									<#if (isPublishOnFly == "true")>
										<#if (content.getAttributeByRole(''jacms:title''))??>
											<#assign title=content.getAttributeByRole(''jacms:title'').text>
											<li class="breadcrumb-item active" aria-current="page"><a>${title}</a></li>
										</#if>
									<#else>									
										<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
									</#if>
								<#else>
									<li class="breadcrumb-item"><a href="${currentTarget.url}" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" /> ${currentTarget.title}"><strong>${currentTarget.title}</strong></a><span class="separator">/</span></li>
									<#if (!first)>
										
									</#if>
								</#if>
							<#else>
								<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
							</#if>
							<#assign lastPageTitle = currentTarget.title />
						</@wp.nav>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_template_header_navigation_menu',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/metroca-core.tld"]>

<div class="logo-burger  
    <#-- 
        <#if sessionScope.currentUser != "guest">-user</#if> 
    -->">
    <div class="logoimg-burger">
        <a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"> 
            <img src="<@wp.resourceURL/>/cittametro-homepage-bundle/static/img/logo_cittametro_cagliari_verde.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />" />
        </a>
    </div>
    <div class="logotxt-burger">
        <a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" /></a>
    </div>
</div>

<h2 class="sr-only"><@wp.i18n key="CITTAMETRO_HEADER_MENU_TITLE" /></h2>

<@wp.currentPage param="code" var="paginaCorrente" />
<@wp.freemarkerTemplateParameter var="paginaCorrente" valueName="paginaCorrente" />
<@uc.metrocaPageInfoTag pageCode="${paginaCorrente}" info="pathArray" var="pathCorrente"/> 

<#assign "livelloPrecedente"=-1 />
<#assign "livello"=0 />

<ul class="nav navmenu">

<@wp.nav var="pagina" spec="code(homepage).subtree(1)">
        <#if paginaPrecedente??>
            <#assign livelloPrecedente=paginaPrecedente.level />
            <#assign livello=pagina.level />
        </#if>
        <#assign "aURL"=pagina.url />

<#if 0 == livelloPrecedente>
        </li>
    </#if>

<#if livello == livelloPrecedente>
    </li>
    <li>
<#elseif livello gt livelloPrecedente>
    <#if livello gt 1>
        <ul>
    </#if>
    <li>
<#elseif livello lt livelloPrecedente>
    </li>
    <#list livello..livelloPrecedente-1 as x>
        </ul></li>
    </#list>
 <li>
</#if>

    <#assign "aperto" = false />

    <#list pathCorrente as pgitem>
 
        <#if (pgitem == pagina.code) && (pagina.code != "homepage")>
            <#assign "aperto" = true />
        </#if>

    </#list>

    <a href="${aURL}" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> :  ${pagina.title}"  <#if aperto  || (pagina.code == paginaCorrente)> class="current"</#if> >
        ${pagina.title}
    </a>

    <#assign "paginaPrecedente"= pagina />

</@wp.nav>

<#if 1 == livelloPrecedente>
</li>
</#if>
<#list livello..livelloPrecedente as x>
</li></ul>
</#list>
</ul>',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cittametro_navigation_child','cittametro_navigation_child',NULL,'<#assign wp=JspTaglibs["/aps-core"]> 
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/WEB-INF/tld/metroca-core.tld"]>

<@wp.currentPage param="code" var="paginaCorrente" />

<#assign "livelloCorrente"=-1/>
<@wp.nav spec="code(homepage).subtree(20)" var="cercaPagina" >
    <#if cercaPagina.code == paginaCorrente >
        <#assign livelloCorrente=cercaPagina.level />
    </#if>
</@wp.nav>
<@wp.nav spec="code(amministrazione_trasparente).subtree(10)" var="cercaPagina2" >
    <#if cercaPagina2.code == paginaCorrente>
        <#assign livelloCorrente = cercaPagina2.level + 1 />
    </#if>
</@wp.nav>

<@wp.pageInfo pageCode=paginaCorrente info="hasChild" var="hafigli" />

<#if hafigli=="true">

    <#assign "numerononvolanti"=0 />
    <@wp.nav spec="code(${paginaCorrente}).subtree(1)" var="cercaFigliNonVolanti" >
        <@uc.metrocaPageInfoTag pageCode=cercaFigliNonVolanti.code info="isPublishOnFly" var="isPublishOnFly"/>
       
        <#if  (isPublishOnFly=="false") && (cercaFigliNonVolanti.code != paginaCorrente ) >
            <#assign numerononvolanti=numerononvolanti+1 />
        </#if>
    </@wp.nav>

    <#if  numerononvolanti == 0>
         
         <#assign hafigli="false" />
    </#if>
</#if>
<#if  hafigli="false">
    <@uc.metrocaPageInfoTag pageCode=paginaCorrente pageVar="paginaPadre" var="pg" />
    <#assign paginaCorrente=paginaPadre.parentCode />
    <#assign livelloCorrente=livelloCorrente-1/>
</#if>

<nav>
    <h4>
        <#assign "etichetta_titolo"="CITTAMETRO_PORTAL_ALL" />
        <#if livelloCorrente == 1>
          <#assign "etichetta_titolo"= etichetta_titolo + paginaCorrente ?upper_case/>
            <@wp.i18n key=etichetta_titolo  />
        </#if>
        <#if livelloCorrente gt 1 || livelloCorrente == -1 >
            <@wp.pageInfo pageCode=paginaCorrente info="title" var="titoloPaginaCorrente" />
            ${titoloPaginaCorrente}
        </#if>
    </h4>
    <#assign "npagine"=0 />
    <ul class="list-group">
        <@wp.nav var="pagina" spec="code(${paginaCorrente}).subtree(1)">
            <#assign "link"=pagina.url />
            <#if pagina.code != paginaCorrente >
                <#if npagine == 5>
                    </ul><ul class="list-group collapse" id="menu-lista-altre">
                </#if>
                <li class="list-group-item">     
                    <a href="${link}" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> ${pagina.title} ">
                        ${pagina.title}
                    </a>
                </li>
                <#assign "npagine" = npagine+1 />
            </#if>
        </@wp.nav>
    </ul>
    <#if npagine gt 5>
        <p class="list-group-item">     
            <a href="#menu-lista-altre" id="menu-lista-button" role="button" aria-expanded="false" aria-controls="menu-lista-altre" data-toggle="collapse">
                <svg class="icon"><use xlink:href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-more_vert"></use></svg>
                <span class="menu-lista-apri" title="<@wp.i18n key="CITTAMETRO_PORTAL_EXPAND" />"><@wp.i18n key="CITTAMETRO_PORTAL_EXPAND" /></span>
                <span class="menu-lista-chiudi" title="<@wp.i18n key="CITTAMETRO_PORTAL_CLOSE" />"><@wp.i18n key="CITTAMETRO_PORTAL_CLOSE" /></span>
            </a>
        </p>
    
        <p class="list-group-item mt16">
            <#assign "frameattivo"="sezioni-servizi" />
            <@wp.currentWidget frame=6 var="widget" param="code" />
            <#if widget??>
                <#assign "frameattivo"="sezioni-inevidenza"/>
            </#if>
            <a href="<@wp.url page=paginaCorrente />#${frameattivo}" title="<@wp.i18n key="ENTANDO_API_GOTO_DETAILS" />">
                <strong><@wp.i18n key="CITTAMETRO_PORTAL_SEEALL" /></strong>
            </a>
        </p>
    </#if>
</nav>
',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_widget_argomenti_consigliati','cagliari_widget_argomenti_consigliati','whitelabel','<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@jacms.contentList listName="contentList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />
<div class="widget">
<#if (titleVar??)>
	<div class="row">
		<div class="col-md-12">
			<div class="titolosezione">
				<h3>
				<#if (pageLinkVar??)>
					<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
				<#else>
					${titleVar}
				</#if>
				</h3>
			</div>
		</div>
	</div>
</#if>

<#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
	<@wp.pager listName="contentList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
		<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
			<div class="row">
				<div class="offset-md-2 col-md-8 offset-md-2 col-sm-12">
					<div class="argomenti altri-argomenti">
						<div class="altri-argomenti-elenco">
							<#list contentList as contentId>
								<#if (contentId_index >= groupContent.begin) && (contentId_index <= groupContent.end)>
									<@jacms.content contentId="${contentId}" />
								</#if>
							</#list>
						</div>
					</div>
				</div>
			</div>
		</@wp.freemarkerTemplateParameter>
	</@wp.pager>
<#else>
	<div class="row">
		<div class="offset-md-2 col-md-8 offset-md-2 col-sm-12">
			<div class="argomenti altri-argomenti">
				<div class="altri-argomenti-elenco">
					<a class="badge badge-pill badge-argomenti"><@wp.i18n key="CITTAMETRO_PORTAL_NOARGUMENTS" /></a>
				</div>
			</div>
		</div>
	</div>
</#if>
<#assign contentList="">
</div>',NULL,1);
