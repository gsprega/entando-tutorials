INSERT INTO authroles (rolename,descr) VALUES ('admin','Administrator');
INSERT INTO authroles (rolename,descr) VALUES ('editor','Editor');
INSERT INTO authroles (rolename,descr) VALUES ('approver','Approver');
INSERT INTO authroles (rolename,descr) VALUES ('reviewer','Reviewer');
